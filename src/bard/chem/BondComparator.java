// $Id: BondComparator.java 2440 2008-12-01 22:18:46Z nguyenda $
package bard.chem;

import chemaxon.struc.MolBond;

public interface BondComparator {
    public boolean match (MolBond a, MolBond b);
}
