
package bard.ui.event;

import java.util.EventObject;
import bard.core.SearchParams;

public class SearchEvent extends EventObject {
    private SearchParams query;

    public SearchEvent (Object source) {
	super (source);
    }

    public SearchEvent (Object source, SearchParams query) {
	super (source);
	this.query = query;
    }

    public SearchParams getQuery () { return query; }
}
