package bard.ui.event;

import java.util.EventObject;
import bard.core.Entity;
import bard.ui.content.State;

public class StateChangeEvent<T extends Entity> extends BARDEvent {
    public StateChangeEvent (State<T> source, String name, 
                             Object oldValue, Object newValue) {
        super (source, name, oldValue, newValue);
    }

    public State<T> getState () { return (State<T>) getSource (); }
}

