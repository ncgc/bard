package bard.ui.event;

import java.util.EventObject;
import bard.core.SearchParams;
import bard.core.EntityService;
import bard.core.ServiceIterator;
import bard.core.Entity;

public class SearchResultEvent<T extends Entity> extends EventObject {
    private ServiceIterator<T> iterator;
    private SearchParams query;

    public SearchResultEvent (EntityService<T> source, 
                              SearchParams query,
                              ServiceIterator<T> iterator) {
        super (source);
        this.query = query;
        this.iterator = iterator;
    }

    public EntityService<T> getService () { 
        return (EntityService) getSource (); 
    }
    public ServiceIterator<T> getIterator () { 
        return iterator; 
    }
    public SearchParams getQuery () { return query; }
}
