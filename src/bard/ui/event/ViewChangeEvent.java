package bard.ui.event;

import java.util.EventObject;

import bard.core.Entity;
import bard.ui.content.View;

public class ViewChangeEvent extends EventObject {
    private View.Type oldView;
    private View.Type newView;

    public ViewChangeEvent 
        (Object source, View.Type oldView, View.Type newView) {
        super (source);
        this.oldView = oldView;
        this.newView = newView;
    }

    public View.Type getOldView () { return oldView; }
    public View.Type getNewView () { return newView; }
}
