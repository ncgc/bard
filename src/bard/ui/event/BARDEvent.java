// $Id$

package bard.ui.event;
import java.util.EventObject;

public class BARDEvent extends EventObject {
    /*
     * well known change event
     */
    public static final String SELECTION = "selection";
    public static final String SIZE = "size";
    public static final String BUSY = "busy";
    public static final String VIEW = "view";
    public static final String LASTVIEWED = "lastviewed";
    public static final String NEWCOUNT = "newcount";
    public static final String STATE = "state";
    public static final String CONTENT = "content"; // content changed
    public static final String ENTITY = "entity";
    public static final String ERROR = "error";
    public static final String SORT = "sort";
    public static final String STATUS = "status";
    public static final String NEWDATA =  "newdata";


    private String name;
    private Object oldValue;
    private Object newValue;

    public BARDEvent (Object source, String name, 
                      Object oldValue, Object newValue) {
	super (source);
	this.name = name;
	this.oldValue = oldValue;
	this.newValue = newValue;
    }

    public String getName () { return name; }
    public Object getOldValue () { return oldValue; }
    public Object getNewValue () { return newValue; }
}
