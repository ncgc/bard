// $Id$

package bard.ui.event;

import java.util.EventObject;

import bard.core.Entity;
import bard.ui.content.Content;

public class ContentChangeEvent<T extends Entity> extends BARDEvent {
    public ContentChangeEvent (Content<T> source, String name, 
			       Object oldValue, Object newValue) {
	super (source, name, oldValue, newValue);
    }

    public Content<T> getContent () { return (Content<T>)getSource(); }
}
