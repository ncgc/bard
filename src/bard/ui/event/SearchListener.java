// $Id$

package bard.ui.event;

import java.util.EventListener;

public interface SearchListener extends EventListener {
    void searchPerformed (SearchEvent e);
}
