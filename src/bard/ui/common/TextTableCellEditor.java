// $Id$

package bard.ui.common;

import com.jidesoft.swing.SimpleScrollPane;
import org.jdesktop.swingx.border.DropShadowBorder;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellEditor;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import bard.core.Entity;
import bard.core.Value;
import bard.ui.util.ColorUtil;


public class TextTableCellEditor
        extends AbstractCellEditor implements TableCellEditor {

    private JTable table;
    private JPanel pane;
    private JTextArea textArea;
    private Color border = null;
    private String field;
    private Object value;
    private SimpleScrollPane ssp;

    public TextTableCellEditor() {
        this(null, null, new JTextArea());
    }

    public TextTableCellEditor(String field) {
        this(field, null, new JTextArea());
    }

    public TextTableCellEditor(Color border) {
        this(null, border, new JTextArea());
    }

    public TextTableCellEditor(String field, Color border) {
        this(field, border, new JTextArea());
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public TextTableCellEditor
            (String field, Color border, JTextArea textArea) {
        this.field = field;
        this.textArea = textArea;
        this.border = border;
        init();
    }

    protected void init() {
	Font font = UIManager.getFont("Table.font").deriveFont(12.f);

        //textArea.setOpaque(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setEditable(false);
        textArea.setBackground(Color.white);
	textArea.setFont(font);

        pane = new JPanel(new BorderLayout());
        pane.setOpaque(false);

        /*
      JScrollPane sp = UIUtil.iTunesScrollPane(textArea);
      sp.setHorizontalScrollBarPolicy
          (JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
      pane.add(sp);
      */
        ssp = new SimpleScrollPane(textArea);
        ssp.setBackground(Color.white);
        ssp.setHorizontalScrollBarPolicy
                (JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pane.add(ssp);

        if (border != null) {
            pane.setBorder(BorderFactory.createCompoundBorder
                    (new DropShadowBorder(),
                            BorderFactory.createLineBorder(border)));
        } else {
            pane.setBorder(new DropShadowBorder());
        }
    }

    public void setValue(Object value) {
        this.value = value;
        if (value instanceof Entity) {
            Value v = ((Entity)value).getValue(field);
            value = v != null ? v.getValue() : null;
        }

        // doesn't work!
        if (value == null || "".equals(value)) {
            TableCellEditor editor = table.getCellEditor();
            if (editor != null) {
                editor.cancelCellEditing();
                table.setCellEditor(null);
            }
        }

        // in case we have a date object, we provide some formatting
        String valueText = null;
        if (value != null) {
            if (value instanceof Date) {
                DateFormat fmt = new SimpleDateFormat("MMMMM yyyy");
                valueText = fmt.format(value);
            } else valueText = value.toString();
        }
        textArea.setText(valueText);
        textArea.setCaretPosition(0);
    }

    public Object getCellEditorValue() {
        return value;
    }

    public Component getTableCellEditorComponent
            (JTable table, Object value, boolean selected, int row, int column) {
        if (selected) {
            pane.setOpaque(true);
            pane.setBackground(table.getSelectionBackground());
            pane.setForeground(table.getSelectionForeground());
        } 
        else {
            Color c = (row % 2 == 0 ? ColorUtil.EvenRow : ColorUtil.OddRow);
            textArea.setBackground(c);
            ssp.setBackground(c);

            pane.setOpaque(false);
            pane.setForeground(table.getForeground());
            pane.setBackground(table.getBackground());
        }
        this.table = table;
        setValue(value);
        return pane;
    }
}
