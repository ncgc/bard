package bard.ui.common;

import bard.core.Value;
import bard.core.adapter.EntityAdapter;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link DefaultTreeModel} representation for annotations on arbitrary entities.
 *
 * @author Rajarshi Guha
 */
class AnnotationTreeModel<T> extends DefaultTreeModel {
    T entity;

    AnnotationTreeModel() {
        super(new DefaultMutableTreeNode());
    }

    void setEntity(EntityAdapter entityAdapter) {
        this.entity = (T) entityAdapter.getEntity();

        DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();
        root.removeAllChildren();

        Map<String, List<String>> annos = new HashMap<String, List<String>>();
        Collection<Value> values = entityAdapter.getAnnotations();
        for (Value v : values) {

            String key = v.getId();
            String val = (String) v.getValue();

            List<String> tmp = annos.get(key);
            if (tmp == null) {
                tmp = new ArrayList<String>();
                tmp.add(val);
                annos.put(key, tmp);
            } else tmp.add(val);
        }

        // add in the nodes
        for (Value v : values) {
            //if (v.getId().equals("COLLECTION"))
            addNode(root, annos, (String) v.getValue());
        }
        reload();
    }

    void addNode(DefaultMutableTreeNode root,
                 Map<String, java.util.List<String>> anno,
                 String sv) {

        String[] child = sv.split("\\|");
        DefaultMutableTreeNode n;
        if (child.length > 0) {
            n = new DefaultMutableTreeNode(child[0]);
            for (int i = 1; i < child.length; ++i) {
                String[] c = child[i].split(":");
                if (c.length > 0) {
                    DefaultMutableTreeNode p = null;
                    for (Enumeration en = n.children();
                         en.hasMoreElements(); ) {
                        DefaultMutableTreeNode k =
                                (DefaultMutableTreeNode) en.nextElement();
                        if (c[0].equals(k.getUserObject())) {
                            p = k;
                            break;
                        }
                    }
                    if (p == null) {
                        n.add(p = new DefaultMutableTreeNode
                                (c[0]));
                    }

                    for (int j = 1; j < c.length; ++j) {
                        p.add(new DefaultMutableTreeNode(c[j]));
                    }
                } else {
                    n.add(new DefaultMutableTreeNode(child[i]));
                }
            }
        } else {
            n = new DefaultMutableTreeNode(sv);
        }

        java.util.List<String> vals = anno.get(n.getUserObject());
        if (vals != null) {
            for (String v : vals) {
                n.add(new DefaultMutableTreeNode(v));
            }
        }

        if ("ChemIDPlus diffs".equals(n.getUserObject())) {
            // ignore this bogus collection
        } else {
            root.add(n);
        }
    }

    T getEntity() {
        return entity;
    }

    void clear() {
        entity = null;
        ((DefaultMutableTreeNode) getRoot()).removeAllChildren();
        reload();
    }
}
