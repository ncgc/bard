// $Id: EntityTableModel.java 3487 2009-10-29 15:48:57Z nguyenda $

package bard.ui.common;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.beans.*;
import javax.swing.SizeSequence;
import javax.swing.table.*;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEvent;

import static bard.ui.explodingpixels.widgets.TableUtils.SortDirection;

import bard.core.Entity;

public class EntityTableModel extends AbstractTableModel 
    implements ListEventListener {

    private static final Logger logger = 
	Logger.getLogger(EntityTableModel.class.getName());

    public enum RowStatus {
	ROW_OK,
	    ROW_MODIFIED,
	    ROW_NOEDIT,
	    ROW_UNKNOWN
    }

    /*
     * define special column locations
     */
    private static final int ROW_STATUS_COLUMN = 0;

    interface RowModel {
	Row createRow (Object value);
	int getColumnCount ();
	Class getColumnClass (int column);
	String getColumnName (int column);

	/*
	 * sorting (optional)
	 */
	Comparator getComparator (int column);
	void setComparator (int column, Comparator comparator);
    }

    interface Row {
	Object getValueAt (int column);
	void setValueAt (Object value, int column);
	boolean isCellEditable (int column);
	RowStatus getStatus ();
	RowModel getModel ();
    }

    /*
     * dummy RowModel
     */
    static class RowModelAdapter implements RowModel {
	public RowModelAdapter () {}
	public Row createRow (Object value) { return null; }
	public int getColumnCount () { return 0; }
	public Class getColumnClass (int column) { return Object.class; }
	public String getColumnName (int column) { return null; }
	public Comparator getComparator (int column) { return null; }
	public void setComparator (int column, Comparator comparator) {}
    }

    static class DefaultRowModel extends RowModelAdapter {
	// just a wrapper around the value
	class RowImpl implements Row {
	    Object value;
	    RowImpl (Object value) {
		this.value = value;
	    }
	    public Object getValueAt (int column) {
		return value;
	    }
	    public void setValueAt (Object value, int column) {
		this.value = value;
	    }
	    public boolean isCellEditable (int column) {
		return true;
	    }
	    public RowStatus getStatus () { return RowStatus.ROW_OK; }
	    public RowModel getModel () {
		return DefaultRowModel.this; 
	    }
	}

	String name;
	Class clazz;

	public DefaultRowModel () {
	}
	public DefaultRowModel (String name) {
	    this.name = name;
	}
	public DefaultRowModel (String name, Class clazz) {
	    this.name = name;
	    this.clazz = clazz;
	}
	public Row createRow (Object value) {
	    return new RowImpl (value);
	}
	public int getColumnCount () { return 1; }
	public String getColumnName (int column) { return name; }
	public Class getColumnClass (int column) { return clazz; }
    }

    static class ColumnCoordinate {
	int index;
	int offset;
	
	ColumnCoordinate (int index, int offset) { 
	    this.index = index;
	    this.offset = offset;
	}
    }

    static class CompositeRowModel extends RowModelAdapter {
	List<RowModel> columns = new ArrayList<RowModel>();

	class CompositeRow implements Row {
	    List<Row> rows = new ArrayList<Row>();

	    CompositeRow () {}
	    CompositeRow (int span) {
		// fill with empty cells
		for (int i = 0; i < span; ++i) {
		    rows.add(null);
		}
	    }

	    public void addRow (Row row) {
		rows.add(row);
	    }
	    public RowModel getModel () {
		return CompositeRowModel.this;
	    }
	    public Object getValueAt (int column) {
		ColumnCoordinate cc = getColumnCoordinate (column);
		if (cc != null) {
		    Row r = rows.get(cc.index);
		    if (r != null) {
			return r.getValueAt(cc.offset);
		    }
		}
		return null;
	    }
	    public void setValueAt (Object value, int column) {
		ColumnCoordinate cc = getColumnCoordinate (column);
		if (cc != null) {
		    Row r = rows.get(cc.index);
		    if (r != null) {
			r.setValueAt(value, cc.offset);
		    }
		    else {
			RowModel rm = columns.get(cc.index);
			rows.set(cc.index, rm.createRow(value));
		    }
		}
		else {
		    throw new IllegalArgumentException
			("Invalid column specified: "+column);
		}
	    }
	    public boolean isCellEditable (int column) {
		ColumnCoordinate cc = getColumnCoordinate (column);
		if (cc != null) {
		    Row r = rows.get(cc.index);
		    if (r != null) {
			return r.isCellEditable(cc.offset);
		    }
		}
		return false;
	    }
	    public RowStatus getStatus () {
		for (Row r : rows) {
		    if (r != null && r.getStatus() != RowStatus.ROW_OK) {
			return r.getStatus();
		    }
		}
		return RowStatus.ROW_OK;
	    }
	}
	
	public CompositeRowModel () {
	}

	ColumnCoordinate getColumnCoordinate (int column) {
	    int index = 0;
	    for (int i = 0; i < columns.size(); ++i) {
		RowModel rm = columns.get(i);
		if (column >= index && column < (index+rm.getColumnCount())) {
		    return new ColumnCoordinate (i, column-index);
		}
		index += rm.getColumnCount();
	    }
	    return null;
	}

	public void addRowModel (RowModel rowModel) {
	    columns.add(rowModel);
	}

	public int size () { return columns.size(); }
	public RowModel getRowModel (int i) { return columns.get(i); }

	public int getColumnCount () {
	    int total = 0;
	    for (RowModel rm : columns) {
		total += rm.getColumnCount();
	    }
	    return total;
	}

	public Class getColumnClass (int column) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		return rm.getColumnClass(cc.offset);
	    }
	    return Object.class;
	}

	public String getColumnName (int column) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		return rm.getColumnName(cc.offset);
	    }
	    return "";
	}

	public Row createRow (Object value) {
	    if (value != null && value instanceof Integer) {
		return new CompositeRow ((Integer)value);
	    }
	    return new CompositeRow (columns.size());
	}

	public Comparator getComparator (int column) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		return rm.getComparator(cc.offset);
	    }
	    else {
		throw new IllegalArgumentException
		    ("Invalid column specified: "+column);
	    }
	}
	public void setComparator (int column, Comparator cmp) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		rm.setComparator(cc.offset, cmp);
	    }
	    else {
		throw new IllegalArgumentException
		    ("Invalid column specified: "+column);
	    }
	}
    }

    class SortOrderColumn implements Comparator<Integer> {
	int column;
	int viewColumn;
	SortDirection order;
	Integer[] map;
	Comparator cmp;

	SortOrderColumn (int column, SortDirection order, Comparator cmp) {
	    this.column = column;
	    this.order = order;

	    map = new Integer[getRowCount ()];
	    for (int i = 0; i < map.length; ++i) {
		map[i] = i;
	    }

	    this.cmp = cmp;
	    sort ();
	}

	public void setSortOrder (SortDirection order) {
	    this.order = order;
	}

	public void sort () {
	    if (cmp != null) {
		Arrays.sort(map, this);
		/*
		System.err.print("Column model "+column+": [");
		for (int i = 0; i < map.length; ++i) {
		    System.err.print(map[i]);
		    if (i+1 < map.length) {
			System.err.print(",");
		    }
		}
		System.err.println("]");
		*/
	    }
	}

	public int compare (Integer i, Integer j) {
	    if (order == SortDirection.DESCENDING) 
		return cmp.compare(getValueAt(j, column), 
				   getValueAt(i, column));
	    return cmp.compare(getValueAt(i, column), 
			       getValueAt(j, column));
	}

	public int convertRowIndex (int row) { 
	    return row >= 0 && row < map.length ? map[row] : row; 
	}
    }

    class _SortOrderColumn implements Comparator<Integer> {
	Comparator cmp;
	int column;
	SortDirection order;
	Integer[] map;

	_SortOrderColumn (int column, SortDirection order) {
	    this.order = order;
	    map = new Integer[_getRowCount ()];
	    for (int i = 0; i < map.length; ++i) {
		map[i] = i;
	    }

	    cmp = _rowModel.getComparator(column);
	    this.column = column;

	    sort ();
	}

	public void setSortOrder (SortDirection order) {
	    this.order = order;
	}
	public SortDirection getSortOrder () { return order; }

	public void sort () {
	    if (cmp != null) {
		Arrays.sort(map, this);
	    }
	}

	public int compare (Integer i, Integer j) {
	    if (order == SortDirection.DESCENDING) {
		return cmp.compare
		    (_getValueAt (j, column), _getValueAt (i, column));
	    }
	    return cmp.compare(_getValueAt (i, column),
			       _getValueAt (j, column));
	}

	public int convertRowIndex (int row) { 
	    return row >= 0 && row < map.length ? map[row] : row; 
	}
    }

    /*
     * data is store in column major order
     */
    private EventList<EventList> data = new BasicEventList<EventList>();
    private FilterList anchorColumn = null;

    private EventList<Class> classes = new BasicEventList<Class>();
    private EventList<String> columns = new BasicEventList<String> ();
    private EventList<RowStatus> status = new BasicEventList<RowStatus>();

    /*
     * handling multi-column sorting
     */
    private ArrayList<SortOrderColumn> sortOrderColumns = 
	new ArrayList<SortOrderColumn>();

    private ArrayList<_SortOrderColumn> _sortOrderColumns = 
	new ArrayList<_SortOrderColumn>();

    private CompositeRowModel _rowModel = new CompositeRowModel ();
    private List<CompositeRowModel.CompositeRow> _rows = 
	new ArrayList<CompositeRowModel.CompositeRow>();

    // either all or nothing editable property
    private boolean isEditable = false;

    public EntityTableModel () {
    }

    // create an EntityTableModel with the given anchor EventList
    public EntityTableModel (Class clazz, EventList anchorColumn) {
	setAnchorColumn (clazz, anchorColumn);
    }

    public void _addColumn (RowModel rowModel, List columnData) {
	_addColumn (rowModel, columnData.toArray(new Object[0]));
    }

    public <T> void _addColumn (RowModel rowModel, T[] columnData) {
	if (_rows.isEmpty()) {
	    for (Object datum : columnData) {
		CompositeRowModel.CompositeRow row = 
		    (CompositeRowModel.CompositeRow)_rowModel.createRow(null);
		row.addRow(rowModel.createRow(datum));
		_rows.add(row);
	    }
	}
	else {
	    if (_rows.size() < columnData.length) {
		for (int i = _rows.size(); i < columnData.length; ++i) {
		    // pad with empty rows
		    _rows.add((CompositeRowModel.CompositeRow)
			      _rowModel.createRow(null));
		}
	    }

	    int r = 0;
	    for (; r < columnData.length; ++r) {
		CompositeRowModel.CompositeRow row = _rows.get(r);
		row.addRow(rowModel.createRow(columnData[r]));
	    }

	    for (; r < _rows.size(); ++r) {
		CompositeRowModel.CompositeRow row = _rows.get(r);
		row.addRow(rowModel.createRow(null));		
	    }
	}
	_rowModel.addRowModel(rowModel);
    }

    public Object _getValueAt (int row, int column) {
	for (_SortOrderColumn soc : _sortOrderColumns) {
	    row = soc.convertRowIndex(row);
	}
	return _rows.get(row).getValueAt(column);
    }
    public void _setValueAt (Object value, int row, int column) {
	for (_SortOrderColumn soc : _sortOrderColumns) {
	    row = soc.convertRowIndex(row);
	}
	_rows.get(row).setValueAt(value, column);
    }

    public RowStatus _getRowStatus (int row) { 
	return _rows.get(row).getStatus(); 
    }
    public int _getRowCount () { return _rows.size(); }
    public int _getColumnCount () { return _rowModel.getColumnCount(); }


    public void setAnchorColumn (Class clazz, EventList anchorColumn) {
	if (this.anchorColumn != null) {
	    this.anchorColumn.removeListEventListener(this);
	    int nrows = getRowCount ();

	    data.getReadWriteLock().writeLock().lock();
	    try {
		for (int c = 0; c < data.size(); ++c) {
		    EventList list = data.get(c);
		    if (list == this.anchorColumn) {
			this.anchorColumn = new FilterList (anchorColumn);
			data.set(c, this.anchorColumn);
			classes.set(c, clazz);
			columns.set(c, clazz.getName());
		    }
		    else if (list.size() < anchorColumn.size()) {
			for (int i = list.size(); 
			     i < anchorColumn.size(); ++i) {
			    list.add(null); // pad
			}
		    }
		}
	    }
	    finally {
		data.getReadWriteLock().writeLock().unlock();
	    }

	    if (this.anchorColumn.size() < nrows) {
		status.getReadWriteLock().writeLock().lock();
		try {
		    for (int r = this.anchorColumn.size(); r < nrows; ++r) {
			this.anchorColumn.add(null); // pad
			status.add(RowStatus.ROW_OK);
		    }
		}
		finally {
		    status.getReadWriteLock().writeLock().unlock();
		}
	    }
	}
	else {
	    this.anchorColumn = new FilterList (anchorColumn);
	    columns.add(clazz.getName());
	    classes.add(clazz);
	    data.add(this.anchorColumn);

	    status.getReadWriteLock().writeLock().lock();
	    try {
		status.clear();
		int size = anchorColumn.size();
		for (int i = 0; i < size; ++i) {
		    status.add(RowStatus.ROW_OK); // default status
		}
	    }
	    finally {
		status.getReadWriteLock().writeLock().unlock();
	    }
	}
	this.anchorColumn.addListEventListener(this);
    }

    public FilterList getAnchorColumn () { return anchorColumn; }

    public void listChanged (ListEvent e) {
	while (e.nextBlock()) {
	    switch (e.getType()) {
	    case ListEvent.INSERT:
		status.getReadWriteLock().writeLock().lock();
		try {
		    for (int i = e.getBlockStartIndex(); 
			 i <= e.getBlockEndIndex(); ++i) {
			status.add(RowStatus.ROW_OK);
		    }
		}
		finally {
		    status.getReadWriteLock().writeLock().unlock();
		}
		fireTableRowsInserted 
		    (e.getBlockStartIndex(), e.getBlockEndIndex());

		break;

	    case ListEvent.DELETE:

		status.getReadWriteLock().writeLock().lock();
		try {
		}
		finally {
		    status.getReadWriteLock().writeLock().unlock();
		}
		fireTableRowsDeleted 
		    (e.getBlockStartIndex(), e.getBlockEndIndex());
		
		break;

	    case ListEvent.UPDATE:
		status.getReadWriteLock().writeLock().lock();
		try {
		    for (int i = e.getBlockStartIndex(); 
			 i <= e.getBlockEndIndex(); ++i) {
			//status.set(i, RowStatus.ROW_MODIFIED);
		    }
		}
		finally {
		    status.getReadWriteLock().writeLock().unlock();
		}
		fireTableRowsUpdated 
		    (e.getBlockStartIndex(), e.getBlockEndIndex());

		break;
	    }
	}
	//dump(System.out);
	//fireTableDataChanged ();
    }

    public void addColumn (Class clazz, List columnData) {
	addColumn (null, clazz, columnData);
    }

    public void addColumn (String columnName, Class clazz, List columnData) {
	if (!isEditable ()) {
	    throw new IllegalArgumentException ("Table model is not editable");
	}

	if (clazz == null) {
	    clazz = Object.class;
	}

	if (columnName == null) {
	    columnName = clazz.getName();
	}

	columns.getReadWriteLock().writeLock().lock();
	try {
	    columns.add(columnName);
	}
	finally {
	    columns.getReadWriteLock().writeLock().unlock();
	}

	classes.getReadWriteLock().writeLock().lock();
	try {
	    classes.add(clazz);
	}
	finally {
	    classes.getReadWriteLock().writeLock().unlock();
	}

	int nrows = columnData.size();
	if (anchorColumn != null && anchorColumn.size() < nrows) {
	    nrows = anchorColumn.size();
	    logger.log(Level.WARNING, "New column is larger than anchor "
		       +"column; truncating new column to "+nrows);
	}

	EventList newColumn = new BasicEventList (nrows);
	for (int r = 0; r < nrows; ++r) {
	    newColumn.set(r, columnData.get(r));
	}

	data.getReadWriteLock().writeLock().lock();
	try {
	    for (EventList list : data) {
		for (int r = list.size(); r < nrows; ++r) {
		    list.add(null); // add emtpy cells
		}
	    }

	    data.add(newColumn);
	    fireTableStructureChanged ();
	}
	finally {
	    data.getReadWriteLock().writeLock().unlock();
	}
    }

    public EventList removeColumn (int column) {
	if (!isEditable ()) {
	    throw new IllegalArgumentException ("Table model is not editable");
	}

	if (column == 0) {
	    return null;
	}

	data.getReadWriteLock().writeLock().lock();
	try {
	    return data.remove(column-1);
	}
	finally {
	    data.getReadWriteLock().writeLock().unlock();
	}
    }

    public EventList getColumnData (int column) { 
	if (column == 0) {
	    return status;
	}

	data.getReadWriteLock().readLock().lock();
	try {
	    return data.get(column-1); 
	}
	finally {
	    data.getReadWriteLock().readLock().unlock();
	}
    }

    public int getRowCount () { 
	if (anchorColumn != null) {
	    return anchorColumn.size();
	}

	// otherwise return the size of any column
	data.getReadWriteLock().readLock().lock();
	try {
	    for (EventList list : data) {
		return list.size();
	    }
	}
	finally {
	    data.getReadWriteLock().readLock().unlock();
	}
	return 0;
    }

    public int getColumnCount () { 
	data.getReadWriteLock().readLock().lock();
	try {
	    return data.size() + 1; // status
	}
	finally {
	    data.getReadWriteLock().readLock().unlock();
	}
    }

    public RowStatus getRowStatus (int row) {
	status.getReadWriteLock().readLock().lock();
	try {
	    row = convertRowIndex (row);
	    return status.get(row);
	}
	finally {
	    status.getReadWriteLock().readLock().unlock();
	}
    }

    public void setRowStatus (int row, RowStatus rs) {
	if (!isEditable ()) {
	    throw new IllegalArgumentException ("Table model is not editable");
	}
	/*
	status.getReadWriteLock().writeLock().lock();
	try {
	    status.set(convertRowIndex (row), rs);
	}
	finally {
	    status.getReadWriteLock().writeLock().unlock();
	}
	*/
    }

    @Override
    public String getColumnName (int column) { 
	if (column == 0) {
	    return null;
	}
	return columns.get(column-1); 
    }
    public void setColumnName (int column, String columnName) {
	if (!isEditable ()) {
	    throw new IllegalArgumentException ("Table model is not editable");
	}

	if (column == 0) {
	    return;
	}

	columns.getReadWriteLock().writeLock().lock();
	try {
	    columns.set(column - 1, columnName);
	}
	finally {
	    columns.getReadWriteLock().writeLock().unlock();
	}
	fireTableDataChanged ();
    }

    @Override
    public Class<?> getColumnClass (int column) { 
	if (column == 0) {
	    return RowStatus.class;
	}

	classes.getReadWriteLock().readLock().lock();
	try {
	    return classes.get(column - 1); 
	}
	finally {
	    classes.getReadWriteLock().readLock().unlock();
	}
    }
    public void setColumnClass (int column, Class clazz) {
	if (!isEditable ()) {
	    throw new IllegalArgumentException ("Table model is not editable");
	}

	if (column == 0) {
	    return;
	}

	classes.getReadWriteLock().writeLock().lock();
	try {
	    classes.set(column-1, clazz);
	}
	finally {
	    classes.getReadWriteLock().writeLock().unlock();
	}
	fireTableDataChanged ();
    }

    public void addRow (List rowData) {
	if (!isEditable ()) {
	    throw new IllegalArgumentException ("Table model is not editable");
	}

	int ncols = rowData.size();
	if (ncols > getColumnCount ()) {
	    ncols = getColumnCount ();
	    logger.log(Level.WARNING, "New row has more columns than "
		       +"table; truncate row to "+ncols+" columns");
	}
	data.getReadWriteLock().writeLock().lock();
	try {
	    int row = getRowCount ();
	    for (int c = 0; c < ncols; ++c) {
		EventList list = data.get(c);
		list.add(rowData.get(c));
	    }
	    fireTableRowsInserted (row, row);
	}
	finally {
	    data.getReadWriteLock().writeLock().unlock();
	}
    }

    int convertRowIndex (int row) {
	for (SortOrderColumn soc : sortOrderColumns) {
	    row = soc.convertRowIndex(row);
	}
	return row;
    }

    public Object getValueAt (int row, int column) {
	if (column == 0) {
	    return getRowStatus (row);
	}

	data.getReadWriteLock().readLock().lock();
	try {
	    return data.get(column - 1).get(convertRowIndex (row));
	}
	finally {
	    data.getReadWriteLock().readLock().unlock();
	}
    }

    public void setValueAt (Object value, int row, int column) {
	if (!isEditable ()) {
	    throw new IllegalArgumentException
		("Table model is not editable");
	}

	if (column == 0) {
	    setRowStatus (row, (RowStatus)value);
	}
	else {
	    data.getReadWriteLock().readLock().lock();
	    try {
		row = convertRowIndex (row);
		data.get(column-1).set(row, value);
		fireTableCellUpdated (row, column);
	    }
	    finally {
		data.getReadWriteLock().readLock().unlock();
	    }
	}
    }

    public boolean isCellEditable (int row, int column) {
	return column != 0 ? isEditable : false;
    }
    public boolean isEditable () { return isEditable; }
    public synchronized void setEditable (boolean editable) {
	this.isEditable = editable;
    }

    public void clearSortColumns () {
	sortOrderColumns.clear();
	fireTableDataChanged ();
    }

    public void addSortColumn (int column, SortDirection order, 
			       Comparator cmp) {
	SortOrderColumn soc = null;
	for (SortOrderColumn c : sortOrderColumns) {
	    if (c.column == column) {
		soc = c;
		break;
	    }
	}

	if (order == SortDirection.NONE) {
	    sortOrderColumns.remove(soc);
	}
	else {
	    if (soc != null) {
		sortOrderColumns.remove(soc);
	    }
	    soc = new SortOrderColumn (column, order, cmp);
	    sortOrderColumns.add(soc);
	}
	fireTableDataChanged ();
    }

    public void _clearSortColumns () {
	_sortOrderColumns.clear();
	fireTableDataChanged ();
    }

    public void _addSortColumn (int column, SortDirection order) {
	_SortOrderColumn soc = null;
	for (_SortOrderColumn c : _sortOrderColumns) {
	    if (c.column == column) {
		soc = c;
		break;
	    }
	}

	if (order == SortDirection.NONE) {
	    _sortOrderColumns.remove(soc);
	}
	else {
	    if (soc != null) {
		_sortOrderColumns.remove(soc);
	    }
	    soc = new _SortOrderColumn (column, order);
	    _sortOrderColumns.add(soc);
	}
	fireTableDataChanged ();
    }

    public void dump (java.io.OutputStream os) {
	java.io.PrintStream ps = new java.io.PrintStream (os, true);
	int nrows = getRowCount(), ncols = getColumnCount ();
	ps.println(nrows + " "+ncols);
	for (int r = 0; r < nrows; ++r) {
	    for (int c = 0; c < ncols; ++c) {
		ps.printf("[%1$2d,%2$2d]: "+getValueAt(r,c)+"\n",r,c);
	    }
	}
    }

    public static void main (String[] argv) throws Exception {
	EntityTableModel etm = new EntityTableModel ();
	final Comparator cmp = new Comparator () {
		public int compare (Object o1, Object o2) {
		    //System.err.println("comparing "+o1+" and "+o2);
		    if (o1 == null && o2 == null) return 0;
		    if (o1 == null) return -1;
		    if (o2 == null) return 1;
		    if (o1 instanceof Number && o2 instanceof Number) {
			double d = ((Number)o1).doubleValue() 
			    - ((Number)o2).doubleValue();
			if (d < 0.) return -1;
			if (d > 0.) return 1;
			return 0;
		    }
		    return ((Comparable)o1).compareTo((Comparable)o2);
		}
	    };
	DefaultRowModel drm1 = new DefaultRowModel 
	    ("column 1", String.class) {
		public Comparator getComparator (int column) { return cmp; }
	    };

	etm._addColumn(drm1, new String[]{"one","two","three", "four"});
	DefaultRowModel drm2 = new DefaultRowModel 
	    ("column 2", Number.class) {
		public Comparator getComparator (int column) { return cmp; }
	    };
	etm._addColumn(drm2, new Number[]{new Integer (1), new Integer(2), 
					  new Double (3.1415926535),
					  new Double (3.1415926535),
					  new Long (0xdeadbeefl)});
	etm._addColumn(drm2, new Number[]{new Integer(1234), 
					  new Integer(999)});
	etm._setValueAt(new Float(2.71f), 3, 2);
	//etm._addSortColumn(0, SortDirection.ASCENDING);
	etm._addSortColumn(2, SortDirection.DESCENDING);
	etm._addSortColumn(1, SortDirection.ASCENDING);

	etm._setValueAt(Math.PI, 3,2);
	System.out.println("Table size: "+etm._getRowCount() 
			   + " x "+etm._getColumnCount());
	for (int r = 0; r < etm._getRowCount(); ++r) {
	    System.out.print(etm._getValueAt(r, 0));
	    for (int c = 1; c < etm._getColumnCount(); ++c) {
		System.out.print(" "+etm._getValueAt(r,c));
	    }
	    System.out.println();
	}
	etm._clearSortColumns();
	System.out.println("--");
	for (int r = 0; r < etm._getRowCount(); ++r) {
	    System.out.print(etm._getValueAt(r, 0));
	    for (int c = 1; c < etm._getColumnCount(); ++c) {
		System.out.print(" "+etm._getValueAt(r,c));
	    }
	    System.out.println();
	}
    }
}
