
package bard.ui.common;

import java.awt.Color;

public interface GridCellHighlighter {
    public Color getCellColor (Grid g, Object value, int cell);
}
