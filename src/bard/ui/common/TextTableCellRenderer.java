// $Id$
package bard.ui.common;

import bard.core.Entity;
import bard.core.Value;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Collection;

public class TextTableCellRenderer
        extends JPanel implements TableCellRenderer {

    private JTextArea textArea;
    //private JEditorPane textArea;
    private String field; // entity field

    public TextTableCellRenderer() {
        this(null);
    }

    public TextTableCellRenderer(String field) {
        this.field = field;

        setOpaque(false);
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(1, 0, 1, 0));

	Font font = UIManager.getFont("Table.font").deriveFont(12.f);

        textArea = new JTextArea();
        textArea.setOpaque(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
	textArea.setFont(font);
        /*
      textArea = new JEditorPane ();
      textArea.setFont(UIManager.getFont("TextArea.font"));
      textArea.setContentType("text/html");
      textArea.setOpaque(false);
      */

        add(textArea);
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Component getTableCellRendererComponent
            (JTable table, Object value, boolean selected,
             boolean focus, int row, int column) {

        if (selected) {
            setOpaque(true);
            setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } else {
            setOpaque(false);
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        if (value == null) {
        } 
        else if (value instanceof Entity) {
            if (field != null) {
                value = ((Entity)value).getValues(field);
            }
        }

        if (value != null) {
            StringBuilder sb = new StringBuilder();
            if (value.getClass().isArray()) {
                Value[] ary = (Value[]) value;
                for (int i = 0; i < ary.length; ++i) {
                    if (ary[i] != null) {
                        if (sb.length() > 0) {
                            sb.append("\n");
                        }
                        sb.append(ary[i].getValue().toString());
                    }
                }
            }
            else if (value instanceof Collection) {
            }
            else if (value instanceof Date) {
                DateFormat fmt = new SimpleDateFormat("MMMMM yyyy");
                sb.append(fmt.format(value));
            } 
            else sb.append(value.toString());
            textArea.setText(sb.toString());
        } 
        else {
            textArea.setText(null);
        }

        return this;
    }
}
