package bard.ui.common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import bard.ui.common.plaf.SliderUI;
import bard.ui.explodingpixels.macwidgets.BottomBar;
import bard.ui.explodingpixels.macwidgets.BottomBarSize;
import bard.ui.explodingpixels.macwidgets.MacButtonFactory;
import bard.ui.explodingpixels.macwidgets.MacIcons;
import bard.ui.icons.IconFactory;

public class DefaultBottomBar extends BottomBar {

    private JSlider slider;
    
    public DefaultBottomBar () {
	super (BottomBarSize.SMALL);
	
	init();
    }

    private void init() {
	slider = new JSlider ();
	Dimension dim = slider.getPreferredSize();
	dim.width = 150;
	slider.setPreferredSize(dim);
	slider.setUI(new SliderUI (slider));
	slider.setEnabled(false);
	
	JPanel sp = new JPanel (new BorderLayout (5, 0));
	sp.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 10));
	sp.setOpaque(false);
	sp.add(new JLabel (MacIcons.MINUS), BorderLayout.WEST);
	sp.add(new JLabel (MacIcons.PLUS), BorderLayout.EAST);
	sp.add(slider);
	this.addComponentToLeft(sp);

	JPanel bp = new JPanel (new BorderLayout (10, 0));
	bp.setOpaque(false);

	final Icon leftIcon = IconFactory.App.SmallLeftTriangle.getIcon();
	final Icon rightIcon = IconFactory.App.SmallRightTriangle.getIcon();
	JComponent btn = MacButtonFactory.createGradientButton
	    (rightIcon, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			JButton btn = (JButton)e.getSource();
			Icon icon = btn.getIcon() == leftIcon 
			    ? rightIcon : leftIcon;
			btn.setIcon(icon);
			btn.setPressedIcon(icon);

//			infoSplit.setDividerLocation
//			    (icon == rightIcon 
//			     ? infoSplit.getLastDividerLocation() 
//			     : infoSplit.getMaximumDividerLocation());
		    }
		});
	btn.setToolTipText("Show/hide info pane");
	btn.setBorder(BorderFactory.createEmptyBorder());

	bp.add(sp);
	//bp.add(btn, BorderLayout.EAST);
	this.addComponentToRight(bp);
    }
    
    public void setSliderEnabled(boolean enabled) {
	slider.setEnabled(enabled);
    }
    
    public void setSliderRangeModel(BoundedRangeModel model) {
	if (model != null) {
	    slider.setEnabled(true);
	    slider.setModel(model);
	} else
	    slider.setEnabled(false);
    }
}

