// $Id: EntityCellEditorRenderer.java 3486 2009-10-29 15:48:13Z nguyenda $

package bard.ui.common;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import java.text.NumberFormat;

import com.jidesoft.swing.*;
import bard.ui.explodingpixels.macwidgets.*;

import bard.core.Entity;
import bard.core.Value;


public class EntityCellEditorRenderer extends AbstractCellEditor
    implements TableCellEditor, TableCellRenderer, ActionListener {

    public enum DisplayStyle {
	PLAIN, // show field contents as plain text with space separated
	    TOKENS, // show field contents as tokens
	    HTML // 
    }

    private Entity entity;
    private JTable table;
    private int row, column;

    private JComponent editorComponent;
    private Dimension preferredSize = new Dimension ();

    private RoundLabel ellipsis = null;
    //private BalloonTip balloon;
    private CheckBoxList checkboxList;

    private TableCellRenderer defaultRenderer;
    private TableCellEditor defaultEditor;

    private NumberFormat nf;

    private boolean editable; // is this field editable?
    private String field;
    // values of the specified field
    private Vector values = new Vector (); 
    private boolean tokenDisplay = true; // display multi-value as tokens


    public EntityCellEditorRenderer () {
	this (null, false);
    }

    public EntityCellEditorRenderer (boolean editable) {
	this (null, editable);
    }

    public EntityCellEditorRenderer (String field) {
	this (field, false);
    }

    public EntityCellEditorRenderer (String field, boolean editable) {
	this.field = field;
	this.editable = editable;

	JPanel pane = new JPanel (new FlowLayout (FlowLayout.LEFT, 2, 1));
	pane.setBorder(BorderFactory.createEmptyBorder(1,0,0,0));
	pane.setOpaque(false);
	editorComponent = pane;

	Font f = UIManager.getFont("Table.font").deriveFont(11.f);
	editorComponent.setFont(f);

	defaultRenderer = new TextTableCellRenderer ();
	defaultEditor = new TextTableCellEditor ();

	nf = NumberFormat.getInstance();
	nf.setGroupingUsed(true);
    }

    public boolean isTokenDisplay () { return tokenDisplay; }
    public void setTokenDisplay (boolean tokenDisplay) {
	this.tokenDisplay = tokenDisplay;
    }

    public Component getTableCellEditorComponent
	(JTable table, Object value,  boolean selected,
	 int row, int column) {

	if (!tokenDisplay) {
	    value = ((Entity)value).getValue(field);
	    return defaultEditor.getTableCellEditorComponent
		(table, value, selected, row, column);
	}


	if (selected) {
	    editorComponent.setOpaque(true);
	    editorComponent.setBackground(table.getSelectionBackground());
	    editorComponent.setForeground(table.getSelectionForeground());
	}
	else {
	    editorComponent.setOpaque(false);
	    editorComponent.setForeground(table.getForeground());
	    editorComponent.setBackground(table.getBackground());
	}

        this.table = table;
        this.row = row;
        this.column = column;
        if (value instanceof Entity) {
	    setEntity ((Entity)value, true);
	}

	return editorComponent;
    }

    public Component getTableCellRendererComponent
	(JTable table, Object value, boolean selected,
	 boolean hasFocus, int row, int column) {

	if (selected) {
	    editorComponent.setOpaque(true);
	    editorComponent.setBackground(table.getSelectionBackground());
	    editorComponent.setForeground(table.getSelectionForeground());
	}
	else {
	    editorComponent.setOpaque(false);
	    editorComponent.setForeground(table.getForeground());
	    editorComponent.setBackground(table.getBackground());
	}

	if (!tokenDisplay) {
	    value = ((Entity)value).getValue(field);
	}

	if (value == null) {
	    if (editorComponent instanceof StringTokenPane) {
		((StringTokenPane)editorComponent).clear();
	    }
	}
	else if (value instanceof Entity) {
	    this.table = table;
	    this.row = row;
	    this.column = column;

	    setEntity ((Entity)value, false);
	}
	else {
	    return defaultRenderer.getTableCellRendererComponent
		(table, value, selected, hasFocus, row, column);
	}

	return editorComponent;
    }


    protected Component createEditorContent () {
	checkboxList = new CheckBoxList (new DefaultListModel ());
	checkboxList.setClickInCheckBoxOnly(true);
	checkboxList.setCheckBoxEnabled(editable);

	JPanel pane = new JPanel (new BorderLayout (0, 2));
	JScrollPane sp = IAppWidgetFactory.makeIAppScrollPane
	    (new JScrollPane (checkboxList));
	// very annoying... shouldn't this be part the l&f?
	sp.setBorder(BorderFactory.createMatteBorder
		     (1, 1, 1, 1, new Color (0xa5a5a5)));
	pane.add(sp);
	pane.setPreferredSize(new Dimension (200, 150));

	if (editable) {
	    Box box = Box.createHorizontalBox();
	    JButton btn;
	    box.add(btn = new JButton (MacIcons.MINUS));
	    btn.setToolTipText("Delete selected tag(s)");
	    btn.addActionListener(new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			Object[] selected = 
			    checkboxList.getCheckBoxListSelectedValues();
			for (Object t : selected) {
			    values.remove(t);
			    ((DefaultListModel)checkboxList
			     .getModel()).removeElement(t);
			}
		    }
		});
	    box.add(Box.createHorizontalStrut(2));
	    box.add(btn = new JButton (MacIcons.PLUS));
	    btn.setToolTipText("Add new tag");
	    final JTextField field = new JTextField ();
	    ActionListener action = new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			String value = field.getText();
			if (value.length() > 0) {
			    Object obj = toValue (value);
			    if (!values.contains(obj)) {
				((DefaultListModel)checkboxList
				 .getModel()).addElement(obj);
				values.add(obj);
			    }
			}
		    }
		};
	    btn.addActionListener(action);
	    field.addActionListener(action);
	    box.add(Box.createHorizontalStrut(5));
	    box.add(field);
	    pane.add(box, BorderLayout.SOUTH);
	}

	return pane;
    }

    protected Value[] getValues (Entity entity) {
	if (field != null) {
	    return entity.getValues(field).toArray(new Value[0]);
	}
	return null;
    }

    protected void setEntity (Entity entity, boolean editor) {

	Rectangle cell = table.getCellRect(row, column, true);
	int maxW = cell.width 
	    + 2*table.getColumnModel().getColumnMargin();
	int maxH = cell.height/* - 1*/;

	editorComponent.removeAll();

	Value[] ev = getValues (entity);
	if (ev == null || ev.length == 0) {
	    return;
	}
	//System.err.println("cell "+row+" "+column+": "+field+ " "+cell);

	ArrayList<ArrayList<Component>> rows = 
	    new ArrayList<ArrayList<Component>>();
	values.setSize(ev.length);

	if (ellipsis == null) {
	    ellipsis = new RoundLabel ("...");	
	    ellipsis.setFont(table.getFont());
	}

	int size = ev.length, h = 0;
	for (int i = 0; i < ev.length; ++i) {
	    values.set(i, ev[i]); // make a copy
	    RoundLabel l = new RoundLabel (toString (ev[i]));
	    l.setFont(table.getFont());

	    Dimension dim = l.getPreferredSize();
	    if (dim.width+15 > maxW) {
		dim.width = maxW-15;
		l.setPreferredSize(dim);
	    }
	    //System.err.println("  token "+i+" \""+ev[i]+"\" "+dim);

	    int w = l.getPreferredSize().width;
	    /*
	      if (ncomps+1 < size) {
	      // +2 FlowLayout gap
	      w += ellipsis.getPreferredSize().width + 2; 
	      }
	    */
	    if ((editorComponent.getPreferredSize().width+w) < maxW) {
		editorComponent.add(l);
	    }
	    else if (h + l.getPreferredSize().height+1 <= maxH) { 
		// new row
		h += l.getPreferredSize().height+1;
		ArrayList<Component> row = new ArrayList<Component>();
		for (Component c : editorComponent.getComponents()) {
		    row.add(c);
		}
		rows.add(row);
		editorComponent.removeAll();
		editorComponent.add(l);
	    }
	    else {
		break;
	    }
	}

	if (editorComponent.getComponentCount() > 0) {
	    ArrayList<Component> row = new ArrayList<Component>();
	    int rh = 0;
	    for (Component c : editorComponent.getComponents()) {
		Dimension d = c.getPreferredSize();
		if (d.height > rh) {
		    rh = d.height;
		}
		row.add(c);
	    }

	    if (rh+h < maxH) {
		rows.add(row);
	    }
	}

	editorComponent.removeAll();
	for (ArrayList<Component> r : rows) {
	    for (Component c : r) {
		editorComponent.add(c);
	    }
	}

	int ncomps = editorComponent.getComponentCount();
	if (ncomps < size) {
	    if (ncomps > 0) {
		editorComponent.remove(ncomps-1);
	    }
	    editorComponent.add(ellipsis);
	}

	if (editor) {
	    DefaultListModel model = 
		(DefaultListModel)checkboxList.getModel();
	    model.clear();
	    for (Object v : values) {
		model.addElement(v);
	    }
	}

	this.entity = entity;
    }

    public String getField () { return field; }
    public void setField (String field) { this.field = field; }
    public boolean isEditable () { return editable; }
    public void setEditable (boolean editable) { this.editable = editable; }

    // should be overriden to provide custom value
    protected String toString (Object v) {
	return v != null ? v.toString() : null;
    }

    // subclass must override this method to convert value from a string
    protected Object toValue (String value) {
	return value;
    }

    public Object getCellEditorValue() {
	return entity;
    }
    public boolean isCellEditable (EventObject e) {
	if (e instanceof MouseEvent) { 
	    return ((MouseEvent)e).getClickCount() >= 2;
	}
	return true;
    }
    	
    public boolean shouldSelectCell (EventObject e) { 
	return true; 
    }

    public boolean startCellEditing (EventObject e) {
	return true;
    }

    public boolean stopCellEditing () { 
	//System.out.println(getClass() + ": cell stop editing");
	// update the values...
	if (entity != null) {
	    Collection<Value> v = entity.getValues(getField ());
            // update values here
	}
	return super.stopCellEditing();
    }
	
    public void cancelCellEditing () { 
	super.cancelCellEditing(); 
    }

    public void actionPerformed (ActionEvent e) {
    }
}
