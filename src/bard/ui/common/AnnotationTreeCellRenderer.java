package bard.ui.common;

import bard.ui.icons.IconFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;

/**
 * Tree cell renderer for annotations.
 *
 * Currently pretty much the same as the {@link CollectionTreeCellRenderer} and so
 * in the future the icons should probably be modified.
 *
 * @author Rajarshi Guha
 */
public class AnnotationTreeCellRenderer extends DefaultTreeCellRenderer {

    static final Icon LEAF_ICON = IconFactory.Misc.Brightness.getIcon();
    static final Icon COLLECTION_ICON = IconFactory.Misc.Tag.getIcon();
    static final Icon STAR_ICON = IconFactory.Misc.Star.getIcon();
    static final Icon RAINBOW_ICON = IconFactory.Misc.Rainbow.getIcon();
    static final Icon PILL_ICON = IconFactory.Misc.Pill.getIcon();

    AnnotationTreeCellRenderer() {
        setLeafIcon(LEAF_ICON);
        //setOpenIcon (IconFactory.Misc.ArrowMergeDown.getIcon());
        //setClosedIcon (IconFactory.Misc.ArrowMergeLeft.getIcon());
        setOpenIcon(null);
        setClosedIcon(null);
    }

    @Override
    public Component getTreeCellRendererComponent
            (JTree tree, Object value, boolean sel, boolean expanded,
             boolean isLeaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        if (value == null) {
        } else {
            value = node.getUserObject();
            String par = "";
            if (node.getParent() != null) {
                par = (String) ((DefaultMutableTreeNode) node
                        .getParent()).getUserObject();
                if (par == null) {
                    par = "";
                }
            }

            Icon leaf = LEAF_ICON;
            if ("Community curation".equals(value)
                    || "Community curation".equals(par)) {
                leaf = STAR_ICON;
            } else if ("QC spectra".equals(value)
                    || "QC spectra".equals(par)) {
                leaf = RAINBOW_ICON;
            } else if ("ClinicalTrials.gov".equals(value)
                    || "ClinicalTrials.gov".equals(par)) {
                leaf = IconFactory.Content.ClinicalTrial2.getIcon();
            } else if (value != null) {
                String s = ((String) value).toLowerCase();
                if (s.indexOf("approved") >= 0
                        || s.indexOf("fda") >= 0) {
                    //leaf = PILL_ICON;
                } else if (par.toLowerCase().indexOf("approved") >= 0
                        || par.toLowerCase().indexOf("fda") >= 0) {
                    leaf = PILL_ICON;
                }
            }
            setLeafIcon(leaf);
        }
        return super.getTreeCellRendererComponent
                (tree, value, sel, expanded, isLeaf, row, hasFocus);
    }
}