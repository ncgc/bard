// $Id$

package bard.ui.common;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import com.jidesoft.swing.JideSplitPane;
import com.jidesoft.swing.SimpleScrollPane;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import bard.ui.explodingpixels.macwidgets.*;
import bard.ui.util.ColorUtil;

/*
 * ColumnTree is a component for hierarchical data structures such
 * as trees.  It's sometimes called Miller's columns as popularized
 * in iTunes.
 */
public class ColumnTree extends JPanel implements ListSelectionListener {

    private static final String NODE_KEY = ".Node";
    private static final Color EVEN_COLUMN_COLOR = ColorUtil.EvenRow;
    private static final Color TRANSPARENT_COLOR = new Color(0, 0, 0, 0);
    private static final Font ITEM_FONT = 
	UIManager.getFont("Label.font");//.deriveFont(11.0f);
    private static final Font ITEM_SELECTED_FONT = 
	ITEM_FONT.deriveFont(Font.BOLD);

    private TreeModel model;
    private JideSplitPane split;
    private JScrollPane scroller;
    private Stack<JList> display = new Stack<JList>();
    private DefaultTreeSelectionModel selection;

    private SourceListColorScheme colorScheme = 
	new SourceListStandardColorScheme();

    class ColumnListCellRenderer implements ListCellRenderer  {
	
	PanelBuilder builder;

        private SourceListCountBadgeRenderer badgeRenderer = 
	    new SourceListCountBadgeRenderer
	    (colorScheme.getSelectedBadgeColor(), 
	     colorScheme.getActiveUnselectedBadgeColor(),
	     colorScheme.getInativeUnselectedBadgeColor(), 
	     colorScheme.getBadgeTextColor());

	DefaultListCellRenderer cellRenderer = new DefaultListCellRenderer ();
	Dimension size = new Dimension (1, -1);
	CellConstraints cc = new CellConstraints();

	ColumnListCellRenderer () {
            // definte the FormLayout columns and rows.
            FormLayout layout = new FormLayout
		("fill:0px:grow, 2px, p, 2px", "1px, fill:p:grow, 1px");
            // create the builders with our panels as the component to 
	    ///   be filled.
            builder = new PanelBuilder(layout);
            builder.getPanel().setOpaque(false);
	    builder.getPanel().setMinimumSize(new Dimension (1, -1));
        }

        public Component getListCellRendererComponent
	    (JList list, Object value, int index, 
	     boolean selected, boolean hasFocus) {

            JPanel panel = builder.getPanel();
	    panel.removeAll();
	    if (selected) {
		panel.setForeground(list.getSelectionForeground());
		panel.setBackground(list.getSelectionBackground());
	    }
	    else {
		panel.setForeground(list.getForeground());
		panel.setBackground(list.getBackground());
	    }

	    if (value != null) {
		TreeNode node = (TreeNode) value;
		Component c = cellRenderer.getListCellRendererComponent
		    (list, getText (node), index, selected, hasFocus);
	
		if (hasFocus || selected || node.isLeaf()) {
		    // don't show if this item is expanded (this means that it
		    //  has children)
		    return c;
		}
		else {
		    c.setMinimumSize(size);
		    builder.add(c, cc.xywh(1, 1, 1, 3));
		    builder.add(badgeRenderer.getComponent(), 
				cc.xy(3, 2, "center, fill"));
		    badgeRenderer.setState(node.getChildCount(), false);
		}
	    }
            return panel;
        }

	protected String getText (TreeNode node) {
	    if (node instanceof DefaultMutableTreeNode) {
		Object value = ((DefaultMutableTreeNode)node).getUserObject();
		return value != null ? value.toString() : "";
	    }
	    return node.toString();
	}
    }


    public ColumnTree () {
	this (createDefaultTreeModel ());
    }

    public ColumnTree (TreeNode root) {
	this (new DefaultTreeModel (root));
    }

    public ColumnTree (TreeModel model) {
	init ();
	setModel (model);
    }

    static java.util.Random rand = new java.util.Random (1);
    static void createChildNodes 
	(int depth, int max, DefaultMutableTreeNode parent) {
	int d = 0;
	for (MutableTreeNode n = parent; n != null; ++d) {
	    n = (MutableTreeNode)n.getParent();
	}
	if (max == 0 || d+1 > depth) {
	    return;
	}

	int nchild = rand.nextInt(max);
	for (int n = 0; n <= nchild; ++n) {
	    String p = parent.getUserObject().toString();
	    DefaultMutableTreeNode child = new DefaultMutableTreeNode 
		(p+"/L"+depth+"."+n);
	    parent.add(child);
	    createChildNodes (depth + 1, rand.nextInt(max), child);
	}
    }

    static protected TreeModel createDefaultTreeModel () {
	DefaultMutableTreeNode root = new DefaultMutableTreeNode ("");
	createChildNodes (6, 10, root);
	return new DefaultTreeModel (root);
    }

    protected void init () {
	split = new JideSplitPane (JideSplitPane.HORIZONTAL_SPLIT);
	split.setProportionalLayout(true);
	//split.setInitiallyEven(true);
	split.setDividerSize(2);

	setLayout (new BorderLayout ());
	scroller = new JScrollPane (split);
	/*
	scroller.setVerticalScrollBarPolicy
	    (ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
	*/
	add (scroller);
	/*
	add (new SimpleScrollPane (split, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
				  JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
	*/

	selection = new DefaultTreeSelectionModel ();
	selection.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    public void addTreeSelectionListener (TreeSelectionListener l) {
	selection.addTreeSelectionListener(l);
    }

    public void removeTreeSelectionListener (TreeSelectionListener l) {
	selection.removeTreeSelectionListener(l);
    }

    public TreePath getSelectionPath () { 
	return selection.getSelectionPath(); 
    }

    public void setModel (TreeModel model) {
	this.model = model;
	split.removeAll();
	for (JList list : display) {
	    list.removeListSelectionListener(this);
	}
	display.clear();

	Object root = model.getRoot();
	display.push(addChild (root));
    }
    public TreeModel getModel () { return model; }

    protected JList addList (Object node, JList list) {
	list.setCellRenderer(new ColumnListCellRenderer ());
	list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	list.setMinimumSize(new Dimension (100, -1));
	list.addListSelectionListener(this);
	list.putClientProperty(NODE_KEY, node);

	split.addPane(list);
	split.revalidate();

	//System.err.println(split.getComponentCount()+ " children");
	//return spane;
	return list;
    }

    protected JList addChild (Object node) {
	int childCount = model.getChildCount(node);
	Object[] children = new Object[childCount];
	for (int i = 0; i < childCount; ++i) {
	    children[i] = model.getChild(node, i);
	}
	return addList (node, new JList (children));
    }

    /*
     * ListSelectionListener
     */
    public void valueChanged (ListSelectionEvent e) {
	if (e.getValueIsAdjusting()) {
	    return;
	}

	JList list = (JList)e.getSource();
	while (!display.empty()) {
	    JList l = display.pop();
	    if (l != list) {
		//System.out.println("removing "+c.getClientProperty(NODE_KEY));
		l.removeListSelectionListener(this);
		split.removePane(l);
		split.revalidate();
	    }
	    else {
		display.push(l); // put back
		break;
	    }
	}
	list.repaint();

	boolean isLeaf = false;
	Object node = list.getSelectedValue();
	if (model.getChildCount(node) > 0) {
	    //System.out.println("adding "+node);
	    JList c = addChild (node);
	    display.push(c);
	    if (display.size() % 2 == 0) {
		c.setBackground(EVEN_COLUMN_COLOR);
	    }
	    Point pt = scroller.getViewport().getViewPosition();
	    pt.x += c.getPreferredSize().width;
	    pt.y = 0;
	    scroller.getViewport().setViewPosition(pt);
	}
	else {
	    isLeaf = true;
	}

	Object[] path = new Object[display.size() + (isLeaf ? 1 : 0)];
	for (int i = 0; i < display.size(); ++i) {
	    JComponent c = display.get(i);
	    path[i] = c.getClientProperty(NODE_KEY);
	}

	if (isLeaf) {
	    path[path.length-1] = node;
	}

	selection.setSelectionPath(new TreePath (path));

	for (JComponent c : display) {
	    c.invalidate();
	}
	
	revalidate();
    }

    public static void main (String[] argv) throws Exception {
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    JFrame f = new JFrame ("Test ColumnTree");
		    ColumnTree ctree = new ColumnTree ();
		    ctree.addTreeSelectionListener
			(new TreeSelectionListener () {
				public void valueChanged
				    (TreeSelectionEvent e) {
				    TreePath path = e.getPath();
				    System.out.println(path);
				}
			});
		    f.getContentPane().add(ctree);
		    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    f.pack();
		    f.setSize(600, 400);
		    f.setVisible(true);
		}
	    });
    }
}
