// $Id: MolCellEditor.java 3486 2009-10-29 15:48:13Z nguyenda $

package bard.ui.common;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import java.awt.geom.RoundRectangle2D;
import java.awt.event.MouseAdapter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.jdesktop.swingx.border.DropShadowBorder;

import chemaxon.marvin.beans.MViewPane;
import chemaxon.marvin.common.UserSettings;
import chemaxon.marvin.paint.DispOptConsts;
import chemaxon.struc.DPoint3;
import chemaxon.struc.Molecule;
import chemaxon.struc.MDocument;
import chemaxon.struc.MolAtom;

import org.bushe.swing.event.EventBus;

import bard.core.Value;
import bard.core.Entity;
import bard.core.Compound;
import bard.core.MolecularValue;
import bard.core.adapter.CompoundAdapter;
import bard.core.StructureSearchParams;

import bard.ui.util.ColorUtil;
import bard.ui.event.SearchEvent;
import bard.chem.ChemUtil;


public class MolCellEditor extends AbstractCellEditor
        implements GridCellEditor, TableCellEditor,
		   ActionListener, PropertyChangeListener, MouseListener {

    static private final Logger logger = Logger.getLogger
	(MolCellEditor.class.getName());

    class CellEditorLayerUI extends AbstractLayerUI<JPanel> {
	RoundRectangle2D rect = new RoundRectangle2D.Float();
	Color color = new Color (0x10, 0x10, 0x10, 150);

	CellEditorLayerUI () {
	}


	@Override
	protected void paintLayer (Graphics2D g2, JXLayer layer) {
	    super.paintLayer(g2, layer);
	    if (labelVisible && labelRenderer != null) {
		Rectangle r = layer.getBounds();
		labelRenderer.renderLabel
		    (g2, 5, 5, 4, r.width, r.height, getLabel ());
	    }
	    /*
	    g2.setRenderingHint(RenderingHints.KEY_RENDERING, 
				RenderingHints.VALUE_RENDER_QUALITY);
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
				RenderingHints.VALUE_ANTIALIAS_ON);

	    Rectangle r = layer.getBounds();
	    g2.setPaint(color);
	    g2.setStroke(new BasicStroke(2.f));
	
	    float w = (float)(Math.min(r.width, r.height)*0.2+0.5);
	    w = Math.max(16.f, w);

	    float o = (float)(Math.min(r.width,r.height)*0.1+0.5);
    
	    rect.setRoundRect(r.x+o, r.y+o, w, w, w/4.f, w/4.f);
	    g2.fill(rect);
	    */
	}

	@Override
	protected void processMouseEvent (MouseEvent e, JXLayer layer) {
	    // intercepting mouse event...
	    //System.err.println("MouseEvent: "+e);
	    
	    // MAC right-click
	    if (e.getClickCount() > 0 
		&& e.getButton() == MouseEvent.BUTTON1
		&& e.getID() == MouseEvent.MOUSE_CLICKED
		&& e.isControlDown()) {
		// transfer the event to the child component
		super.processMouseEvent(e, layer);
	    }
	    // single or double left-click
	    else if (e.getClickCount() > 0 
		&& e.getButton() == MouseEvent.BUTTON1
		&& e.getID() == MouseEvent.MOUSE_CLICKED) {
		doAction (e);
	    }
	    // right-click
	    else if (e.getClickCount() > 0 
		&& e.getButton() == MouseEvent.BUTTON3
		&& e.getID() == MouseEvent.MOUSE_CLICKED) {
		// transfer the event to the child component
		super.processMouseEvent(e, layer);
	    }
	    // other events lost, like drag (rotate molecule)
	    e.toString();
	}

	@Override
	public long getLayerEventMask() { 
	    /*
	     * this prevents the layer from intercepting the input masks 
	     * such as keyboard and mouse events.  we have to do this
	     * because turning on the masks will mess around with the
	     * AWT's default toolkit event listener that prevents it
	     * to run within webstart sandbox.
	     */
	    return enableAction ? super.getLayerEventMask() : 0l;
	}
    }

    private String field;
    
    private JPanel epane;
    private JPopupMenu popup;
    private MViewPane mview;
    private boolean modified = false;
    private DPoint3[] points = {};
    private Object value, oldValue;
    private Color borderColor = null;
    private PropertyChangeSupport pcs = 
	new PropertyChangeSupport (this);
    private boolean enableAction = true;
    private boolean isEditing = false;
    private boolean ignoreEZ = false;
    private boolean showUndefinedStereo = true;
    private Map props = new HashMap ();

    private boolean labelVisible = false;
    private LabelRenderer labelRenderer = new LabelRenderer ();

    public MolCellEditor () {
        this(null, null, new MViewPane());
    }

    public MolCellEditor (String field) {
        this (field, null, new MViewPane());
    }

    public MolCellEditor (Color border) {
        this (null, border, new MViewPane());
    }

    public MolCellEditor (String field, Color border) {
	this (field, border, new MViewPane ());
    }

    public MolCellEditor (String field, Color border, MViewPane mview) {
        this.mview = mview;
        mview.setNavmode("rotZ");
        this.borderColor = border;
        this.field = field;
        init();
    }

    public void mouseClicked (MouseEvent e) {
	doPopupGesture (e);
    }
    public void mouseEntered (MouseEvent e) {
    }
    public void mouseExited (MouseEvent e) {
    }
    public void mousePressed (MouseEvent e) {
	doPopupGesture (e);
    }
    public void mouseReleased (MouseEvent e) {
	doPopupGesture (e);
    }

    void doPopupGesture (MouseEvent e) {
	if (e.isPopupTrigger()) {
	    popup.show((Component)e.getSource(), e.getX(), e.getY());
	}
    }

    public void addPropertyChangeListener (PropertyChangeListener l) {
	pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener (PropertyChangeListener l) {
	pcs.removePropertyChangeListener(l);
    }

    public MViewPane getMView () {
        return mview;
    }

    public String getField () {
        return field;
    }

    public void setField (String field) {
        this.field = field;
    }

    public void setEnableAction (boolean enableAction) {
	this.enableAction = enableAction;
    }
    public boolean isActionEnabled () { return enableAction; }

    public void setIgnoreEZ (boolean ignoreEZ) {
	boolean oldValue = this.ignoreEZ;
	this.ignoreEZ = ignoreEZ;
	pcs.firePropertyChange("ignoreEZ", oldValue, ignoreEZ);
    }
    public boolean getIgnoreEZ () { return ignoreEZ; }

    public void setShowUndefinedStereo (boolean visible) {
	boolean old = showUndefinedStereo;
	showUndefinedStereo = visible;
	pcs.firePropertyChange("showUndefinedstereo", 
			       old, showUndefinedStereo);
    }

    public boolean getShowUndefinedStereo () { return showUndefinedStereo; }

    protected void init () {
	mview.setLocation(10, 10);
        mview.addActionListener(this);
        mview.setDetachable(false);
        mview.setEditable(1);
        mview.addPropertyChangeListener(this);
        mview.setImplicitH(UserSettings.IMPLICITH_HETERO_S);
        mview.setAtomMappingVisible(false);
	mview.setBackground(Color.white);
	mview.setChiralitySupport(Molecule.CHIRALITYSUPPORT_ALL);
	mview.setEzVisible(true);
	mview.setAtomsize(.6);
	mview.setBondSpacing(.3);
	mview.setDispQuality(1);
	mview.setDraggable(false);

        JMenuItem item;
        popup = new JPopupMenu();

        JMenu edit = new JMenu("Edit");
        mview.makeEditMenu(edit);
        {
            Component sep = null;
            for (int c = 0; c < edit.getMenuComponentCount(); ++c) {
                Component comp = edit.getMenuComponent(c);
                //System.err.println(c+":"+comp);
                if (comp instanceof JMenuItem) {
                    item = (JMenuItem) comp;
                    if (item.getText().equals("Paste")
			|| item.getText().startsWith("Preferences")
			|| item.getText().equals("Source")) {
                        edit.remove(item);
                    }
                } 
		else if (comp instanceof JSeparator) {
                    sep = comp;
                }
            }
            edit.remove(sep); // remove last separator
        }
        popup.add(edit);
        popup.addSeparator();

        mview.makeSaveAsMenu(popup);
        mview.addSaveImageMenu(popup);
        //popup.addSeparator();

	mview.getVisibleCellComponent(0).addMouseListener(this);
	mview.setPopupMenusEnabled(false);
    }

    public void setLabelVisible (boolean visible) {
	this.labelVisible = visible;
    }
    public boolean getLabelVisible () { return labelVisible; }

    public void setLabelRenderer (LabelRenderer renderer) {
	this.labelRenderer = renderer;
    }
    public LabelRenderer getLabelRenderer () { return labelRenderer; }

    public void setGridEditorPane (GridEditorPane ep) {
	ep.removeAll();
	ep.add(mview);
	
	final JXLayer layer = new JXLayer (ep, new CellEditorLayerUI ());
	
	epane = new JPanel (new BorderLayout ()) {
		@Override
		public void setToolTipText (String text) {
		    super.setToolTipText(text);
		    layer.setToolTipText(text);
		    mview.getVisibleCellComponent(0).setToolTipText(text);
		}
	    };
	epane.setOpaque(false);
	epane.add(layer);
    }

    Component getGridEditor (Grid g) {
	// lazy evaluation...
	if (epane == null) {
            GridEditorPane gep = new GridEditorPane ();
            gep.setPreferredSize(new Dimension 
                                 (g.getCellSize(), g.getCellSize()));
	    setGridEditorPane (gep);
	}
	return epane;
    }

    Component getTableEditor () {
	if (epane == null) {
	    epane = new JPanel (new BorderLayout ());
	    epane.setOpaque(false);

	    JXLayer layer = new JXLayer(mview, new CellEditorLayerUI ());
	    epane.add(layer);

	    Border border;
	    if (borderColor != null) {
		border = BorderFactory.createCompoundBorder
		    (new DropShadowBorder(),
		     BorderFactory.createLineBorder(borderColor));
	    } 
	    else {
		border = new DropShadowBorder();
	    }
	    epane.setBorder(border);
	}
	return epane;
    }

    String getLabel () {
	if (value == null) {
	    return null;
	}
        Class clazz = value.getClass();

        if (Molecule.class.isAssignableFrom(clazz)) {
	    return ((Molecule)value).getName();
	}
	else if (Entity.class.isAssignableFrom(clazz)) {
	    return ((Entity)value).getName();
	}

	return value.toString();
    }

    public void doAction (MouseEvent e) {
	pcs.firePropertyChange("action-click:"+e.getClickCount(), oldValue, value);
    }

    public void setAtomMapVisible (boolean visible) {
	mview.setAtomMappingVisible(visible);
    }
    public boolean isAtomMapVisible () { return mview.isAtomMappingVisible(); }

    public void setValue (Object value) {
        if (value != null) {
            Class clazz = value.getClass();
            if (Molecule.class.isAssignableFrom(clazz)) {
		setMol ((Molecule)value);
            }
	    else if (Compound.class.isAssignableFrom(clazz)) {
                Compound cmpd = (Compound) value;
                CompoundAdapter adapter = new CompoundAdapter (cmpd);
                Molecule mol = (Molecule)adapter.toFormat
                    (CompoundAdapter.Format.NATIVE);
                setMol (mol);
            }
	    else if (value instanceof Entity && field != null) {
                Value v = ((Entity) value).getValue(field);
                // for now, show the last molecule if we have more than 1
                Object mol = v != null ? v.getValue() : null;
                if (mol instanceof Molecule) {
		    setMol ((Molecule) mol);
		}
		else if (mol instanceof Compound) {
		    // recursive...sigh
		    setValue (mol);
		}
            }
	    else {
		mview.setM(0, ChemUtil.toMol(value.toString(), ignoreEZ));
            }

            Molecule m = mview.getM(0);
            if (m != null) {
                points = (DPoint3[]) m.getPoints().clone();
            }
        }
	else {
            mview.setM(0, (Molecule) null);
            points = null;
        }
        modified = false;
	this.oldValue = this.value;
        this.value = value;
    }

    void setMol (Molecule mol) {
	Molecule m = mol.cloneMolecule();
        if (m.getDim() < 2) {
            m.clean(2, null);
        }
	// clear all Object properties, otherwise we can't 
	//  perform copy action
	props.clear();
	for (int i = 0; i < m.getPropertyCount(); ++i) {
	    String key = m.getPropertyKey(i);
	    Object obj = m.getPropertyObject(key);
	    if (!(obj instanceof String)) {
		// remove none String object
		m.setPropertyObject(key, null);
		props.put(key, obj);
	    }
	}

	if (ignoreEZ) {
	    ChemUtil.resetEZ(m);
	}

	if (getShowUndefinedStereo ()) {
	    MolAtom[] atoms = m.getAtomArray();
	    for (int i = 0; i < atoms.length; ++i) {
		int chiral = m.getChirality(i);
		if (atoms[i].getAtno() == 7) { // N inversion
		}
		else if (chiral == MolAtom.CHIRALITY_R 
		    || chiral == MolAtom.CHIRALITY_S) {
		}
		else if (chiral != 0) {
		    atoms[i].setSelected(true);
		}
	    }
	}

	//mview.setM(0, m);
	MDocument doc = new MDocument (m);
	doc.setBondSetThickness(0, .125);
        mview.setColorScheme
	    (m.hasAtomSet() || m.hasBondSet()
	     ? DispOptConsts.MONO_SCHEME_S 
	     : DispOptConsts.CPK_SCHEME_S);
	
	mview.setDocument(0, doc);
    }

    public void propertyChange(PropertyChangeEvent e) {
	if (e.getSource() != mview) {
	    return;
	}
        String prop = e.getPropertyName();
	/*
	logger.info("propery="+prop + " old="+e.getOldValue()
		    + " new="+e.getNewValue());
	*/
	if (prop.equals("windowClosing")) {
	    isEditing = false;
	}
	else if (prop.equals("windowOpened")) {
	}
        else if (prop.equals("selectedIndex")) {
        } 
	else if (prop.equals("doc0")) {
        } 
	else {
            return;
        }
    }

    public void actionPerformed (ActionEvent e) {
	String action = e.getActionCommand();
        logger.info("action=" + action + " " + e.getSource());

	if ("copy".equals(action)) {
	    
	}

        // any menu action performed is consider as modified..
        modified = true;
	isEditing = action.equals("msketch");
    }

    public JPopupMenu getPopupMenu () { return popup; }

    @Override
    public void cancelCellEditing () {
	isEditing = false;
	super.cancelCellEditing();
    }

    @Override
    public boolean stopCellEditing() {
        if (popup.isVisible() || isEditing) {
            return false;
        }
        return super.stopCellEditing();
    }

    public Object getCellEditorValue() {
        if (value == null || mview.getCellCount() == 0) {
            return null;
        }

        try {
            mview.applyRotationMatrices();
        }
        catch (Exception ex) {
        }

        Molecule m = mview.getM(0);
        if (m == null) {
            return null;
        }

        if (!modified) {
            DPoint3[] pt = m.getPoints();
            if (pt.length != points.length) {
                modified = true;
            } 
	    else {
                for (int i = 0; i < pt.length && !modified; ++i) {
                    if (!pt[i].equals(points[i])) {
                        modified = true;
                    }
                }
            }
        }

	if (getShowUndefinedStereo ()) {
	    // turn off selection...
	    for (MolAtom a : m.getAtomArray()) {
		if (a.isSelected()) {
		    a.setSelected(false);
		}
	    }
	}

	// now restore the non-string properties
	for (Object obj : props.entrySet()) {
	    Map.Entry me = (Map.Entry)obj;
	    m.setPropertyObject((String)me.getKey(), me.getValue());
	}

        // create a new instance to force the renderer to be generated!
        if (value instanceof Compound) {
            CompoundAdapter ca = new CompoundAdapter ((Compound)value);
            ca.setMolecule(modified ? m.cloneMolecule() : m);
            /*
            ((Entity) value).set(Entity.ExtObj,
                    modified ? m.cloneMolecule() : m);
            */
            // TODO: need to propagate the changes to the entity here!!!
        }
	else if (value instanceof Molecule) {
	    m.clonecopy((Molecule)value);
	}
	else {
	    Molecule mol = ChemUtil.toMol(value.toString(), ignoreEZ);
	    mol.clonecopy(m);
	}

        return value;
    }

    public Component getGridCellEditorComponent
            (Grid g, Object value, boolean selected, int cell) {
        setValue (value);
        return getGridEditor (g);
    }

    public Component getTableCellEditorComponent
            (JTable table, Object value, boolean selected, 
	     int row, int column) {
        Color c = row % 2 == 0 ? ColorUtil.EvenRow : ColorUtil.OddRow;
        mview.setMolbg(c);
        mview.setBackground(c);

        setValue (value);
        return getTableEditor ();
    }

    protected void showSimilarCompounds (Compound cmpd) {
        MolecularValue mv =
            (MolecularValue)cmpd.getValue(Compound.MolecularValue);
	Molecule mol = ((Molecule)mv.toFormat(MolecularValue.Format.NATIVE))
            .cloneMolecule();
	String molstr = mol.toFormat("smiles:a");
	showSimilarCompounds (molstr);
    }

    protected void showSimilarCompounds (String molstr) {
	StructureSearchParams query = new StructureSearchParams
	    (molstr, StructureSearchParams.Type.Similarity);
	EventBus.publish(new SearchEvent (this, query));
    }

    public void instrumentContextMenu () {
	JPopupMenu popup = getPopupMenu ();
	JMenuItem item = new JMenuItem ("Show similar compounds");
	item.setToolTipText("Search for similar compounds");
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    Object value = getCellEditorValue ();
		    if (value == null) {
			return;
		    }

		    if (value instanceof Compound) {
			Compound cmpd = (Compound)value;
			showSimilarCompounds (cmpd);
		    }
		    else {
			showSimilarCompounds (value.toString());
		    }
		}
	    });
	popup.insert(item, 0);
	popup.insert(new JPopupMenu.Separator(), 1);
    }
}
