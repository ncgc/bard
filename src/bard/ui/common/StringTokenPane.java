// $Id$

package bard.ui.common;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;

public class StringTokenPane extends JTextPane
    implements Highlighter.HighlightPainter {

    private static final Logger logger = Logger.getLogger
	(StringTokenPane.class.getName());

    private static final Color COLOR_OUTLINE = new Color (100, 150, 250);
    private static final Color COLOR_FILL = new Color (241, 245, 250);

    private ArrayList<String> tokens = new ArrayList<String>();
    private Highlighter highlighter = new DefaultHighlighter ();

    float strokeWidth = 1.f;
    private BasicStroke stroke = new BasicStroke (strokeWidth);
    RoundRectangle2D.Double rect = new RoundRectangle2D.Double();

    public StringTokenPane () {
	init ();
    }

    public StringTokenPane (String... tokens) {
	init ();
	setTokens (tokens);
    }

    protected void init () {
	setOpaque (false);
	setHighlighter (highlighter);
	setEditable (false);
    }

    public void setTokens (String... tokens) {
	clear ();
	for (String t : tokens) {
	    addToken (t);
	}
    }

    public void clear () {
	highlighter.removeAllHighlights();
	setText (null);
    }

    public void addToken (String token) {
	try {
	    Document doc = getDocument ();
	    int p0 = doc.getLength();
	    if (p0 == 0) {
		// first token
		doc.insertString(p0++, " ", null);
	    }
	    doc.insertString(p0++, "  ", null);
	    doc.insertString(p0, token, null);
	    int p1 = doc.getLength();
	    doc.insertString(p1, "  ", null);
	    highlighter.addHighlight(p0, p1, this);
	    //highlighter.addHighlight(p0, p1, new DefaultHighlighter.DefaultHighlightPainter(COLOR_FILL));
	}
	catch (BadLocationException ex) {
	    logger.log(Level.SEVERE, "Bad location", ex);
	}
    }

    public String[] getTokens () {
	Highlighter.Highlight[] highlights = highlighter.getHighlights();
	String[] tokens = new String[highlights.length];
	try {
	    Document doc = getDocument ();
	    for (int i = 0; i < highlights.length; ++i) {
		int p1 = highlights[i].getEndOffset();
		int p0 = highlights[i].getStartOffset();
		if (p1 > p0) {
		    tokens[i] = doc.getText(p0, p1-p0);
		}
		System.out.println(i+": \""+tokens[i]+"\" "+p0+" "+p1);
	    }
	}
	catch (BadLocationException ex) {
	    logger.log(Level.SEVERE, "Bad location", ex);
	}
	return tokens;
    }

    public void paint (Graphics g, int p0, int p1, 
		       Shape bounds, JTextComponent c) {
	Graphics2D g2 = (Graphics2D)g;
	try {
	    Rectangle r0 = c.modelToView(p0);
	    Rectangle r1 = c.modelToView(p1);
	    //String s = c.getText(p0, p1-p0);
	    //System.err.println(p0 + " " + p1 + " " + s+" "+bounds);
	    g2.setComposite(AlphaComposite.Src);
	    g2.setRenderingHint(RenderingHints.KEY_RENDERING, 
				RenderingHints.VALUE_RENDER_QUALITY);
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
				RenderingHints.VALUE_ANTIALIAS_ON);
	    
	    /*if (r0.y == r1.y)*/ {
		Rectangle r = r0.union(r1);
		r.x -= 6;
		r.width += 6;
		r.y += 1;
		r.height -= 2;
		
		g2.setPaint(COLOR_FILL);
		g2.fill(r);

		g2.setStroke(stroke);
		g2.setColor(COLOR_OUTLINE);
		int arc = getRoundedDiameter (r.height);
		rect.setRoundRect(r.x+strokeWidth/2., r.y+strokeWidth/2., 
				  r.width, r.height, (double)arc, (double)arc);
		g2.draw(rect);
	    }
	}
	catch (BadLocationException ex) {
	    
	}
    }

    private int getRoundedDiameter (int controlHeight) {
	int roundedDiameter = (int) (controlHeight * .98 + 0.5);
	return roundedDiameter - roundedDiameter % 2;
    }


    public static void main (final String[] argv) throws Exception {
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    JFrame f = new JFrame ("Test frame for StringTokenPane");
		    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    StringTokenPane stp = new StringTokenPane (argv);
		    stp.addToken("huh?");
		    for (String t : stp.getTokens()) {
			System.err.println(t);
		    }
		    f.getContentPane().add(stp);
		    f.pack();
		    f.setSize(400, 400);
		    f.setVisible(true);
		}
	    });
    }
}
