// $Id$

package bard.ui.common.plaf;

import javax.swing.*;
import javax.swing.plaf.basic.BasicSliderUI;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.RoundRectangle2D;

import bard.ui.util.ColorUtil;

public class SliderUI extends BasicSliderUI {
    private static final int KNOB_SIZE = 11;
    private static final int TRACK_HEIGHT = 5;

    public static final Color BORDER_COLOR = Color.white;
    private static final int BORDER_WIDTH = 1;

    public SliderUI (JSlider slider) {
        super (slider);
    }

    @Override
    protected void installDefaults (JSlider slider) {
        super.installDefaults(slider);
        slider.setOpaque(false);
    }

    @Override
    protected Dimension getThumbSize () {
        return new Dimension (KNOB_SIZE, KNOB_SIZE);
    }

    @Override
    public void paint(Graphics g, JComponent c) {
	if (!c.isEnabled()) {
            ((Graphics2D)g).setComposite(AlphaComposite.getInstance
					 (AlphaComposite.SRC_OVER, 0.5f));
        }
        super.paint(g, c);
    }

    @Override
    public void setThumbLocation(int x, int y) {
        super.setThumbLocation(x, y);
        // repaint the whole slider -- it's easier than trying to figure out whats dirty, especially
        // since the thumb will be drawn outside of the thumbRect (the shadow part).
        slider.repaint();
    }
    
    @Override
    public void paintThumb (Graphics g) {
	Graphics2D g2 = (Graphics2D)g;
        Paint paint = createSliderKnobButtonPaint
	    (isDragging(), thumbRect.height);

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			   RenderingHints.VALUE_ANTIALIAS_ON);

        int x = thumbRect.x;
        int y = thumbRect.y;
        int width = thumbRect.width;
        int height = thumbRect.height;
	/*
        g2.setColor(ColorUtil.LightShadow);
        g2.draw(new Ellipse2D.Double(x, y, width - BORDER_WIDTH, height));

        g2.setColor(ColorUtil.DarkShadow);
        g2.draw(new Ellipse2D.Double(x, y, width - BORDER_WIDTH, 
				     height + BORDER_WIDTH));
	*/
        g2.setPaint(paint);
        g2.fill(new Ellipse2D.Double(x, y /*+ BORDER_WIDTH*/, 
				     width, height/* - BORDER_WIDTH*/));

        g2.setColor(ColorUtil.LightShadow);
        g2.draw(new Ellipse2D.Double(x, y, width - BORDER_WIDTH, 
				     height - BORDER_WIDTH));

        g2.setColor(ColorUtil.LightShadow);
	Shape dot = new Ellipse2D.Double
	    (x+(width-3.)/2., y+(height-3.-BORDER_WIDTH)/2., 3., 3.);
        g2.fill(dot);
	g2.setColor(ColorUtil.DarkShadow);
	g2.draw(dot);
    }

    @Override
    public void paintTrack (Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			     RenderingHints.VALUE_ANTIALIAS_ON);

        double trackY = (slider.getHeight() - TRACK_HEIGHT - 2) / 2.0;
        RoundRectangle2D track = new RoundRectangle2D.Double(
                0, trackY, slider.getWidth() - 2, TRACK_HEIGHT, 6, 4);

        g.setColor(ColorUtil.LightShadow);
        g2d.fill(track);
        g2d.setColor(ColorUtil.DarkShadow);
        g2d.draw(track);
    }


    @Override
    public void paintFocus(Graphics g) {
        // don't paint focus.
    }

    private static Paint createSliderKnobButtonPaint
	(boolean isPressed, int height) {
        Color bottomColor = isPressed 
	    ? ColorUtil.Highlighter : ColorUtil.Default;
        // compenstate for the two pixel shadow drawn below the slider thumb.
        int bottomY = height;// - (BORDER_WIDTH+BORDER_WIDTH);
        return new GradientPaint(0, 0, Color.white, 0, bottomY, bottomColor);
    }
}
