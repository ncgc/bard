// $Id: EntityCellEditorRenderer.java 3486 2009-10-29 15:48:13Z nguyenda $

package bard.ui.common;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import org.jdesktop.swingx.border.DropShadowBorder;
import com.jidesoft.swing.SimpleScrollPane;

import bard.core.Entity;
import bard.core.Value;
import bard.ui.util.ColorUtil;

public class TokenCellEditorRenderer extends AbstractCellEditor
    implements TableCellEditor, TableCellRenderer, ActionListener {

    //private Entity entity;
    private Object value;
    private JTable table;
    private int row, column;

    private JComponent editorComponent;
    private JComponent renderComponent;

    private RoundLabel ellipsis = null;
    private String field;
    private JList list;
    private SimpleScrollPane ssp;
    private Font font;
    private boolean outlineVisible = false;

    public TokenCellEditorRenderer () {
	this (null);
    }

    public TokenCellEditorRenderer (String field) {
	this (field, ColorUtil.Default);
    }

    public TokenCellEditorRenderer (boolean outlineVisible) {
	this (null, outlineVisible);
    }

    public TokenCellEditorRenderer (String field, boolean outlineVisible) {
	this (field, ColorUtil.Default);
	setOutlineVisible (outlineVisible);
    }

    public TokenCellEditorRenderer (String field, Color border) {
	this.field = field;

	JPanel pane = new JPanel (new FlowLayout (FlowLayout.LEFT, 2, 1));
	pane.setBorder(BorderFactory.createEmptyBorder(1,0,0,0));
	pane.setOpaque(false);
	font = UIManager.getFont("Table.font").deriveFont(12.f);
	pane.setFont(font);
	renderComponent = pane;

	pane = new JPanel (new BorderLayout ());
	list = new JList (new DefaultListModel ()) {
		public String getToolTipText (MouseEvent e) {
		    JList l = (JList)e.getSource();
		    int index = l.locationToIndex(e.getPoint());
		    if (index >= 0) {
			Object value = list.getModel().getElementAt(index);
			if (value != null) {
			    return value.toString();
			}
		    }
		    return null;
		}
	    };
	list.setFont(font);
	/*
	JScrollPane sp = IAppWidgetFactory.makeIAppScrollPane
	    (new JScrollPane ();
	pane.add(sp);
	sp.setBackground(Color.white);
	*/
	ssp = new SimpleScrollPane (list);
	ssp.setBackground(Color.white);
	ssp.setHorizontalScrollBarPolicy
	    (JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	pane.add(ssp);

	if (border != null) {
	    pane.setBorder(BorderFactory.createCompoundBorder
			   (new DropShadowBorder (), 
			    BorderFactory.createLineBorder(border)));
	}
	else {
	    pane.setBorder(new DropShadowBorder ());
	}
	editorComponent = pane;
    }

    public void setOutlineVisible (boolean outlineVisible) {
	this.outlineVisible = outlineVisible;
    }
    public boolean isOutlineVisible () { return outlineVisible; }


    public Component getTableCellEditorComponent
	(JTable table, Object value,  boolean selected, int row, int column) {

	if (selected) {
	    editorComponent.setOpaque(true);
	    editorComponent.setBackground(table.getSelectionBackground());
	    editorComponent.setForeground(table.getSelectionForeground());
	}
	else {
	    Color c = row%2 == 0 ? ColorUtil.EvenRow : ColorUtil.OddRow;
	    list.setBackground(c);
	    ssp.setBackground(c);

	    editorComponent.setOpaque(false);
	    editorComponent.setForeground(table.getForeground());
	    editorComponent.setBackground(table.getBackground());
	}

	this.table = table;
	this.row = row;
	this.column = column;
	this.value = value;
	
	DefaultListModel model = (DefaultListModel)list.getModel();
	model.clear();

        Value[] values = parseValue (value);
        if (values != null) {
            for (Value v : values) {
                model.addElement(toString (v));
            }
		
            if (values.length > 0) {
                list.setSelectedIndex(0);
            }
	}

	return editorComponent;
    }

    public Component getTableCellRendererComponent
	(JTable table, Object value, boolean selected,
	 boolean hasFocus, int row, int column) {

	if (selected) {
	    renderComponent.setOpaque(true);
	    renderComponent.setBackground(table.getSelectionBackground());
	    renderComponent.setForeground(table.getSelectionForeground());
	}
	else {
	    renderComponent.setOpaque(false);
	    renderComponent.setForeground(table.getForeground());
	    renderComponent.setBackground(table.getBackground());
	}

	this.table = table;
	this.row = row;
	this.column = column;
	//this.value = value;

	renderComponent.removeAll();
        Value[] values = parseValue (value);
        if (values != null && values.length > 0) {
            showValues (values);
        }

	return renderComponent;
    }

    public Value[] parseValue (Object value) {
	if (value == null) {
	    return new Value[0];
	}
	else if (value instanceof Entity) {
	    if (field != null) {
		return ((Entity)value).getValues(field).toArray(new Value[0]);
	    }
	}
	else if (value instanceof Value[]) {
	    return (Value[])value;
	}
        return new Value[0];
    }

    protected void showValues (Value[] ev) {
	Rectangle cell = table.getCellRect(row, column, true);
	int maxW = cell.width 
	    + 2*table.getColumnModel().getColumnMargin();
	int maxH = cell.height/* - 1*/;

	//System.err.println("cell "+row+" "+column+": "+field+ " "+cell);

	ArrayList<ArrayList<Component>> rows = 
	    new ArrayList<ArrayList<Component>>();

	if (ellipsis == null) {
	    ellipsis = new RoundLabel ();
	    ellipsis.setFont(font);
	}

	int size = ev.length, h = 0;
	for (int i = 0; i < ev.length; ++i) {
	    RoundLabel l = new RoundLabel (toString (ev[i]));
	    l.setOutlineVisible(outlineVisible);
	    l.setFont(font);

	    Dimension dim = l.getPreferredSize();
	    if (dim.width+15 > maxW) {
		dim.width = maxW-15;
		l.setPreferredSize(dim);
	    }
	    //System.err.println("  token "+i+" \""+ev[i]+"\" "+dim);

	    int w = l.getPreferredSize().width;
	    /*
	      if (ncomps+1 < size) {
	      // +2 FlowLayout gap
	      w += ellipsis.getPreferredSize().width + 2; 
	      }
	    */
	    if ((renderComponent.getPreferredSize().width+w) < maxW) {
		renderComponent.add(l);
	    }
	    else if ((h + l.getPreferredSize().height) <= maxH) { 
		// new row
		//h += l.getPreferredSize().height+1;
		ArrayList<Component> row = new ArrayList<Component>();
		for (Component c : renderComponent.getComponents()) {
		    row.add(c);
		}
		rows.add(row);
		h = rows.size() * (l.getPreferredSize().height+1);
		renderComponent.removeAll();
		renderComponent.add(l);
	    }
	    else {
		break;
	    }
	}

	if (renderComponent.getComponentCount() > 0) {
	    ArrayList<Component> row = new ArrayList<Component>();
	    int rh = 0;
	    for (Component c : renderComponent.getComponents()) {
		Dimension d = c.getPreferredSize();
		if (d.height > rh) {
		    rh = d.height;
		}
		row.add(c);
	    }

	    if (rh+h < maxH) {
		rows.add(row);
	    }
	}

	renderComponent.removeAll();
	for (ArrayList<Component> r : rows) {
	    for (Component c : r) {
		renderComponent.add(c);
	    }
	}

	int ncomps = renderComponent.getComponentCount();
	if (ncomps < size) {
	    if (ncomps > 0) {
		renderComponent.remove(ncomps-1);
	    }
	    ellipsis.setText
                ("<html>...<font color=\"red\" size=\"-3\">"+(size-ncomps+1));
	    ellipsis.setOutlineVisible(outlineVisible);
	    renderComponent.add(ellipsis);
	}
    }

    public String getField () { return field; }
    public void setField (String field) { this.field = field; }

    // should be overriden to provide custom value
    protected String toString (Value v) {
	return v.getValue() != null ? v.getValue().toString() : "";
    }

    // subclass must override this method to convert value from a string
    protected Object toValue (String value) {
	return value;
    }

    public Object getCellEditorValue() {
	return value;
    }
    public boolean isCellEditable (EventObject e) {
	return true;
    }
    	
    public boolean shouldSelectCell (EventObject e) { 
	return true; 
    }

    public boolean startCellEditing (EventObject e) {
	return true;
    }

    /*
    public boolean stopCellEditing () { 
	//System.out.println(getClass() + ": cell stop editing");
	// update the values...
	if (entity != null) {
	    entity.set(getField (), values.toArray(new Object[0]));
	}
	return super.stopCellEditing();
    }
	
    public void cancelCellEditing () { 
	super.cancelCellEditing(); 
    }
    */

    public void actionPerformed (ActionEvent e) {
    }
}
