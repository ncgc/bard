// $Id: GridCellEditor.java 3286 2009-09-28 16:35:59Z nguyenda $ 

package bard.ui.common;

import java.awt.Component;
import javax.swing.CellEditor;

public interface GridCellEditor extends CellEditor {
    public Component getGridCellEditorComponent 
	(Grid g, Object value, boolean selected, int cell);
}
