package bard.ui.common;

import bard.core.adapter.EntityAdapter;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.util.Enumeration;

/**
 * A tree view to summarize annotations.
 *
 * @author Rajarshi Guha
 */
public class AnnotationTree<T> extends JTree implements TreeSelectionListener {
    AnnotationTreeModel model = new AnnotationTreeModel();

    public AnnotationTree() {
        setModel(model);
        setRootVisible(false);
        setCellRenderer(new AnnotationTreeCellRenderer());
        /*
                 getSelectionModel().setSelectionMode
                 (TreeSelectionModel.SINGLE_TREE_SELECTION);
                 addTreeSelectionListener (this);
        */
    }

    public void setEntity(EntityAdapter entityAdapter) {
        model.setEntity(entityAdapter);
        DefaultMutableTreeNode root =
                (DefaultMutableTreeNode) model.getRoot();
        for (Enumeration en = root.depthFirstEnumeration();
             en.hasMoreElements(); ) {
            DefaultMutableTreeNode leaf =
                    (DefaultMutableTreeNode) en.nextElement();
            TreePath tp = new TreePath(leaf.getPath());
            expandPath(tp);
        }
    }

    public void clear() {
        clearSelection();
        model.clear();
    }

    public void valueChanged(TreeSelectionEvent e) {
        if (model.getEntity() == null) {
            return;
        }
        TreePath path = getSelectionPath();
        if (path != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
        }
    }
}