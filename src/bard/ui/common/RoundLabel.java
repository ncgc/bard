// $Id: RoundLabel.java 3486 2009-10-29 15:48:13Z nguyenda $

package bard.ui.common;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicLabelUI;

import bard.ui.explodingpixels.macwidgets.MacFontUtils;

public class RoundLabel extends JLabel {

    private static final Color DEFAULT_COLOR_OUTLINE = 
	new Color (100, 150, 250);
    private static final Color DEFAULT_COLOR_FILL = new Color (241, 245, 250);

    private int padx = 12, pady = 0;
    private Paint outlinePaint = DEFAULT_COLOR_OUTLINE;
    private Paint fillPaint = DEFAULT_COLOR_FILL;
    private RoundRectangle2D.Double rect = new RoundRectangle2D.Double();
    private float strokeWidth = 1.f;
    private BasicStroke stroke = new BasicStroke (strokeWidth);
    private boolean outline = true, fill = false;
    
    public RoundLabel () { 
	this ("");
    }
    public RoundLabel (String text) { 
	this (text, LEADING); 	
    }

    public RoundLabel (String text, int horizontalAlignment) {
	super (text, null, horizontalAlignment);
	setBorder (BorderFactory.createEmptyBorder(pady, padx, pady, padx));
	setOpaque (false);	
    }

    public RoundLabel (String text, Color background) {
	this (text, LEADING);
	setBackground (background);
    }

    public void setIcon (Icon icon) {
	/*
	throw new IllegalArgumentException 
	    ("This label doesn't support icon!");
	*/
    }

    public void setOutlineVisible (boolean outline) {
	this.outline = outline;
    }
    public boolean isOutlineVisible () { return outline; }

    public void setFillVisible (boolean fill) {
	this.fill = fill;
    }
    public boolean isFillVisible () { return fill; }

    public void setFillPaint (Paint fillPaint) {
	if (fillPaint == null) {
	    fillPaint = DEFAULT_COLOR_FILL;
	}
	this.fillPaint = fillPaint;
    }
    public Paint getFillPaint () { return fillPaint; }
    public void setOutlinePaint (Paint outlinePaint) {
	if (outlinePaint == null) {
	    outlinePaint = DEFAULT_COLOR_OUTLINE;
	}
	this.outlinePaint = outlinePaint;
    }
    public Paint getOutlinePaint () { return outlinePaint; }
    
    @Override
    protected void paintComponent (Graphics g) {
	Rectangle r = getBounds ();
	Graphics2D g2 = (Graphics2D)g; //img.createGraphics();

	g2.setComposite(AlphaComposite.Src);
	g2.setRenderingHint(RenderingHints.KEY_RENDERING, 
			    RenderingHints.VALUE_RENDER_QUALITY);
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			    RenderingHints.VALUE_ANTIALIAS_ON);

	int arc = getRoundedDiameter (r.height);
	rect.setRoundRect(0/*strokeWidth/2.*/, 0/*strokeWidth/2.*/, 
			  r.width-strokeWidth, 
			  r.height-strokeWidth, (double)arc, (double)arc);

	if (isFillVisible ()) {
	    g2.setPaint(fillPaint);
	    g2.setComposite(AlphaComposite.SrcAtop);
	    g2.fill(rect);
	}

	super.paintComponent(g2);

	if (isOutlineVisible ()) {
	    g2.setStroke(stroke);
	    g2.setPaint(outlinePaint);
	    g2.draw(rect);
	}
    }

    private int getRoundedDiameter (int controlHeight) {
	int roundedDiameter = (int) (controlHeight * .98 + 0.5);
	return roundedDiameter - roundedDiameter % 2;
    }

    public static void main (final String[] argv) throws Exception {
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    JFrame f = new JFrame ("Test of RoundLabel");
		    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    JPanel panel = new JPanel 
			(new FlowLayout (FlowLayout.LEFT, 2, 1));
		    panel.setBackground(Color.white);
		    f.getContentPane().add(panel);
		    for (String s : argv) {
			panel.add(new RoundLabel (s));
		    }
		    f.pack();
		    f.setSize(400, 400);
		    f.setVisible(true);
		}
	    });
    }
}
