
package bard.ui.common;

public interface EntityTableColumnFactory {
    public EntityTableColumn[] createColumns 
	(int modelIndex, EntityTableModel model);
}
