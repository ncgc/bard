// $Id: EntityTable.java 3487 2009-10-29 15:48:57Z nguyenda $

package bard.ui.common;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.swing.DefaultListSelectionModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXImagePanel;

import bard.core.Entity;
import bard.ui.explodingpixels.macwidgets.plaf.ITunesTableUI;
import bard.ui.explodingpixels.widgets.TableUtils;
import bard.ui.explodingpixels.widgets.TableUtils.SortDirection;
import bard.ui.icons.IconFactory;
import ca.odell.glazedlists.EventList;

public class EntityTable extends JTable
    implements MouseMotionListener, 
	       MouseListener,
	       ListSelectionListener,
	       FocusListener,
	       TableUtils.SortDelegate {

    static private final Logger logger = 
	Logger.getLogger(EntityTable.class.getName());


    private Map<Class, EntityTableColumnFactory> columnFactory; 
    
    private static ListSelectionModel dummySelectionModel = new DefaultListSelectionModel();

    private javax.swing.Timer timer = new javax.swing.Timer
	(10000, new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    cancelEditing ();
		}
	    });

    static class StatusHeaderCellRenderer 
	extends JXImagePanel implements TableCellRenderer {
	StatusHeaderCellRenderer () {
	    setOpaque (false);
	    setImage (IconFactory.Status.None.getIcon().getImage());
	}

	public Component getTableCellRendererComponent
	    (JTable table, Object value, boolean isSelected, 
	     boolean hasFocus, int row, int column) {
	    return this;
	}
    }

    static class StatusCellRenderer 
	extends JLabel implements TableCellRenderer {
	Icon icon = null;

	StatusCellRenderer () {
	    setOpaque (false);
	    setHorizontalAlignment (SwingConstants.CENTER);
	}

	StatusCellRenderer (Icon icon) {
	    super (icon, SwingConstants.CENTER);
	    setOpaque (false);
	    this.icon = icon;
	}

	public Component getTableCellRendererComponent
	    (JTable table, Object value, boolean isSelected, 
	     boolean hasFocus, int row, int column) {

	    if (isSelected) {
		setBackground(table.getSelectionBackground());
		setForeground(table.getSelectionForeground());
	    }
	    else {
		setForeground(table.getForeground());
		setBackground(table.getBackground());
	    }

	    if (value == null) {
		if (icon == null) {
		    setIcon (null);
		}
	    }
	    else {
		EntityTableModel.RowStatus status = 
		    (EntityTableModel.RowStatus)value;
		switch (status) {
		case ROW_OK:
		    /*
		    setIcon (IconFactory.getInstance()
			     .getImageIcon(IconFactory.ICON_AVAILABLE));
		    */
		    setIcon (null);
		    break;
		case ROW_MODIFIED:
		    setIcon (IconFactory.Status.Partial.getIcon());
		    break;
		case ROW_NOEDIT:
		    setIcon (IconFactory.Status.None.getIcon());
		    break;
		case ROW_UNKNOWN:
		    setIcon (IconFactory.Status.Unavailable.getIcon());
		    break;
		}
	    } 
	    return this;
	}
    }

    class TableTransferHandler extends TransferHandler {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        private int[] rows = null;
        
        private int addIndex = -1; //Location where items were added

        private int addCount = 0; //Number of items added.

        protected String exportString(JComponent c) {
            JTable table = (JTable) c;
            rows = table.getSelectedRows();
            int colCount = table.getColumnCount();

            StringBuffer buff = new StringBuffer();

            for (int i = 0; i < rows.length; i++) {
              for (int j = 0; j < colCount; j++) {
                Object val = table.getValueAt(rows[i], j);
                
                if (val instanceof Entity && table.getColumnModel().getColumn(j) instanceof EntityTableColumn) {                    
                    TableCellRenderer tcr = table.getColumnModel().getColumn(j).getCellRenderer();
                    Component comp = tcr.getTableCellRendererComponent(table, val, false, false, rows[i], j);
                    if (comp instanceof JLabel)
                        buff.append(((JLabel)comp).getText());
                    else if (comp instanceof javax.swing.JPanel)
                        for (Component comp2: ((javax.swing.JPanel)comp).getComponents()) {
                            if (comp2 instanceof JLabel)
                                buff.append(((JLabel)comp2).getText());
                            else
                                buff.append(comp2.toString());
                        }
                    buff.append(",");
                } else {
                    buff.append(val == null ? "" : val.toString());
                    if (j != colCount - 1) {
                        buff.append(",");
                    }
                }
              }
              if (i != rows.length - 1) {
                buff.append("\n");
              }
            }

            return buff.toString();            
        }

        protected Transferable createTransferable(JComponent c) {
          return new StringSelection(exportString(c));
        }

        public int getSourceActions(JComponent c) {
          return COPY_OR_MOVE;
        }

        public boolean importData(JComponent c, Transferable t) {
          if (canImport(c, t.getTransferDataFlavors())) {
            try {
              String str = (String) t
                  .getTransferData(DataFlavor.stringFlavor);
              importString(c, str);
              return true;
            } catch (UnsupportedFlavorException ufe) {
            } catch (IOException ioe) {
            }
          }

          return false;
        }

        protected void exportDone(JComponent c, Transferable data, int action) {
          cleanup(c, action == MOVE);
        }

        public boolean canImport(JComponent c, DataFlavor[] flavors) {
          for (int i = 0; i < flavors.length; i++) {
            if (DataFlavor.stringFlavor.equals(flavors[i])) {
              return true;
            }
          }
          return false;
        }

        protected void importString(JComponent c, String str) {
            JTable target = (JTable) c;
            DefaultTableModel model = (DefaultTableModel) target.getModel();
            int index = target.getSelectedRow();

            //Prevent the user from dropping data back on itself.
            //For example, if the user is moving rows #4,#5,#6 and #7 and
            //attempts to insert the rows after row #5, this would
            //be problematic when removing the original rows.
            //So this is not allowed.
            if (rows != null && index >= rows[0] - 1
                && index <= rows[rows.length - 1]) {
              rows = null;
              return;
            }

            int max = model.getRowCount();
            if (index < 0) {
              index = max;
            } else {
              index++;
              if (index > max) {
                index = max;
              }
            }
            addIndex = index;
            String[] values = str.split("\n");
            addCount = values.length;
            int colCount = target.getColumnCount();
            for (int i = 0; i < values.length && i < colCount; i++) {
              model.insertRow(index++, values[i].split(","));
            }
          }

        protected void cleanup(JComponent c, boolean remove) {
            JTable source = (JTable) c;
            if (remove && rows != null) {
              DefaultTableModel model = (DefaultTableModel) source.getModel();

              //If we are moving items around in the same table, we
              //need to adjust the rows accordingly, since those
              //after the insertion point have moved.
              if (addCount > 0) {
                for (int i = 0; i < rows.length; i++) {
                  if (rows[i] > addIndex) {
                    rows[i] += addCount;
                  }
                }
              }
              for (int i = rows.length - 1; i >= 0; i--) {
                model.removeRow(rows[i]);
              }
            }
            rows = null;
            addCount = 0;
            addIndex = -1;
          }

    }
    
    public EntityTable () {
	this (new EntityTableModel ());
    }

    public EntityTable (EntityTableModel model) {
	super (model);
	init ();
    }

    protected void init () {
	setUI (new ITunesTableUI ());

	setAutoCreateColumnsFromModel (true);
	addMouseListener (this);
	addMouseMotionListener (this);
	addFocusListener (this);

	setDefaultRenderer (EntityTableModel.RowStatus.class, 
			    new StatusCellRenderer ());
	TableUtils.makeSortable(this, this);
	EntityTableHeaderDecorator.decorate(getTableHeader ());
	this.setTransferHandler(new TableTransferHandler());
    }

    @Override
    public void setModel (TableModel model) {
	if (!(model instanceof EntityTableModel)) {
	    throw new IllegalArgumentException
		("Table model not an instance of EntityTableModel!");
	}
	cancelEditing ();
	TableModel old = getModel ();
	if (old instanceof EntityTableModel) {
	    ((EntityTableModel)old).removeTableModelListener(this);
	}
	((EntityTableModel)model).addTableModelListener(this);
	super.setModel(model);
	adjustHeaderSize ();
    }

    @Override
    public void tableChanged (TableModelEvent e) {
	if (//e.getType() == TableModelEvent.INSERT ||
	    e.getType() == TableModelEvent.DELETE) {
	    getSelectionModel().clearSelection();
	    logger.severe("table model event --- clear selection --- should update selection instead");
	}
	cancelEditing ();
	
	// Anticipate table model events that will clear the selection state in super(); decouple Content's listSelectionModel
	if (e == null || e.getFirstRow() == TableModelEvent.HEADER_ROW) {
	    ListSelectionModel lsm = getSelectionModel();
	    setSelectionModel(dummySelectionModel);
	    super.tableChanged(e);
	    setSelectionModel(lsm);
	} else {
	    super.tableChanged(e);
	}
    }

    public void adjustHeaderSize () {
	JTableHeader header = getTableHeader ();
	if (header != null) {
	    header.setPreferredSize
		(new Dimension (getColumnModel().getTotalColumnWidth(), 30));
	}
    }

    public Entity[] getSelectedEntities () {
	int[] rows = getSelectedRows ();
	Entity[] selections = null;
	if (rows.length > 0) {
	    EventList data = ((EntityTableModel)getModel()).getAnchorColumn();
	    selections = new Entity[rows.length];
	    for (int r = 0; r < rows.length; ++r) {
		selections[r] = (Entity)data.get(rows[r]);
	    }
	}
	return selections;
    }

    public void setSelectedEntities (Entity... selections) {
	ListSelectionModel model = getSelectionModel ();

	model.clearSelection();
	EventList data = ((EntityTableModel)getModel()).getAnchorColumn();
	for (Entity e : selections) {
	    int pos = data.indexOf(e);
	    if (pos >= 0) {
		model.addSelectionInterval(pos, pos);
	    }
	}
    }

    @Override
    public void createDefaultColumnsFromModel () {
	// preserve the existing columns..
	EntityTableModel m = (EntityTableModel) getModel ();

	if (m == null) {
	    return;
	}

	Set<Integer> keep = new TreeSet<Integer>();
	ArrayList<TableColumn> remove = new ArrayList<TableColumn>();

	for (Enumeration<TableColumn> en = getColumnModel().getColumns();
	     en.hasMoreElements(); ) {
	    EntityTableColumn column = (EntityTableColumn)en.nextElement();
	    if (column.getTableModel() == m && column.isVisible()) {
		keep.add(column.getModelIndex());
	    }
	    else {
		remove.add(column);
	    }
	}

	for (TableColumn column : remove) {
	    getColumnModel().removeColumn(column);
	}

	for (int i = 0; i < m.getColumnCount(); ++i) {
	    if (!keep.contains(i)) {
		Class clazz = m.getColumnClass(i);
		EntityTableColumnFactory fac = getColumnFactory()
		    .get(clazz);

		if (fac != null) {
		    EntityTableColumn[] cols = fac.createColumns(i, m);
		    for (EntityTableColumn c : cols) {
			addColumn (c);
		    }
		}
		else {
		    EntityTableColumn column = new EntityTableColumn (i, m);
		    if (clazz.isAssignableFrom
			(EntityTableModel.RowStatus.class)) {
			column.setResizable(false);
			column.setPreferredWidth(20);
			column.setHeaderValue
			    (IconFactory.Status.None.getIcon());
		    }
		    addColumn (column);
		}
	    }
	}
    }

    Map<Class, EntityTableColumnFactory> getColumnFactory () {
    	if (columnFactory == null) {
	    columnFactory = new HashMap<Class, EntityTableColumnFactory>();
	}
	return columnFactory;
    }

    public void setEntityColumnFactory 
	(Class<?> clazz, EntityTableColumnFactory factory) {
	getColumnFactory().put(clazz, factory);
    }
    public EntityTableColumnFactory getEntityColumnFactory (Class clazz) {
	return getColumnFactory().get(clazz);
    }

    protected void cancelEditing () {
	TableCellEditor editor = getCellEditor ();
	if (editor != null) {
	    editor.cancelCellEditing();
	    setCellEditor (null);
	}
    }

    @Override
    public void setRowHeight (int height) {
	cancelEditing ();
	super.setRowHeight(height);
    }

    @Override
    public void columnMoved (TableColumnModelEvent e) {
	super.columnMoved(e);
	cancelEditing ();
    }

    @Override
    public void columnRemoved (TableColumnModelEvent e) {
	super.columnRemoved(e);
	cancelEditing ();
    }

    @Override
    public void columnMarginChanged (ChangeEvent e) {
	super.columnMarginChanged(e);
	cancelEditing ();
	adjustHeaderSize ();
    }

    public void focusGained (FocusEvent e) {
    }

    public void focusLost (FocusEvent e) {
	//cancelEditing ();
    }

    public void mouseDragged (MouseEvent e) {
	// ideally all rows should also be resized, but it's not
	//  clear how can we determine which row is currently being
	//  adjusted?
	getTableHeader().repaint();
	cancelEditing ();
    }

    public void mouseMoved (MouseEvent e) {
	editCell (e);
    }

    void editCell (MouseEvent e) {
	Point pt = e.getPoint();
	int col = columnAtPoint (pt);
	int row = rowAtPoint (pt);
        Object value = getValueAt (row, col);
	if (col < 0 || row < 0 || value == null) {
            //setToolTipText (null);
	    cancelEditing ();
	}
	else {
            //setToolTipText (value.toString());
	    //requestFocusInWindow ();
	    //timer.restart();
	    editCellAt (row, col, e);
	}
    }

    public void mouseClicked (MouseEvent e) {
	//logger.info("## "+e);
	if (e.getModifiersEx() == MouseEvent.BUTTON1_DOWN_MASK) {
	    //editCell (e);
	}
	else {
	    cancelEditing ();
	}
    }
    public void mouseEntered (MouseEvent e) {}
    public void mouseExited (MouseEvent e) { 
	//logger.info("Component "+e.getSource());
    }
    public void mousePressed (MouseEvent e) { }
    public void mouseReleased (MouseEvent e) {}

    public void addSortColumn (int column, SortDirection dir) {
	TableColumn tc = getColumnModel().getColumn(column);
	Comparator comp = null;
	try {
	    Method m = tc.getClass().getMethod("getComparator", (Class<?>[])null);
	    comp = (Comparator)m.invoke(tc, (Object[])null);
	} catch (Exception ex) {}

	logger.info("Sorting column "+column+" " 
		    +tc.getHeaderValue() + " "+dir);
	((EntityTableModel)getModel()).addSortColumn
	    (tc.getModelIndex(), dir, comp);
	repaint ();
    }

    public void clearSortColumns () { 
	((EntityTableModel)getModel()).clearSortColumns();
    }

    public void sort (final int column, final SortDirection dir) {
	cancelEditing ();
	// column is view (not model as in  original macwidgets)
	if (column < 0) {
	    clearSortColumns ();
	}
	else {
	    SwingUtilities.invokeLater(new Runnable () {
		    public void run () {
			addSortColumn (column, dir);
		    }
		});
	}
    }
}
