// $Id$

package bard.ui.common;

import java.util.Comparator;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeEvent;

import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import bard.core.Entity;
import bard.core.Value;

public class EntityTableColumn extends TableColumn {
    public static final String PROP_VISIBLE = "visible";

    private boolean visible = true;
    private PropertyChangeSupport props = new PropertyChangeSupport (this);
    private EntityTableModel tableModel;
    private Comparator comparator;

    public EntityTableColumn () {
    }

    public EntityTableColumn (int columnIndex, EntityTableModel tableModel) {
	this (null, columnIndex, tableModel);
    }

    public EntityTableColumn (Object headerValue, 
			      int columnIndex, 
			      EntityTableModel tableModel) {
	super (columnIndex);
	if (headerValue == null) {
	    headerValue = tableModel.getColumnName(columnIndex);
	}
	setHeaderValue (headerValue);
	this.tableModel = tableModel;
	
	setComparator(new Comparator() {
	    public int compare(Object o1, Object o2) {
		if (o2 == null) {
		    if (o1 == null)
			return 0;
		    return 1;
		} else if (o1 == null) {
		    return -1;
		}
		if (o1 instanceof Entity && o2 instanceof Entity) {
		    TableCellRenderer tcr = EntityTableColumn.this.getCellRenderer();
		    if (tcr instanceof TokenCellEditorRenderer && tcr != null) {
			StringBuffer s1 = new StringBuffer();
			StringBuffer s2 = new StringBuffer();
			for (Value v: ((TokenCellEditorRenderer)tcr).parseValue(o1))
			    s1.append(v.getValue().toString()+"|");
			for (Value v: ((TokenCellEditorRenderer)tcr).parseValue(o2))
			    s2.append(v.getValue().toString()+"|");
			return s1.toString().compareTo(s2.toString());
		    }
		}
		return o1.toString().compareTo(o2.toString());
	    }
	});
    }

    public void setTableModel (EntityTableModel tableModel) {
	this.tableModel = tableModel;
    }
    public EntityTableModel getTableModel () { return tableModel; }

    public void setEntityField (String field) {
	setIdentifier (field);
    }
    public Object getEntityField () {
	return getIdentifier ();
    }

    public void setComparator (Comparator comparator) {
	this.comparator = comparator;
    }
    public Comparator getComparator () { return comparator; }
    
    public void setVisible (boolean visible) {
	boolean old = this.visible;
	this.visible = visible;
	firePropertyChange (PROP_VISIBLE, old, visible);
    }
    public boolean isVisible () { return visible; }
    public boolean isSortable () { return comparator != null; }

    protected void firePropertyChange 
	(String prop, Object oldValue, Object newValue) {
	props.firePropertyChange(prop, oldValue, newValue);
    }

    public void addPropertyChangeListener (PropertyChangeListener l) {
	super.addPropertyChangeListener(l);
	props.addPropertyChangeListener(l);
    }
    public void addPropertyChangeListener 
	(String prop, PropertyChangeListener l) {
	props.addPropertyChangeListener(prop, l);
    }
    public void removePropertyChangeListener (PropertyChangeListener l) {
	props.removePropertyChangeListener(l);
    }
    public void removePropertyChangeListener 
	(String prop, PropertyChangeListener l) {
	props.removePropertyChangeListener(prop, l);
    }
}
