// $Id$

package bard.ui.common;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;

import com.jidesoft.swing.SimpleScrollPane;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.basic.BasicGraphicsUtils;

import bard.ui.explodingpixels.macwidgets.HudPanel;
import bard.ui.explodingpixels.macwidgets.ITunesNavigationHeader;

import bard.ui.util.ColorUtil;
import bard.ui.util.UIUtil;

public class XTabPane extends JPanel implements ActionListener {
    private static final int BORDER_WIDTH = 1;
    private static final int TOP_AND_BOTTOM_MARGIN = 2;
    private static final int LEFT_AND_RIGHT_MARGIN = 13;
    private static final float FONT_SIZE = 11.f;

    // some sensible background color...
    private static final Color BACKGROUND = Color.white;
    private static final Color FOREGROUND = ColorUtil.ForegroundXRef;

    private static final Color TOP_COLOR = new Color(0xbbbbbb);
    private static final Color BOTTOM_COLOR = new Color(0x969696);
    private static final Color BORDER_COLOR = new Color(190, 190, 190);

    private static final String CONTENT_COMP = "ContentComponent";


    static class TabButtonUI extends BasicButtonUI {
	boolean active;
	RoundRectangle2D rect = new RoundRectangle2D.Double();

	TabButtonUI () {
	}

	@Override
	protected void installDefaults (AbstractButton b) {
	    super.installDefaults(b);
	    b.setOpaque(false);
	    b.setHorizontalTextPosition(AbstractButton.CENTER);
	    b.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
	    b.setBorder(BorderFactory.createEmptyBorder
			(TOP_AND_BOTTOM_MARGIN, LEFT_AND_RIGHT_MARGIN,
			 TOP_AND_BOTTOM_MARGIN - BORDER_WIDTH, 
			 LEFT_AND_RIGHT_MARGIN));
	    Font font = UIManager.getFont("Button.font");
	    b.setFont(font.deriveFont(Font.BOLD, FONT_SIZE));
	    b.setForeground(FOREGROUND);
	}

        @Override
	public void paint(Graphics g, JComponent c) {
	    AbstractButton btn = (AbstractButton) c;
	    Graphics2D g2 = (Graphics2D) g;

	    Color foreground = btn.getForeground();
	    if (!c.isEnabled()) {
		g2.setComposite(AlphaComposite.getInstance
				(AlphaComposite.SRC_OVER, 0.5f));
	    }

	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
				RenderingHints.VALUE_ANTIALIAS_ON);

	    ButtonModel model = btn.getModel();
	    active = model.isPressed()
		|| model.isRollover() || model.isSelected();

	    if (active) {
		int width = c.getWidth() - 2*BORDER_WIDTH;
		int height = c.getHeight() - 2*BORDER_WIDTH;
		int arc = getRoundedDiameter (height);

		rect.setRoundRect(BORDER_WIDTH, BORDER_WIDTH, 
				  width, height, arc, arc);

		//g2.setPaint(ColorUtil.Default);
		GradientPaint paint = new GradientPaint
		    (0, 1, TOP_COLOR, 0, height, BOTTOM_COLOR);
		// install the paint and fill a rectangle with it.
		g2.setPaint(paint);
		g2.fill(rect);

		rect.setRoundRect(BORDER_WIDTH, BORDER_WIDTH, 
				  width-1, height-1, arc, arc);
		g2.setColor(ColorUtil.DarkShadow);
		g2.draw(rect);
	    }

	    super.paint(g, c);
	}

	private int getRoundedDiameter (int controlHeight) {
            int roundedDiameter = (int) (controlHeight * .98 + 0.5);
            return roundedDiameter - roundedDiameter % 2;
        }

	@Override
	protected void paintText
	    (Graphics g, AbstractButton button, 
	     Rectangle textRect, String text) {
	    FontMetrics fontMetrics = g.getFontMetrics(button.getFont());
	    int mnemonicIndex = button.getDisplayedMnemonicIndex();
	    
	    g.setColor(Color.white);
	    BasicGraphicsUtils.drawStringUnderlineCharAt
		(g, text, mnemonicIndex,
		 textRect.x + getTextShiftOffset(),
		 textRect.y + fontMetrics.getAscent() + getTextShiftOffset());
	}
    }

    private JComponent xpane;
    private JPanel content;
    private CardLayout layout = new CardLayout ();
    private ButtonGroup group = new ButtonGroup ();

    private java.util.List<ChangeListener> listeners = 
	new ArrayList<ChangeListener>();
    
    public XTabPane () {
	init ();
    }

    public void addChangeListener (ChangeListener l) {
	listeners.add(l);
    }
    public void removeChangeListener (ChangeListener l) {
	listeners.remove(l);
    }
    public ChangeListener[] getChangeListeners () { 
	return listeners.toArray(new ChangeListener[0]);
    }

    protected void fireStateChanged () {
	for (ChangeListener l : listeners) {
	    l.stateChanged(new ChangeEvent (this));
	}
    }

    protected void init () {
	setLayout (new BorderLayout ());
	setBorder (null);

	xpane = new ITunesNavigationHeader ();
	xpane.setLayout(new FlowLayout (FlowLayout.LEADING));

	SimpleScrollPane sp = new SimpleScrollPane (xpane);
	sp.setBackground(BORDER_COLOR);
	sp.getViewport().setPreferredSize(new Dimension (-1, 30));

	//sp.setBorder(null);
	sp.setBorder(BorderFactory.createMatteBorder
		     (0, 0, 1, 0, BORDER_COLOR));

	add (sp, BorderLayout.NORTH);
	add (content = new JPanel (layout));
    }

    protected AbstractButton createTab (String name, Component c) {
	JToggleButton btn = new JToggleButton (name);
	btn.setUI(new TabButtonUI ());
	btn.addActionListener(this);
	btn.setRolloverEnabled(true);
	btn.putClientProperty(CONTENT_COMP, c);
	group.add(btn);
	btn.setSelected(true);
	content.add(c, name);
	layout.show(content, name);
	return btn;
    }

    public void addTab (String name, Component comp) {
	for (Component c : xpane.getComponents()) {
	    AbstractButton ab = (JToggleButton)c;
	    if (name.equals(ab.getText())) {
		// override content
		ab.putClientProperty(CONTENT_COMP, comp);
		ab.doClick(10);
		return;
	    }
	}

	// add a new tab
	AbstractButton ab = createTab (name, comp);
	xpane.add(ab);
    }

    public Component getComponentAt (int index) {
	AbstractButton ab = (AbstractButton)xpane.getComponent(index);
	return (Component)ab.getClientProperty(CONTENT_COMP);
    }

    public int getSelectedIndex () {
	for (int i = 0; i < xpane.getComponentCount(); ++i) {
	    AbstractButton ab = (AbstractButton)xpane.getComponent(i);
	    if (ab.isSelected()) {
		return i;
	    }
	}
	return -1;
    }

    public Component getTabComponentAt (int index) {
	return xpane.getComponent(index);
    }

    public String getTitleAt (int index) {
	AbstractButton ab = (AbstractButton)xpane.getComponent(index);
	return ab.getText();
    }
    public void setTitleAt (int index, String title) {
	AbstractButton ab = (AbstractButton)xpane.getComponent(index);
	ab.setText(title);
    }

    public void setSelectedIndex (int index) {
	AbstractButton ab = (AbstractButton)xpane.getComponent(index);
	ab.doClick(10);
    }

    public void actionPerformed (ActionEvent e) {
	AbstractButton ab = (AbstractButton)e.getSource();
	Component c = (Component)ab.getClientProperty(CONTENT_COMP);
	layout.show(content, ab.getText());
	fireStateChanged ();
    }

    public int getTabCount () { return xpane.getComponentCount(); }

    public static void main (String[] argv) throws Exception {
	EventQueue.invokeLater(new Runnable () {
		public void run () {
		    XTabPane tab = new XTabPane ();
		    for (int i = 0; i < 10; ++i) {
			tab.addTab("Tab "+i, new JLabel ("Content of tab "+i));
		    }
		    tab.setSelectedIndex(0);

		    JFrame f = new JFrame ("XTabPane test");
		    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    f.getContentPane().add(tab);
		    f.setSize(400, 400);
		    f.pack();
		    f.setVisible(true);
		}
	    });
    }
}
