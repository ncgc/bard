// $Id$
package bard.ui.common;

import chemaxon.util.BrowserLauncher;
import org.jdesktop.swingx.JXHyperlink;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.IOException;

import bard.core.Entity;
import bard.core.Value;

public class HyperlinkTextCellRenderer
        extends JPanel implements TableCellRenderer {

    private JTextArea textArea;
    private JXHyperlink hyperLink;
    private String textField, linkField;

    public HyperlinkTextCellRenderer() {
        this (null, null);
    }

    public HyperlinkTextCellRenderer (String text, String target) {
        this.textField = text;
        this.linkField = target;

        setOpaque(false);
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(1, 0, 1, 0));

        hyperLink = new JXHyperlink();
        add(hyperLink);

//        textArea = new JTextArea();
//        textArea.setOpaque(false);
//        textArea.setLineWrap(true);
//        textArea.setWrapStyleWord(true);
//        add(textArea);
    }


    public Component getTableCellRendererComponent
            (JTable table, Object value, boolean selected,
             boolean focus, int row, int column) {

        if (selected) {
            setOpaque(true);
            setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } else {
            setOpaque(false);
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        if (value == null) {
        } 
        else if (value instanceof Entity) {
            if (textField != null && linkField != null) {
                Value v = ((Entity)value).getValue(textField);
                String text = v != null ? (String) v.getValue() : null;

                v = ((Entity)value).getValue(linkField);
                String link = v != null ? (String) v.getValue() : null;

                hyperLink.setText(text);
                hyperLink.setAction(new LinkAction(link));
            }
        }
        return this;
    }

    class LinkAction extends AbstractAction {
        String link;

        LinkAction(String link) {
            super(link);
            this.link = link;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (link != null) try {
                BrowserLauncher.openURL(link);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
