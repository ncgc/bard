// $Id: EntityTableDescriptor.java 3487 2009-10-29 15:48:57Z nguyenda $

package bard.ui.common;

import java.util.*;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;

import bard.core.Entity;
import bard.core.Value;

public class EntityTableDescriptor 
    implements TableFormat<Entity>, WritableTableFormat<Entity> {

    public static final String ADDCOLUMN = "addColumn";
    public static final String DELCOLUMN = "delColumn";
    public static final String MODCOLUMN = "modColumn";
    public static final String ALLCOLUMN = "allColumn";

    private ArrayList<String> columns = new ArrayList<String>();
    private Map<String, EntityCellEditorRenderer> cellEditorRenderer = 
	new HashMap<String, EntityCellEditorRenderer>();
    private PropertyChangeSupport props = new PropertyChangeSupport (this);

    public EntityTableDescriptor () {
    }

    public EntityTableDescriptor (String... columns) {
	this (Arrays.asList(columns));
    }

    public EntityTableDescriptor (Collection<String> columns) {
	setColumns (columns);
    }

    public void setColumns (Collection<String> columns) {
	this.columns.clear();
	cellEditorRenderer.clear();
	for (String f : columns) {
	    if (!cellEditorRenderer.containsKey(f)) {
		this.columns.add(f);
		EntityCellEditorRenderer cell = 
		    new EntityCellEditorRenderer (f);
		cellEditorRenderer.put(f, cell);
	    }
	}
	props.firePropertyChange(ALLCOLUMN, null, columns.size()-1);
    }

    public void addColumn (String column, EntityCellEditorRenderer cell) {
	if (!cellEditorRenderer.containsKey(column)) {
	    int colidx = columns.size();
	    columns.add(column);
	    if (cell == null) {
		cell = new EntityCellEditorRenderer (column);
	    }
	    cellEditorRenderer.put(column, cell);
	    props.firePropertyChange(ADDCOLUMN, colidx, colidx);
	}
	else if (cell != null) {
	    int colidx = columns.indexOf(column);
	    // update new cell editor/renderer
	    cellEditorRenderer.put(column, cell);
	    props.firePropertyChange(MODCOLUMN, colidx, colidx);
	}
    }

    public void setColumnEditable (int column, boolean editable) {
	String field = columns.get(column);
	EntityCellEditorRenderer editor = cellEditorRenderer.get(field);
	if (editor != null) {
	    editor.setEditable(editable);
	}
	else {
	    throw new IllegalArgumentException 
		("Invalid column specified: " + column);
	}
    }
    public boolean isColumnEditable (int column) {
	String field = columns.get(column);

	EntityCellEditorRenderer editor = cellEditorRenderer.get(field);
	if (editor != null) {
	    return editor.isEditable();
	}
	throw new IllegalArgumentException 
	    ("Invalid column specified: " + column);
    }

    public TableCellEditor getColumnCellEditor (int column) {
	return getColumnCellEditorRenderer (column);
    }
    public TableCellRenderer getColumnCellRenderer (int column) {
	return getColumnCellEditorRenderer (column);
    }

    public EntityCellEditorRenderer getColumnCellEditorRenderer (int column) {
	return cellEditorRenderer.get(columns.get(column));
    }

    public void addPropertyChangeListener (PropertyChangeListener l) {
	props.addPropertyChangeListener(l);
    }
    public void addPropertyChangeListener 
	(String name, PropertyChangeListener l) {
	props.addPropertyChangeListener(name, l);
    }
    public void removePropertyChangeListener (PropertyChangeListener l) {
	props.removePropertyChangeListener(l);
    }
    public void removePropertyChangeListener 
	(String name, PropertyChangeListener l) {
	props.removePropertyChangeListener(name, l);
    }

    /**
     * TableFormat interface
     */
    public int getColumnCount () { return columns.size(); }
    public String getColumnName (int column) { return columns.get(column); }
    public Object getColumnValue (Entity e, int column) {
	return e.getValue(columns.get(column));
    }

    /**
     * WritableTableFormat interface
     */
    public boolean isEditable (Entity e, int column) {
	/*
	 * Note that this is different than the normal sense.  Enabling
	 * editing here simply means that the user can double-click on
	 * the cell to bring up the balloon showing the content of the
	 * cell.  To be able to edit the content, use setColumnEditable
	 * instead!
	 */
	return true;
    }

    public Entity setColumnValue (Entity e, Object value, int column) {
	String field = columns.get(column);
	if (field != null) {
            // update value: value -> v
            Value v = e.getValue(field);
	}
	return e;
    }
}
