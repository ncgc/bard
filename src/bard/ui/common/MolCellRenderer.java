// $Id: MolCellRenderer.java 3486 2009-10-29 15:48:13Z nguyenda $

package bard.ui.common;

import java.util.Map;
import java.util.HashMap;
import java.util.WeakHashMap;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.ehcache.Element;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import chemaxon.formats.MolImporter;
import chemaxon.marvin.MolPrinter;
import chemaxon.marvin.common.UserSettings;
import chemaxon.marvin.paint.DispOptConsts;
import chemaxon.struc.MolAtom;
import chemaxon.struc.MolBond;
import chemaxon.struc.Molecule;
import chemaxon.struc.MDocument;
import chemaxon.struc.graphics.*;
import chemaxon.util.MolHandler;
import chemaxon.struc.StereoConstants;

import bard.core.Compound;
import bard.core.Entity;
import bard.core.Value;
import bard.core.adapter.CompoundAdapter;

import bard.chem.ChemUtil;
import bard.ui.util.MolRenderer;

public class MolCellRenderer extends JPanel 
    implements StereoConstants, GridCellRenderer, TableCellRenderer {

    static final private Logger logger = 
	Logger.getLogger(MolCellRenderer.class.getName());

    static final Color[] BACKGROUND_COLORS = new Color[]{
        new Color (0xf1,0xf5,0xfa), Color.white
    };

    private final static Cache cache;
    static {
	cache = new Cache (MolCellRenderer.class.getName(), 
			   1000, false, false, 60*60, 60*30);
	CacheManager.create(MolCellRenderer.class.getResource
			    ("resources/ehcache.xml")).addCache(cache);
    }

    private String field;
    private MolRenderer renderer = new MolRenderer ();
    private MolHandler mhandler = new MolHandler();
    protected Object value;
    protected boolean selected;
    private boolean ignoreEZ = false;

    // only available for TableCellRenderer
    private boolean labelVisible = false; 
    private LabelRenderer labelRenderer = new LabelRenderer ();

    public MolCellRenderer (String field, boolean ignoreEZ) {
        this.field = field;
	this.ignoreEZ = ignoreEZ;

        setOpaque(false);
        setBackground(Color.white);
    }

    public MolCellRenderer (String field) {
	this (field, false);
    }
    public MolCellRenderer (boolean ignoreEZ) {
	this (null, ignoreEZ);
    }
    public MolCellRenderer () {
        this (null, false);
    }

    public MolCellRenderer (MolRenderer renderer) {
        this ();
        setMolRenderer (renderer);
    }

    public void setLabelVisible (boolean labelVisible) {
	if (labelVisible && labelRenderer == null) {
	    labelRenderer = new LabelRenderer ();
	}
	this.labelVisible = labelVisible;
    }
    public boolean getLabelVisible () { return labelVisible; }
    public void setLabelRenderer (LabelRenderer labelRenderer) {
	this.labelRenderer = labelRenderer;
	clearAndRepaint ();
    }
    public LabelRenderer getLabelRenderer () { return labelRenderer; }

    public void setShadowVisible (boolean visible) {
        renderer.setShadowVisible(visible);
        clearAndRepaint ();
    }
    public float getShadowRadius () { return renderer.getShadowRadius(); }
    public void setShadowRadius (float radius) { 
        renderer.setShadowRadius(radius); 
        clearAndRepaint ();
    }
    public int getShadowOffset () { return renderer.getShadowOffset(); }
    public void setShadowOffset (int offset) {
        renderer.setShadowOffset(offset);
        clearAndRepaint ();
    }
    public float getShadowTranslucent () { 
        return renderer.getShadowTranslucent(); 
    }
    public void setShadowTranslucent (float translucent) {
        renderer.setShadowTranslucent(translucent);
        clearAndRepaint ();
    }
    public boolean getShadowVisible () { return renderer.getShadowVisible(); }
    public void setAtomMapVisible (boolean visible) {
        renderer.setAtomMapVisible(visible);
        clearAndRepaint ();
    }
    public boolean getAtomMapVisible () {
        return renderer.getAtomMapVisible();
    }

    public void setDisplayOptions (int flags) {
	renderer.setDisplayOptions(flags);
        clearAndRepaint ();
    }
    public int getDisplayOptions () { return renderer.getDisplayOptions(); }

    public void setStereoCentersVisible (boolean visible) {
        renderer.setStereoCentersVisible(visible);
        clearAndRepaint ();
    }
    public boolean getStereoCentersVisible () {
        return renderer.getStereoCentersVisible();
    }

    public void setEZVisible (boolean visible) {
        renderer.setEZVisible(visible);
        clearAndRepaint ();
    }
    public boolean getEZVisible () { return renderer.getEZVisible(); }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public MolRenderer getMolRenderer () { return renderer; }
    public void setMolRenderer (MolRenderer renderer) {
        this.renderer = renderer;
        clearAndRepaint ();
    }

    public void setValue (Object value) {
	setValue (value, false);
    }

    public void setValue (Object value, boolean selected) {
	this.value = value;
	this.selected = selected;
    }

    public void setIgnoreEZ (boolean ignoreEZ) { this.ignoreEZ = ignoreEZ; }
    public boolean getIgnoreEZ () { return ignoreEZ; }

    public Component getGridCellRendererComponent
	(Grid g, Object value, boolean selected, int cell) {
        if (value instanceof Entity && field != null) {
            value = ((Entity)value).getValue(field);
        }
        /*
        int bg = (cell + (cell/g.getCellsPerRow() & 1)) 
            % BACKGROUND_COLORS.length;
        renderer.setBackground(BACKGROUND_COLORS[bg]);
        */

        this.value = value;
        this.selected = selected;
        return this;
    }

    void clearAndRepaint () {
        clearCache ();
        repaint ();
    }

    public synchronized static void clearCache() {
	cache.removeAll();
    }

    public Component getTableCellRendererComponent
            (JTable table, Object value, boolean selected,
             boolean focus, int row, int column) {

        if (value instanceof Entity && field != null) {
            Value v = ((Entity)value).getValue(field);
            // for now, show the last molecule if we have more than 1
            value = v != null ? v.getValue() : null;
        }

        this.value = value;
        this.selected = selected;
        
        Color background = row%2 == 1 ? table.getBackground(): 
            new Color(241, 245, 250);
        background = selected ? table.getSelectionBackground() 
        	: background;
        
        setBackground (background);
        renderer.setBackground(background);

        return this;
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (value == null) {
            return;
        }

        Graphics2D g2 = (Graphics2D) g;

        Rectangle r = getBounds();
        //int x = r.x, y = r.y;
        //r.x = r.y = 0;

        g2.setPaint(getBackground());
        g2.fillRect(0, 0, r.width, r.height);

        Image img = getImage(g, value, r);
        if (img != null) {
            // centering
            g.drawImage(img, (r.width-img.getWidth(null))/2, 
                        (r.height-img.getHeight(null))/2, null);
        }

	if (labelVisible && labelRenderer != null) {
	    labelRenderer.renderLabel
		(g2, selected ? 5 : 0, 
		 5, 4, r.width, r.height, getLabel (value));
	}
    }

    String getLabel (Object value) {
        Class clazz = value.getClass();

        if (Molecule.class.isAssignableFrom(clazz)) {
	    return ((Molecule)value).getName();
	}
	else if (Entity.class.isAssignableFrom(clazz)) {
	    return ((Entity)value).getName();
	}

	return value.toString();
    }
    

    /**
     * caching the rendered value... this is much faster than
     * not caching even though caching is already performed at the
     * Grid component layer.
     */
    protected Image getImage (Graphics g, Object value, Rectangle r) {
        Class clazz = value.getClass();

        if (Molecule.class.isAssignableFrom(clazz)) {
	    Molecule mol = (Molecule) value;
	    if (ignoreEZ) {
		mol = mol.cloneMolecule();
		ChemUtil.resetEZ(mol);
		value = mol;
	    }
        } 
	else if (Compound.class.isAssignableFrom(clazz)) {
            Compound compound = (Compound) value;
            CompoundAdapter adapter = new CompoundAdapter (compound);
	    //ChemUtil.toMol(compound, ignoreEZ);
            value = adapter.toFormat(CompoundAdapter.Format.NATIVE);
        } 
	else {
            value = ChemUtil.toMol(value.toString(), ignoreEZ);
        }

        if (value == null)
            return null;

        Molecule mol = (Molecule)value;
	int hash = getMolHash (mol);
	Element el = cache.get(hash);
	Image img = null;
        if (el != null) {
	    img = (Image)el.getObjectValue();
            if (img.getWidth(null) != r.width
                    || img.getHeight(null) != r.height) {
                // force new image to be generated
                img = null;
            }
        }

        if (img == null && value != null) {
            img = renderer.createImage(mol, Math.min(r.width, r.height));
            cache.put(new Element (hash, img));
        }

        return img;
    }

    /*
     * this is the only way to detect changes within the molecule?
     */
    static int getMolHash (Molecule mol) {
        Map objs = new HashMap ();
	for (int i = 0; i < mol.getPropertyCount(); ++i) {
	    String key = mol.getPropertyKey(i);
	    Object obj = mol.getPropertyObject(key);
	    if (!(obj instanceof String)) {
		// remove none String object
		mol.setPropertyObject(key, null);
		objs.put(key, obj);
	    }
	}

	int hash = mol.toFormat("mrv").hashCode();
        // now put back the objects
        for (Object obj : objs.entrySet()) {
            Map.Entry me = (Map.Entry)obj;
            mol.setPropertyObject((String)me.getKey(), me.getValue());
        }

        return hash;
    }

    public Image createImage (Molecule mol, int width, int height) {
        return createImage (mol, Math.max(width, height));
    }

    public Image createImage (Molecule mol, int size) {
        return renderer.createImage(mol, size);
    }

    protected static void doImport(MolImporter molimp, Grid g)
            throws Exception {
        for (Molecule m; (m = molimp.read()) != null;) {
            m.dearomatize();
            m.hydrogenize(false);
            g.addValue(m);
        }
    }

    public static void main(final String[] argv) throws Exception {
        final Grid g = new Grid();
        g.setHgap(20);
        g.setVgap(10);
        g.setCellSize(80);
        final MolCellRenderer renderer = new MolCellRenderer();
        g.setCellRenderer(renderer);
        g.setCellEditor(new MolCellEditor());
        g.setCellAnnotator(new GridCellAnnotator() {
            public String getGridCellAnnotation(Grid g, Object v, int c) {
                return ((Molecule) v).getName();
            }

            public String getGridCellBadgeAnnotation
                    (Grid g, Object v, int c) {
                return String.valueOf(c + 1);
            }
        });

        JPanel top = new JPanel(new BorderLayout(0, 5));
        top.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        top.add(new JScrollPane(g));

        Box north = Box.createHorizontalBox();
        top.add(north, BorderLayout.NORTH);

        JSlider slider = new JSlider(50, 550, 80);
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                final JSlider slider = (JSlider) e.getSource();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        g.setCellSize(slider.getValue());
                    }
                });
            }
        });
        JCheckBox shadowCb = new JCheckBox ("Shadow");
        shadowCb.addActionListener(new ActionListener () {
                public void actionPerformed (ActionEvent e) {
                    JCheckBox cb = (JCheckBox)e.getSource();
                    renderer.setShadowVisible(cb.isSelected());
                    g.refresh();
                }
            });
        shadowCb.setSelected(renderer.getShadowVisible());
        north.add(shadowCb);
        north.add(Box.createHorizontalStrut(10));

        JSlider translucent = new JSlider
            (0, 100, (int)(100*renderer.getShadowTranslucent()));
        translucent.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSlider slider = (JSlider) e.getSource();
                renderer.setShadowTranslucent((float)slider.getValue()/100.f);
                g.refresh();
            }
        });
        north.add(new JLabel ("Translucent:"));
        north.add(translucent);
        north.add(Box.createHorizontalStrut(10));

        north.add(new JLabel ("Size:"));
        //north.add(Box.createHorizontalStrut(3));
        north.add(slider);
        
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(top);
        f.setSize(200, 200);
        f.pack();
        f.setVisible(true);

        new java.util.Timer().schedule(new TimerTask() {
            public void run() {
                try {
                    if (argv.length == 0) {
                        System.err.println("** reading from stding");
                        doImport(new MolImporter(System.in), g);
                    } else {
                        for (String a : argv) {
                            MolImporter molimp = new MolImporter
                                    (new FileInputStream(a));
                            doImport(molimp, g);
                            molimp.close();
                        }
                    }
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            g.repaint();
                        }
                    });
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, 0);
    }
}
