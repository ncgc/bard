// $Id$

package bard.ui.common;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import bard.ui.explodingpixels.widgets.TableHeaderUtils;

public class EntityTableHeaderDecorator
    implements MouseListener, MouseMotionListener, 
	       ActionListener, TableColumnModelListener {

    private static final String COLUMN_KEY = ".TableColumn";

    private JPopupMenu popup;
    private JTableHeader header;

    public static void decorate (JTableHeader header) {
	new EntityTableHeaderDecorator (header);
    }

    private EntityTableHeaderDecorator (final JTableHeader header) {
	this.header = header;

	popup = new JPopupMenu ();
	JMenuItem item;
	popup.add(item = new JMenuItem ("Clear sorting"));
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    TableHeaderUtils.clearSortColumn(header);
		    ((EntityTable)header.getTable()).sort(-1, null);
		}
	    });
	popup.addSeparator();
	header.addMouseListener(this);
	header.addMouseMotionListener(this);
	header.getTable().getColumnModel().addColumnModelListener(this);
    }

    public void actionPerformed (ActionEvent e) {
	JComponent c = (JComponent)e.getSource();
	EntityTableColumn column = 
	    (EntityTableColumn)c.getClientProperty(COLUMN_KEY);
	if (column != null) {
	    if (((JCheckBoxMenuItem)c).isSelected()) {
		header.getTable().getColumnModel().addColumn(column);
		header.repaint();
	    }
	    else {
		if (header.getTable().getColumnCount() > 1) {
		    header.getTable().getColumnModel().removeColumn(column);
		}
		else { // can't remove the last column... 
		    ((JCheckBoxMenuItem)c).setSelected(true);
		}
	    }
	}
    }

    public void mouseClicked (MouseEvent e) {
	showPopup (e);
    }

    public void mouseEntered (MouseEvent e) {
    }

    public void mouseExited (MouseEvent e) {
    }

    public void mousePressed (MouseEvent e) {
	showPopup (e);
    }

    public void mouseReleased (MouseEvent e) {
    }

    public void mouseDragged (MouseEvent e) {
    }
    public void mouseMoved (MouseEvent e) {
	JTable table = header.getTable();
	int col = table.convertColumnIndexToModel
	    (table.columnAtPoint(e.getPoint()));
	table.getTableHeader().setReorderingAllowed(col != 0);
    }

    void showPopup (MouseEvent e) {
	if (!e.isPopupTrigger()) {
	    return;
	}

	popup.show(header, e.getX(), e.getY());
    }

    public void columnAdded (TableColumnModelEvent e) {
	TableColumnModel model = (TableColumnModel)e.getSource(); 
	TableColumn column = model.getColumn(e.getToIndex());
	for (Component c : popup.getComponents()) {
	    if (column == ((JComponent)c).getClientProperty(COLUMN_KEY)) {
		return;
	    }
	}
	Object value = column.getHeaderValue();
	if (value != null && !(value instanceof Icon)) {
	    JMenuItem item = new JCheckBoxMenuItem ((String)value);
	    item.putClientProperty(COLUMN_KEY, column);
	    if (column instanceof EntityTableColumn)
		item.setSelected(((EntityTableColumn)column).isVisible());
	    item.addActionListener(this);
	    popup.add(item);
	}
    }
    public void columnMarginChanged (ChangeEvent e) {
    }
    public void columnMoved (TableColumnModelEvent e) {
    }
    public void columnRemoved (TableColumnModelEvent e) {
    }
    public void columnSelectionChanged (ListSelectionEvent e) {
    }
}
