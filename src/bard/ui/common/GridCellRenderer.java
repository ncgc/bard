// $Id: GridCellRenderer.java 3286 2009-09-28 16:35:59Z nguyenda $

package bard.ui.common;

import java.awt.Component;

public interface GridCellRenderer {
    public Component getGridCellRendererComponent
	(Grid g, Object value, boolean selected, int cell);
}
