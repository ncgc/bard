package bard.ui.common;

import bard.core.HillCurveValue;
import bard.core.StringDisplayValue;
import bard.core.StringValue;
import bard.core.Value;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TextArea;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Arrays;

public class ActivityCellRenderer extends JPanel {

    ActivityPanel activityPanel;
    ActivityPanel curvePanel, spPanel;
    ActivityPanel textAreaPanel;

    public ActivityCellRenderer() {
        setOpaque(false);
        setLayout(new BorderLayout());
        curvePanel = new CurvePanel();
        spPanel = new SinglePointPanel();
        textAreaPanel = new TextAreaPanel();
    }


    ArrayList<String> activityRank = new ArrayList<String>();
    {
        activityRank.add("UNCLASS");
        activityRank.add("SP");
        activityRank.add("MULTCONC");
        activityRank.add("CR_NO_SER");
        activityRank.add("CR_SER");
    }
    
    public void setActivities(Value[] activities) {
        if (activities.length == 0) {
            activityPanel = null;
            return;
        }
        
        int rank = 0;
        for (Value activity: activities) {
            Value resp = activity.getChild("responseClass");
            if (resp != null && activityRank.indexOf(resp.getValue()) > rank)
                rank = activityRank.indexOf(resp.getValue());
        }
        if (rank > 1) {
            curvePanel.setActivities(activities);
            activityPanel = curvePanel;
            add((Component) curvePanel, BorderLayout.CENTER);
        } else if (rank == 1) {
            spPanel.setActivities(activities);
            activityPanel = spPanel;
            add((Component) spPanel, BorderLayout.CENTER);
        } else {
            textAreaPanel.setActivities(activities);
            activityPanel = textAreaPanel;
            add((Component) textAreaPanel, BorderLayout.CENTER);
        }

        //add((Component) activityPanel, BorderLayout.CENTER);
    }

    public String getLabel() {
	if (activityPanel != null)
	    return activityPanel.getLabel();
	else return "";
    }

    class CurvePanel extends JPanel implements ActivityPanel {
        private static final long serialVersionUID = -6533127286104776824L;
        LabelRenderer labeler = new LabelRenderer();

        Color[] CURVE_COLOR = new Color[]{
                Color.red,
                Color.blue,
                //Color.green,
                // taken from x11 color name
                new Color(138, 43, 226), // BlueViolet
                new Color(165, 42, 42), // Brown
                new Color(95, 158, 160), // CadetBlue
                new Color(210, 105, 30), // Chocolate
                new Color(255, 127, 80), // coral
                new Color(100, 149, 237), // Cornflowerblue
                new Color(220, 20, 60), // crimson
                new Color(0, 0, 139), // darkblue
                new Color(0, 100, 0), // darkgreen
                new Color(139, 0, 139), // dargmagenta
                new Color(255, 140, 0), // darkorange
                new Color(153, 50, 204), // darkorchild
                new Color(30, 144, 255), // dodgerblue
                new Color(178, 34, 34), // firebrick
                new Color(75, 0, 130), // indigo
                new Color(65, 105, 225), // royalblue
                new Color(0, 255, 127), // springgreen
                new Color(255, 99, 71) // tomato
        };

        ChartPanel chart1 = null;
        XYPlot xyplot;
        java.awt.Shape shape;
        String label = null;
        String mouseOver;

        public String getLabel() {
            return label;
        }

        public String getToolTipText(MouseEvent event) {
            return mouseOver;
        }

        public CurvePanel() {
            setLayout(new BorderLayout());

            if (chart1 != null) return;

            chart1 = new ChartPanel
                    (ChartFactory.createScatterPlot
                            (null, null, null, null, PlotOrientation.HORIZONTAL,
                                    false, false, false));
            chart1.setBackground(Color.white);
            JFreeChart jfc = chart1.getChart();
            jfc.setBorderPaint(Color.white);
            jfc.setBackgroundPaint(Color.white);
            jfc.setAntiAlias(true);
            jfc.setTextAntiAlias(true);
            jfc.getPlot().setBackgroundAlpha(.5f);

            xyplot = jfc.getXYPlot();
            xyplot.setRangeGridlinesVisible(false);
            xyplot.setOutlineVisible(false);
            //xyplot.setDomainCrosshairVisible(false);
            xyplot.setDomainGridlinesVisible(false);
            xyplot.setRangeZeroBaselineVisible(false);
            xyplot.setDomainZeroBaselineVisible(false);
            xyplot.setRangeAxis(new NumberAxis("Potency (log [uM])"));
            xyplot.setDomainAxis(new NumberAxis("Efficacy"));

            XYItemRenderer renderer = new XYLineAndShapeRenderer();
            for (int i = 0; i < CURVE_COLOR.length; ++i) {
                renderer.setSeriesPaint(i, CURVE_COLOR[i]);
            }
            xyplot.setRenderer(renderer);

            shape = new java.awt.geom.Ellipse2D.Double(0., 0., 4., 4.);
            //setBorder (BorderFactory.createEmptyBorder(2,2,2,2));
            setBackground(Color.white);

            labeler.setLabelFilled(true);
            labeler.setLabelTop(true);
            JXLayer layer = new JXLayer(chart1, new AbstractLayerUI() {
                @Override
                protected void paintLayer(Graphics2D g2, JXLayer layer) {
                    super.paintLayer(g2, layer);
                    if (getLabel() != null) {
                        Rectangle r = layer.getBounds();
                        labeler.renderLabel
                                (g2, 0, 5, 4, r.width, r.height, getLabel());
                    }
                }
            });
            add(layer, BorderLayout.CENTER);
        }

        HillCurveValue[] getHillCurves(Value[] activities) {
            ArrayList<HillCurveValue> curves = new ArrayList<HillCurveValue>();
            for (Value v : activities) {
                for (Value c : v.getChildren("Activity")) {
                    if (c instanceof HillCurveValue) curves.add((HillCurveValue) c);
                }
            }
            return curves.toArray(new HillCurveValue[0]);
        }


        public void setActivities(Value[] activities) {
            mouseOver = ActivityCellRenderer.getMouseOverText(activities);

            DefaultXYDataset dataset = new DefaultXYDataset();
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) xyplot.getRenderer();
            HillCurveValue[] curves = getHillCurves(activities);
            Double[] potency = new Double[curves.length];
            int potencyCounts = 0;
            String units = null;
            for (HillCurveValue c: curves) {
                double[][] yx = c.getLogConcResponse();
                int series = dataset.getSeriesCount();
                if (yx.length != 0 && yx[0].length != 0 && yx[1].length != 0) {
                    renderer.setSeriesShapesVisible(series, true);
                    renderer.setSeriesLinesVisible(series, false);
                    renderer.setSeriesShape(series, shape);
                    dataset.addSeries(series + "-raw", yx);
                }
                series = dataset.getSeriesCount();
                renderer.setSeriesShapesVisible(series, false);
                renderer.setSeriesLinesVisible(series, true);
                yx = c.getFittedConcResponseCurve();
                if (yx.length > 0) {
                    dataset.addSeries(series + "-fit", yx);
                }

                Double ac50 = c.getSlope();
                if (ac50 != null) { // || ac50 < potency) {   Not sure why this last part of logic is needed, ac50 != null supercedes it
                    double pot = ac50*Math.pow(10, c.getOffset());
                    BigDecimal bd = new BigDecimal(pot);
                    bd = bd.round(new MathContext(3));
                    
                    potency[potencyCounts] = bd.doubleValue();
                    potencyCounts++;
                    units = c.getTestUnit();
                }
            }

            String potLabel = "";
            if (potencyCounts > 0) {
        	Arrays.sort(potency, 0, potencyCounts);
        	for (Double pot: potency) {
        	    if (potLabel.length() > 0)
        		potLabel = potLabel + ",";
        	    if (pot == null) potLabel = potLabel + "inactive";
        	    else potLabel = potLabel + pot.doubleValue() + units;
        	}
            }
            
            if (potLabel.length() > 0) {
                label = potLabel + " Potency";
            } else {
                label = null;
            }
            xyplot.setDataset(dataset);
        }
    }

    class SinglePointPanel extends JPanel implements ActivityPanel {
        private static final long serialVersionUID = -3835164770490536630L;
        String label;
        String mouseOver;

        public SinglePointPanel() {
            setLayout(new BorderLayout());
        }

        @Override
        public void setActivities(Value[] activities) {
            mouseOver = ActivityCellRenderer.getMouseOverText(activities);

            if (label != null) return;
            StringDisplayValue v = (StringDisplayValue) activities[0].getChild("Activity");
            label = v == null ? "NA" : v.getValue() + " " + v.getDisplay();
            add(new JLabel(label), BorderLayout.CENTER);
        }

        @Override
        public String getLabel() {
            return label;
        }

        public String getToolTipText(MouseEvent event) {
            return mouseOver;
        }
    }

    class TextAreaPanel extends JPanel implements ActivityPanel {
        private static final long serialVersionUID = -3835164770490536630L;
        String label;
        String mouseOver;

        public TextAreaPanel() {
            setLayout(new BorderLayout());
        }

        @Override
        public void setActivities(Value[] activities) {
            mouseOver = ActivityCellRenderer.getMouseOverText(activities);
            
            String value = null;
            String display = "";
            if (label != null) return;
            label = "";
            for (Value act: activities) {
            for (Value val: act.getChildren("Activity")) {
            if (!(val instanceof StringDisplayValue)) {
                //System.err.println("Didn't know what to show for activity!");
                if (val != null && val.getValue() != null) {
                    value = val.getId().toString();
                    display = val.getValue().toString();
                }
            } else {
                StringDisplayValue v = (StringDisplayValue) val;
                value = v.getValue();
                display = v.getDisplay();
            }
            label += value == null ? "NA" : value + " " + display + "\n";
            }
            }
            label = "<html><body>" + label.replaceAll("\n", "<br>\n") + "</body></html>";
            JLabel jlabel = new JLabel(label);
            jlabel.setToolTipText(mouseOver);
            add(new JScrollPane(jlabel), BorderLayout.CENTER);
        }

        @Override
        public String getLabel() {
            return label;
        }
        
        public String getToolTipText(MouseEvent event) {
            return mouseOver;
        }
    }

    static public String getMouseOverText(Value[] activities) {
        String value = null;
        String display = "";

        String label = "";
        for (Value act: activities) {
            label += "http://bard.nih.gov/api/v17/exptdata/"+act.getId()+"\n";
        for (Value val: act.getChildren()) {
        if (!(val instanceof StringDisplayValue)) {
            //System.err.println("Didn't know what to show for activity!");
            if (val != null && val.getValue() != null) {
                value = val.getId().toString();
                display = val.getValue().toString();
            }
        } else {
            StringDisplayValue v = (StringDisplayValue) val;
            value = v.getValue();
            display = v.getDisplay();
        }
        label += value == null ? "NA" : value + " " + display + "\n";
        }
        }
        label = "<html><body>" + label.replaceAll("\n", "<br>\n") + "</body></html>";
        return label;
    }

    interface ActivityPanel {
        public void setActivities(Value[] activities);

        public String getLabel();
    }
}
