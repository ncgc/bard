package bard.ui.common;

import bard.core.*;
import bard.core.adapter.CompoundAdapter;
import bard.core.rest.RESTExperimentService;
import bard.ui.content.CompoundContent;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.StateChangeEvent;

import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;
import org.jdesktop.swingworker.SwingWorker;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ca.odell.glazedlists.EventList;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.Entity;
import bard.core.Experiment;
import bard.core.ExperimentService;
import bard.core.ServiceIterator;
import bard.core.Value;
import bard.ui.content.CompoundContent;
import bard.ui.event.ContentChangeEvent;

public class BioAssayTableColumn extends TableColumn 
	implements EventSubscriber, TableCellRenderer
	{

    private static final long serialVersionUID = 1L;

    static private Logger logger = Logger.getLogger
	(BioAssayTableColumn.class.getName());

    private Assay assay;
    private CompoundContent content;
    private Value etag;
    private EntityTable table;
    private HashMap<Object, ArrayList<String>> etagCIDs = new HashMap<Object, ArrayList<String>>();
    private HashMap<String,ArrayList<Value>> data = new HashMap<String,ArrayList<Value>>();
    private JLabel label = new JLabel();
    private ActivityCellRenderer acr = new ActivityCellRenderer ();
    private ActivityRecordLoader arl = null;
    private ArrayList<Value> emptyList = new ArrayList<Value>();
    private Comparator comparator;
    
    public BioAssayTableColumn(Assay assay, CompoundContent content, EntityTable table) {
	this.modelIndex = 1;
	this.assay = assay;
	this.content = content;
	if (content.getResource() instanceof Value)
	    etag = (Value)content.getResource();
	else {
	    logger.severe("Content resource not of type Value --- doesn't look like an etag! "+content.getResource());
	}
	this.table = table;
	this.setHeaderValue("Assay "+assay.getId());
	this.setIdentifier(assay.getName());
	this.setCellRenderer(this);
	acr.setPreferredSize(new Dimension (80, 100));
	notifyLoading(etag);
	if (content.getEntities().size() > 0)
	    loadActivityData();
	else
	    logger.severe("!!!! I don't see any content!");
	EventBus.subscribe(ContentChangeEvent.class, this);
        EventBus.subscribe(StateChangeEvent.class, this);

	setComparator(new Comparator() {
	    public int compare(Object o1, Object o2) {
		if (o2 == null) {
		    if (o1 == null)
			return 0;
		    return 1;
		} else if (o1 == null) {
		    return -1;
		}
		if (o1 instanceof Entity && o2 instanceof Entity) {
		    String eId1 = ((Entity)o1).getId().toString();
		    String eId2 = ((Entity)o2).getId().toString();
		    ArrayList<Value> a1 = data.get(eId1);
		    ArrayList<Value> a2 = data.get(eId2);
		    if (a2 == null || a2.size() == 0) {
			if (a1 == null || a1.size() == 0)
			    return 0;
			return -1;
		    } else if (a1 == null || a1.size() == 0) {
			return 1;
		    }
		    double[] da1 = new double[3];
		    for (Value v: a1) {
			scoreActivityValue(da1, v);
		    }
		    double[] da2 = new double[3];
		    for (Value v: a2) {
			scoreActivityValue(da2, v);
		    }
		    int compare = da1[0] > da2[0] ? -1 : da1[0] < da2[0] ? 1 : 0;
		    //if (compare == 0) compare = da1[1] > da2[1] ? -1 : da1[1] < da2[1] ? 1 : 0; !!! ignore score for now -- some are inverted
		    if (compare == 0) compare = da1[2] < da2[2] ? -1 : da1[2] > da2[2] ? 1 : 0;
		    return compare;
		}
		return o1.toString().compareTo(o2.toString());
	    }
	});
    }

    private void scoreActivityValue(double[] da, Value act) {
	double[] newAct = {0., 0., 10000.};
	String[] fields = {"outcome", "score", "potency"};
	for (int i=0; i<fields.length; i++) {
	    Value v = act.getChild(fields[i]);
	    if (v != null) {
		if (v.getValue() instanceof Integer) newAct[i] = (Integer)v.getValue();
		else if (v.getValue() instanceof Double) newAct[i] = (Double)v.getValue();
		else if (i==0 && v.getValue() instanceof String) newAct[i] = RESTExperimentService.PubChemOutcome.valueOf((String)v.getValue()).score();
	    }
	}
	Value v = act.getChild("Activity");
	if (v != null && v instanceof HillCurveValue)
	    newAct[2] = ((HillCurveValue)v).getSlope() == null ? newAct[2] : ((HillCurveValue)v).getSlope();
	if (newAct[0] == 3 || newAct[0] == 4) newAct[0] = 1.5; // Pubchem activity outcomes for inconclusive
	boolean update = false;
	if (newAct[0] > da[0]) update = true;
	else if (newAct[0] == da[0] && newAct[1] > da[1]) update = true;
	else if (newAct[0] == da[0] && newAct[1] == da[1] && (newAct[2] < da[2] || da[2] == 0)) update = true;
	if (update)
	    for (int i=0; i<fields.length; i++)
		da[i] = newAct[i];
    }
    
    private void notifyLoading(Object etag) {
	if (etagCIDs.containsKey(etag)) {
	    for (String cid: etagCIDs.get(etag))
		data.put(cid, emptyList);
	} else {
	    for (int i=0; i<table.getModel().getRowCount(); i++) {
		String cmpdId = null;
		Object obj = table.getModel().getValueAt(i, 1);
		if (obj instanceof Compound && ((Compound)obj).getId() != null) cmpdId = ((Compound)obj).getId().toString();
		data.put(cmpdId, emptyList);
	    }
	}
    }
    
    public void setComparator (Comparator comparator) {
	this.comparator = comparator;
    }
    public Comparator getComparator () { return comparator; }
    
    public Assay getAssay() {return assay;}
    
    public void setActivities(List<Value> values, Object etag, ArrayList<String> cids) {
	if (this.etag == etag) {
	    for (int i=0; i<table.getModel().getRowCount(); i++) {
		String cmpdId = null;
		Object obj = table.getModel().getValueAt(i, 1);
		if (obj instanceof Compound && ((Compound)obj).getId() != null) cmpdId = ((Compound)obj).getId().toString();
		data.remove(cmpdId);
	    }
	} else {
	    if (!etagCIDs.containsKey(etag)) {
		etagCIDs.put(etag, cids);
	    }
	    for (String cid: etagCIDs.get(etag))
		data.remove(cid);
	}
	for (Value value: values) {
	    String cid = value.getChild("cid").getValue().toString();
	    ArrayList<Value> activities = data.get(cid);
	    if (activities == null) {
		activities = new ArrayList<Value>();
		data.put(cid, activities);
	    }
	    activities.add(value);
	}
    }
    
    public ArrayList<Value> getActivity(Object obj) {
	Object entityId = null;
	if (obj instanceof Entity && ((Entity)obj).getId() != null) entityId = ((Entity)obj).getId().toString();
	if (entityId != null) {
	    ArrayList<Value> activities = data.get(entityId);
	    return activities;
	}
	return null;
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value,
	    boolean isSelected, boolean hasFocus, int row, int column) {
	Object obj = table.getModel().getValueAt(row, modelIndex);
	ArrayList<Value> activities = getActivity(obj);
	if (activities != null && activities.size() > 0) {
	    acr.setActivities(activities.toArray(new Value[0]));
	    return acr;
	} else if (activities != null) {
	    label.setText("<html><i>loading...</i></html>");
	} else
	    label.setText("No Data");
	return label;
    }
    
    class ActivityRecordLoader
    	extends SwingWorker<List<Value>, Void> {

	BioAssayTableColumn related;
	ActivityCellRenderer renderer;
	Assay assay;
	Value etag;
	ArrayList<String> cids = new ArrayList<String>();

        ActivityRecordLoader (BioAssayTableColumn related,
		ActivityCellRenderer renderer,
		Assay assay, Value etag) {
	    this.related = related;
	    this.renderer = renderer;
	    this.assay = assay;
	    this.etag = etag;
	}

	@Override
	protected List<Value> doInBackground() {

	    ArrayList<Value> values = new ArrayList<Value>();

	    // if etag has many more compounds than currently loaded OR etag is composite ... then just get temp etag for currently loaded compounds
	    boolean newetag = false;
	    int count = 100000;
	    try {
		count = Integer.valueOf(etag.getChildren("count").toArray(new Value[0])[0].getValue().toString());
	    } catch (Exception e) {;}	    
	    EventList<Compound> compounds = content.getEntities();
            compounds.getReadWriteLock().writeLock().lock();
            try {
                if (content.getResource() instanceof Value) {
                    Value etag = (Value)content.getResource();
                    Value type = etag.getChild("type");
                    if (type != null && type.getValue().toString().endsWith("ETag")) {
                        newetag = true;
                    }
                }
        	if (content.getEntities().size() < count + 200) {
        	    newetag = true;
        	}
        	if (newetag == true) {
        	    for (Compound c: compounds)
        		if (c.getId() != null) {
        		    String cid = c.getId().toString();
        		    try {
        			Long.parseLong(cid);
        			cids.add(c.getId().toString());
        		    } catch(NumberFormatException nfe) {;}
        		}
        	}
            }
            finally {
                compounds.getReadWriteLock().writeLock().unlock();
            }
            if (newetag) {
        	String etagName = "";
        	if (etag != null && etag.getValue() != null)
        	    etagName = etag.getValue().toString();
        	else if (content.getResource() != null)
        	    if (content.getResource() instanceof Value)
        	        etagName = ((Value)content.getResource()).getChild("name").getValue().toString();
        	    else etagName = content.getResource().toString();
        	etagName += "/" + assay.getId();
                if (cids.size() == 0)
                    logger.severe("no cids in list to retrieve bioassay data for, etag:"+etagName);
                else {
                    Object obj = content.getService().newETag(etagName, cids);
                    if (obj != null && obj instanceof Value) {
                        etag = (Value)obj;
                    }
                }
            }

	    for (Value expt: assay.getValues(Experiment.class.toString()))
		values = getActivityResponse((ExperimentService)related.content.getContent
			(Experiment.class).getService(), expt, etag, values);


//        // get expt ids and sids
//        List<Object> eids = new ArrayList<Object>();
//        for (Value expt : assay.getValues(Experiment.class.toString())) {
//            eids.add(expt.getValue().toString());
//        }
//
//        List<Compound> cmpds = new ArrayList<Compound>();
//        int nrow = table.getModel().getRowCount();
//        for (int i = 0; i < nrow; i++) cmpds.add((Compound) table.getModel().getValueAt(i, 1));
//
//        List<Long> sids = new ArrayList<Long>();
//        EntityServiceManager esm = content.getServiceManager();
//        for (Compound c : cmpds) {
//            CompoundAdapter ca = new CompoundAdapter(c, esm);
//            sids.addAll(Arrays.asList(ca.getPubChemSIDs()));
//        }
//
//        ExperimentService es = (ExperimentService) related.content.getContent(Experiment.class).getService();
//        ServiceIterator<Value> iter = es.activities(eids.toArray(new Object[0]), sids.toArray(new Object[0]));
//        while (iter.hasNext()) {
//            values.add(iter.next());
//        }
//        iter.done();
//        logger.severe("Got " + values.size() + " exptdata for " + eids.size() + " experiments and " + cmpds.size() + " compounds");
	    return values;	
	}

	public ArrayList<Value> getActivityResponse(ExperimentService es, Value expt, Value etag, ArrayList<Value> values) {

	    ServiceIterator<Value> iter = es.activities(expt.getValue().toString(), etag);
	    while (iter.hasNext()) {
		Value v = iter.next();
		//System.out.println(" >> "+v);
		values.add(v);
	    }
	    iter.done();

	    //System.out.println("## "+exprId+"."+etag+"..."
	    //                   +iter.getConsumedCount());

	    logger.info("Experiment "+expt.getValue().toString()+" etag "
		    +etag+" has activites="+values.size());

	    return values;
	}

	@Override
	protected void done () {
	    try {
		List<Value> activities = get ();
		if (activities != null)
		    related.setActivities(activities, etag, cids);
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Can't run plugin", ex);
	    }
	    //related.setBusy(false);
	}
    }

    public void loadActivityData() {
        //if (arl == null)
            arl = new ActivityRecordLoader (this, acr, assay, etag);
        arl.execute();
    }
    
    public void onEvent (Object ev) {
        if (ev instanceof ContentChangeEvent) {
            onEvent ((ContentChangeEvent)ev);
        }
        else if (ev instanceof StateChangeEvent) {
            onEvent ((StateChangeEvent)ev);
        }
    }

    public void onEvent (StateChangeEvent e) {  // for delayed loading of compounds, assays in composite ETags especially
        if (e.getState() == content.getState() && "size".equals(e.getName())) {
            loadActivityData();
        }
    }

    public void onEvent(ContentChangeEvent e) {
	if (e.getContent() != content) {
	    return;
	}
	
	String prop = e.getName();
	if (prop.equals(ContentChangeEvent.SELECTION)) {
	}
        else if (e.getName().equals(ContentChangeEvent.STATE)) {
//            State<Compound> state = (State<Compound>)e.getNewValue();
//            grid.setListSource(state.getEntities());
//            grid.setSelectionModel(state.getSelectionModel());
//            state.getSelectionModel().removeListSelectionListener(grid);
//            state.getSelectionModel().addListSelectionListener(grid);
        }
    }



}
