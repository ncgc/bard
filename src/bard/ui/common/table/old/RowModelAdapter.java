// $Id$

package bard.ui.common.table.old;

import java.util.Comparator;

/*
 * dummy RowModel
 */
public class RowModelAdapter implements RowModel {
    public RowModelAdapter () {}
    public Row createRow (Object value) { return null; }
    public int getColumnCount () { return 0; }
    public Class getColumnClass (int column) { return Object.class; }
    public String getColumnName (int column) { return null; }
    public Comparator getComparator (int column) { return null; }
    public void setComparator (int column, Comparator comparator) {}
}

