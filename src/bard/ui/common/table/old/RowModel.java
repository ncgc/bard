// $Id$

package bard.ui.common.table.old;

import java.util.Comparator;

public interface RowModel {
    public Row createRow (Object value);
    public int getColumnCount ();
    public Class getColumnClass (int column);
    public String getColumnName (int column);
    
    /*
     * sorting (optional)
     */
    public Comparator getComparator (int column);
    public void setComparator (int column, Comparator comparator);
}
