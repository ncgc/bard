// $Id$

package bard.ui.common.table.old;

public class DefaultRowModel extends RowModelAdapter {
    // just a wrapper around the value
    class RowImpl implements Row {
	Object value;
	RowImpl (Object value) {
	    this.value = value;
	}
	public Object getSource () { return value; }
	public Object getValueAt (int column) {
	    return value;
	}
	public void setValueAt (Object value, int column) {
	    this.value = value;
	}
	public boolean isCellEditable (int column) {
	    return true;
	}
	public Status getStatus () { return Status.ROW_OK; }
	public RowModel getModel () {
	    return DefaultRowModel.this; 
	}
    }
    
    String name;
    Class clazz;
    
    public DefaultRowModel () {
    }
    public DefaultRowModel (String name) {
	this.name = name;
    }
    public DefaultRowModel (String name, Class clazz) {
	this.name = name;
	this.clazz = clazz;
    }
    public Row createRow (Object value) {
	return new RowImpl (value);
    }
    public int getColumnCount () { return 1; }
    public String getColumnName (int column) { return name; }
    public Class getColumnClass (int column) { return clazz; }
}
