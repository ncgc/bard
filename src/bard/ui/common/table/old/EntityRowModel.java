// $Id$

package bard.ui.common.table.old;

import java.util.*;

import bard.core.Entity;
import bard.core.Value;

public class EntityRowModel implements RowModel {

    class EntityRow implements Row {
	Entity entity;

	EntityRow (Entity entity) {
	    this.entity = entity;
	}

	public Object getSource () { return entity; }
	public Object getValueAt (int column) {
            return entity.getValues(fields.get(column));
	}
	public void setValueAt (Object value, int column) {
	}
	public boolean isCellEditable (int column) { return true; }
	public Status getStatus () { return Status.ROW_OK; }
	public RowModel getModel () { return EntityRowModel.this; }
    }

    protected List<String> fields = new ArrayList<String>();
    protected Map<String, Class> classes = new HashMap<String, Class>();
    protected Map<String, Comparator> comparators = 
	new HashMap<String, Comparator>();

    public EntityRowModel () {
    }

    public void addColumn (String field) {
	fields.add(field);
    }

    public void addColumn (String field, Class clazz) {
	fields.add(field);
	classes.put(field, clazz);
    }
    public Row createRow (Object value) {
	if (!(value instanceof Entity)) {
	    return null;
	}
	return new EntityRow ((Entity)value);
    }

    public int getColumnCount () { return fields.size(); }
    public String getColumnName (int column) {
	return fields.get(column);
    }
    public Class getColumnClass (int column) {
	Class c = classes.get(fields.get(column));
	return c != null ? c : Object.class;
    }
    public Comparator getComparator (int column) {
	return comparators.get(fields.get(column));
    }
    public void setComparator (int column, Comparator cmp) {
	comparators.put(fields.get(column), cmp);
    }
}
