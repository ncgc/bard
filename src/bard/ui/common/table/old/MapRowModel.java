// $Id$

package bard.ui.common.table.old;

import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * RowModel based on Map
 */

public class MapRowModel extends RowModelAdapter {
    /*
     * each entry in the Map instance is a column for this row
     */
    class RowMap implements Row {
	Map map;
	RowMap (Map map) {
	    this.map = map;
	}

	public Object getSource () { return map; }
	public Object getValueAt (int column) {
	    return map.get(columns.get(column));
	}
	public void setValueAt (Object value, int column) {
	    map.put(columns.get(column), value);
	}
	public Status getStatus () { return Status.ROW_OK; }
	public RowModel getModel () { return MapRowModel.this; }
	public boolean isCellEditable (int column) { return true; }
    }

    List columns = new ArrayList ();
    Map classes = new HashMap ();
    Map comparators = new HashMap ();

    public MapRowModel () {
    }

    public void addColumn (Object column, Class clazz) {
	columns.add(column);
	classes.put(column, clazz);
    }
    public void removeColumn (Object column) {
	columns.remove(column);
	classes.remove(column);
    }

    public void setComparator (Object column, Comparator cmp) {
	comparators.put(column, cmp);
    }
    public Comparator getComparator (Object column) {
	return (Comparator)comparators.get(column);
    }
    
    public Row createRow (Object value) {
	if (!(value instanceof Map)) {
	    return null;
	}

	Map map = (Map)value;
	return new RowMap (map);
    }

    public int getColumnCount () { return columns.size(); }
    public Class getColumnClass (int column) {
	return (Class)classes.get(columns.get(column));
    }
    public String getColumnName (int column) { 
	Object obj = columns.get(column);
	return obj != null ? obj.toString() : null;
    }
    public Comparator getComparator (int column) {
	return (Comparator)comparators.get(columns.get(column));
    }
    public void setComparator (int column, Comparator cmp) {
	comparators.put(columns.get(column), cmp);
    }
}
