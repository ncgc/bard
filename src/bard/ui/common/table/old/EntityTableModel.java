// $Id: EntityTableModel.java 3487 2009-10-29 15:48:57Z nguyenda $

package bard.ui.common.table.old;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.beans.*;
import javax.swing.SizeSequence;
import javax.swing.table.*;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEvent;

import static bard.ui.explodingpixels.widgets.TableUtils.SortDirection;

import bard.util.Base26Codec;
import bard.core.Entity;
import bard.core.Value;

public class EntityTableModel extends AbstractTableModel {
    private static final Logger logger = 
	Logger.getLogger(EntityTableModel.class.getName());

    static class ColumnCoordinate {
	int index;
	int offset;
	
	ColumnCoordinate () {}
	ColumnCoordinate (int index, int offset) { 
	    this.index = index;
	    this.offset = offset;
	}
    }

    static class CompositeRowModel extends RowModelAdapter {
	List<RowModel> columns = new ArrayList<RowModel>();

	class CompositeRow implements Row {
	    List<Row> rows = new ArrayList<Row>();

	    CompositeRow () {}
	    CompositeRow (int span) {
		// fill with empty cells
		for (int i = 0; i < span; ++i) {
		    rows.add(null);
		}
	    }

	    public void addRow (Row row) {
		rows.add(row);
	    }
	    public void setRow (int index, Row row) {
		rows.set(index, row);
	    }
	    public RowModel getModel () {
		return CompositeRowModel.this;
	    }
	    public Object getSource () { return null; }
	    public Object getValueAt (int column) {
		ColumnCoordinate cc = getColumnCoordinate (column);
		if (cc != null) {
		    Row r = rows.get(cc.index);
		    if (r != null) {
			return r.getValueAt(cc.offset);
		    }
		}
		return null;
	    }
	    public void setValueAt (Object value, int column) {
		ColumnCoordinate cc = getColumnCoordinate (column);
		if (cc != null) {
		    Row r = rows.get(cc.index);
		    if (r != null) {
			r.setValueAt(value, cc.offset);
		    }
		    else {
			RowModel rm = columns.get(cc.index);
			rows.set(cc.index, rm.createRow(value));
		    }
		}
		else {
		    throw new IllegalArgumentException
			("Invalid column specified: "+column);
		}
	    }
	    public boolean isCellEditable (int column) {
		ColumnCoordinate cc = getColumnCoordinate (column);
		if (cc != null) {
		    Row r = rows.get(cc.index);
		    if (r != null) {
			return r.isCellEditable(cc.offset);
		    }
		}
		return false;
	    }
	    public Status getStatus () {
		for (Row r : rows) {
		    if (r != null && r.getStatus() != Status.ROW_OK) {
			return r.getStatus();
		    }
		}
		return Status.ROW_OK;
	    }
	}
	
	ColumnCoordinate cc = new ColumnCoordinate ();

	public CompositeRowModel () {
	}

	ColumnCoordinate getColumnCoordinate (int column) {
	    int index = 0;
	    for (int i = 0; i < columns.size(); ++i) {
		RowModel rm = columns.get(i);
		if (column >= index && column < (index+rm.getColumnCount())) {
		    cc.index = i;
		    cc.offset = column-index;
		    return cc;
		}
		index += rm.getColumnCount();
	    }
	    return null;
	}

	public void addRowModel (RowModel rowModel) {
	    columns.add(rowModel);
	}

	public int size () { return columns.size(); }
	public RowModel getRowModel (int i) { return columns.get(i); }

	public int getColumnCount () {
	    int total = 0;
	    for (RowModel rm : columns) {
		total += rm.getColumnCount();
	    }
	    return total;
	}

	public Class getColumnClass (int column) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		return rm.getColumnClass(cc.offset);
	    }
	    return Object.class;
	}

	public String getColumnName (int column) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		return rm.getColumnName(cc.offset);
	    }
	    return "";
	}

	public Row createRow (Object value) {
	    if (value != null && value instanceof Integer) {
		return new CompositeRow ((Integer)value);
	    }
	    return new CompositeRow (columns.size());
	}

	public Comparator getComparator (int column) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		return rm.getComparator(cc.offset);
	    }
	    else {
		throw new IllegalArgumentException
		    ("Invalid column specified: "+column);
	    }
	}
	public void setComparator (int column, Comparator cmp) {
	    ColumnCoordinate cc = getColumnCoordinate (column);
	    if (cc != null) {
		RowModel rm = columns.get(cc.index);
		rm.setComparator(cc.offset, cmp);
	    }
	    else {
		throw new IllegalArgumentException
		    ("Invalid column specified: "+column);
	    }
	}
    }


    class SortOrderColumn implements Comparator<Integer> {
	Comparator cmp;
	int column;
	SortDirection order;
	Integer[] map;

	SortOrderColumn (int column, SortDirection order) {
	    this.order = order;
	    map = new Integer[getRowCount ()];
	    cmp = rowModel.getComparator(column);
	    this.column = column;

	    sort ();
	}

	public void setSortOrder (SortDirection order) {
	    this.order = order;
	}
	public SortDirection getSortOrder () { return order; }

	public void sort () {
	    for (int r = 0; r < map.length; ++r) {
		map[r] = EntityTableModel.this.convertRowIndex(r);
	    }
	    
	    if (cmp != null) {
		Arrays.sort(map, this);
	    }
	}

	Object getValueAt (int r) {
	    return rows.get(r).getValueAt(column);
	}

	public int compare (Integer i, Integer j) {

	    if (order == SortDirection.ASCENDING) {
		System.out.println("Comparing "+i+":"+getValueAt(i)
				   +" vs " + j+":"+getValueAt(j));
	    }
	    else {
		System.out.println("Comparing "+j+":"+getValueAt(j)
				   +" vs " + i+":"+getValueAt(i));
	    }

	    System.out.print("** mapping: "+map[0]);
	    for (int k = 1; k < map.length; ++k) {
		System.out.print(" "+map[k]);
	    }
	    System.out.println();

	    Object vali = getValueAt (i);
	    Object valj = getValueAt (j);
	    if (vali == null && valj == null) {
		return 0;
	    }

	    /*
	     * always force null rows to be at the bottom
	     */
	    if (vali == null) {
		return 1;
	    }

	    if (valj == null) {
		return -1;
	    }

	    return order == SortDirection.DESCENDING 
		? cmp.compare(valj, vali) : cmp.compare(vali, valj);
	}

	public int convertRowIndex (int row) { 
	    return row >= 0 && row < map.length ? map[row] : row; 
	}
    }

    /*
     * handling multi-column sorting
     */
    private List<SortOrderColumn> sortOrderColumns = 
	new ArrayList<SortOrderColumn>();
    private SortOrderColumn currentSortOrder = null;

    private CompositeRowModel rowModel = new CompositeRowModel ();
    private List<CompositeRowModel.CompositeRow> rows = 
	new ArrayList<CompositeRowModel.CompositeRow>();

    // either all or nothing editable property
    private boolean isEditable = true;

    public EntityTableModel () {
    }

    // create an EntityTableModel with the given anchor EventList
    public EntityTableModel (RowModel rowModel, List columnData) {
	addColumn (rowModel, columnData);
    }

    public int addColumn (RowModel rowModel, List columnData) {
	final int column =  addColumn 
	    (rowModel, columnData.toArray(new Object[0]));

	if (columnData instanceof EventList) {
	    EventList list = (EventList)columnData;
	    list.addListEventListener(new ListEventListener () {
		    public void listChanged (ListEvent e) {
			EventList source = (EventList)e.getSource();
			setColumn (column, source.toArray(new Object[0]));
		    }
		});
	}
	return column;
    }

    public <T> int addColumn (RowModel rowModel, T... columnData) {
	if (!isEditable) {
	    throw new IllegalArgumentException ("TableModel not editable");
	}

	if (rows.isEmpty()) {
	    for (Object datum : columnData) {
		CompositeRowModel.CompositeRow row = 
		    (CompositeRowModel.CompositeRow)
		    this.rowModel.createRow(null);
		row.addRow(rowModel.createRow(datum));
		rows.add(row);
	    }
	}
	else {
	    for (int i = rows.size(); i < columnData.length; ++i) {
		// pad with empty rows
		rows.add((CompositeRowModel.CompositeRow)
			 this.rowModel.createRow(null));
	    }

	    int r = 0;
	    for (; r < columnData.length; ++r) {
		CompositeRowModel.CompositeRow row = rows.get(r);
		row.addRow(rowModel.createRow(columnData[r]));
	    }

	    for (; r < rows.size(); ++r) {
		CompositeRowModel.CompositeRow row = rows.get(r);
		row.addRow(rowModel.createRow(null));		
	    }
	}
	int column = getColumnCount ();
	this.rowModel.addRowModel(rowModel);

	return column;
    }

    /*
     * replace the current column 
     */
    public <T> void setColumn (int column,  T... columnData) {
	if (!isEditable) {
	    throw new IllegalArgumentException ("TableModel not editable");
	}

	for (int i = rows.size(); i < columnData.length; ++i) {
	    // pad with empty rows
	    rows.add((CompositeRowModel.CompositeRow)
		     this.rowModel.createRow(null));
	}

	ColumnCoordinate cc = rowModel.getColumnCoordinate(column);
	RowModel rm = rowModel.getRowModel(cc.index);

	int r = 0;
	for (; r < columnData.length; ++r) {
	    CompositeRowModel.CompositeRow row = rows.get(r);
	    row.setRow(cc.index, rm.createRow(columnData[r]));
	}
	
	for (; r < rows.size(); ++r) {
	    CompositeRowModel.CompositeRow row = rows.get(r);
	    row.setRow(cc.index, rm.createRow(null));
	}
    }

    /*
     * Add a new column based on the formula
     */
    public int addColumn (String name, String formula) {
	return 0;
    }

    public Object getValueAt (int row, int column) {
	row = convertRowIndex (row);
	return rows.get(row).getValueAt(column);
    }
    public void setValueAt (Object value, int row, int column) {
	if (!isEditable) {
	    throw new IllegalArgumentException ("TableModel not editable");
	}
	row = convertRowIndex (row);
	rows.get(row).setValueAt(value, column);
    }

    public Row.Status getRowStatus (int row) { 
	row = convertRowIndex (row);	
	return rows.get(row).getStatus(); 
    }

    public RowModel getRowModel (int column) {
	return rowModel.getRowModel(column);
    }

    public int getRowCount () { return rows.size(); }
    public int getColumnCount () { return rowModel.getColumnCount(); }

    @Override
    public String getColumnName (int column) { 
	return rowModel.getColumnName(column); 
    }

    @Override
    public Class<?> getColumnClass (int column) { 
	return rowModel.getColumnClass(column); 
    }

    /*
     * Each column can also be identified by a label.  This label
     * serves as a short-hand notation for used in formula.  The
     * label is similar to how Excel labels its columns:
     * A, B, ..., Z, AA, AB, ..., AZ, etc.
     */
    public String getColumnLabel (int column) {
	return Base26Codec.encode(column);
    }

    /*
     * Reverse lookup from column label to its index.
     */
    public int getColumnIndex (String label) {
	return (int)Base26Codec.decode(label);
    }

    protected int convertRowIndex (int row) {
	return currentSortOrder != null 
	    ? currentSortOrder.convertRowIndex(row) : row;
    }

    public boolean isCellEditable (int row, int column) {
	Row r = rows.get(convertRowIndex (row));
	return r.isCellEditable(column);
    }
    public boolean isEditable () { return isEditable; }
    public synchronized void setEditable (boolean editable) {
	this.isEditable = editable;
    }

    public void clearSortColumns () {
	sortOrderColumns.clear();
	currentSortOrder = null;
	fireTableDataChanged ();
    }

    public void addSortColumn (int column, SortDirection order) {
	for (SortOrderColumn c : sortOrderColumns) {
	    if (c.column == column) {
		return; // already sorted by this column
	    }
	}

	if (order != SortDirection.NONE) {
	    SortOrderColumn soc = new SortOrderColumn (column, order);
	    sortOrderColumns.add(soc);
	    currentSortOrder = soc;
	}
	fireTableDataChanged ();
    }

    public void setSortColumn (int column, SortDirection order) {
	clearSortColumns ();
	addSortColumn (column, order);
    }

    public void clearLastSort () {
	if (sortOrderColumns.isEmpty()) {
	    return;
	}

	int index = sortOrderColumns.size()-1;
	if (index == 0) {
	    clearSortColumns ();
	    return;
	}

	ListIterator<SortOrderColumn> listIter = 
	    sortOrderColumns.listIterator(index);
	// position current sort order to be the one right before the last
	//   sort order
	currentSortOrder = listIter.previous();
	listIter.next(); // skip over currentSortOrder
	SortOrderColumn soc = listIter.next();
	listIter.remove();  // remove last sort order
	currentSortOrder.sort();
    }

    public void dump (java.io.OutputStream os) {
	java.io.PrintStream ps = new java.io.PrintStream (os, true);
	int nrows = getRowCount(), ncols = getColumnCount ();
	ps.println(nrows + " "+ncols);
	if (ncols == 0) {
	    return;
	}
	ps.print("["+getColumnLabel (0)+":"+getColumnName (0)+"]");
	for (int c = 1; c < ncols; ++c) {
	    ps.print(" ["+getColumnLabel (c) + ":"+getColumnName(c)+"]");
	}
	ps.println();
	for (int r = 0; r < nrows; ++r) {
	    ps.print(r+":"+getValueAt (r, 0));
	    for (int c = 1; c < ncols; ++c) {
		ps.print(","+getValueAt(r,c));
	    }
	    ps.println();
	}
    }

    public static void main (String[] argv) throws Exception {
	EntityTableModel etm = new EntityTableModel ();
	final Comparator cmp = new Comparator () {
		public int compare (Object o1, Object o2) {
		    //System.err.println("comparing "+o1+" and "+o2);
		    if (o1 instanceof Number && o2 instanceof Number) {
			double d = ((Number)o1).doubleValue() 
			    - ((Number)o2).doubleValue();
			if (d < 0.) return -1;
			if (d > 0.) return 1;
			return 0;
		    }
		    return ((Comparable)o1).compareTo((Comparable)o2);
		}
	    };
	DefaultRowModel drm1 = new DefaultRowModel 
	    ("column 1", String.class) {
		public Comparator getComparator (int column) { return cmp; }
	    };

	etm.addColumn(drm1, new String[]{"one","two","three", "four"});
	DefaultRowModel drm2 = new DefaultRowModel 
	    ("column 2", Number.class) {
		public Comparator getComparator (int column) { return cmp; }
	    };
	etm.addColumn(drm2, new Number[]{new Integer (1), new Integer(2), 
					  new Double (3.1415926535),
					  new Double (3.1415926535),
					  new Long (0xdeadbeefl)});
	etm.addColumn(drm2, new Number[]{new Integer(1234), 
					  new Integer(999)});
	etm.setValueAt(new Float(2.71f), 3, 2);
	System.out.println("input matrix");
	etm.dump(System.out);

	//etm.addSortColumn(0, SortDirection.ASCENDING);
	etm.addSortColumn(1, SortDirection.DESCENDING);
	System.out.println("sorting columns ["+etm.getColumnLabel(1)
			   +":"+etm.getColumnName(1)+"]");
	etm.dump(System.out);

	etm.addSortColumn(2, SortDirection.ASCENDING);
	System.out.println("sorting columns ["+etm.getColumnLabel(1)
			   +":"+etm.getColumnName(1)+"] and ["
			   +etm.getColumnLabel(2)+":"
			   +etm.getColumnName(2)+"]");
	etm.dump(System.out);

	etm.clearLastSort();
	System.out.println("clearing last sort");
	etm.dump(System.out);

	etm.addSortColumn(2, SortDirection.DESCENDING);
	System.out.println("adding new sort order");
	etm.dump(System.out);

	System.out.println("setting cell 3,2..");
	etm.setValueAt(Math.PI, 3,2);
	etm.dump(System.out);

	etm.clearSortColumns();
	System.out.println("clear sorting");
	etm.dump(System.out);

	System.out.println("--");
	for (int r = 0; r < etm.getRowCount(); ++r) {
	    System.out.print(etm.getValueAt(r, 0));
	    for (int c = 1; c < etm.getColumnCount(); ++c) {
		System.out.print(" "+etm.getValueAt(r,c));
	    }
	    System.out.println();
	}

	Map<String, Number> map1 = new HashMap<String, Number>();
	map1.put("LogS", new Double(0.12345));
	map1.put("LogP", new Double(-0.1234));
	map1.put("LogS+LogP", new Double(0.));
	MapRowModel mrm = new MapRowModel();
	mrm.addColumn("LogS", Number.class);
	mrm.addColumn("LogP", Number.class);
	int column = etm.addColumn(mrm, map1);
	System.out.println("--");
	System.out.print(etm.getColumnName(0));
	for (int c = 1; c < etm.getColumnCount(); ++c) {
	    System.out.print(" "+etm.getColumnName(c));
	}
	System.out.println();
	for (int r = 0; r < etm.getRowCount(); ++r) {
	    System.out.print(etm.getValueAt(r, 0));
	    for (int c = 1; c < etm.getColumnCount(); ++c) {
		System.out.print(" "+etm.getValueAt(r,c));
	    }
	    System.out.println();
	}
	Map<String, Number> map2 = new HashMap<String, Number>();
	map2.put("LogS", new Double(1.1));
	map2.put("LogP", new Double(2.2));
	etm.setColumn(column, map2);

	System.out.println("--");
	for (int r = 0; r < etm.getRowCount(); ++r) {
	    System.out.print(etm.getValueAt(r, 0));
	    for (int c = 1; c < etm.getColumnCount(); ++c) {
		System.out.print(" "+etm.getValueAt(r,c));
	    }
	    System.out.println();
	}
    }
}
