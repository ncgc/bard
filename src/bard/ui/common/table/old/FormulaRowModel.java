// $Id$

package bard.ui.common.table.old;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.nfunk.jep.*;

public class FormulaRowModel implements RowModel, Comparator {

    class RowImpl implements Row {
	Map vars;
	Object value;

	RowImpl (Map vars) {
	    this.value = eval (vars);
	    this.vars = vars;
	}

	Object eval (Map vars) {
	    for (Object  e : vars.entrySet()) {
		Map.Entry me = (Map.Entry)e;
		Variable v = jep.getVar((String)me.getKey());
		if (v != null) {
		    v.setValue(me.getValue());
		}
		else {
		    return null;
		}
	    }
	    return jep.getValueAsObject();
	}

	public Object getSource () { return vars; }
	public Object getValueAt (int column) { return value; }
	public Status getStatus () { return Status.ROW_MODIFIED; }
	public boolean isCellEditable (int column) { return false; }
	public RowModel getModel () { return FormulaRowModel.this; }
	public void setValueAt (Object value, int column) {}
    }

    private JEP jep;
    private String formula;
    private String name;
    private Comparator cmp;
    
    public FormulaRowModel () {
	jep = new JEP ();
	jep.addStandardConstants();
	jep.addStandardFunctions();
	jep.setAllowUndeclared(true);
	jep.setAllowAssignment(true);
    }

    public void setName (String name) {
	this.name = name;
    }
    public String getName () { return name; }

    public void setFormula (String formula) {
	Node node = jep.parseExpression(formula);
	if (node == null) {
	    throw new IllegalArgumentException 
		("Bad formula: "+formula+"; "+jep.getErrorInfo());
	}
	this.formula = formula;
    }
    public String getFormula () { return formula; }

    public Row createRow (Object value) {
	if (value instanceof Map) {
	    return null;
	}
	return new RowImpl ((Map)value);
    }

    /*
     * TODO: this should depend on the formula; e.g., if a vector
     * is returned, the the number of columns should be the length
     * of the vector!
     */
    public int getColumnCount () { return 1; }
    public Class getColumnClass (int column) { return Number.class; }
    public String getColumnName (int column) { 
	return name != null ? name : formula;
    }

    public Comparator getComparator (int column) { 
	return cmp == null ? this : cmp;
    }
    public void setComparator (int column, Comparator cmp) {
	this.cmp = cmp;
    }

    public int compare (Object o1, Object o2) {
	if (o1 == null && o2 == null) return 0;
	if (o1 == null || o2 == null) return 1;
	Number n1 = (Number)o1, n2 = (Number)o2;
	double d = n1.doubleValue() - n2.doubleValue();
	if (d < 0.) return -1;
	if (d > 0.) return 1;
	return 0;
    }
}
