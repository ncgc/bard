// $Id$

package bard.ui.common.table.old;

public interface Row {
    public enum Status {
	ROW_OK,
	    ROW_MODIFIED,
	    ROW_NOEDIT
    }

    public Object getSource ();
    public Object getValueAt (int column);
    public void setValueAt (Object value, int column);
    public boolean isCellEditable (int column);
    public Status getStatus ();
    public RowModel getModel ();
}
