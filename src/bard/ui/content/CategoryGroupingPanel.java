// $Id$

package bard.ui.content;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoundedRangeModel;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.jdesktop.swingx.painter.BusyPainter;

import bard.ui.event.ContentChangeEvent;
import bard.ui.explodingpixels.macwidgets.BottomBar;
import bard.ui.explodingpixels.macwidgets.ITunesNavigationHeader;
import bard.ui.explodingpixels.macwidgets.plaf.HudButtonUI;
import bard.ui.explodingpixels.macwidgets.plaf.HudComboBoxUI;
import bard.ui.explodingpixels.macwidgets.plaf.HudPaintingUtils;
import bard.ui.icons.IconFactory;


public class CategoryGroupingPanel extends JPanel 
    implements ActionListener, View, EventSubscriber<ContentChangeEvent> {

    static private Logger logger = Logger.getLogger
	(CategoryGroupingPanel.class.getName());

    static private String UNGROUP = "Ungroup";

    /*
     * Maximum number of categories
     */
    private static final int MAX_CATEGORIES = 5; 

    private JComboBox catCombo;
    private JToggleButton ungroupToggle;
    private JPanel togglePanel;
    private JPanel contentPanel;
    private ITunesNavigationHeader hudPanel;
    private CategoryGroupingModel model;
    protected Content content;

    /*
     * Default grid for ungrouped entities
     */
    private String currentGroup = null;
    private CategoryView currentCategory = null;
    private PropertyChangeSupport props;

    private javax.swing.Timer busyTimer = new javax.swing.Timer(1000, this);
    BusyPainter painter;

    public CategoryGroupingPanel (Content content) {
	this.content = content;
	initUI ();
    }

    public CategoryGroupingPanel (Content content, 
				  CategoryGroupingModel model) {
	this (content);
	setModel (model);
    }

    protected void initUI () {
	busyTimer.setRepeats(true);
	EventBus.subscribe(ContentChangeEvent.class, this);
	
	setOpaque (false);
	setLayout (new BorderLayout ());

	hudPanel = new ITunesNavigationHeader ();
	hudPanel.setLayout(new BorderLayout ());
	{ JPanel p = new JPanel ();
	    p.setOpaque(false);
	    catCombo = new JComboBox (new DefaultComboBoxModel ());
	    catCombo.setUI(new HudComboBoxUI ());
	    p.add(catCombo);
	    hudPanel.add(p, BorderLayout.WEST);
	}
	{
	    JPanel p = new JPanel (new FlowLayout (FlowLayout.CENTER));
	    p.setOpaque(false);
	    togglePanel = new JPanel (new GridLayout ());
	    togglePanel.setOpaque(false);
	    ((GridLayout)togglePanel.getLayout()).setHgap(-1);
	    p.add(togglePanel);
	    hudPanel.add(p);
	}
	{
	    JPanel p = new JPanel ();
	    p.setOpaque(false);
	    ungroupToggle  = createToggle ("Ungroup");
	    p.add(ungroupToggle);
	    hudPanel.add(p, BorderLayout.EAST);
	}
	add (hudPanel, BorderLayout.NORTH);

	final Dimension dim = new Dimension (50, 50);
	painter = new BusyPainter
	    (new Rectangle2D.Float(0,0,13.500001f,4.2f),
	     new Ellipse2D.Float(7.5f,7.5f,35.0f,35.0f));
	painter.setTrailLength(4);
	painter.setPoints(12);
	painter.setFrame(-1);

	AbstractLayerUI layerUI = new AbstractLayerUI () {
		@Override
		protected void paintLayer (Graphics2D g2, JXLayer layer) {
		    super.paintLayer(g2, layer);
		    if (isBusy ()) {
			JComponent c = (JComponent)layer.getView();
			Dimension rect = layer.getSize();
			int x = (rect.width - dim.width)/2;
			int y = (rect.height - dim.height)/2;
			g2.translate(x, y);
			painter.paint(g2, null, dim.width, dim.height);
		    }
		}
	    };

	/*
	java.util.Timer busyTimer = 
	    new java.util.Timer ("Category grouping timer", true);
	busyTimer.scheduleAtFixedRate(new java.util.TimerTask () {
		public void run () {
		    if (isBusy ()) {
			int frame = painter.getFrame() + 1;
			if (frame > painter.getPoints()) {
			    frame = 0;
			}
			painter.setFrame(frame);
			contentPanel.repaint();
		    }
		}
	    }, 0, 100);
	*/

	contentPanel = new JPanel (new CardLayout ());
	JXLayer layer = new JXLayer (contentPanel, layerUI);
	add (layer);

	catCombo.addActionListener(this);
	ungroupToggle.addActionListener(this);
    }

    public void onEvent (ContentChangeEvent e) {
	if (e.getContent() != content) {
	    return;
	}

	if (e.getName().equals(ContentChangeEvent.BUSY)) {
	    int value = (Integer)e.getNewValue();
	    if (value == 0 && busyTimer.isRunning()) {
		busyTimer.stop();
	    }
	    else if (value > 0 && !busyTimer.isRunning()) {
		busyTimer.start();
	    }
	}
    }

    protected JToggleButton createToggle (String name) {
	JToggleButton toggle = new JToggleButton (name);
	toggle.setUI(new HudButtonUI 
		     (HudPaintingUtils.Roundedness.RECTANGLE_BUTTON));
	return toggle;
    }

    protected void setupUI (CategoryGroupingModel model) {
	String[] groups = model.getCategoryGroups();
	contentPanel.removeAll();
	    
	catCombo.removeAllItems();
	remove (hudPanel);
	if (groups.length > 0) {
	    for (String g : groups) {
		catCombo.addItem(g);
	    }
	    catCombo.setSelectedIndex(0);
	    add (hudPanel, BorderLayout.NORTH);
	}
	else {
	    contentPanel.add(getUngroupComponent (), UNGROUP);
	}
	validate ();
    }

    public void setModel (CategoryGroupingModel model) {
	CategoryGroupingModel old = this.model;
	this.model = model;
	setupUI (model);
    }

    protected void show (String name) {
	((CardLayout)contentPanel.getLayout()).show(contentPanel, name);
    }

    public CategoryGroupingModel getModel () { return model; }

    public void actionPerformed (ActionEvent e) {
	Object source = e.getSource();
	if (source == catCombo) {
	    String group = (String)catCombo.getSelectedItem();
	    CategoryView[] categories = model.getCategories(group);
	    if (categories.length > MAX_CATEGORIES) {
		logger.log(Level.WARNING, "Group \""+group+"\" has "
			   +categories.length + " categories; only "
			   +MAX_CATEGORIES+" will be displayed!");
	    }
	    setCategories (categories);
	    currentGroup = group;
	    EventBus.publish(new CategoryGroupingEvent (this, group, null));
	}
	else if (source == ungroupToggle) {
	    show (UNGROUP);
	    currentCategory = null;
	    EventBus.publish(new CategoryGroupingEvent (this, true));
	}
	else if (source == busyTimer) {
	    int frame = painter.getFrame() + 1;
	    if (frame > painter.getPoints()) {
		frame = 0;
	    }
	    painter.setFrame(frame);
	    contentPanel.repaint();
	}
    }
    
    public CategoryView getSelectedCategory () { return currentCategory; }
    public String getSelectedGroup () { return currentGroup; }

    /*
     * subclass should override to return a component when the user
     * client on the ungroup button
     */
    protected Component getUngroupComponent () {
	return new JPanel ();
    }

    protected void setCategories (CategoryView[] categories) {
	togglePanel.removeAll();

	int ncat = Math.min(MAX_CATEGORIES, categories.length);
	((GridLayout)togglePanel.getLayout()).setColumns(ncat);

	currentCategory = null;
	ButtonGroup bg = new ButtonGroup ();
	for (int i = 0; i < ncat; ++i) {
	    final CategoryView cat = categories[i];
	    JToggleButton toggle = createToggle (cat.getName());

	    // initialize this category with the corresponding Content 
	    cat.setContent(content);
	    contentPanel.add(cat.getComponent(), cat.getName());

	    toggle.addActionListener(new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			currentCategory = cat;
			show (cat.getName());
			EventBus.publish(new CategoryGroupingEvent 
					 (CategoryGroupingPanel.this, 
					  getSelectedGroup (), cat));
		    }
		});
	    togglePanel.add(toggle);
	    bg.add(toggle);
	}

	bg.add(ungroupToggle);
	ungroupToggle.setSelected(true);
	contentPanel.add(getUngroupComponent (), UNGROUP);
	show (UNGROUP);

	hudPanel.invalidate();
    }

    protected boolean isBusy () {
	return currentCategory != null && currentCategory.isBusy();
    }

    /*
     * View interface
     */
    public Type getType () { return Type.Grid; }
    public Icon getIcon () { return IconFactory.View.Grid.getIcon(); }
    public String getName () { return "Grid"; }
    public String getDescription () { return "Grid view"; }
    public Component getComponent () { return this; }
    public BottomBar getBottomBar () { return null; }
    public BoundedRangeModel getBoundedRangeModel () { return null; }
    public Adjustable getAdjustable () { return null; }
}
