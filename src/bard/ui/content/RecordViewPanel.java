package bard.ui.content;

import bard.core.Assay;
import bard.core.Biology;
import bard.core.Compound;
import bard.core.Entity;
import bard.core.EntityNamedSources;
import bard.core.EntityServiceManager;
import bard.core.EntityValue;
import bard.core.Experiment;
import bard.core.ExperimentService;
import bard.core.MolecularData;
import bard.core.MolecularValue;
import bard.core.Project;
import bard.core.ServiceIterator;
import bard.core.StringDisplayValue;
import bard.core.Value;
import bard.core.adapter.CompoundAdapter;
import bard.core.adapter.EntityAdapter;
import bard.core.adapter.ProjectAdapter;
import bard.ui.common.ActivityCellRenderer;
import bard.ui.common.Grid;
import bard.ui.common.GridCellAnnotator;
import bard.ui.common.LabelRenderer;
import bard.ui.common.MolCellEditor;
import bard.ui.common.MolCellRenderer;
import bard.ui.content.plugin.BadAppleUNM;
import bard.ui.content.plugin.SMARTCyp;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.StateChangeEvent;
import bard.ui.explodingpixels.macwidgets.BottomBar;
import bard.ui.explodingpixels.macwidgets.ITunesNavigationHeader;
import bard.ui.explodingpixels.macwidgets.MacFontUtils;
import bard.ui.explodingpixels.macwidgets.plaf.HudButtonUI;
import bard.ui.explodingpixels.macwidgets.plaf.HudPaintingUtils;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import bard.util.BardHttpClient;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import chemaxon.formats.MolImporter;
import chemaxon.sss.search.MolSearch;
import chemaxon.struc.Molecule;
import com.jidesoft.dialog.JideOptionPane;
import com.jidesoft.swing.JideTabbedPane;
import com.jidesoft.swing.SimpleScrollPane;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RecordViewPanel extends JPanel 
    implements View, ActionListener, 
	       EventSubscriber,
	       ListEventListener, HyperlinkListener {

    static private Logger logger = Logger.getLogger
	(RecordViewPanel.class.getName());

    static final Icon LEFT = IconFactory.Misc.Backward.getIcon();
    static final Icon RIGHT = IconFactory.Misc.Forward.getIcon();

    static final String HTML = "<body style=\"font: 10px 'Lucida Grande',"
	+"Verdana, Helvetica, Arial, Geneva, sans-serif; color: #333;\">";

    static final Border BORDER = 
	BorderFactory.createLineBorder(new Color (0xa5a5a5));


    interface UIRecord<T> {
	void clear ();
	void update (T e);
	JComponent component ();
	Component trailer (); // trailing component for tab-based
    }

    public enum CurationStatus {
	No (IconFactory.Misc.StarEmpty.getIcon(), null,
	    "This record has not been curated; "
	    +"click the icon to curate this record."),

	    /*
	    Partial (RecordViewPanel.getIcon("Icon.question_frame"),
		     RecordViewPanel.getIcon("Icon.question"),
		          "This record is partially curated"),
	    */

	    Yes (IconFactory.Misc.Star.getIcon(),
		 null, "This record has been curated");

	final Icon icon;
	final Icon rollover;
	final String mesg;
	CurationStatus (Icon icon, Icon rollover, String mesg) {
	    this.icon = icon;
	    this.rollover = rollover;
	    this.mesg = mesg;
	}
	Icon getIcon () { return icon; }
	Icon getRollover () { return rollover; }
	String getMesg () { return mesg; }
    }

    static void addSeparator (JPanel pane, String title) {
	pane.add(label (title), "gapbottom 1, span, split 2, aligny center");
	pane.add(new JSeparator (), "gapleft rel,growx");
    }

    static JComponent label (String l) { 
	JLabel label = new JLabel (l); 
	label.setFont(MacFontUtils.ITUNES_TABLE_HEADER_FONT);
	return label;
    }
    
    static JTextField field () { 
	JTextField tf = new JTextField ();
	tf.setEditable(false);
	tf.setOpaque(false);
	return tf;
    }

    static class TrailerPane extends JPanel 
	implements javax.swing.plaf.UIResource {

	TrailerPane () {}
	TrailerPane (LayoutManager layout) {
	    super (layout);
	}
    }

    static abstract class UIRecordPanel<T> extends JPanel 
        implements UIRecord<T> {
        boolean busy;
        LabelRenderer labeler = new LabelRenderer ();
        JXLayer layer;

	UIRecordPanel () {
            this (new BorderLayout ());
	}
	UIRecordPanel (LayoutManager layout) {
	    super (layout);
	    setBackground (Color.white);

	    labeler.setLabelFilled(false);
	    labeler.setLabelFont
		(labeler.getLabelFont().deriveFont(Font.BOLD));

	    setBackground (Color.white);
            layer = new JXLayer (this, new AbstractLayerUI () {
                    @Override
                    protected void paintLayer (Graphics2D g2, JXLayer layer) {
                        super.paintLayer(g2, layer);
                        if (isBusy ()) {
                            Rectangle r = layer.getBounds();
                            labeler.renderLabel(g2, r.height/2, 
                                                5, 5, r.width, r.height, 
                                                "Loading...");
                        }
                    }
		});
	}

	public void setBusy (boolean busy) {
	    this.busy = busy;
	    repaint ();
	}
	public boolean isBusy () { return busy; }

	public JComponent component () { return layer; }
	public TrailerPane trailer () { return null; }
    }

    static String concat (String... tokens) {
	if (tokens != null && tokens.length > 0) {
	    StringBuilder sb = new StringBuilder (tokens[0]);
	    for (int i = 1; i < tokens.length; ++i) {
		if (tokens[i] != null) {
		    sb.append(" "+tokens[i]);
		}
	    }
	    return sb.toString();
	}
	return "";
    }
    
    static void recurseAnnotations(DefaultListModel model, ArrayList<String> prefix, Value v) {
        if (v.getValue() != null) {
            StringBuffer sb = new StringBuffer();
            for (String s: prefix) {
                sb.append(s+"/");
            }
            sb.append(v.getId());
            sb.append(":");
            sb.append(v.getValue());
            if (v instanceof StringDisplayValue) {
                StringDisplayValue sdv = (StringDisplayValue)v;
                if (sdv.getDisplay() != sdv.getValue() && sdv.getURL() != sdv.getDisplay())
                    sb.append(" | "+sdv.getDisplay()+" |");
            }
            sb.append(" [");
            sb.append(v.getSource().getName());
            sb.append(" ");
            sb.append(v.getSource().getVersion());
            sb.append("] ");
            sb.append(v.getURL() == null ? (v.getSource().getURL() == null ? "" : v.getSource().getURL()) : v.getURL());
            if (v.getChildCount() == 0)
                model.addElement(sb.toString());
            else {
                prefix.add(v.getId()+":"+v.getValue());
                for (Value child: v.getChildren()) {
                    recurseAnnotations(model, prefix, child);
                }
                prefix.remove(prefix.size()-1);
            }
            return;
        }
    }
    
    public void hyperlinkUpdate (HyperlinkEvent e) {
	//URL url = e.getURL();
        java.net.URI uri = UIUtil.getLinkURI(e);
	if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
	    logger.info("launching "+uri.toString());
	    try {
		//new BrowserLauncher().openURLinBrowser(uri.toString());
		EventBus.publish(e);
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Can't launch external browser "+uri, ex);
	    }
	}
	else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
	    JComponent c = (JComponent)e.getSource();
	    c.setToolTipText(uri != null ? uri.toString() : null);
	}
    }
    
//    class LinkMouseListener extends MouseAdapter {
//        public void mouseClicked(java.awt.event.MouseEvent evt) {
//            JLabel l = (JLabel) evt.getSource();
//            String linkText = l.getText();
//            linkText = linkText.substring(linkText.indexOf("<a href=\"")+9);
//            linkText = linkText.substring(0, linkText.indexOf("\">"));
//            try {
//                java.net.URI uri = new java.net.URI(linkText);
//                (RecordViewPanel.this).hyperlinkUpdate(new HyperlinkEvent(l, HyperlinkEvent.EventType.ACTIVATED, uri.toURL()));
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//    }
//    
    JEditorPane createEP () {
	return createEP (null);
    }
        
    JEditorPane createEP (String title) {
	JEditorPane ep = new JEditorPane ();
	ep.setContentType("text/html");
	ep.setEditable(false);
	ep.setOpaque(false);
	ep.addHyperlinkListener(this);
	if (title != null) {
	    ep.setBorder(BorderFactory.createTitledBorder
			 (BORDER, title, TitledBorder.LEADING, 
			  TitledBorder.DEFAULT_POSITION, 
			  MacFontUtils.ITUNES_TABLE_HEADER_FONT));
	}
	    /*
	    ep.setBorder
		(BorderFactory.createCompoundBorder
		 (BorderFactory.createLineBorder(new Color (0xa5a5a5)),
		  BorderFactory.createEmptyBorder(2,1,1,1)));
	    */
	return ep;
    }
    
    static JList createList () {
	return new JList (new DefaultListModel ()) {
		public String getToolTipText (MouseEvent e) {
		    JList l = (JList)e.getSource();
		    int index = l.locationToIndex(e.getPoint());
		    if (index >= 0) {
			Object value = l.getModel().getElementAt(index);
			if (value != null) {
			    return value.toString();
			}
		    }
		    return null;
		}
	    };
    }

    static JScrollPane createSimpleScrollPane (JComponent c) {
	SimpleScrollPane sp = new SimpleScrollPane 
	    (c, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	     JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	sp.setOpaque(false);
	sp.getViewport().setOpaque(false);
	sp.setBorder(BORDER);
	return sp;
    }
    
    static JScrollPane createSimpleScrollPane (String title, JComponent c) {
	SimpleScrollPane sp = new SimpleScrollPane 
	    (c, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
	     JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	sp.setOpaque(false);
	sp.getViewport().setOpaque(false);
	sp.setBorder(BorderFactory.createTitledBorder
		     (BORDER, title, TitledBorder.LEADING, 
		      TitledBorder.DEFAULT_POSITION, 
		      MacFontUtils.ITUNES_TABLE_HEADER_FONT));
	return sp;
    }
    
    JScrollPane createScrollPane (JComponent c) {
	JScrollPane sp = UIUtil.iTunesScrollPane(c);
	sp.setOpaque(false);
	sp.getViewport().setOpaque(false);
	sp.setHorizontalScrollBarPolicy
	    (JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	sp.setBorder(BORDER);
	return sp;
    }
    
    static JScrollPane createScrollPane (String title, JComponent c) {
	JScrollPane sp = UIUtil.iTunesScrollPane(c);
	sp.setOpaque(false);
	sp.getViewport().setOpaque(false);
	sp.setHorizontalScrollBarPolicy
	    (JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	sp.setBorder(BorderFactory.createTitledBorder
		     (BORDER, title, TitledBorder.LEADING, 
		      TitledBorder.DEFAULT_POSITION, 
		      MacFontUtils.ITUNES_TABLE_HEADER_FONT));
	return sp;
    }

    static JSplitPane createSplitPane (int which) {
	JSplitPane split = new JSplitPane (which);
	split.setDividerSize(4);
        split.setContinuousLayout(true);
        split.setBorder(BorderFactory.createEmptyBorder());
	return split;
    }


    class RelatedRecordLoader<T extends Entity, E extends Entity>
        extends SwingWorker<java.util.List<E>, Void> {
	UIRelatedRecordPanel<T, E> related;
	T entity;

	RelatedRecordLoader (UIRelatedRecordPanel<T, E> related, T entity) {
	    this.related = related;
	    this.entity = entity;
	}

	@Override
	protected java.util.List<E> doInBackground () {
            return related.getRelatedEntities(entity);
	}

	@Override
	protected void done () {
	    try {
		JTabbedPane tab = getRelatedContentTab ();
		int i = 0;
		for (; i < tab.getTabCount(); ++i) {
		    if (related == tab.getComponentAt(i)) {
			break;
		    }
		}
		
		java.util.List<E> entities = get ();
                related.setEntities(entities);
                /*
                tab.setTitleAt(i, related.getRelatedContent().getName()
                               + (entities.isEmpty() ? "" : 
                                  " ("+entities.size()+")"));
                */
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Can't get related entities", ex);
	    }
	    related.setBusy(false);
	}
    }
		

    class UIRelatedRecordPanel<T extends Entity, E extends Entity> 
        extends UIRecordPanel<T> implements HyperlinkListener {
	Cache cache;
	Content<T> content;

	java.util.List<E> entities = new ArrayList<E>();
        Class<E> relatedClass;

	T anchor = null; // anchor entity
	JXTaskPaneContainer taskpane;
	LabelRenderer labeler = new LabelRenderer ();
	boolean busy = false;
	
	UIRelatedRecordPanel (Content<T> content, Class<E> clazz) {
            this.relatedClass = clazz;
	    this.content = content;

            String name = content.getName()+"::"+clazz.getName();
	    cache = CacheManager.getInstance().getCache(name);
	    if (cache == null) {
                logger.info("Creating cache "+name+"...");
		cache = new Cache (name,
				   10, // max elms in memory
				   false, // overflow to disk?
				   false, // live forever?
				   60, // time to live (seconds)
				   60); // time to idel (seconds)
		CacheManager.getInstance().addCache(cache);
	    }

	    labeler.setLabelFilled(false);
	    labeler.setLabelFont
		(labeler.getLabelFont().deriveFont(Font.BOLD));

	    setBackground (Color.white);
	    setLayout (new BorderLayout ());
	    taskpane = new JXTaskPaneContainer ();

	    JPanel panel = new JPanel (new BorderLayout ());
	    panel.add(createScrollPane (taskpane));

	    JXLayer layer = new JXLayer (panel, new AbstractLayerUI () {
		    @Override
		    protected void paintLayer (Graphics2D g2, JXLayer layer) {
			super.paintLayer(g2, layer);
			if (isBusy ()) {
			    Rectangle r = layer.getBounds();
			    labeler.renderLabel(g2, r.height/2, 
						5, 5, r.width, r.height, 
						"Loading...");
			}
		    }
		});
	    add (layer);
	}

	public void clear () {
            entities.clear();
	    taskpane.removeAll(); // remove all components
	    repaint ();
	}

	public void update (T entity) {
	    this.anchor = entity; // current anchor entity

	    setBusy (true);
	    new RelatedRecordLoader (this, entity).execute();
	    //logger.info(entity.getId() + " => "+links.length);
	}

	void setEntities (java.util.List<E> entities) {
	    clear ();
            if (entities != null && !entities.isEmpty()) {
                this.entities.addAll(entities);

                JXTaskPane first = null;
                for (Entity e : entities) {
                    JComponent c = createComponent (e);
                    if (c != null) {
                        JXTaskPane tp = createTaskPane (e.getName());
                        if (first == null) {
                            first = tp;
                        }
                        tp.add(c);
                        tp.setCollapsed(false); // !!! for debugging
                        taskpane.add(tp);
                    }
                }
                if (first != null) {
                    first.setCollapsed(false);
                }
            }
	}

	java.util.List<E> getRelatedEntities (T e) {
            java.util.List list;
	    Element el = cache.get(e.getId());
	    if (el == null) {
                list = new ArrayList<E>();
		try {
                    ServiceIterator<E> iter = getContent().getService()
                        .iterator(e, getRelatedClass ());
                    /*
                    while (iter.hasNext()) {
                        list.add(iter.next());
                    }
                    */
                    if (iter != null) {
                	list.addAll(iter.next(10));
                	iter.done();
                    }

                    cache.put(new Element (e.getId(), list));
                    logger.info(Thread.currentThread()+" "
                                +e.getClass()+" "+e.getId()+": "+list.size()
                                +" "+getRelatedClass ());
		}
		catch (Exception ex) {
		    logger.log(Level.SEVERE, "Can't get related content for "
			       +e.getId(), ex);
		}
	    }
	    else {
		list = (java.util.List)el.getObjectValue();
	    }
	    return list;
	}

	// override by subclass
	JComponent createComponent (Entity e) {
	    return null;
	}

	public int getCount () { return entities.size(); }
	public Content<T> getContent () { return content; }
        public Class<E> getRelatedClass () { 
            return relatedClass; 
        }

	JXTaskPane createTaskPane (String title) {
	    JXTaskPane tp = new JXTaskPane ();
	    tp.setLayout(new BorderLayout ());
	    tp.setTitle(title);
	    tp.setCollapsed(true);
	    tp.setScrollOnExpand(true);
	    ((JComponent)tp.getContentPane()).setBackground(Color.white);
	    return tp;
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent e) {
        java.net.URI uri = UIUtil.getLinkURI(e);
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
        	//logger.info("hyperlink "+uri);
        	try {
        		EventBus.publish(e);
        	}
        	catch (Exception ex) {
        		logger.log(Level.SEVERE, "Invalid hyperlink "+uri, ex);
        	}
        }
        else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
        	JComponent c = (JComponent)e.getSource();
        	c.setToolTipText(uri.toString());
        }
	}
    }

    class UIRelatedProjectRecordPanel<T extends Entity> 
        extends UIRelatedRecordPanel<T, Project> {

	UIRelatedProjectRecordPanel (Content<T> content) {
	    super (content, Project.class);
	}

	JComponent createComponent (Entity e) {
	    if (!(e instanceof Project)) {
		throw new IllegalArgumentException
		    ("Entity is not of type Project!");
	    }
	    Project project = (Project)e;

	    StringBuilder sb = new StringBuilder (UIUtil.CSS);

	    sb.append("<a href=\"bard://"
                +e.getClass().getSimpleName().toLowerCase()+"s"
                +"/"+e.getId()+"?view="
                +bard.ui.content.View.Type.Record+"\">Go to project ...</a><br><br>");
	    
        Value nprobeValue = e.getValue(Project.NumberOfProbesValue);
        Integer nprobes = 0;
        if (nprobeValue != null) nprobes = (Integer) nprobeValue.getValue();
        if (nprobes > 0) sb.append("<b>"+nprobes+" Probes declared</b><br><br> ");
        
        final Vector<String> biologyRank = new Vector<String>();
        biologyRank.add("species");
        biologyRank.add("protein");
        //biologyRank.add("gene");
        StringBuffer string = new StringBuffer();
        int rank = -1;
        for (Value v : project.getValues()) {
            String entry = v.getValue().toString();
            if (entry.indexOf('|') > -1) entry = entry.substring(entry.lastIndexOf('|') + 1);
            if (biologyRank.indexOf(v.getId()) == rank && rank > -1) {
                if (string.length() > 0) string.append("<br> ");
                string.append(entry);
            } else if (biologyRank.indexOf(v.getId()) > rank) {
                rank = biologyRank.indexOf(v.getId());
                string = new StringBuffer(entry);
            }
        }
        if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
        else if (project.getTargets().size() > 0) {
            for (Biology biology : project.getTargets()) {
                if (string.indexOf(biology.getName()) == -1) {
                    if (string.length() > 0) string.append("<br> ");
                    string.append(biology.getName());
                }
            }
        }
        if (string.length() == 0) {
            for (Value v : project.getValues("species name")) {
                if (string.indexOf(v.getValue().toString()) == -1 && !"Homo sapiens".equals(v.getValue().toString())) {
                    if (string.length() > 0) string.append("<br> ");
                    string.append(v.getValue().toString());
                }
            }
        }
        if (string.length() > 0) {
        	string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
            sb.append(string.toString());
            sb.append("<br> ");
        }
        
        if (project.getDescription() != null) {
            sb.append("<br> ");
            sb.append(project.getDescription().replace("\n", "<br>"));
            sb.append("<br> ");
        }
        
        ProjectAdapter pa = new ProjectAdapter(project);
        Collection<Value> annos = pa.getAnnotations(); //!!! needs to handle lazy loading as well
        for (Value v : annos) {
            if (v.getSource().getName().equals(EntityNamedSources.KEGGDiseaseCategoryAnnotationSource)) {
                sb.append(v.getValue().toString()+"<br> ");
            }
        }

	    JEditorPane ep = createEP ();
        ep.addHyperlinkListener(this);
	    ep.setText(sb.toString());

	    return ep;
	}
    }

    class UIRelatedBiologyRecordPanel<T extends Entity> extends UIRelatedRecordPanel<T, Biology> {

        UIRelatedBiologyRecordPanel(Content<T> content) {
            super(content, Biology.class);
        }

        JComponent createComponent(Entity e) {
            if (!(e instanceof Biology)) {
                throw new IllegalArgumentException
                        ("Entity is not of type Biology!");
            }
            Biology biology = (Biology) e;

            StringBuilder sb = new StringBuilder(UIUtil.CSS);
            sb.append(biology.toString());
            JEditorPane ep = createEP();
            ep.setText(sb.toString());

            return ep;
        }
    }

    class UIRelatedExperimentRecordPanel<T extends Entity> 
        extends UIRelatedRecordPanel<T, Experiment> {

        UIRelatedExperimentRecordPanel(Content<T> content) {
            super(content, Experiment.class);
        }

        JComponent createComponent(Entity e) {
            if (!(e instanceof Experiment)) {
                throw new IllegalArgumentException
                        ("Entity is not of type Experiment!");
            }
            Experiment expt = (Experiment) e;

            StringBuilder sb = new StringBuilder(UIUtil.CSS);
            sb.append(expt.getDescription().replace("\n", "<br>"));
            JEditorPane ep = createEP();
            ep.setText(sb.toString());

            return ep;
        }
    }

    class ActivityRecordLoader<T extends Entity> 
        extends SwingWorker<Value[], Void> {

    	UIRelatedAssayRecordPanel<T> related;
        ActivityCellRenderer renderer;
    	Compound cmpd;
    	Assay assay;

    	ActivityRecordLoader (UIRelatedAssayRecordPanel<T> related, 
                              ActivityCellRenderer renderer,
                              Compound cmpd, Assay assay) {
            this.related = related;
            this.renderer = renderer;
            this.cmpd = cmpd;
            this.assay = assay;
    	}
        
    	@Override
    	protected Value[] doInBackground () {
    	    if (assay.getId() == null) {
    		System.err.println("yoho!");
    		return null;
    	    }
            return related.getActivityResponse(related.content.getServiceManager(), cmpd, assay);
    	}
        
    	@Override
    	protected void done () {
            try {
                Value[] activities = get ();
                if (activities != null)
                    renderer.setActivities(activities);
            }
            catch (Exception ex) {
                logger.log(Level.SEVERE, "Can't run plugin", ex);
            }
            related.setBusy(false);
    	}
    }

    static public Value[] getActivityResponse(EntityServiceManager esm, Compound cmpd, Integer[] exptIds) {
        ArrayList<Value> acts = new ArrayList<Value>();

        Long[] sids = new CompoundAdapter(cmpd, esm).getPubChemSIDs();
        ExperimentService es = esm.getService(Experiment.class);
        ServiceIterator<Value> iter = es.activities(exptIds, sids);
        while (iter.hasNext()) {
            Value v = iter.next();
            //System.out.println(" >> "+v);
            acts.add(v);
        }
        iter.done();

        logger.info("Compound "+cmpd.getId()+" and "
                +exptIds.length
                +" experiments; activites="+acts.size());

        return acts.toArray(new Value[0]);
    }

    class UIRelatedAssayRecordPanel<T extends Entity> 
        extends UIRelatedRecordPanel<T, Assay> {

	UIRelatedAssayRecordPanel (Content<T> content) {
	    super (content, Assay.class);
	}

	public Value[] getActivityResponse(EntityServiceManager esm, Compound cmpd, Assay assay) {
//            Collection<Experiment> exprs = assay.getExperimentIds();
//            if (exprs == null && assay.getId() != null) {
//                Content<Assay> asscon = 
//                    getContent().getContent(Assay.class);
//                
//                // get experiments
//                ArrayList<Experiment> exprsList = new ArrayList<Experiment>();
//                ServiceIterator<Experiment> iter = 
//                    asscon.getService().iterator(assay, Experiment.class);
//                while (iter.hasNext()) {
//                    Experiment expr = iter.next();
//                    exprsList.add(expr);
//                }
//                iter.done();
//                assay.setExperiments(exprsList);
//                
//                exprs = assay.getExperiments();
//            }
            
            // TODO Auto-generated method stub
//            ExperimentService es = 
//                (ExperimentService)getContent().getContent
//                (Experiment.class).getService();

            ArrayList<Value> acts = new ArrayList<Value>();
//            for (Experiment expr: assay.getExperiments()) {
//        	if (expr.getId().toString().equals("509") && cmpd.getId().toString().equals("5994"))
//        	    System.err.println("yoho!!!");


            Long[] sids = new CompoundAdapter(cmpd, esm).getPubChemSIDs();
            Collection<Value> exptLinks = assay.getValues(Experiment.class.toString());
            ArrayList<Integer> expts = new ArrayList<Integer>();
            for (Value val: exptLinks)
        	expts.add((Integer)val.getValue());
            Integer[] exptIds = expts.toArray(new Integer[0]);
            ExperimentService es = esm.getService(Experiment.class);
            ServiceIterator<Value> iter = es.activities(exptIds, sids);
                while (iter.hasNext()) {
                    Value v = iter.next();
                    //System.out.println(" >> "+v);
                    acts.add(v);
                }
            iter.done();

                //System.out.println("## "+expr.getId()+"."+cmpd.getId()+"..."
                //                   +iter.getConsumedCount());
            
            logger.info("Compound "+cmpd.getId()+" assay "
                        +assay.getId()+" has "+assay.getExperimentCount()
                        +" experiments; activites="+acts.size());
            
            return acts.toArray(new Value[0]);
	}

	JComponent createComponent (Entity e) {
	    if (!(e instanceof Assay)) {
		throw new IllegalArgumentException
		    ("Entity is not of type Assay!");
	    }
	    Assay assay = (Assay)e;
	    if (assay.getId() == null)
		System.err.println("yoho!!!");

	    StringBuilder sb = new StringBuilder (UIUtil.CSS);

	    sb.append("<a href=\"bard://"
                +e.getClass().getSimpleName().toLowerCase()+"s"
                +"/"+e.getId()+"?view="
                +bard.ui.content.View.Type.Record+"\">Go to assay ...</a><br><br>");
	    
	    final Vector<String> biologyRank = new Vector<String>();
	    biologyRank.add("species");
	    biologyRank.add("protein");
	    //biologyRank.add("gene");
	    StringBuffer string = new StringBuffer();
	    int rank = -1;
	    for (Value v : assay.getValues()) {
		String entry = v.getValue().toString();
		if (entry.indexOf('|') > -1) entry = entry.substring(entry.lastIndexOf('|') + 1);
		if (biologyRank.indexOf(v.getId()) == rank && rank > -1) {
		    if (string.length() > 0) string.append("<br> ");
		    string.append(entry);
		} else if (biologyRank.indexOf(v.getId()) > rank) {
		    rank = biologyRank.indexOf(v.getId());
		    string = new StringBuffer(entry);
		}
	    }
	    if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
	    else {
		for (Value val: assay.getValues(Biology.class.toString())) {
		    Biology biology = (Biology)assay.getEntityFromValue((EntityValue)val);
		    if (biology != null && string.indexOf(biology.getName()) == -1) {
			if (string.length() > 0) string.append("<br> ");
			string.append(biology.getName());
		    }
		}
	    }
	    if (string.length() == 0) {
		for (Value v : assay.getValues("species name")) {
		    if (string.indexOf(v.getValue().toString()) == -1 && !"Homo sapiens".equals(v.getValue().toString())) {
			if (string.length() > 0) string.append("<br> ");
			string.append(v.getValue().toString());
		    }
		}
	    }
	    if (string.length() > 0) {
		string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
		sb.append(string.toString());
		sb.append("<br> ");
	    }

	    //        sb.append("<br> ");
	    //        sb.append(assay.getDescription().replace("\n", "<br>"));
	    //        sb.append("<br> ");	    

	    JEditorPane ep = createEP ();
	    ep.addHyperlinkListener(this);
	    ep.setText(sb.toString());

	    if (anchor instanceof Compound) {
	    	
	    	JSplitPane split = new JSplitPane (JSplitPane.HORIZONTAL_SPLIT);
	    	split.setBackground(Color.white);
	    	split.setContinuousLayout(true);
	    	split.setBorder(BorderFactory.createEmptyBorder());
	    	split.setLeftComponent(ep);
	    	split.setResizeWeight(.5);
	    	split.setDividerSize(4);
	    	ActivityCellRenderer acr = new ActivityCellRenderer ();
	    	acr.setPreferredSize(new Dimension (150, 150));
	    	split.setRightComponent(acr);
	    	split.setResizeWeight(0.9);
	    	
        	new ActivityRecordLoader<T> 
                    (this, acr, (Compound)anchor, assay).execute();
                logger.info(assay.getId() + " Update activity record view ");

	    	return split;
	    }

	    return ep;
	}
    }

    class UIRelatedCompoundRecordPanel<T extends Entity>
            extends UIRelatedRecordPanel<T, Compound> {

        UIRelatedCompoundRecordPanel(Content<T> content) {
            super(content, Compound.class);
        }

        /*
         * FIXME!!
         */
        JComponent createComponent(Entity e) {
            if (!(e instanceof Compound)) {
                throw new IllegalArgumentException("Entity is not of type Compound!");
            }
            Compound c = (Compound) e;

            final Grid grid = createMolGrid();
            grid.addValue(c);
            grid.addComponentListener(new ComponentAdapter() {
                public void componentResized(ComponentEvent e) {
                    JComponent c = (JComponent) e.getSource();
                    Rectangle r = c.getBounds();
                    grid.setCellSize((int) (Math.min(r.width, r.height) * .8 + 0.5));
                }
            });

            //grid.setUseDefaultBackground(false);

            JSplitPane split = createSplitPane(JSplitPane.HORIZONTAL_SPLIT);
            split.setLeftComponent(grid);
            split.setRightComponent(createCompoundInfoPane(c));
            split.setResizeWeight(.1);

            return split;
        }

        Component createCompoundInfoPane(Compound c) {
            if (anchor instanceof Project) {
                Project project = (Project)anchor;
                JPanel panel = new JPanel();
                panel.setLayout(new GridLayout(0,1));
                HashMap<Integer, Experiment> exptMap = new HashMap<Integer, Experiment>();
                Integer[] expts = new Integer[project.getValues(Experiment.class.toString()).size()];
                int index = 0;
                for (Value value: project.getValues(Experiment.class.toString())) {
                    expts[index] = Integer.valueOf(value.getValue().toString());
                    exptMap.put(expts[index], ((Experiment)project.getEntityFromValue((EntityValue)value)));
                    index++;
                }
                Value[] values = getActivityResponse(content.getServiceManager(), c, expts);
                for (Value value: values) {
                    ActivityCellRenderer acr = new ActivityCellRenderer ();
                    acr.setPreferredSize(new Dimension (150, 150));
                    acr.setMaximumSize(new Dimension(250, 250));
                    Value[] acts = new Value[1];
                    acts[0] = value;
                    acr.setActivities(acts);
                    JSplitPane jsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
                    Integer exptId = Integer.valueOf(value.getChild("eid").getValue().toString());
                    String label = "<html><p>"+exptMap.get(exptId).getName();
                    String assayId = exptMap.get(exptId).getValue(Assay.AssayBARDID).getValue().toString();
                    label += " <br> <a href=\"bard://assays/"+assayId+"?view="+bard.ui.content.View.Type.Record+"\">Assay "+assayId+"</a>";
                    label += "</p></html>";
                    JEditorPane jlabel = new JEditorPane();
                    jlabel.setContentType("text/html");
                    jlabel.setEditable(false);
                    jlabel.setText(label);
                    jlabel.addHyperlinkListener(this);
                    jsp.setLeftComponent(jlabel);
                    jsp.setRightComponent(acr);
                    jsp.setDividerLocation(0.7);
                    jsp.setResizeWeight(0.7);
                    panel.add(jsp);
                }
                return panel;
            }
            
            JEditorPane ep = new JEditorPane ();
            ep.setContentType("text/html");
            ep.setEditable(false);
            ep.setOpaque(false);
            StringBuilder sb = new StringBuilder(UIUtil.CSS);
            sb.append("...&nbsp;<i><a href=\"bard://"
                            +c.getClass().getSimpleName().toLowerCase()+"s"
                            +"/"+c.getId()+"?view="
                            +bard.ui.content.View.Type.Record+"\">See full record</a></i>");
            ep.setText(sb.toString());
            ep.addHyperlinkListener(this);
            
            return ep;
        }
        
        
    }

    class RecordLayerUI extends AbstractLayerUI {
	LabelRenderer labeler = new LabelRenderer ();

	RecordLayerUI () {
	    labeler.setLabelFilled(false);
	    labeler.setLabelFont
		(labeler.getLabelFont().deriveFont(Font.BOLD));
	}

	@Override
	protected void paintLayer (Graphics2D g2, JXLayer layer) {
	    super.paintLayer(g2, layer);
	    if (message != null) {
		Rectangle r = layer.getBounds();
		labeler.renderLabel(g2, r.height/2, 
				    5, 5, r.width, r.height, message);
	    }
	}
    }
    
    class UIPluginsRecordPanel<T extends Entity> 
    extends UIRecordPanel<T> implements GridCellAnnotator {

	Content<T> content;

	T anchor = null; // anchor entity
	JXTaskPaneContainer taskpane;
	LabelRenderer labeler = new LabelRenderer ();
	boolean busy = false;

	ArrayList<Integer> score = new ArrayList<Integer>();
	ArrayList<JsonNode> info = new ArrayList<JsonNode>();
	int index = 0;

	Grid grid;
	JEditorPane ep;
	MolSearch molSearch = new MolSearch();
	
	UIPluginsRecordPanel (Content<T> content) {
	    this.content = content;

	    labeler.setLabelFilled(false);
	    labeler.setLabelFont
	    (labeler.getLabelFont().deriveFont(Font.BOLD));

	    setBackground (Color.white);
	    setLayout (new BorderLayout ());
	    taskpane = new JXTaskPaneContainer ();

	    JPanel panel = new JPanel (new BorderLayout ());
	    panel.add(createScrollPane (taskpane));

	    add (panel);
	}

	@Override
	public String getGridCellBadgeAnnotation(Grid g, Object value, int cell) {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getGridCellAnnotation (Grid g, Object v, int c) {
	    if (v == null) {
		return null;
	    }
	    Molecule cmpd = (Molecule)v;

	    String name = cmpd.getName();
	    return name;
	}

	Component initUI() {
	    grid = createMolGrid ();
	    grid.setCellAnnotator(this);

	    JPanel toggle = new JPanel();
	    JButton but1 = new JButton(IconFactory.getImageIcon("arrow-left-24.png", 16));
	    but1.addActionListener(new java.awt.event.ActionListener() {    
	          public void actionPerformed(java.awt.event.ActionEvent e) {
	              previous();
	          }
	     });

	    JButton but2 = new JButton(IconFactory.getImageIcon("arrow-right-24.png", 16));
	    but2.addActionListener(new java.awt.event.ActionListener() {    
	          public void actionPerformed(java.awt.event.ActionEvent e) {
	              next();
	          }
	     });

	    JButton but3 = new JButton(IconFactory.getImageIcon("question-octagon-frame.png", 16));
	    but3.addActionListener(new java.awt.event.ActionListener() {    
	          public void actionPerformed(java.awt.event.ActionEvent e) {
	              JideOptionPane.showMessageDialog(grid,
	        	      "BADAPPLE, the BARD Promiscuity Plugin, from UNM; returns evidence-based promiscuity scores.\n VERSION: 0.9alpha (Jan 2013)\n UNM BARD Team: Jeremy Yang, Anna Waller, Cristian Bologa, Oleg Ursu, Steve Mathias, Tudor Oprea, Larry Sklar\n database: carlsbad.health.unm.edu:5432:openchord:badapple2\n note: (none)");
	          }
	     });
	    
	    toggle.add(but1);
	    toggle.add(but2);
	    
	    JPanel bottomControl = new JPanel();
	    bottomControl.add(but3, BorderLayout.LINE_START);
	    bottomControl.add(toggle, BorderLayout.CENTER);
	    
	    if (score.size() < 2) but1.setEnabled(false);
	    if (score.size() < 2) but2.setEnabled(false);
	    
	    JPanel panel = new JPanel(new BorderLayout());
	    panel.add(grid, BorderLayout.CENTER);
	    panel.add(bottomControl, BorderLayout.SOUTH);
	    
	    JSplitPane split = createSplitPane (JSplitPane.HORIZONTAL_SPLIT);
	    split.setLeftComponent(panel);

	    ep = createEP ();
	    split.setRightComponent(ep);
	    split.setResizeWeight(.4);
	    
	    return split;
	}
	
	void next() {
	    index = index + 1;
	    if (index > info.size() - 1) index = 0;
	    updateToIndex();
	}
	
	void previous() {
	    index = index - 1;
	    if (index < 0) index = info.size() - 1;
	    updateToIndex();
	}
	
	void updateToIndex() {
	    grid.clear();
	    try {
		Molecule cmpd = (Molecule)((MolecularValue)((Compound)anchor).getValue(Compound.MolecularValue)).toFormat(MolecularData.Format.NATIVE);
		cmpd.clean(2, null);
		cmpd.setDim(2);
		Molecule clone = cmpd.cloneMolecule();
		clone.aromatize();
		molSearch.setTarget(clone);
		Molecule scaffold = MolImporter.importMol(info.get(index).get("scafsmi").asText());
		scaffold.aromatize();
		scaffold.clean(2, null);
		scaffold.setDim(2);
		molSearch.setQuery(scaffold);
		int[] matches = molSearch.findFirst();
		if (matches != null)
			for (int match: matches) {
				cmpd.getAtom(match).setAtomMap(match);
			}
		//MolHandler mh = new MolHandler();
		//mh.setMolecule(scaffold);
		//mh.align(cmpd, matches);
		cmpd.setName("Match");
		scaffold.setName("Scaffold");
		grid.addValue(cmpd);
		grid.addValue(scaffold);
	    } catch (Exception ex) {ex.printStackTrace();}
	    
	    int val = score.get(index);
            StringBuilder sb = new StringBuilder (UIUtil.CSS);
            
            String comment = "<font bgcolor=\"00FF00\">(low)</font>";
            if (val > 99) comment = "<font bgcolor=\"FFFF00\">(medium)</font>";
            if (val > 299) comment = "<font bgcolor=\"FF0000\">(high)</font>";
        	
            
            sb.append("<b>Score: "+val+"</b> &nbsp&nbsp&nbsp&nbsp&nbsp "+comment+" <br><br>");
            Iterator<String> si = info.get(index).getFieldNames();
            ArrayList<String> sia = new ArrayList<String>();
            while (si.hasNext()) {
        	sia.add(si.next());
            }
            String[] sta = sia.toArray(new String[0]);
            Arrays.sort(sta);
            for (String field: sta) {
        	String entry = info.get(index).get(field).asText();
        	sb.append(field+": "+entry+"<br>");
            }
            ep.setText(sb.toString());
            return;
	}
	
	Object[] getPluginResponse (Entity e) {        
	    if (!(e instanceof Compound)) {
		throw new IllegalArgumentException
		    ("Entity is not of type Compound!");
	    }
	    ArrayList<Integer> score = new ArrayList<Integer>();
	    ArrayList<JsonNode> info = new ArrayList<JsonNode>();
	    BardHttpClient httpclient = new BardHttpClient("pluginResponse");
//	    DefaultHttpClient httpclient = new DefaultHttpClient();
            try {
        	if (e.getValue(Compound.PubChemCIDValue) == null) return null;
        	String cid = e.getValue(Compound.PubChemCIDValue).getValue().toString();
        	String url = "http://bard.nih.gov/api/latest/plugins/badapple/prom/cid/"+cid;
        	HttpGet get = new HttpGet(url);
        	HttpResponse response = httpclient.execute(get);
        	HttpEntity entity = response.getEntity();
        	if (entity != null && response.getStatusLine()
        		.getStatusCode() == 200) {
        	    InputStream is = entity.getContent();
        	    ObjectMapper mapper = new ObjectMapper();
        	    JsonNode root = mapper.readTree(is);
                if (root == null) return null;

                JsonNode scaffIds = root.get("hscafs");
                if (scaffIds == null) return null;

        	    for (int i=0; i <scaffIds.size() && i<5; i++) {
        		int scaf = scaffIds.get(i).get("scafid").asInt();
        		int val = scaffIds.get(i).get("pScore").asInt();
        		score.add(val);
                	String url2 = "https://bard.nih.gov/api/latest/plugins/badapple/prom/scafid/"+scaf;
                	HttpGet get2 = new HttpGet(url2);
                	HttpResponse response2 = httpclient.execute(get2);
                	InputStream is2 = response2.getEntity().getContent();
                	JsonNode node = mapper.readTree(is2);
                	info.add(node);
        	    }
        	    if (score.size() == 0) return null;
        	    
        	    Object[] retVal = {score, info};
        	    return retVal;        	    
        	}
            } catch (Exception ex) {ex.printStackTrace();} 
            finally { 
        	httpclient.close();
//        	httpclient.getConnectionManager().shutdown();
            }
	    return null;
	}
	public void clear () {
	    taskpane.removeAll(); // remove all components
	    repaint ();
	}

        public void update(CompoundAdapter c) {
            update((T)c.getCompound());
        }
        
	public void update (T entity) {
//	    //logger.info(anchor+":"+entity.getId());
//	    if (entity!=null && !entity.equals(anchor)) {
//		this.anchor = entity; // current anchor entity
//		clear();
//		setBusy (true);
//		new PluginRecordLoader<T> (this, entity).execute();
//		logger.info(entity.getId() + " Update plugin record view ");
//	    }

	    clear();
	    
	    if (entity instanceof Compound) {
		new BadAppleUNM().update((Compound)entity, taskpane);
		
		new SMARTCyp().update((Compound)entity, taskpane);
	    }
	}

	@SuppressWarnings("unchecked")
	void setScoreInfo (Object[] retVal) {
	    clear();
	    index = 0;
	    if (retVal == null) {
	    	score = new ArrayList<Integer>();
	    	info = new ArrayList<JsonNode>();
		    JXTaskPane tp = createTaskPane("BADAPPLE, the BARD Promiscuity Plugin, from UNM; returns evidence-based promiscuity scores.");
		    tp.setCollapsed(false);
		    tp.add(new JLabel("BARDAPPLE analysis not available for this compound, sorry!"));
		    taskpane.add(tp);
	    	return;
	    }
	    
	    score = (ArrayList<Integer>)retVal[0];
	    info = (ArrayList<JsonNode>)retVal[1];
	    JXTaskPane tp = createTaskPane("BADAPPLE, the BARD Promiscuity Plugin, from UNM; returns evidence-based promiscuity scores.");
	    tp.setCollapsed(false);
	    tp.add(initUI());
	    updateToIndex();
	    taskpane.add(tp);
	}

	public Content<T> getContent () { return content; }

	JXTaskPane createTaskPane (String title) {
	    JXTaskPane tp = new JXTaskPane ();
	    tp.setLayout(new BorderLayout ());
	    tp.setTitle(title);
	    tp.setCollapsed(true);
	    tp.setScrollOnExpand(true);
	    ((JComponent)tp.getContentPane()).setBackground(Color.white);
	    return tp;
	}

    }

    protected Content content;
    private ITunesNavigationHeader hudPanel;
    protected JComponent contentPane;
    private JButton nextBtn, prevBtn;
    private JLabel counterLabel;
    private JTextField counterField;
    private AbstractButton statusBtn;
    protected JTabbedPane relatedContentTab;
    private int iterator = 0;

    private String message = null;

    public RecordViewPanel (Content content) {
	if (content == null) {
	    throw new IllegalArgumentException ("Content is null");
	}
	initUI (content);

	//content.getEntities().addListEventListener(this); !!! do we really need to do this? noel
	EventBus.subscribe(ContentChangeEvent.class, this);
	EventBus.subscribe(StateChangeEvent.class, this);
    }

    // override by subclass to initialize the view
    protected void initUI (Content content) {
	setOpaque (false);
	setLayout (new BorderLayout ());

	this.content = content;

	hudPanel = new ITunesNavigationHeader ();
	hudPanel.setLayout(new BorderLayout ());
	hudPanel.setPreferredSize(new Dimension (-1, 30));
	add (hudPanel, BorderLayout.NORTH);

	{
	    JPanel p = new JPanel (new GridLayout (1, 2, -1, 0));
	    p.setOpaque(false);
	    prevBtn = new JButton (LEFT);
	    prevBtn.setUI(new HudButtonUI 
			  (HudPaintingUtils.Roundedness.RECTANGLE_BUTTON));
	    prevBtn.setToolTipText("Advance to previous record");
	    prevBtn.addActionListener(this);
	    prevBtn.setEnabled(false);
	    p.add(prevBtn);
	    nextBtn = new JButton (RIGHT);
	    nextBtn.setUI(new HudButtonUI 
			  (HudPaintingUtils.Roundedness.RECTANGLE_BUTTON));
	    nextBtn.addActionListener(this);
	    nextBtn.setToolTipText("Advance to next record");
	    nextBtn.setEnabled(false);
	    p.add(nextBtn);

	    JPanel pp = new JPanel (new FlowLayout (FlowLayout.CENTER));
	    pp.setOpaque(false);
	    pp.add(p);

	    hudPanel.add(pp, BorderLayout.EAST);
	}

	counterLabel = new JLabel ();
	counterLabel.setHorizontalAlignment(JLabel.CENTER);
	HudPaintingUtils.initHudComponent(counterLabel);
	counterField = new JTextField (5);
	counterField.setHorizontalAlignment(JTextField.TRAILING);
	counterField.setBorder(null);
	counterField.setCaretColor(Color.white);
	counterField.setToolTipText("Advance to specific record");
	counterField.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    JTextField tf = (JTextField)e.getSource();
		    int size = getContent().getEntities().size();

		    String text = null;
		    if (iterator >= 0 && iterator < size) {
			text = String.valueOf(iterator+1);
		    }
		    try {
			int index = Integer.parseInt(tf.getText());
			if (index > 0 && index <= size) {
			    updateView (index-1);
			}
			else {
			    counterField.setText(text);
			}
		    }
		    catch (NumberFormatException ex) {
			counterField.setText(text);
		    }
		}
	    });
	HudPaintingUtils.initHudComponent(counterField);
	{
	    Box box = Box.createHorizontalBox();
	    box.add(counterField);
	    //box.add(Box.createHorizontalStrut(5));
	    box.add(counterLabel);

	    JPanel p = new JPanel ();
	    p.setOpaque(false);
	    p.add(box);
	    hudPanel.add(p);
	}

	// default unknown status
	AbstractButton[] controls = getControls ();
	if (content.isCurationSupported()) {
	    Box box = Box.createHorizontalBox();
	    box.add(Box.createHorizontalStrut(5));
	    statusBtn = new JButton ();
	    statusBtn.setUI(new HudButtonUI 
			    (HudPaintingUtils.Roundedness.RECTANGLE_BUTTON, 
			     2, 2));
	    /*
	    statusBtn.setMargin(new Insets (1, 1, 1, 1));
	    statusBtn.setFocusPainted(false);
	    statusBtn.setContentAreaFilled(false);
	    statusBtn.setRolloverEnabled(true);
	    statusBtn.setBorderPainted(true);
	    */
	    statusBtn.addActionListener(new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			curateCurrentEntity ();
		    }
		});
	    setCurationStatus (CurationStatus.No);

	    box.add(statusBtn);
	    if (controls != null && controls.length > 0) {
		for (AbstractButton ab : controls) {
		    box.add(Box.createHorizontalStrut(-1));
		    box.add(ab);
		}
	    }
	    hudPanel.add(box, BorderLayout.WEST);
	}
	else if (controls != null && controls.length > 0) {
	    Box box = Box.createHorizontalBox();
	    box.add(Box.createHorizontalStrut(5));
	    for (AbstractButton ab : controls) {
		box.add(ab);
		box.add(Box.createHorizontalStrut(-1));
	    }
	    hudPanel.add(box, BorderLayout.WEST);
	}

	JPanel panel = new JPanel (new BorderLayout ());
	JXLayer layer = new JXLayer (panel, new RecordLayerUI ());
	add (layer);

	panel.add(createView ());
    }

    public Content getContent () { return content; }
    protected JComponent getContentPane () { return contentPane; }
    protected JTabbedPane getRelatedContentTab () { return relatedContentTab; }

    protected void setMessage (String message) {
	this.message = message;
	repaint ();
    }

    // override appropriately by subclass
    protected JComponent createView () {
	//JSplitPane split = createSplitPane (JSplitPane.VERTICAL_SPLIT);
	//split.setBackground(Color.white);
	//split.setTopComponent(contentPane = createContentPane ());
	//split.setBottomComponent(createRelatedContentPane ());
	//split.setDividerLocation(401);
	//return split;
        
        
        //createRelatedContentPane();
	contentPane = createContentPane ();
	return contentPane;
    }

    // override appropriately by subclass
    protected JComponent createContentPane () {
	return new JPanel (new BorderLayout ());
    }

    // any additional control
    protected AbstractButton[] getControls () { return null; }

    /*
     * override this method if curation is supported!
     */
    protected void curateEntity (Entity e) {
    }

    protected void curateCurrentEntity () {
	Entity ent = getContent().getEntity();
	if (ent != null) {
	    curateEntity (ent);
	}
    }

    public void setCurationStatus (CurationStatus as) {
	statusBtn.setIcon(as.getIcon());
	if (as.getRollover() != null) {
	    statusBtn.setRolloverIcon(as.getRollover());
	}
	statusBtn.setToolTipText(as.getMesg());
	statusBtn.putClientProperty("CurationStatus", as);
    }

    public CurationStatus getCurationStatus () {
	return (CurationStatus)statusBtn
	    .getClientProperty("CurationStatus");
    }

    public void actionPerformed (ActionEvent e) {
	Object source = e.getSource();
	updateView (source == prevBtn ? iterator - 1 : iterator + 1);
    }

//    public boolean next () {
//	boolean ok = nextBtn.isEnabled();
//	if (ok) {
//	    updateView (++iterator);
//	    ok = nextBtn.isEnabled();
//	}
//	return ok;
//    }
//    
//    public boolean previous () {
//	boolean ok = prevBtn.isEnabled();
//	if (ok) {
//	    updateView (--iterator);
//	    ok = prevBtn.isEnabled();
//	}
//	return ok;
//    }
//
    public void onEvent (Object ev) {
        if (ev instanceof ContentChangeEvent) {
            onEvent ((ContentChangeEvent)ev);
        }
    }

    public void onEvent (ContentChangeEvent e) {
	if (e.getContent() != this.content) {
	    return;
	}

	String name = e.getName();
        logger.info("## "+e.getSource()+" name="+name);

	if (name.equals(ContentChangeEvent.STATE) || name.equals(ContentChangeEvent.ENTITY)) {
	    EventList<Entity> list = getContent().getEntities();
            Entity ent = null;
	    if (name.equals(ContentChangeEvent.STATE)) {
		State state = (State) e.getNewValue();
		list = state.getEntities();
		ent = getContent().getEntity();
	    } else {
		ent = (Entity)e.getNewValue();	
	    }

	    // Entity change
            int index = 0;
//	    iterator = 0;
	    if (list.isEmpty()) {
//		iterator = -1;
		index = -1;
	    }
	    else {
//                ent = getContent().getEntity();
		if (ent != null) {
		    list.getReadWriteLock().readLock().lock();
		    try {
			index = list.indexOf(ent);
//			if (index >= 0) {
//			    iterator = index;
//			}
			if (index < 0)
			    return; // entity likely from state other than current state; no need to update view or iterator
		    }
		    finally {
			list.getReadWriteLock().readLock().unlock();
		    }
                }
                else {
                    ent = list.get(0);
                }
	    }

//            updateView (iterator);
	    updateView(index);
	    
	    if (ent != null)
		show (ent);
	} else if (name.equals(ContentChangeEvent.SIZE) || name.equals(ContentChangeEvent.NEWCOUNT)) {
	    counterLabel.setText("/" +((Content)e.getSource()).getState().getEntities().size());	    
	}
//	else if (name.equals(ContentChangeEvent.ENTITY)) {
//	    Entity ent = (Entity)e.getNewValue();
//	    if (ent != null) {
//		EventList<Entity> list = getContent().getEntities();
//		list.getReadWriteLock().readLock().lock();
//		try {
//		    int index = list.indexOf(ent);
//		    if (index != iterator && index > -1) {
//			iterator = index;
//			if (iterator > -1) // !!! Not sure how to handle cases where content is still loading, but this was put in to avoid updates from past states ruining the view on the current state
//			    updateView (iterator);
//		    }
//		}
//		finally {
//		    list.getReadWriteLock().readLock().unlock();
//		}
//	    }
//	    
//	    if (iterator > -1) // avoid updates from past states ruining the view on the current state
//		show (ent);
//	}
    }

    protected void updateView (int index) {
	iterator = index;
        setMessage ("Loading ...");

        /*
        try {
            throw new Exception ("Viewing record "+index+"...");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        */

	if (index < 0 || getContent().getEntities().isEmpty()) { //!!!!
	    prevBtn.setEnabled(false);
	    nextBtn.setEnabled(false);
	    //getContent().setEntity(null); //!!!
	    counterLabel.setText("Empty Record");
	    counterField.setText(null);
	    if (statusBtn != null) {
		statusBtn.setEnabled(false);
	    }

            setMessage (null);
            show(null);
	    return;
	}
	
	if (statusBtn != null) {
	    statusBtn.setEnabled(true);
	}

	EventList<Entity> list = getContent().getEntities();
	list.getReadWriteLock().readLock().lock();
	try {
	    Entity e = list.get(index);
	    counterLabel.setText("/" +list.size());
	    counterField.setText(String.valueOf(index+1));
	    nextBtn.setEnabled(index+1 < list.size());
	    prevBtn.setEnabled(index-1 >= 0);
	    if (e != null && !e.equals(getContent().getEntity()))
		getContent().setEntity(e);
	}
	finally {
	    list.getReadWriteLock().readLock().unlock();
	}

        if (!nextBtn.isEnabled() || list.size() - index < 5) {
            getContent().getState().fetch();
        }
        setMessage (null);
    }

    // Override to display the entity content; this method shouldn't
    //  be called directly! It's indirectly called by the Content.PROP_ENTITY
    //  content event.
    protected void show (Entity e) {
	clear ();
	if (e == null) {
	    return;
	}
	logger.info(e.toString());

	JTabbedPane tab = getRelatedContentTab ();
	if (tab != null) {
	    for (int i = 0; i < tab.getTabCount(); ++i) {
		UIRelatedRecordPanel form = 
		    (UIRelatedRecordPanel)tab.getComponentAt(i);
		form.update(e);
		//tab.setTitleAt(i, form.getRelatedContent().getName());
	    }
	}
    }

    // Override by subclass to clear the content of the current view
    protected void clear () {
	JTabbedPane tab = getRelatedContentTab ();
	if (tab != null) {
	    for (int i = 0; i < tab.getTabCount(); ++i) {
		UIRelatedRecordPanel form = 
		    (UIRelatedRecordPanel)tab.getComponentAt(i);
		form.clear();
		//tab.setTitleAt(i, form.getRelatedContent().getName());
	    }
	}
    }


    public void listChanged (ListEvent e) {
	while (e.nextBlock()) {
	    switch (e.getType()) {
	    case ListEvent.INSERT:
		break;
	    case ListEvent.UPDATE:
		break;
	    case ListEvent.DELETE:
		break;
	    }
	}

	int index = 0;
        EventList<Entity> list = (EventList)e.getSource();        

        if (iterator < list.size()) {
            index = iterator;
        }
        updateView(index);
    }

    @Deprecated
    protected JComponent createRelatedContentPane () {
	relatedContentTab = new JideTabbedPane ();
	((JideTabbedPane)relatedContentTab)
	    .setTabShape(JideTabbedPane.SHAPE_BOX);
	initRelatedContentTab (relatedContentTab);

	JPanel pane = new JPanel (new BorderLayout ());
	pane.add(relatedContentTab);
	return pane;
    }

    /*
     * subclass override to fill reloaded content
     */
    protected void initRelatedContentTab (JTabbedPane tab) {
        relatedContentTab = tab;
    }

    protected Grid createMolGrid () {
	Grid grid = new Grid ();
	grid.setCellRenderer(new MolCellRenderer ());
	MolCellEditor mce = new MolCellEditor ();
	mce.instrumentContextMenu();
	grid.setCellEditor(mce);
	//grid.setUseDefaultBackground(false);

	grid.setPreferredSize(new Dimension (200, 200));
	return grid;
    }

    /*
     * View interface
     */
    public Type getType () { return Type.Record; }
    public Icon getIcon () { return IconFactory.View.Record.getIcon(); }
    public String getName () { return "Record"; }
    public String getDescription () { return "Record view"; }
    public Component getComponent () { return this; }
    public BottomBar getBottomBar () { return null; }
    public BoundedRangeModel getBoundedRangeModel () { return null; }
    public Adjustable getAdjustable () { return null; }
//    public ListSelectionModel getSelectionModel () { return null; }
    public void setSelectionModel(ListSelectionModel selections) { }
}
