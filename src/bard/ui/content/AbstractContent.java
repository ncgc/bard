// $Id: AbstractContent.java 4007 2010-02-01 20:29:59Z nguyenda $

package bard.ui.content;

import java.awt.Adjustable;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileFilter;

import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventService;
import org.bushe.swing.event.EventSubscriber;
import org.bushe.swing.event.ThreadSafeEventService;

import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.IntValue;
import bard.core.SearchParams;
import bard.core.ServiceIterator;
import bard.core.Value;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.SearchResultEvent;
import bard.ui.event.StateChangeEvent;
import bard.ui.event.ViewChangeEvent;
import bard.ui.util.UIUtil;
import bard.ui.util.UIUtil.BARDUri;
import ca.odell.glazedlists.EventList;

public abstract class AbstractContent<T extends Entity>
    implements Content<T>, 
               EventSubscriber, 
               PropertyChangeListener,
               AdjustmentListener,
               ActionListener {

    private static final Logger logger = 
	Logger.getLogger(AbstractContent.class.getName());

    protected static ExecutorService threadPool = 
	Executors.newCachedThreadPool();

    protected final Map<View.Type, View> views = 
        new HashMap<View.Type, View>();

    protected Icon icon;
    protected String name;
    protected String description;
    protected long lastViewed = 0l;
    protected JPopupMenu popup = new ContentPopupMenu ();
    protected JToolBar toolbar = null;
    protected long size = 0;
    protected long newCount = 0;
    protected long lastNewCountUpdated = 0l;
    protected AtomicInteger busyCount = new AtomicInteger ();

    protected View.Type view; // current view
    protected final LinkedList<State<T>> history = new LinkedList<State<T>>();
    protected final LinkedList<State<T>> queue = new LinkedList<State<T>>();

    // this represents the default contents
    protected ContentManager contentManager;
    protected Object resource; // initial resource for this content
    protected EntityService<T> service;

    protected EventService eventService = new ThreadSafeEventService ();

    /*
     * If this variable is not null, then the content is current being
     * loaded.  As such, calling search or load while loader is not-null
     * will return immediately.
     */
    private final ReentrantLock locker = new ReentrantLock ();

    class ContentPopupMenu<T> extends JPopupMenu {
	String addMenuName = "Add selected items to ...";
	String removeMenuName = "Remove selected items from ...";
	JMenuItem selectAllItem = new JMenuItem ("Select All");
	JMenuItem selectNoneItem = new JMenuItem ("Clear Selection");
	ArrayList<String> ignore = new ArrayList<String>();
	{
	    ignore.add("Projects");
	    ignore.add("Compounds");
	    ignore.add("Assays");
	    ignore.add("Experiments");
	}
	
	ContentPopupMenu () {
	    selectNoneItem.addActionListener(AbstractContent.this);
	    selectAllItem.addActionListener(AbstractContent.this);
	}

	@Override
	public void show (Component c, int x, int y) {
	    ArrayList<Content> collections = new ArrayList<Content>();
	    EntityService es = getService();
	    if (es != null) {
		ContentManager cm = ContentManager.getInstance(getService().getServiceManager());
		for (Content content: cm.getContents()) {
		    String name = content.getName();
		    if (content != AbstractContent.this && content.getEntityClass() == getEntityClass() && !ignore.contains(name)) {
			collections.add(content);
		    }
		}
	    }

	    JMenu addMenu = null;
	    JMenu removeMenu = null;
	    
	    for (Component comp : getComponents ()) {
		if (comp instanceof JMenu) {
		    JMenu m = (JMenu)comp;
		    if (m.getText().equals(addMenuName)) {
			addMenu = m;
		    } else if (m.getText().equals(removeMenuName)) {
			removeMenu = m;
		    }
		}
	    }
	    //logger.info("tag menu: "+tag);
	    
	    if (addMenu == null) {
		addMenu = new JMenu (addMenuName);
		addMenu.setToolTipText("Add selections to collection");
		removeMenu = new JMenu (removeMenuName);
		removeMenu.setToolTipText("Remove selections from collection");
		JMenuItem item;
		if (!ignore.contains(AbstractContent.this.getName())) {
		    item = new JMenuItem(AbstractContent.this.getName());
		    item.setActionCommand("remove");
		    item.addActionListener(AbstractContent.this);
		    removeMenu.add(item);
		}
		for (Content content: collections) {
		    item = new JMenuItem(content.getName());
		    item.setActionCommand("add");
		    item.addActionListener(AbstractContent.this);
		    addMenu.add(item);
		    item = new JMenuItem(content.getName());
		    item.setActionCommand("remove");
		    item.addActionListener(AbstractContent.this);
		    removeMenu.add(item);
		}
		item = new JMenuItem("New collection");
		item.addActionListener(AbstractContent.this);
		addMenu.add(item);
		add (addMenu);
		add (removeMenu);
		addSeparator ();
		add (selectAllItem);
		add (selectNoneItem);
	    }
	    
	    State state = AbstractContent.this.getState();
	    if (state == null || state.getSelectionModel().getMinSelectionIndex() == -1) {
		addMenu.setEnabled(false);
		removeMenu.setEnabled(false);
	    } else {
		addMenu.setEnabled(true);
		if (removeMenu.getComponentCount() > 0)
		    removeMenu.setEnabled(true);
		else
		    removeMenu.setEnabled(false);
	    }

	    super.show(c, x, y);
	}
    }

    protected AbstractContent () {
    }

    protected AbstractContent (EntityService<T> service) {
        this (service, null);
    }

    protected AbstractContent (EntityService<T> service, Object resource) {
        initContent (service, resource);
    }

    public void initContent (EntityService<T> service, Object resource) {
        this.resource = resource;
        this.service = service;

        EventBus.subscribe(StateChangeEvent.class, this);
        EventBus.subscribe(ContentChangeEvent.class, this);
        EventBus.subscribe(SearchResultEvent.class, this);

	lastViewed = getPreferences().getLong("lastviewed", 0l);
        String view = getPreferences().get("view", null);
        if (view != null) {
            this.view = View.Type.valueOf(View.Type.class, view);
        }

        for (View.Type type : getSupportedViewTypes ()) {
            View v = getView (type); // instantiate all supported views
            if (v != null && this.view == null) {
                this.view = type;
            }
        }

        if (resource == null) {
        }
        else if (resource instanceof URI) {
            
        }
        else { // etag
            if (resource instanceof Value) {
                Value etag = (Value)resource;
                Value attr = etag.getChild("name");
                setName (attr != null ? (String)attr.getValue() 
                         : etag.getId());
                attr = etag.getChild("description");
                if (attr != null)
                    setDescription ((String)attr.getValue());
            }
            else {
                this.name = resource.toString();
            }
        }
        logger.info("Initializing content "+getClass()
                    +" ("+getName()+"); resource="+resource);

	checkContentForChanges ();
    }

    abstract protected Preferences getPreferences ();
    abstract public Class<T> getEntityClass ();
    abstract public View.Type[] getSupportedViewTypes ();

    public Object getResource() {return resource;} // initial resource for this content
    
    public String getName () { return name; }
    public void setName (String name) { this.name = name; }

    public String getDescription () { return description; }
    public void setDescription (String description) { 
        this.description = description; 
    }

    public Icon getIcon () {
        return ContentManager.getIcon(getEntityClass ());
    }

    public <E extends Entity> Content<E> getContent (Class<E> clazz) {
        ContentManager cm = ContentManager.getInstance
            (getService().getServiceManager());
        return cm.getContent(clazz);
    }

    public EventList<T> getEntities () { 
        State<T> state = getState ();

        // lazy loading
        if (state == null) {
            state = getInitState ();
        }
            
        return state.getEntities(); 
    }
    public T getEntity () { 
	State<T> state = getState();
	if (state != null)
	    return state.getEntity(); 
	return null;
    }
    public void setEntity (T entity) {
        getState().setEntity(entity);
    }

    public View getView () { 
        return getView (this.view); 
    }
    public void setView (View.Type view) {
        if (view != this.view) {
            View.Type old = this.view;
            this.view = view;
            // publish view change event on the EDT
            EventBus.publish(new ViewChangeEvent (this, old, view));
        }
    }

    protected View getView (View.Type type) { 
        View view = views.get(type);
        if (view == null) {
            switch (type) {
            case List: 
                views.put(type, view = createListView ());
                break;
            case Grid:
                views.put(type, view = createGridView ());
                break;
            case Record:
                views.put(type, view = createRecordView ());
                break;
            }

            if (view != null) {
                Adjustable adj = view.getAdjustable();
                if (adj != null) {
                    adj.addAdjustmentListener(this);
                }

            }
        }
        return view;
    }

    public void actionPerformed (ActionEvent e) {
	if (e.getSource() instanceof JMenuItem) {
	    JMenuItem item = (JMenuItem)e.getSource();
	    if ("Clear Selection".equals(item.getText())) {
		getState().getSelectionModel().clearSelection();
	    } else if ("Select All".equals(item.getText())) {
		getState().getSelectionModel().setSelectionInterval(0, getState().getEntities().size());
	    } else if ("New collection".equals(item.getText())){
		ListSelectionModel lsm = getState().getSelectionModel();
		boolean selectAll = true;
		Collection<Object> ids = new ArrayList<Object>();
		{
		    EventList<T> entities = getState().getEntities();
		    if (getState().getETag() == null)
			selectAll = false;
		    if (lsm.getMinSelectionIndex() > 0 || lsm.getMaxSelectionIndex()+1 < entities.size())
			selectAll = false;
		    for (int i=lsm.getMinSelectionIndex(); i<lsm.getMaxSelectionIndex()+1; i++)
			if (i<entities.size())
			    if (lsm.isSelectedIndex(i))
				ids.add(entities.get(i).getId());
			    else selectAll = false;
		}
		
		String collectionName = JOptionPane.showInputDialog(
			(Component)e.getSource(),
			"Please provide a name for this collection.", "Type name here ...");
		if (collectionName == null)
		    return;
		Object obj;
		if (selectAll) {
		    obj = getState().getETag();	
		    // should push new name and get back etag!!!
		} else {
		    obj = getService().newETag(collectionName, ids);
		}
		if (obj != null && obj instanceof Value) {
		    Value etag = (Value)obj;
		    if (etag.getChild("count") == null)
			etag.add(new IntValue(etag, "count", ids.size()));
		    Content content = getContentManager().add(getEntityClass(), etag);
		    ContentChangeEvent event = new ContentChangeEvent(content, ContentChangeEvent.CONTENT, null, collectionName);
		    EventBus.publish(event);
		}
	    } else {
		String destName = item.getText();
		String cmd = item.getActionCommand();
		EntityService<T> es = getService();
		if (es != null) {
		    ContentManager cm = ContentManager.getInstance(getService().getServiceManager());
		    for (Content<?> content: cm.getContents()) {
			if (content.getName().equals(destName)) {
			    if (cmd.equals("add")) {
				logger.severe("Need to "+cmd+" ["+getState().getSelectionModel().getAnchorSelectionIndex()+",...] selected entities to "+content.getName());				
				return;
			    } else { // cmd.equals("remove")
				logger.severe("Need to "+cmd+" ["+getState().getSelectionModel().getAnchorSelectionIndex()+",...] selected entities to "+content.getName());
				return;
			    }
			}
		    }
		}
		
	    }
	} 
    }

    protected State<T> getInitState () {
	State<T> state = getState (resource);
        state.fetch(); // get the next batch
        push (state);
        return state;
    }

    public State<T> getState () {
	if (history.size() == 0)
	    return getInitState();
        return history.peek();
    }

    public State<T> push (State<T> state) {
    	while (queue.size() > 0) // clear out 'next' history
    		queue.pop();
        State<T> old = history.peek();
        state.fetch(); // fetch some data
        history.push(state);
        firePropertyChange (ContentChangeEvent.STATE, old, state);
        this.setEntity(state.getEntity()); //!!! this updates the display of the record with content from the new state
        return state;
    }

    public boolean canPush() {
    	return queue.size() > 0;
    }
    
    public boolean canPop() {
    	return history.size() > 1;
    }
    
    public int findState(State<T> state) {
        for (int i=0; i<history.size(); i++)
            if (history.get(i) == state)
                return -i;
        for (int i=0; i<queue.size(); i++)
            if (queue.get(i) == state)
                return i+1;
        return 0;
    }
    
    public State<T> push() {
    	if (queue.size() == 0) return history.peek();
    	State<T> state = queue.pop();
        State<T> old = history.peek();
        state.fetch(); // fetch some data
        history.push(state);
        firePropertyChange (ContentChangeEvent.STATE, old, state);
        this.setEntity(state.getEntity());
        return state;
    }
    
    public State<T> pop () {
    	if (history.size() < 2) return history.peek(); // can not pop if stack has less than two elements
        State<T> old = history.pop();
        State<T> state = history.peek();
        old.clean(); // clean up this state
        queue.push(old);

        firePropertyChange (ContentChangeEvent.STATE, old, state);
        this.setEntity(state.getEntity());
        return state;
    }

    public State<T> search (SearchParams query) {
        ServiceIterator<T> iter = getService().search(query);
        State<T> state = new DefaultState (this, iter, query, (int)iter.getCount());
        push (state);

        return state;
    }

    protected State<T> getState (Object resource) {
        if (resource == null) {
        	if (history.size() > 0) return history.peek();
            return new DefaultState 
                (this, getService().iterator(), (int)service.size());
        }
        else if (resource instanceof URI || resource instanceof UIUtil.BARDUri) {
        	UIUtil.BARDUri bard;
        	if (resource instanceof URI)
        		bard = new UIUtil.BARDUri((URI)resource);
        	else
        		bard = (UIUtil.BARDUri)resource;
            String[] argv = bard.argv();
            if (bard.check(this) && argv.length > 1) {
                this.view = bard.view();
                T entity = getService().get(argv[1]);
                logger.info("### "+getName()
                            +": retrieving "+argv[1]+" => "+entity);
                return new DefaultState (this, Arrays.asList(entity));
            }

            return new DefaultState 
                (this, getService().iterator(), (int)service.size());
        }

        // etag
        return new DefaultState (this, getService().iterator(resource), 0);
    }

    public State<T> reload () {
        State<T> state = getState();
        if (state == null) return null;
        Entity old = state.getEntity();
        if (old != null) {
            state.clean(); // clean up this state
            Entity reload = this.getService().get(old.getId());
            state.setEntity((T)reload);
            firePropertyChange (ContentChangeEvent.ENTITY, old, reload);
            this.setEntity((T)reload);
        }
        return state;
    }

    public Collection<Value> getFacets () {
        return getState().getFacets();
    }

    public State<T> open (BARDUri uri) {
    	String[] argv = uri.argv();
    	// uri way of changing view on current state
    	if (argv.length == 0) {
    	    this.setView(uri.view());
    	    return this.getState();
    	}
    	// first check to see if in current state
    	String id = argv[argv.length-1];
    	for (Entity e: getEntities()) {
    		if (e.getId().toString().equals(id)) {
    			this.setView(uri.view());
    			this.firePropertyChange(ContentChangeEvent.ENTITY, null, e);
    			return this.getState();
    		}
    	}
        return push ( getState (uri));
    }

    public void propertyChange (PropertyChangeEvent e) {
	String prop = e.getPropertyName();

	logger.info(e.getSource()+": prop="+prop + " old="
                    +e.getOldValue() + " new="+e.getNewValue());
	if (prop.equals("state")) {
	}
	else if (prop.equals(ContentChangeEvent.SIZE)) {
	    int oldsize = (Integer)e.getOldValue();
	    int newsize = (Integer)e.getNewValue();
	    if (oldsize != newsize) {
		//clearSelection (); // clear
	    }
	}

	// forward the event
	if (this != e.getSource()) {
	    firePropertyChange (prop, e.getOldValue(), e.getNewValue());
	}
    }

    public boolean isReadOnly () {
	return service.isReadOnly();
    }

    public void setLastViewed (long time) { 
	long old = this.lastViewed;
	this.lastViewed = time; 
	getPreferences().putLong("lastviewed", time);
	firePropertyChange (ContentChangeEvent.LASTVIEWED, old, time);
    }
    public long getLastViewed () { return lastViewed; }
    public JPopupMenu getContextMenu () { return popup; }
    public JToolBar getContextToolbar () { return toolbar; }

    public long getTotalSize () { 
        if (resource != null) {
            if (resource instanceof Value) {
                Value count = ((Value)resource).getChild("count");
                if (count != null) {
                    return (Integer)count.getValue();
                }
            }
            else { // assume URI?
                return getSize ();
            }
        }
        return service.size();
    }
    
    public long getSize () { return getState().size(); }
    protected void setSize (long size) { 
	long old = this.size;
	this.size = size;
	firePropertyChange (ContentChangeEvent.SIZE, old, size);
    }

    public long getNewCount () { return newCount; }
    public void setNewCount (long newCount) {
	long old = this.newCount;
	this.newCount = newCount;
	lastNewCountUpdated = new Date().getTime();
	firePropertyChange (ContentChangeEvent.NEWCOUNT, old, newCount);
    }

    public void checkContentForChanges () {
	threadPool.submit(new Runnable () {
		public void run () {
		    long count = getCountSinceLastViewed ();
		    if (getNewCount () != count) {
			setNewCount (count);
		    }
		}
	    });
    }

    public void adjustmentValueChanged (AdjustmentEvent e) {
        Adjustable adj = e.getAdjustable();
        if (e.getValue() == 0) {
        }
        else if (e.getValue() + adj.getVisibleAmount() >= adj.getMaximum()) {
            getState().fetch();
        }
    }

    protected long getLastNewCountUpdated () { 
	return lastNewCountUpdated;
    }

    // this can be expensive... so it should be run in a background thread
    protected long getCountSinceLastViewed () {
	return 0;
    }

    public EntityService<T> getService () { return service; }
    public EntityServiceManager getServiceManager () { 
        return service.getServiceManager(); 
    }
    public ContentManager getContentManager () { return contentManager; }
    public void setContentManager (ContentManager contentManager) {
        this.contentManager = contentManager;
    }

    public boolean isBusy () { return busyCount.get() > 0; }
    public void setBusy (boolean busy) { 
	boolean old = busyCount.get() > 0;
	int cnt;
	if (busy) cnt = busyCount.incrementAndGet();
	else cnt = busyCount.decrementAndGet();
	if (cnt < 0) {
	    busyCount.set(0);
	}
	//firePropertyChange (ContentChangeEvent.BUSY, old, cnt > 0);
    }

    protected View createListView () {
        return null;
    }
    protected View createGridView () {
        return null;
    }
    protected View createRecordView () {
        return null;
    }
    protected View createColumnView () {
        return null;
    }

    public void onEvent (Object ev) {
        if (ev instanceof ContentChangeEvent) {
            onEvent ((ContentChangeEvent)ev);
        }
        else if (ev instanceof StateChangeEvent) {
            onEvent ((StateChangeEvent)ev);
        }
        else if (ev instanceof SearchResultEvent) {
            onEvent ((SearchResultEvent)ev);
        }
    }

    public void onEvent (ContentChangeEvent ev) {
        if (this != ev.getContent()) {
            return;
        }

        logger.info("## SOURCE "+ev.getSource()+" name="+ev.getName());

        String name = ev.getName();
        if (name.equals(ContentChangeEvent.ENTITY)) {
            Entity ent = (Entity)ev.getNewValue();
        }
        else if (name.equals(ContentChangeEvent.BUSY)) {
            setBusy ((Boolean)ev.getNewValue());
        }            
    }

    public void onEvent (StateChangeEvent ev) {
        State<T> state = ev.getState();
        if (!history.contains(state)) {
            return;
        }

        logger.info("state="+ev.getState()+" name="+ev.getName());
        /*
                    +" old="+ev.getOldValue()+" new="+ev.getNewValue());
        */
        String name = ev.getName();
        if (name.equals(StateChangeEvent.BUSY)) {
            setBusy ((Boolean)ev.getNewValue());
        }
        else if (name.equals(StateChangeEvent.ENTITY)) {
            
        }

        // now propagate the event as ContentChangeEvent
        firePropertyChange (ev.getName(), ev.getOldValue(), ev.getNewValue());
    }

    public void onEvent (SearchResultEvent ev) {
        EntityService<T> source = ev.getService();

        if (!getEntityClass().getName()
            .equals(source.getEntityClass().getName())) {
            return;
        }

        logger.info("#### "+getName()+" SearchResultEvent: service="+source
                    +" query="+ev.getQuery()+" ####");
        //push (new DefaultState (this, ev.getIterator()));
    }

    public boolean isCurationSupported () { return false; }
    
    static String format (String s, int max) {
	if (s.length() < max) {
	    return s;
	}
	StringBuilder sb = new StringBuilder (s.substring(0, max-3));
	sb.append("...");
	return sb.toString();
    }

    public EventService getEventService () { return eventService; }

    protected void publish (ContentChangeEvent e) {
	publish (e, true);
    }

    protected void publish (ContentChangeEvent e, boolean edt) {
	if (edt) {
	    EventBus.publish(e);
	}
	getEventService().publish(e);
    }

    protected void firePropertyChange 
	(String prop, Object oldValue, Object newValue) {
	publish (new ContentChangeEvent (this, prop, oldValue, newValue));
    }
    protected void firePropertyChange (PropertyChangeEvent e) {
	publish (new ContentChangeEvent 
		 (this, e.getPropertyName(), 
		  e.getOldValue(), e.getNewValue()));
    }

    public void exportData (FileFilter format, File file) 
	throws IOException {;}

    public boolean isExportSupported () {return false;}
    public FileFilter[] getExportFileFilters() {return new FileFilter[0];}

    public void importData (String uri, Map<?,?> options,
	    Map<String, String[]> props) 
		    throws IOException {;}
    public boolean isImportSupported () {return false;}
    public FileFilter[] getImportFileFilters() {return new FileFilter[0];}
}
