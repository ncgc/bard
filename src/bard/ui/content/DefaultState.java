package bard.ui.content;

import bard.core.Entity;
import bard.core.SearchParams;
import bard.core.ServiceIterator;
import bard.core.Value;
import bard.ui.event.StateChangeEvent;
import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import org.bushe.swing.event.EventBus;

import javax.swing.*;
import java.util.*;
import java.util.Timer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class DefaultState<T extends Entity> implements State<T>, Runnable {

    static final Logger logger = 
        Logger.getLogger(DefaultState.class.getName());

    static final int PREFETCH_SIZE = 100;
    static final Integer SENTINEL = new Integer (0xdeadbeef);

    protected Content<T> content;
    protected ServiceIterator<T> iterator;
    volatile protected T current;

    protected ListSelectionModel selection = new DefaultListSelectionModel();
    {
	selection.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }
    // the entity data buffer
    protected EventList<T> buffer = new BasicEventList<T>();
    protected BlockingQueue<Integer> batch = 
        new ArrayBlockingQueue<Integer>(10);

    protected Timer timer;
    protected int size;
    
    protected SearchParams query;
    protected Value etag;

    protected Map<Object, Integer> idOrder = new HashMap<Object, Integer>();

    // background thread
    protected ExecutorService thread = Executors.newSingleThreadExecutor();

    public SearchParams getQuery() {
    	return query;
    }
    
    public DefaultState (Content<T> content, ServiceIterator<T> iterator, SearchParams query, int size) {
        this (content, iterator, 0);
        this.query = query;
    }

    public DefaultState 
        (Content<T> content, ServiceIterator<T> iterator, int size) {
        this.content = content;
        this.iterator = iterator;

        // timer that keep track of changes to iterator count
        if (0 == size) {
            startTimer ();
        }
        else {
            setSize (size);
        }
        thread.submit(this);
    }

    public DefaultState (Content<T> content, java.util.List<T> entities) {
        this.content = content;
        buffer.addAll(entities);
        setSize (entities.size());
        if (!entities.isEmpty()) {
            setEntity(entities.get(0));
        }
    }

    public Map<Object, Integer> getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Map<Object, Integer> idOrder) {
        this.idOrder = idOrder;
    }

    protected Timer startTimer () {
        timer = new java.util.Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                int count = (int) iterator.getCount();
                setSize(count);
            }
        }, 0, 500);
        return timer;
    }

    protected void setSize (int size) {
        if (size != this.size) {
            int old = this.size;
            this.size = size;
            fireStateChangeEvent (StateChangeEvent.SIZE, old, size);
        }
    }

    /*
     * background thread fetching data as needed
     */
    public void run () {
        try {
            for (Integer size; (size = batch.take()) != SENTINEL
                     && !Thread.currentThread().isInterrupted(); ) {

                fireStateChangeEvent (StateChangeEvent.BUSY, false, true);
                long start = System.currentTimeMillis();
                List<T> page = iterator.next(size);

                if (!page.isEmpty()) {
                    if (current == null) {
                        setEntity (page.get(0));
                    }

                    if (etag == null) {
                	etag = iterator.getETag();
                    }
                    
                    //int old = buffer.size();
                    buffer.getReadWriteLock().writeLock().lock();
                    try {
                        buffer.addAll(page);
                    }
                    finally {
                        buffer.getReadWriteLock().writeLock().unlock();
                    }

                    /*
                    fireStateChangeEvent
                        (StateChangeEvent.SIZE, old, buffer.size());
                    */
                }
                logger.info(">>> "+getContent().getName()+": fetching "
                            +page.size()+"/"+size+"..."
                            +String.format("%1$.3fs", 1e-3*(System.currentTimeMillis()-start)));

                fireStateChangeEvent (StateChangeEvent.BUSY, true, false);
                if (page.size() < size) {
                    // no more, so we're done
                    break;
                }
            }

            logger.info(Thread.currentThread()+" finished!");
        }
        catch (InterruptedException ex) {
            logger.info(Thread.currentThread()+" interrupted!");
        }
    }

    public Content<T> getContent() {
        return content;
    }

    public EventList<T> getEntities() {
        if (idOrder.size() > 0) {
            // TODO should we get a lock on buffer?

            if (idOrder.size() != buffer.size()) {
                logger.severe("Size of entity order array does not match buffer");
                throw new RuntimeException("Size of sorted entity array does not match buffer");
            }

            logger.severe("Reordering buffer of " + buffer.size() + " elements");

            Object[] t = new Object[buffer.size()];
            for (Object o : idOrder.keySet()) {
                int position = idOrder.get(o);
                for (T entity : buffer) {
                    if (entity.getId().equals(o)) {
                        t[position] = entity;
                        break;
                    }
                }
            }
            buffer.clear();
            for (Object o : t) buffer.add((T) o);
            return buffer;
        } else return buffer;
    }

    public T getEntity () { return current; }
    synchronized public void setEntity (T entity) {
        T old = current;
        current = entity;
        fireStateChangeEvent (StateChangeEvent.ENTITY, old, current);
    }
    public int size () { return size; }
    public Value getETag() { return etag; }

    public Collection<Value> getFacets () {
        return iterator != null ? iterator.getFacets() : null;
    }

//    public Collection<T> getSelectedEntities () {
//        List<T> entities = new ArrayList<T>();
//        if (!selection.isEmpty()) {
//            buffer.getReadWriteLock().readLock().lock();
//            try {
//                for (Integer index : selection) {
//                    entities.add(buffer.get(index));
//                }
//            }
//            finally {
//                buffer.getReadWriteLock().readLock().unlock();
//            }
//        }
//        return entities;
//    }

    public boolean fetch (int size) {
        if (!thread.isTerminated()) {
            return batch.offer(size); // schedule the next batch
        }
        return false;
    }
    public boolean fetch () {
        return fetch (PREFETCH_SIZE);
    }

//    public Collection<Integer> getSelections () { return selection; }
    public ListSelectionModel getSelectionModel() {return selection;}
    
//    public void setSelection (Integer... indexes) {
//        selection.clear();
//        for (Integer index : indexes) {
//            selection.add(index);
//        }
//    }
//    public void clearSelection () {
//        selection.clear();
//    }

    public void clean () {
        if (!thread.isTerminated()) {
            batch.offer(SENTINEL);
        }
        if (timer != null) {
            timer.cancel();
        }
        thread.shutdownNow(); // make sure the thread dies
    }

    protected void fireStateChangeEvent 
        (String name, Object oldValue, Object newValue) {
	EventBus.publish(new StateChangeEvent<T>
            (this, name, oldValue, newValue));
    }

    public T contains(Object object) {
	if (buffer.contains(object)) return buffer.get(buffer.indexOf(object));
	for (T entity: buffer)
	    if (entity.getId().toString().equals(object.toString()))
		return entity;
	return null;
    }
}
