// $Id$

package bard.ui.content;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.bushe.swing.event.EventBus;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;

import bard.core.Entity;
import bard.core.EntityService;
import bard.core.IntValue;
import bard.core.Value;
import bard.ui.common.ActivityCellRenderer;
import bard.ui.common.BioAssayTableColumn;
import bard.ui.common.DefaultBottomBar;
import bard.ui.common.EntityTable;
import bard.ui.common.EntityTableModel;
import bard.ui.common.MolCellRenderer;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.ViewChangeEvent;
import bard.ui.explodingpixels.macwidgets.BottomBar;
import bard.ui.explodingpixels.macwidgets.MacIcons;
import bard.ui.explodingpixels.macwidgets.MacButtonFactory;
import bard.ui.icons.IconFactory;
import bard.ui.util.MolRenderer;
import bard.ui.util.UIUtil;
import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;

public class ListViewPanel extends JPanel 
    implements View, ChangeListener, MouseListener {

    static private Logger logger = Logger.getLogger
	(ListViewPanel.class.getName());

    class ScrollLayerUI extends AbstractLayerUI {
	Point pt;
	Font font = UIManager.getFont("Label.font")
	    .deriveFont(Font.BOLD, 11.f);

	JScrollBar vsbar = scroller.getVerticalScrollBar();
	NumberFormat nf = NumberFormat.getInstance();
	RoundRectangle2D rect = new RoundRectangle2D.Double();
	Color color = new Color (0x10, 0x10, 0x10, 150);
	double padx = 20., pady = 6.;

	ScrollLayerUI () {
	}

	protected void processMouseMotionEvent (MouseEvent e, JXLayer l) {
	    pt = null;
	    Component c = e.getComponent();
	    if (c != vsbar) {
	    }
	    else if (e.getID() == MouseEvent.MOUSE_DRAGGED) {
		pt = SwingUtilities.convertPoint(c, e.getPoint(), l);
	    }
	    l.repaint();
	}

	
	@Override
	protected void paintLayer (Graphics2D g2, JXLayer layer) {
	    super.paintLayer(g2, layer);
	    if (pt != null) {
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, 
				    RenderingHints.VALUE_RENDER_QUALITY);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
				    RenderingHints.VALUE_ANTIALIAS_ON);
		FontMetrics fm = g2.getFontMetrics(font);

		int h = table.getRowHeight();
		int start = (vsbar.getValue()+h) /h;
                /*
		int end = vsbar.getValue() == vsbar.getMaximum() 
		    ? vsbar.getMaximum() 
		    : start + (vsbar.getVisibleAmount()-h/2)/h;
                */
                int end = (int)(start + (vsbar.getVisibleAmount()-h/2.)/h + 0.5);
		String value = "Rows "+nf.format(start) + " \u2014 " 
		    + nf.format(end);
		Rectangle2D bounds = fm.getStringBounds(value, g2);
		double bh = bounds.getHeight();
		double bw = bounds.getWidth();
		rect.setRoundRect(vsbar.getX()-bw-2*padx, pt.y - bh-pady, 
				  bw+padx+padx/2., bh+pady/2.,.9*bh, .9*bh);
		g2.setPaint(color);
		g2.fill(rect);
		g2.setPaint(Color.white);
		g2.drawString(value, (int)(rect.getX()+padx/2.+0.5), 
			      (int)(pt.y-pady + 0.5));
	    }
	}
    }

    protected Content content;
    protected EntityTable table;
    protected DefaultBottomBar bottomBar;
    protected BoundedRangeModel range;
    protected JScrollPane scroller;
    protected JPopupMenu popupMenu;

    public ListViewPanel (Content content) {
	this.content = content;
	popupMenu = content.getContextMenu();
	initUI ();
    }

    protected void initUI () {
	setOpaque (false);
	setBorder (BorderFactory.createEmptyBorder());
	setLayout (new BorderLayout ());

	JPanel pane = new JPanel (new BorderLayout ());

	table = new EntityTable ();
	//table.setRowHeight(table.getRowHeight()+3);
	table.setRowHeight(getBoundedRangeModel().getValue());
	table.addMouseListener(this);
        table.getTableHeader().addMouseMotionListener
            (new MouseMotionAdapter () {
                public void mouseMoved (MouseEvent e) {
                    updateToolTip (e);
                }

                void updateToolTip (MouseEvent e) {
                    JTableHeader head = (JTableHeader)e.getSource();
                    int col = head.getTable().columnAtPoint(e.getPoint());
                    if (col >= 0) {
                        TableColumn tc = table.getColumnModel().getColumn(col);
                        Object text = tc.getIdentifier();
                        if (text == null) text = tc.getHeaderValue();
                        if (text == null) text = "";
                        head.setToolTipText(text.toString());
                    }
                }
            });

	updateBottomBar();
	
	scroller = UIUtil.iTunesScrollPane(table);
	/*
	scroller.addComponentListener(new ComponentAdapter () {
		public void componentResized (ComponentEvent e) {
		    JComponent c = (JComponent)e.getSource();
		    logger.info(c+" resized: " + c.getPreferredSize());
		}
	    });
	*/
	scroller.getVerticalScrollBar().addAdjustmentListener
	    (new AdjustmentListener () {
		    public void adjustmentValueChanged (AdjustmentEvent e) {
			int height = range.getValue();
			int value = height * 
			    (int)((double)e.getValue()/height + 0.5);
			// make sure the value is a multiple of the rows
			e.getAdjustable().setValue(value);
		    }
	    });

	ScrollLayerUI layerUI = new ScrollLayerUI ();
	JXLayer layer = new JXLayer (scroller, layerUI);
	add (layer);
    }

    protected void updateBottomBar() {
	bottomBar = new DefaultBottomBar();
	bottomBar.setSliderRangeModel(getBoundedRangeModel());

	final JPopupMenu selectMenu = content.getContextMenu();	
        AbstractButton contextButton = 
            (AbstractButton) MacButtonFactory.createGradientButton
            (MacIcons.GEAR, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JButton source = (JButton)e.getSource();
                        selectMenu.show(source, source.getX(), source.getY());
                    }
                });
        contextButton.setBorder(BorderFactory.createEmptyBorder());
	contextButton.setActionCommand("Selection menu");
	contextButton.setToolTipText("Selection menu");
	
	bottomBar.addComponentToLeft(contextButton);
    }

    /*
     * mouse listener
     */
    public void mouseMoved (MouseEvent e) {
    }
    public void mouseClicked (MouseEvent e) {
	if (!popupGesture (e)) {
            if (e.getClickCount() == 2) {
                int row = table.rowAtPoint(e.getPoint());
                int col = table.columnAtPoint(e.getPoint());
                if (row >= 0 && col >= 0) {
                    Entity entity = (Entity)table.getValueAt(row, col);
                    logger.info(entity.getClass()+" "
                                +entity.getId()+" at "+row+","+col);

                    EventBus.publish(new ViewChangeEvent
                                     (this, View.Type.List, View.Type.Record));
                    EventBus.publish(new ContentChangeEvent
                                     (content, ContentChangeEvent.ENTITY, 
                                      content.getEntity(), entity));
                }
//            } else if (e.getClickCount() ==1 && e.getButton() == e.BUTTON3) {
//        	JPopupMenu context = content.getContextMenu();
//        	context.setPreferredSize(new Dimension(200,200));
//        	context.setLocation(e.getXOnScreen(), e.getYOnScreen());
//        	context.setVisible(true);
            } else {
        	//content.getContextMenu().setVisible(false);
            }
        }
    }
    public void mouseEntered (MouseEvent e) {
    }
    public void mouseExited (MouseEvent e) {
	popupGesture (e);
    }
    public void mousePressed (MouseEvent e) {
	popupGesture (e);
    }
    public void mouseReleased (MouseEvent e) {
    }

    boolean popupGesture (MouseEvent e) {
	if (!e.isPopupTrigger() || popupMenu == null) {
	    return false;
	}
	popupMenu.show(table, e.getX(), e.getY());
        return true;
    }


    public void stateChanged (ChangeEvent e) {
	//logger.info("Row height: "+range.getValue());
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    int value = range.getValue();
		    int visible = 
			scroller.getVerticalScrollBar().getVisibleAmount();
		    //logger.info("visible="+visible + " viewport="+scroller.getViewport().getViewRect());
		    // number of visible rows
		    int nrows = (int)((double)visible/value+0.5); 
		    if (nrows > 0) {
			// adjust the range so that there are nrows in
			//   the viewport
			range.setValue(visible/nrows);
			/*
			logger.info("Readjust row height to "
				    + range.getValue());
			*/
		    }
		    int diff = range.getValue() - table.getRowHeight();
		    if (diff != 0) {
			for (int i=0; i<table.getColumnCount(); i++) {
			    TableColumn tc = table.getColumnModel().getColumn(i);
			    TableCellRenderer tcr = tc.getCellRenderer();
			    if (tcr instanceof MolCellRenderer || tcr instanceof BioAssayTableColumn) {
				int width = tc.getWidth() + diff;
				tc.setPreferredWidth(width);
			    }
			}
		    }
		    table.setRowHeight(range.getValue());
		    table.repaint();
		}
	    });	
    }

//    public void setListSource (Class clazz, java.util.List list) {
//        setListSource (clazz, new BasicEventList (list));
//    }

//    public void setListSource (Class clazz, EventList list) {
//	table.setModel(new EntityTableModel (clazz, list));
//    }

    public void setListSource (Class clazz, State state) {
	table.setModel(new EntityTableModel (clazz, state.getEntities()));
	table.setSelectionModel(state.getSelectionModel());
    }

    public EntityTable getTable () { return table; }


    /*
     * View interface
     */
    public Type getType () { return Type.List; }
    public Icon getIcon () { return IconFactory.View.List.getIcon(); }
    public String getName () { return "List"; }
    public String getDescription () { return "List view"; }
    public Component getComponent () { return this; }
    public BoundedRangeModel getBoundedRangeModel () { 
        if (range == null) {
            range = new DefaultBoundedRangeModel 
                (50, 0, 10, 200);
            range.addChangeListener(this);
        }
        return range; 
    }
    public Adjustable getAdjustable () { 
        return scroller.getVerticalScrollBar(); 
    }

    public BottomBar getBottomBar() {
	return bottomBar;
    }
}
