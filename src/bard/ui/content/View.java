package bard.ui.content;

import java.awt.Adjustable;
import java.awt.Component;

import javax.swing.BoundedRangeModel;
import javax.swing.Icon;

import bard.ui.explodingpixels.macwidgets.BottomBar;

public interface View {
    enum Type {
        Unknown,
            List,
            Grid,
            Record,
            Column;
    }

    Type getType ();
    Icon getIcon ();
    String getName ();
    String getDescription ();

    /**
     * display component
     */
    Component getComponent ();

    /**
     * bottom tool bar for view
     */
    BottomBar getBottomBar ();
    BoundedRangeModel getBoundedRangeModel ();

    /**
     * component viewing window
     */
    Adjustable getAdjustable ();

}
