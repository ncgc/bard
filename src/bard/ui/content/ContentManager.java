package bard.ui.content;

import bard.core.Assay;
import bard.core.Biology;
import bard.core.Compound;
import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.Experiment;
import bard.core.Project;
import bard.ui.icons.IconFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;


public class ContentManager {
    static final Logger logger = 
        Logger.getLogger(ContentManager.class.getName());

    static Map<Class, Class> CONTENTS = new ConcurrentHashMap<Class,Class> ();
    static {
        CONTENTS.put(Compound.class, CompoundContent.class);
        CONTENTS.put(Project.class, ProjectContent.class);
        CONTENTS.put(Assay.class, AssayContent.class);
//        CONTENTS.put(Biology.class, BiologyContent.class);
        CONTENTS.put(Experiment.class, ExperimentContent.class);
    }
    
    final protected EntityServiceManager esm;
    protected List<Content> contents = new ArrayList<Content>();

    public ContentManager (EntityServiceManager esm) {
        this.esm = esm;
    }

    public Content add (EntityService service) {
        return add (service, null);
    }

    public Content add (EntityService service, Object resource) {
        Class clz = CONTENTS.get(service.getEntityClass());
        if (clz != null) {
            try {
                AbstractContent content = (AbstractContent)clz.newInstance();
                content.setContentManager(this);
                content.initContent(service, resource);

                contents.add(content);
                return content;
            }
            catch (Exception ex) {
                throw new IllegalArgumentException (ex);
            }
        }

        throw new IllegalArgumentException 
            ("No matching Content found for service "+service.getClass());
    }

    public Content add (Class<? extends Entity> clazz) {
        return add (esm.getService(clazz));
    }

    public Content add (Class<? extends Entity> clazz, Object etag) {
        return add (esm.getService(clazz), etag);
    }

    public EntityServiceManager getServiceManager () {
        return esm;
    }

    public Content[] getContents () {
        return contents.toArray(new Content[0]);
    }

    private int state = 0;
    public <T extends Entity> Content<T> getContent 
                      (Class<? extends Entity> entityClass) {
	try {
	    while (state == 1)
		Thread.sleep(100);
	    state = 1;
	    for (Content c : contents) {
		if (entityClass.equals(c.getEntityClass())) {
		    return c;
		}
	    }
	    // add if we don't already have it
	    Content<T> content = add(entityClass);
	    return content;
	} catch (InterruptedException e) {
	    e.printStackTrace();
	} finally {
	    state = 0;
	}
	return null;
    }

    static Map<EntityServiceManager, ContentManager> instances = 
        new ConcurrentHashMap<EntityServiceManager, ContentManager> ();

    public static ContentManager getInstance (EntityServiceManager esm) {
        return getInstance (esm, null);
    }

    public static ContentManager getInstance 
        (EntityServiceManager esm, Object resource) {
        ContentManager cm = instances.get(esm);
        if (cm == null) {
            cm = new ContentManager (esm);
            /*
             * in the same order that show up under CONTENTS
             */
            cm.add(Project.class, resource);
            cm.add(Compound.class, resource);
            cm.add(Assay.class, resource);
            //cm.add(Biology.class);

            instances.put(esm, cm);
        }

        return cm;
    }

    static Map<Class<? extends Entity>, Icon> ICONS = 
                               new HashMap<Class<? extends Entity>, Icon>() {{
        put (Compound.class, IconFactory.Content.Compound.getIcon(16));
        put (Project.class, IconFactory.Content.Project.getIcon());
        put (Assay.class, IconFactory.Content.Assay.getIcon(16));
        put (Biology.class, IconFactory.Content.Target.getIcon(16));
        put (Experiment.class, IconFactory.Content.Assay.getIcon(16));
        }};

    public static Icon getIcon (Class<? extends Entity> clazz) {
        return ICONS.get(clazz);
    }
}
