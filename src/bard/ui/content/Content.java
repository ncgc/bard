// $Id: Content.java 4007 2010-02-01 20:29:59Z nguyenda $

package bard.ui.content;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileFilter;

import org.bushe.swing.event.EventService;

import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.SearchParams;
import bard.core.Value;
import bard.ui.util.UIUtil.BARDUri;
import ca.odell.glazedlists.EventList;

/**
 * A Content contains views and states. Views are different presentations
 * of the data. States are different layering of the data such as search,
 * filtering, etc.
 */
public interface Content<T extends Entity> extends ActionListener {

    public Object getResource ();
    public String getName ();
    public void setName (String name);
    public Icon getIcon ();

    public String getDescription ();
    public void setDescription (String description);

    public long getTotalSize (); // return total size
    public long getSize (); // return size as currently loaded
    public long getNewCount (); // return the new count (unseen entities)
    public void setNewCount (long count);

    // get the primary service for this content
    public Class<T> getEntityClass ();
    public EntityService<T> getService ();
    public EntityServiceManager getServiceManager ();
    public ContentManager getContentManager ();
    public <E extends Entity> Content<E> getContent (Class<E> clazz);

    public State<T> getState (); // current state
    public int findState(State<T> state);
    // push a new state onto the content stack
    public State<T> push (State<T> state);
    public boolean canPush();
    public boolean canPop();
    public State<T> push(); // return state from previous stack back to current stack
    public State<T> pop (); // remove current state

    public State<T> search (SearchParams query);
    public State<T> open (BARDUri uri); // open a new state based on uri
    public State<T> reload (); // reload current content

    public Collection<Value> getFacets ();

    public EventList<T> getEntities (); // current collection
    public T getEntity (); // current entity
    public void setEntity (T entity);

    public View getView (); // current view of current state
    public void setView (View.Type type);
    public View.Type[] getSupportedViewTypes ();

    public long getLastViewed ();
    public void setLastViewed (long time);

    public void setBusy (boolean busy);
    public boolean isBusy ();
    public boolean isReadOnly (); // content read-only?

    public void actionPerformed (ActionEvent e);
    public JPopupMenu getContextMenu ();
    public JToolBar getContextToolbar ();

    public EventService getEventService ();
    public boolean isCurationSupported ();

    public void exportData (FileFilter format, File file) 
	throws IOException;

    public boolean isExportSupported ();
    public FileFilter[] getExportFileFilters();

    public void importData (String uri, Map<?,?> options,
	    Map<String, String[]> props) 
		    throws IOException;
    public boolean isImportSupported ();
    public FileFilter[] getImportFileFilters();
}
