// $Id$

package bard.ui.content;

import bard.core.Assay;
import bard.core.AssayValues;
import bard.core.Compound;
import bard.core.Entity;
import bard.core.EntityValue;
import bard.core.Experiment;
import bard.core.Project;
import bard.core.Value;
import bard.core.adapter.AssayAdapter;
import bard.core.adapter.EntityAdapter;
import bard.ui.common.Grid;
import bard.ui.common.GridCellAnnotator;
import bard.ui.explodingpixels.macwidgets.MacFontUtils;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import com.jidesoft.swing.JideTabbedPane;
import edu.stanford.ejalbert.BrowserLauncher;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXImagePanel;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AssayRecordViewPanel extends RecordViewPanel 
    implements GridCellAnnotator, ChangeListener {

    static private Logger logger = Logger.getLogger
	(AssayRecordViewPanel.class.getName());

    static final String PC_URL1 = 
	"http://pubchem.ncbi.nlm.nih.gov/assay/assay.cgi?aid=";

    static final Icon PLUS_SMALL_ICON = IconFactory.Misc.Plus16.getIcon();
    static final Icon MINUS_SMALL_ICON = IconFactory.Misc.Minus16.getIcon();

    static class FilterTreeModel extends DefaultTreeModel {
	Assay assay;

	FilterTreeModel () {
	    super (new DefaultMutableTreeNode ()); // empty root
	}

	void setFilters (Assay assay) {
	    DefaultMutableTreeNode root = (DefaultMutableTreeNode)getRoot ();
	    root.removeAllChildren();
	    this.assay = assay;
	    reload ();
	}

	Assay getAssay () { return assay; }

	void clear () {
	    assay = null;
	    ((DefaultMutableTreeNode)getRoot()).removeAllChildren();
	    reload ();
	}
    }

    static class FilterTreeCellRenderer extends DefaultTreeCellRenderer {

	static final Icon LEAF_ICON  = IconFactory.Misc.Leaf.getIcon();
	static final Icon FILTER_ICON = IconFactory.Misc.Funnel.getIcon();

	FilterTreeCellRenderer () {
	    setLeafIcon (LEAF_ICON);
	    setOpenIcon (FILTER_ICON);
	    setClosedIcon (FILTER_ICON);
	}

	@Override
	public Component getTreeCellRendererComponent
	    (JTree tree, Object value, boolean sel, boolean expanded, 
	     boolean leaf, int row, boolean hasFocus) {
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
	    if (value == null) {
	    }
	    else if (leaf) {
		Object obj = node.getUserObject();
		value = obj;
	    }

	    return super.getTreeCellRendererComponent
		(tree, value, sel, expanded, leaf, row, hasFocus);
	}
    }


    // also the same as FilterTreeModel... refactor? sigh
    static class CollectionTreeModel extends DefaultTreeModel {
	Assay assay;

	CollectionTreeModel () {
	    super (new DefaultMutableTreeNode ());
	}

	void setAssay (AssayAdapter assay) {
	    DefaultMutableTreeNode root = (DefaultMutableTreeNode)getRoot ();
	    root.removeAllChildren();

            Map<String, java.util.List<String>> anno = 
                new HashMap<String, java.util.List<String>>();

            // first pass grab all relevant annotations
            for (Value v : assay.getAnnotations()) {
                if (v.getId().equals("CompoundSpectra")) {
                    java.util.List<String> values = anno.get("QC spectra");
                    if (values == null) {
                        anno.put("QC spectra", 
                                 values = new ArrayList<String>());
                    }
                    values.add((String)v.getValue());
                }
                else if (v.getId().equals("CLINICALTRIALS")) {
                    java.util.List<String> values = 
                        anno.get("ClinicalTrials.gov");
                    if (values == null) {
                        anno.put("ClinicalTrials.gov", 
                                 values = new ArrayList<String>());
                    }
                    values.add((String)v.getValue());
                }
                else if (v.getId().equals("CompoundDrugLabelRx")
                         || v.getId().equals("CompoundDrugLabelOtc")) {
                    java.util.List<String> values = 
                        anno.get("FDA DailyMed");
                    if (values == null) {
                        anno.put("FDA DailyMed", 
                                 values = new ArrayList<String>());
                    }
                    String[] sv = ((String)v.getValue()).split(";");
                    values.add(sv[1]+" "+sv[2]+", "+sv[4]);
                }
            }

            // second pass match them with the collection
            for (Value v : assay.getAnnotations()) {
                if (v.getId().equals("COLLECTION")) {
                    addNode (root, anno, (String)v.getValue());
                }
            }

	    reload ();
	    this.assay = assay.getAssay();
	}

        void addNode (DefaultMutableTreeNode root, 
                      Map<String, java.util.List<String>> anno, 
                      String sv) {

            String[] child = sv.split("\\|");
            DefaultMutableTreeNode n;
            if (child.length > 0) {
                n = new DefaultMutableTreeNode (child[0]);
                for (int i = 1; i < child.length; ++i) {
                    String[] c = child[i].split(":");
                    if (c.length > 0) {
                        DefaultMutableTreeNode p = null;
                        for (Enumeration en = n.children();
                             en.hasMoreElements(); ) {
                            DefaultMutableTreeNode k = 
                                (DefaultMutableTreeNode)en.nextElement();
                            if (c[0].equals(k.getUserObject())) {
                                p = k;
                                break;
                            }
                        }
                        if (p == null) {
                            n.add(p = new DefaultMutableTreeNode
                                  (c[0]));
                        }

                        for (int j = 1; j < c.length; ++j) {
                            p.add(new DefaultMutableTreeNode (c[j]));
                        }
                    }
                    else {
                        n.add(new DefaultMutableTreeNode (child[i]));
                    }
                }
            }
            else {
                n = new DefaultMutableTreeNode (sv);
            }

            java.util.List<String> vals = anno.get((String)n.getUserObject());
            if (vals != null) {
                for (String v : vals) {
                    n.add(new DefaultMutableTreeNode (v));
                }
            }

            if ("Community curation".equals(n.getUserObject())) {
                root.insert(n, 0);
            }
            else if ("ChemIDPlus diffs".equals(n.getUserObject())) {
                // ignore this bogus collection
            }
            else {
                root.add(n);
            }
        }

	Assay getAssay () { return assay; }

	void clear () {
	    assay = null;
	    ((DefaultMutableTreeNode)getRoot()).removeAllChildren();
	    reload ();
	}	    
    }

    static class CollectionTreeCellRenderer extends DefaultTreeCellRenderer {

	static final Icon LEAF_ICON = IconFactory.Misc.Brightness.getIcon();
	static final Icon COLLECTION_ICON = IconFactory.Misc.Tag.getIcon();
	static final Icon STAR_ICON = IconFactory.Misc.Star.getIcon();
	static final Icon RAINBOW_ICON = IconFactory.Misc.Rainbow.getIcon();
        static final Icon PILL_ICON = IconFactory.Misc.Pill.getIcon();

	CollectionTreeCellRenderer () {
	    setLeafIcon (LEAF_ICON);
	    //setOpenIcon (IconFactory.Misc.ArrowMergeDown.getIcon());
	    //setClosedIcon (IconFactory.Misc.ArrowMergeLeft.getIcon());
            setOpenIcon (null);
            setClosedIcon (null);
	}

	@Override
	public Component getTreeCellRendererComponent
	    (JTree tree, Object value, boolean sel, boolean expanded, 
	     boolean isLeaf, int row, boolean hasFocus) {
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
	    if (value == null) {
	    }
	    else {
		value = node.getUserObject();
                String par = "";
                if (node.getParent() != null) {
                    par = (String)((DefaultMutableTreeNode)node
                                   .getParent()).getUserObject();
                    if (par == null) {
                        par = "";
                    }
                }

                Icon leaf = LEAF_ICON;
                if ("Community curation".equals(value)
                    || "Community curation".equals(par)) {
                    leaf = STAR_ICON;
                }
                else if ("QC spectra".equals(value) 
                         || "QC spectra".equals(par)) {
                    leaf = RAINBOW_ICON;
                }
                else if ("ClinicalTrials.gov".equals(value) 
                         || "ClinicalTrials.gov".equals(par)) {
                    leaf = IconFactory.Content.ClinicalTrial2.getIcon();
                }
                else if (value != null) {
                    String s = ((String)value).toLowerCase();
                    if (s.indexOf("approved") >= 0
                        || s.indexOf("fda") >= 0) {
                        //leaf = PILL_ICON;
                    }
                    else if (par.toLowerCase().indexOf("approved") >= 0
                             || par.toLowerCase().indexOf("fda") >= 0) {
                        leaf = PILL_ICON;
                    }
                }
                setLeafIcon (leaf);
	    }
	    return super.getTreeCellRendererComponent
		(tree, value, sel, expanded, isLeaf, row, hasFocus);
	}
    }
    
    class CollectionTree extends JTree implements TreeSelectionListener {
	CollectionTreeModel model = new CollectionTreeModel ();

	CollectionTree () {
	    setModel (model);
	    setRootVisible (false);
	    setCellRenderer (new CollectionTreeCellRenderer ());
	    /*
              getSelectionModel().setSelectionMode
              (TreeSelectionModel.SINGLE_TREE_SELECTION);
              addTreeSelectionListener (this);
	    */
	}

	public void setAssay (AssayAdapter assay) {
	    model.setAssay(assay);
	    DefaultMutableTreeNode root = 
		(DefaultMutableTreeNode)model.getRoot();
	    for (Enumeration en = root.depthFirstEnumeration ();
		 en.hasMoreElements(); ) {
		DefaultMutableTreeNode leaf = 
		    (DefaultMutableTreeNode)en.nextElement();
		TreePath tp = new TreePath (leaf.getPath());
		expandPath (tp);
	    }
	}

	public void clear () {
	    clearSelection ();
	    model.clear();
	}

	public void valueChanged (TreeSelectionEvent e) {
	    if (model.getAssay() == null) {
		return;
	    }

	    TreePath path = getSelectionPath ();
	    if (path != null) {
		DefaultMutableTreeNode node = 
		    (DefaultMutableTreeNode)path.getLastPathComponent();
	    }
	}
    }


    class AssaySummaryRecord extends UIRecordPanel<AssayAdapter>  {
	JEditorPane description;
	JEditorPane protocol;
	JEditorPane comments;
	JEditorPane nameEP;
        JLabel sourceLabel;
        
        JideTabbedPane tab;

	AssaySummaryRecord () {
	    super (new BorderLayout (0, 5));
	    
            JPanel pane = new JPanel (new BorderLayout ());
            pane.setOpaque(false);
            pane.add(nameEP = createEP ("Name"), BorderLayout.CENTER);
            sourceLabel = new JLabel();
            sourceLabel.setOpaque(false);
            sourceLabel.setBorder(BorderFactory.createTitledBorder
			 (BORDER, "Source", TitledBorder.LEADING, 
				  TitledBorder.DEFAULT_POSITION, 
				  MacFontUtils.ITUNES_TABLE_HEADER_FONT));
            pane.add(sourceLabel, BorderLayout.LINE_START);
	    add (pane, BorderLayout.NORTH);
	    add (createTab ());

	    setBorder (BorderFactory.createEmptyBorder(2,2,2,2));
	}

	JComponent createTab () {
	    tab = new JideTabbedPane ();
	    tab.setTabShape(JideTabbedPane.SHAPE_BOX);
	    tab.setTabPlacement(JTabbedPane.BOTTOM);
	    tab.setOpaque(false);

	    description = createEP("Description");
	    protocol = createEP("Protocol");
	    comments = createEP("Comments");
	    
	    tab.addTab("Description", createScrollPane (description));
	    tab.addTab("Protocol", createScrollPane (protocol));
	    tab.addTab("Comments", createScrollPane (comments));

	    return tab;
	}

	public void clear () {
	    nameEP.setText(null);
	    nameEP.setToolTipText(null);
            sourceLabel.setText(null);
            sourceLabel.setIcon(null);
            description.setText(null);
            protocol.setText(null);
            comments.setText(null);
	}

	public void update (AssayAdapter a) {
            setBusy (true);

	    if (a == null) {
		clear ();
		return;
	    }

	    String name = a.getAssay().getName();
            
	    StringBuilder sb = new StringBuilder (UIUtil.CSS);
            StringBuffer aids = new StringBuffer();
            for (Value expt: a.getAssay().getValues(Experiment.class.toString())) {
                Experiment exp = (Experiment)a.getAssay().getEntityFromValue((EntityValue)expt);
                if (exp != null && aids.length() == 0) {
                    aids.append(exp.getPubchemAid());
                }
            }
	    if (aids.length() > 0) {
                 sb.append("<a href=\""+PC_URL1 + aids+"\">"+name+"</a>");
            }
            else {
                sb.append(name);
            }

	    nameEP.setText(sb.toString());
	    nameEP.setCaretPosition(0);

	    sb = new StringBuilder();
	    if (a.getAssay().getValue("AssaySource") != null)
		sb.append(a.getAssay().getValue("AssaySource").getValue().toString());
	    ImageIcon icon = null;
	    for (IconFactory.CenterIcon center: IconFactory.CenterIcon.values()) {
		if (center.getCenter().equals(sb.toString()) || center.getFullName().equals(sb.toString())) {
		    icon = center.getIcon();
		    sb = new StringBuilder(center.getFullName());
		    break;
		}
	    }
	    if (icon != null) {
		sourceLabel.setIcon(icon);
		sourceLabel.setText(null);
		sourceLabel.setToolTipText(sb.toString());
		if (icon.getIconWidth() < 50)
		    sourceLabel.setPreferredSize(new Dimension(50, 50));
		else sourceLabel.setPreferredSize(null);
	    } else {
		sourceLabel.setIcon(null);
		sourceLabel.setText(sb.toString());
		sourceLabel.setToolTipText(sb.toString());
		sourceLabel.setPreferredSize(null);
	    }

	    String desc = "";
	    if (a.getAssay().getValue(AssayValues.AssayDescription) != null)
	        desc = a.getAssay().getValue(AssayValues.AssayDescription).getValue().toString();
	    desc = desc.replaceAll("\n", "<br>");
	    desc = desc.replaceAll("<br><br>", "<P>");
	    description.setText(UIUtil.CSS+desc);
	    description.setCaretPosition(0);
	    
	    desc = ""+a.getAssay().getValue(AssayValues.AssayProtocol);
	    desc = desc.replaceAll("\n", "<br>");
	    desc = desc.replaceAll("<br><br>", "<P>");
	    if (desc.length() == 0) tab.setEnabledAt(1, false);
	    else tab.setEnabledAt(1, true);
	    protocol.setText(UIUtil.CSS+desc);
	    protocol.setCaretPosition(0);
	    
	    
	    desc = ""+a.getAssay().getComments();
	    desc = desc.replaceAll("\n", "<br>");
	    desc = desc.replaceAll("<br><br>", "<P>");
	    if (desc.length() == 0) tab.setEnabledAt(2, false);
	    else tab.setEnabledAt(2, true);
	    comments.setText(UIUtil.CSS+desc);
	    comments.setCaretPosition(0);
	    	    
	    setBusy (false);
 	}

        void loadSynonyms (AssayAdapter c) {
            DefaultListModel model = (DefaultListModel)createList().getModel();
	    model.clear();

            String[] syns = c.getSynonyms();
            //logger.info("## Compound "+c.getName()+" "+syns.length);
            if (syns.length == 0) {
                //new SynonymLoader (c, model, this).execute();
            }
            else {
                for (String s : syns) {
                    model.addElement(s);
                }
            }
        }
    }


    class ResultsRecord extends UIRecordPanel<AssayAdapter> {
	JEditorPane resultsEP, moaEP;

	ResultsRecord () {
	    super (new MigLayout 
		   ("", "[grow,fill]",
		    "[min:50,grow 50,fill]unrel[min:50,grow 50,fill]"));
	    add (createScrollPane ("Results", resultsEP = createEP ()), 
		 "spanx,grow,wrap");
	    add (createScrollPane ("Mechanism of Action", 
				   moaEP = createEP ()), "spanx,grow");
	}

	public void clear () {
	    resultsEP.setText(null);
	    moaEP.setText(null);
	}

	public void update (AssayAdapter a) {
	    if (a == null) {
		clear ();
		return;
	    }

	    Assay ass = a.getAssay();
	    
            Collection<Value> anno = a.getAnnotations();
            Value moa = null, ind = null;
            for (Value v : anno) {
                if (v.getId().equals("CompoundMOA")) {
                    moa = v;
                }
                else if (v.getId().equals("CompoundIndication")) {
                    ind = v;
                }
            }
            
            if (moa != null) {
                moaEP.setText(UIUtil.CSS+(String)moa.getValue());
                moaEP.setCaretPosition(0);
            }
            if (ind != null) {
                resultsEP.setText(UIUtil.CSS+(String)ind.getValue());
                resultsEP.setCaretPosition(0);
            }
	}
    }

    class PropertiesRecord extends UIRecordPanel<AssayAdapter>
	implements ActionListener {

	JTextField mw, mass, formula, tpsa, stereocenters, xlogp;
	JTextArea smi, iupac;
	JXHyperlink href;
	JXImagePanel img;
        

	PropertiesRecord () {
	    super (new MigLayout ("wrap", "[right][grow,fill]", ""));
            add (new JLabel ("IUPAC Name"), "top");
	    add (createSimpleScrollPane (iupac = new JTextArea ()), "grow");
	    iupac.setOpaque(false);
	    iupac.setRows(3);
	    iupac.setEditable(false);
	    iupac.setLineWrap(true);

	    add (new JLabel ("Molecular Weight"));
	    add (mw = field ());
	    add (new JLabel ("Exact Mass"));
	    add (mass = field ());
            add (new JLabel ("LogP"));
            add (xlogp = field ());
            add (new JLabel ("TPSA"));
            add (tpsa = field ());
	    add (new JLabel ("Molecular Formula"));
	    add (formula = field ());
	    add (new JLabel ("Stereocenters"));
	    add (stereocenters = field ());

	    add (new JLabel ("SMILES"), "top");
	    add (createSimpleScrollPane (smi = new JTextArea ()), "grow");
	    smi.setOpaque(false);
	    smi.setRows(3);
	    smi.setEditable(false);
	    smi.setLineWrap(true);

	    href = new JXHyperlink ();
	    href.setClickedColor(href.getUnclickedColor());
	    href.setText("Click for additional properties...");
	    href.addActionListener(this);
	    add (href, "spanx,grow");
	}

	public void clear () {
	    mw.setText(null);
	    mass.setText(null);
	    formula.setText(null);
	    smi.setText(null);
	    stereocenters.setText(null);
            iupac.setText(null);
            tpsa.setText(null);
            xlogp.setText(null);
            href.setToolTipText(null);
            href.putClientProperty("_target", null);
	}

	public void update (AssayAdapter c) {
	    if (c == null) {
		clear ();
		return;
	    }

	    stereocenters.setText(null);
	    iupac.setToolTipText(null);
	}

	public void actionPerformed (ActionEvent e) {
	    if (e.getSource() == href) {
		String url = (String)href.getClientProperty("_target");
		if (url != null) {
		    try {
			new BrowserLauncher().openURLinBrowser(url);
		    }
		    catch (Exception ex) {
			logger.log(Level.SEVERE, 
				   "Can't launch external browser " +url, ex);
		    }
		}
	    }
	}
    }

    class DrugLabelPane extends UIRecordPanel<AssayAdapter> {
	JEditorPane ep = createEP ();
	String title;

	DrugLabelPane () {
	    super (new BorderLayout ());
	    add (ep);
	}

	DrugLabelPane (String label) {
	    super (new BorderLayout ());
	    add (ep);
	    setLabel (label);
	}

	/*
	 * a label has the following format
	 * 53808-0811;ZIAGEN;tablet, film coated;ABACAVIR SULFATE;300 mg;J220T4J9Q2 WR2TIP26VS
	 */
	public void setLabel (String label) {
	    String[] toks = label.split(";");

	    StringBuilder sb = new StringBuilder (HTML);
	    sb.append("<table>");
	    sb.append("<tr valign=\"top\">"
		      +"<td align=\"right\"><b>NDC</b></td><td>"
		      +"<a target=\"_tripod\" href=\"http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?ndc="+toks[0]+"\">"+toks[0]+"</a></td></tr>");
	    sb.append("<tr valign=\"top\"><td align=\"right\">"
		      +"<b>Active Ingredient</b></td><td>"+toks[3]+"</td></tr>");
	    sb.append("<tr valign=\"top\"><td align=\"right\">"
		      +"<b>Strength</b></td><td>"+toks[4]+"</td></tr>");
	    String[] uniis = toks[toks.length-1].split("[\\s]+");
	    sb.append("<tr valign=\"top\"><td align=\"right\">"
		      +"<b>UNII</b></td><td>");
	    for (String u : uniis) {
		sb.append("<a target=\"_tripod\" href=\"http://chem.sis.nlm.nih.gov/chemidplus/direct.jsp?name=UNII-"+u+"&result=advanced\">"+u+"</a>&nbsp;");
	    }
	    sb.append("</td></tr>");
	    sb.append("</table>");

	    ep.setText(sb.toString());
	    title = toks[1]+" "+toks[2] + ", "+toks[4];
	}

	public String getTitle () { return title; }
	public void clear () { ep.setText(null); }
	public void update (AssayAdapter c) {}
    }

//    private Grid grid; // main assay grid  !!! update with my own assay component
    private Assay assay; // current assay
    private AssayAdapter assayAdapter;

    private java.util.List<UIRecord<AssayAdapter>> forms;
    private JList annoList; // annotations list
    private BoundedRangeModel range;

    public AssayRecordViewPanel (AssayContent content) {
	super (content);

	if (!content.getEntities().isEmpty()) {
	    updateView (0);
//	} else {
//	    updateView(-1);
	}
    }

    public String getGridCellAnnotation (Grid g, Object v, int c) {
	if (v == null) {
	    return null;
	}
	Assay assay = (Assay)v;

	String name = assay.getName();
	if (name == null || name.length() == 0) {
	    //String[] syn = assay.getSynonyms();
	    //name = syn != null && syn.length > 0 ? syn[0] : null;
	}
	return name;
    }

    public String getGridCellBadgeAnnotation (Grid g, Object v, int c) {
        return null;
    }

    public void stateChanged (ChangeEvent e) {
    }
    
    @Override
    public BoundedRangeModel getBoundedRangeModel () { 
	if (range == null) {
	    range = new DefaultBoundedRangeModel (120, 0, 50, 400);
	    range.addChangeListener(this);
	}
	return range;
    }

    @Override
    protected void show (Entity e) {
	if (e == null) {
	    clear ();
	    return;
	}
	else if (!(e instanceof Assay)) {
	    throw new IllegalArgumentException
		("Entity is not a assay instance!");
	}
        assay = (Assay)e;
	assayAdapter = new AssayAdapter (assay);

//	grid.clear();
//	grid.addValue(assay);
	for (UIRecord<AssayAdapter> f : forms) {
	    f.update(assayAdapter);
	}

	try {
	    updateRelatedContent (assayAdapter);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't show related contents", ex);
	}
	//repaint ();
    }


    @Override
    protected void clear () {
	for (UIRecord<AssayAdapter> f : getForms ()) {
	    f.clear();
	}

        JTabbedPane tab = getRelatedContentTab ();
        for (int i = 0; i < tab.getTabCount(); ++i) {
            String title = tab.getTitleAt(i);
            Component c = tab.getComponentAt(i);
            if (c instanceof UIRelatedRecordPanel) {
                UIRelatedRecordPanel form = 
                    (UIRelatedRecordPanel)c;
                form.clear();
                //tab.setTitleAt(i, form.getRelatedContent().getName());
            }
	}

//	grid.clear();
    }

    // show related entities
    protected void updateRelatedContent (EntityAdapter entity) 
        throws Exception {
	//logger.info(getContent().getName()+" "+entity.getId());
	//EntityRegistry reg = getContent().getRegistry();

	/*
	  Class<? extends Entity>[] linkCls = reg.getAvailableLinks();
	  for (Class<? extends Entity> lnk : linkCls) {
	  String[] links = reg.getTargetLinks(lnk, e.getId());
	  logger.info(" => "+ lnk + " "+links.length);
	  }
	*/
	/*
          fragGrid.clear();
          for (String f : entity.get(Compound.FragmentUnique, new String[0])) {
          fragGrid.addValue(f);
          }
          topoGrid.clear();
          for (String f : entity.get(Compound.Topology, new String[0])) {
          topoGrid.addValue(f);
          }
	*/

	// ignore the Fragments & Topologies tabs
	Assay a = ((AssayAdapter)entity).getAssay();
	JTabbedPane tab = getRelatedContentTab ();
	for (int i = 0; i < tab.getTabCount(); ++i) {
	    String title = tab.getTitleAt(i);
            Component comp = tab.getComponentAt(i);
            if (comp instanceof UIRelatedRecordPanel) {
                UIRelatedRecordPanel form = (UIRelatedRecordPanel)comp;
                form.update(entity.getEntity());
                /*
                tab.setTitleAt
                    (i, form.getRelatedContent().getName()
                     +" ("+form.getRelatedCount()+")");
                */
	    } else if ("Annotations".equals(title)) {
		DefaultListModel model = (DefaultListModel)annoList.getModel();
		model.clear();
		// make sure annotations have been loaded
		a.getAnnotations();
		ArrayList<String> prefix = new ArrayList<String>();
		for (Value v: a.getValues()) {
		    recurseAnnotations(model, prefix, v);
		}
	    }
	}
    }

    JComponent createContentComponent (Content c, Entity e) {
	return new JLabel (e.getClass().toString());
    }

    synchronized java.util.List<UIRecord<AssayAdapter>> getForms () {
	if (forms == null) {
	    forms = new ArrayList<UIRecord<AssayAdapter>>();
	}
	return forms;
    }


    @Override
    protected JComponent createContentPane () {
//	grid = createMolGrid ();
//	grid.setCellAnnotator(this);
//	grid.addComponentListener(new ComponentAdapter () {
//		public void componentResized (ComponentEvent e) {
//		    JComponent c = (JComponent)e.getSource();
//		    Rectangle r = c.getBounds();
//		    grid.setCellSize
//			((int)(Math.min(r.width, r.height)*.8+0.5));
//		}
//	    });
//
//	//grid.setUseDefaultBackground(false);

	JSplitPane split = createSplitPane (JSplitPane.HORIZONTAL_SPLIT);
//	split.setLeftComponent(grid);
	split.setRightComponent(createAssayInfoPane ());
	split.setResizeWeight(.4);

	return split;
    }


    JComponent createAssayInfoPane () {
	JPanel pane = new JPanel (new BorderLayout ());
	//pane.setBackground(Color.white);

	JideTabbedPane tab = new JideTabbedPane ();
	tab.setTabShape(JideTabbedPane.SHAPE_BOX);
	//tab.setOpaque(false);

	UIRecord<AssayAdapter> form;
	getForms().add(form = new AssaySummaryRecord ());
	tab.addTab("Summary", form.component());

	//getForms().add(form = new RegulatoryRecord ());
	//tab.addTab("Regulatory Status", form.component());

	getForms().add(form = new ResultsRecord ());
	tab.addTab("Results", form.component());

	//getForms().add(form = new PropertiesRecord ());
	//tab.addTab("Properties", form.component());

	initRelatedContentTab (tab);

	pane.add(tab);
	return pane;
    }

    @Override
    protected void initRelatedContentTab (JTabbedPane tab) {
	super.initRelatedContentTab(tab);

	int size = getBoundedRangeModel().getValue();
	
        createProjectTab (tab);
        //createBiologyTab (tab);
        createCompoundTab (tab);
        createAnnotationTab (tab);
    }

    void createProjectTab (JTabbedPane tab) {
        tab.addTab("Projects", ContentManager.getIcon(Project.class), 
                   new UIRelatedProjectRecordPanel (getContent ()));
    }

    void createCompoundTab (JTabbedPane tab) {
        tab.addTab("Compounds", ContentManager.getIcon(Compound.class), 
                   new UIRelatedCompoundRecordPanel (getContent ()));
    }

    /*
    void createBiologyTab (JTabbedPane tab) {
        //JPanel pane = new JPanel (new BorderLayout ());
        Content<Assay> content = getContent().getContentManager()
            .getContent(Biology.class);
        Component pane = new UIRelatedBiologyRecordPanel(content);
        tab.addTab(content.getName(), content.getIcon(), pane);
    }
    */

    void createAnnotationTab (JTabbedPane tab) {
        //JPanel pane = new JPanel (new BorderLayout ());
        annoList = createList();
        annoList.addListSelectionListener( new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
        	   if (e.getValueIsAdjusting())
        	      return;

        	   String entry = (String)annoList.getSelectedValue();
        	   if (entry != null && entry.indexOf("http://") > -1) {
        	       entry = entry.substring(entry.indexOf("http://"));
        	       try {
        		   URL url = new URL(entry.split("\\s")[0]);
        		   hyperlinkUpdate(new HyperlinkEvent(entry, HyperlinkEvent.EventType.ACTIVATED, url));
        	       } catch (Exception ex) {ex.printStackTrace();}
        	   }
        	   
       	}
        });
        
        tab.addTab("Annotations", IconFactory.Misc.Sticky.getIcon(), createScrollPane(annoList));
    }
}
