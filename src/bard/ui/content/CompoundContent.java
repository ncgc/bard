package bard.ui.content;

import bard.core.*;
import bard.core.adapter.CompoundAdapter;
import bard.ui.common.*;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.ViewChangeEvent;
import bard.ui.explodingpixels.macwidgets.BottomBar;
import bard.ui.explodingpixels.macwidgets.MacButtonFactory;
import bard.ui.explodingpixels.macwidgets.MacIcons;
import bard.ui.util.UIUtil;
import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;


public class CompoundContent extends AbstractContent<Compound> {
    private static final Logger logger = 
        Logger.getLogger(CompoundContent.class.getName());

    private static final Preferences PREFS = 
	Preferences.userNodeForPackage(CompoundContent.class);
    private static final Preferences NODE;
    static {
	Preferences node = PREFS;
	try {
	    node = PREFS.node(CompoundContent.class.getSimpleName());
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't get preference node for "
		       +CompoundContent.class, ex);
	}
	NODE = node;
    }

//    /**
//     * List view
//     */
//    class CompoundListViewPanel extends ListViewPanel 
//	implements EntityTableColumnFactory, 
//		   EventSubscriber<ContentChangeEvent>,
//		   PropertyChangeListener {
//	Entity[] selections;
//
//	CompoundListViewPanel () {
//	    super (CompoundContent.this);
//	    getTable().setEntityColumnFactory(Compound.class, this);
//            setListSource (Compound.class, getEntities ());
//
//	    //getTable().setRowHeight(150);
//	    EventBus.subscribe(ContentChangeEvent.class, this);
//	}
//
//	public void propertyChange (PropertyChangeEvent e) {
//	    String prop = e.getPropertyName();
//	    if (!"action".equals(prop)) {
//		return;
//	    }
//	    final Compound cmpd = (Compound)e.getNewValue();
//	    //logger.info(cmpd.toString());
//
//	    SwingUtilities.invokeLater(new Runnable () {
//		    public void run () {
//		    }
//		});
//	}
//
//	/*
//	 * EntityTableColumnFactory interface
//	 */
//	public EntityTableColumn[] createColumns 
//	    (int modelIndex, EntityTableModel model) {
//	    Class clazz = model.getColumnClass(modelIndex);
//	    if (!clazz.isAssignableFrom(Compound.class)) {
//		return new EntityTableColumn[0];
//	    }
//            
//	    ArrayList<EntityTableColumn> columns = 
//		new ArrayList<EntityTableColumn>();
//	    EntityTableColumn column;
//
//	    column = new EntityTableColumn ("CID", modelIndex, model);
//	    TokenCellEditorRenderer renderer = 
//                new TokenCellEditorRenderer (Compound.PubChemCIDValue);
//            column.setCellRenderer(renderer);
//	    columns.add(column);
//
//            /*
//	    column = new EntityTableColumn ("Probe", modelIndex, model);
//            renderer = new TokenCellEditorRenderer (Compound.ProbeIDValue);
//            column.setCellRenderer(renderer);
//	    columns.add(column);
//            */
//
//	    column = new EntityTableColumn ("Name", modelIndex, model);
//            renderer = new TokenCellEditorRenderer () {
//                    @Override
//                    protected Value[] parseValue (Object value) {
//                        Entity e = (Entity)value;
//                        return new Value[] {
//                            new StringValue 
//                            ((DataSource)null, "Name", e.getName())
//                        };
//                    }
//                };
//            column.setCellRenderer(renderer);
//	    column.setCellEditor(renderer);
//	    columns.add(column);
//
//	    column = new EntityTableColumn ("IUPAC", modelIndex, model);
//            renderer = new TokenCellEditorRenderer (Compound.IUPACNameValue);
//            column.setCellRenderer(renderer);
//	    column.setCellEditor(renderer);
//	    columns.add(column);
//
//	    column = new EntityTableColumn ("SID", modelIndex, model);
//            renderer = new TokenCellEditorRenderer
//                (Compound.PubChemSIDValue, true);
//            column.setPreferredWidth(80);
//	    column.setCellRenderer(renderer);
//	    column.setCellEditor(renderer);
//	    columns.add(column);
//
//	    return columns.toArray(new EntityTableColumn[0]);
//	}
//
//	public void onEvent (ContentChangeEvent e) {
//	    if (e.getContent() != CompoundContent.this) {
//		return;
//	    }
//            /*
//            logger.info("name="+e.getName()
//                        +" old="+e.getOldValue()+" new="+e.getNewValue());
//            */
//            if (e.getName().equals(ContentChangeEvent.STATE)) {
//                State<Compound> state = (State<Compound>)e.getNewValue();
//                logger.info("## STATE: "+CompoundContent.this.getName()
//                            +" old="+e.getOldValue()
//                            +" new="+e.getNewValue()
//                            +" size="+state.size());
//                setListSource (Compound.class, state.getEntities());
//                getTable().setSelectionModel(state.getSelectionModel());
//            }
//	}
//    }
//

    /**
     * Grid view
     */
    class CompoundCategoryGroupingPanel extends CategoryGroupingPanel
	implements GridCellAnnotator, 
		   //ListSelectionListener, 
                   CategoryGroupingModel,
		   ChangeListener, EventSubscriber<ContentChangeEvent>,
		   PropertyChangeListener {

	Grid grid;
	JPanel comp;
	DefaultBottomBar bottomBar;
        BoundedRangeModel range;
        Map<String, CategoryView[]> categories = 
            new TreeMap<String, CategoryView[]>();
        JScrollPane scroller;

	CompoundCategoryGroupingPanel () {
	    super (CompoundContent.this);
            setModel (this);

	    EventBus.subscribe(ContentChangeEvent.class, this);
	}

	@Override
	protected Component getUngroupComponent () { 
	    if (comp == null) {
		grid = new Grid ();

                if (range == null) {
                    range = new DefaultBoundedRangeModel (120, 0, 50, 400);
                    range.addChangeListener(this);
                    if (bottomBar == null) updateBottomBar();
                    bottomBar.setSliderRangeModel(range);
                }
                    
                grid.setCellSize(range.getValue());
		grid.setCellRenderer(new MolCellRenderer ());
                grid.setListSource(getEntities ());

		// listen for double-click on the compound
		MolCellEditor mce = new MolCellEditor ();
		mce.addPropertyChangeListener(this);
		mce.instrumentContextMenu();
		grid.setCellEditor(mce);
		grid.setCellAnnotator(this);
		//grid.addSelectionListener(this);
		grid.setListSource(getEntities ());
		grid.setPopupMenu(getContextMenu ());
		
		comp = new JPanel (new BorderLayout ());
		comp.add(scroller = UIUtil.iTunesScrollPane(grid));
	    }
	    return comp; 
	}


	public String getGridCellAnnotation (Grid g, Object v, int c) {
	    if (v == null || !(v instanceof Entity)) {
		return null;
	    }
	    
	    Entity e = (Entity)v;
	    String name = e.getName();
	    if (name != null && name.length() > 0) {
		return name;
	    }

	    return null;
	}

	public String getGridCellBadgeAnnotation (Grid g, Object v, int c) {
	    if (v == null || !(v instanceof Compound)) {
		return null;
	    }
            Object id = ((Compound)v).getId();
            return id != null ? id.toString() : null;
	}

	@Override
	public BottomBar getBottomBar () { 
	    if (bottomBar == null) updateBottomBar(); //bottomBar = new DefaultBottomBar();
	    return bottomBar; 
	}

	protected void updateBottomBar() {
	    bottomBar = new DefaultBottomBar();
	    bottomBar.setSliderRangeModel(getBoundedRangeModel());

		final JPopupMenu selectMenu = content.getContextMenu();	
	        AbstractButton contextButton = 
	            (AbstractButton) MacButtonFactory.createGradientButton
	            (MacIcons.GEAR, new ActionListener() {
	                    public void actionPerformed(ActionEvent e) {
	                        JButton source = (JButton)e.getSource();
	                        selectMenu.show(source, source.getX(), source.getY());
	                    }
	                });
	        contextButton.setBorder(BorderFactory.createEmptyBorder());
		contextButton.setActionCommand("Selection menu");
		contextButton.setToolTipText("Selection menu");
		
	    bottomBar.addComponentToLeft(contextButton);
	}
	    
	@Override
	public BoundedRangeModel getBoundedRangeModel () { 
	    return range;
	}

        @Override
        public Adjustable getAdjustable () {
            return scroller.getVerticalScrollBar();
        }

	public void stateChanged (ChangeEvent e) {
	    SwingUtilities.invokeLater(new Runnable () {
		    public void run () {
			grid.setCellSize(range.getValue());
		    }
		});
	}

	public void onEvent (ContentChangeEvent e) {
	    if (e.getContent() != CompoundContent.this) {
		return;
	    }

            /*
            logger.info("name="+e.getName()
                        +" old="+e.getOldValue()
                        +" new="+e.getNewValue());
            */
        String prop = e.getName();
        if (prop.equals(ContentChangeEvent.SELECTION)) {
        } else if (prop.equals(ContentChangeEvent.SORT)) {
//            logger.severe("Got a SORT event, will reorder grid  ");
//            State<Compound> state = getState();
//            grid.setListSource(state.getEntities());
//            grid.clearSelection();
//            grid.setSelectionModel(state.getSelectionModel());
//            state.getSelectionModel().removeListSelectionListener(grid);
//            state.getSelectionModel().addListSelectionListener(grid);
        } else if (e.getName().equals(ContentChangeEvent.STATE)) {
            State<Compound> state = (State<Compound>) e.getNewValue();
            grid.setListSource(state.getEntities());
            grid.setSelectionModel(state.getSelectionModel());
            state.getSelectionModel().removeListSelectionListener(grid);
            state.getSelectionModel().addListSelectionListener(grid);
        }
	}

	public void propertyChange (PropertyChangeEvent e) {
	    String prop = e.getPropertyName();
	    logger.info("prop="+prop+" old="+e.getOldValue()+" new="+e.getNewValue());
	    if (prop == null || !prop.startsWith("action-click:")) {
		return;
	    }
	    int clicks = Integer.valueOf(prop.substring(13));
	    final Compound cmpd = (Compound)e.getNewValue();
	    if (clicks > 1) {
		grid.modifySelection(cmpd);
		EventBus.publish(new ViewChangeEvent
			(this, getView().getType(), View.Type.Record));
		EventBus.publish(new ContentChangeEvent
			(CompoundContent.this, ContentChangeEvent.ENTITY, 
				getEntity(), cmpd));
	    } else {
		grid.modifySelection(cmpd);
	    }
	}

        /**
         * CategoryGroupingModel
         */
        public String[] getCategoryGroups () {
            //return categories.keySet().toArray(new String[0]);
            return new String[0];
        }
        
        public CategoryView[] getCategories (String group) {
            return categories.get(group);
        }
    }

    protected List<Object> entityOrder;

    protected CategoryGroupingPanel gridView;
    protected ListViewPanel listView;
    protected CompoundRecordViewPanel recordView;

    protected CompoundContent () {
        setName ("Compounds");
    }

    public CompoundContent (CompoundService service) {
        super (service);
    }

    public CompoundContent (CompoundService service, Object etag) {
        super (service, etag);
    }

    protected Preferences getPreferences () { return NODE; }
    public Class<Compound> getEntityClass () { return Compound.class; }

    public View.Type[] getSupportedViewTypes () {
        return new View.Type[]{
        	View.Type.Grid, View.Type.List, View.Type.Record
        };
    }

    /*
     * Views
     */
    @Override
    protected View createListView() {
        if (listView == null) {
            listView = new MolecularSpreadsheet(this);

            listView.getTable().getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    // if the table gets sorted, we get an UPDATE. Save the resultant
                    // ordering in the state
                    Map<Object, Integer> oldOrder = ((DefaultState) getState()).getIdOrder();
                    if (e.getType() == TableModelEvent.UPDATE) {
                        EntityTableModel model = (EntityTableModel) e.getSource();
                        Map<Object, Integer> orderMap = new HashMap<Object, Integer>();
                        for (int i = 0; i < model.getRowCount(); i++) {
                            Object o = ((Compound) model.getValueAt(i, 1)).getId();
                            orderMap.put(o, i);
                        }
                        ((DefaultState) getState()).setIdOrder(orderMap);

                        // only publish this event if the ordering has actually changed
                        // assumes that the older order map and the one we just calculated
                        // has the exact same keyset
                        for (Object oldKey : oldOrder.keySet()) {
                            int oldPosition = oldOrder.get(oldKey);
                            int newPosition = orderMap.get(oldKey);
                            if (oldPosition != newPosition) {
                                logger.severe("Table order changed, notifying grid to resort");
                                EventBus.publish(
                                        new ContentChangeEvent(
                                                CompoundContent.this,
                                                ContentChangeEvent.SORT,
                                                null, null));
                                break;

                            }
                        }

                    }
                }
            });
        }
        return listView;
    }

    @Override
    protected View createGridView () {
        if (gridView == null) {
            gridView = new CompoundCategoryGroupingPanel ();
        }
        return gridView;
    }

    @Override
    protected View createRecordView () {
        if (recordView == null) {
            recordView = new CompoundRecordViewPanel (this);
        }
        return recordView;
    }

    static final FileFilter FILTER_SDF = new FileNameExtensionFilter 
	("MDL molecular format (.sd, .sdf, .mol)", "sd", "sdf", "mol");
    static final FileFilter FILTER_SMI = new FileNameExtensionFilter
	("Tab-delimited text format (.txt, .smi, .smiles)", "txt", "smi", "smiles");
    static final FileFilter FILTER_CSV = new FileNameExtensionFilter
	("Comma-delimited text format (.csv)", "csv");
    int MAX_EXPORT = 5000;
    int exportLimit = MAX_EXPORT;
    JDialog progressDialog;
    JProgressBar progressBar;

    protected void updateProgressDialog (int current, int max) {
	if (progressDialog == null) {
	    progressDialog = new JDialog();
	    progressDialog.setModal(false);
	    progressDialog.setTitle("Export Collection");
	    progressBar = new JProgressBar(0, 100);
	    progressDialog.add(BorderLayout.CENTER, progressBar);
	    progressDialog.add(BorderLayout.NORTH, new JLabel("Progress..."));
	    JButton cancelBtn = new JButton("Cancel");
	    cancelBtn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    exportLimit = 0;
		    progressDialog.setVisible(false);
		}
	    });
	    progressDialog.add(BorderLayout.SOUTH, cancelBtn);
	    progressDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    progressDialog.setSize(300, 75);
	}
	if (current < max) {
	    progressBar.setMaximum(max);
	    progressBar.setValue(current);
	    progressDialog.setVisible(true);
	} else {
	    progressDialog.setVisible(false);
	}
    }
    
    public boolean isExportSupported () {return true;}

    @Override
    public FileFilter[] getExportFileFilters () {
	return new FileFilter[] {
	    FILTER_SMI, FILTER_SDF, FILTER_CSV
	};
    }

    protected void printHeader(String[] header, PrintStream ps, FileFilter ff) {
	char sep = '\t';
	if (ff == FILTER_CSV) {
	    sep = ',';
	    ps.print("Compound");
	} else {
	    ps.print("#SMILES");
	}
	for (int i=0; i<header.length; i++) {
	    ps.print(sep+header[i]);
	}
	ps.println();	
    }
    
    protected String[] getFieldNames(EntityTable table) {
	ArrayList<String> fields = new ArrayList<String>();
	TableColumnModel tcm = table.getColumnModel();
	for (int col=1; col<tcm.getColumnCount(); col++) { /// skip col 0 which is some kind of status column
	    TableColumn tc = tcm.getColumn(col);
	    String colName = tc.getHeaderValue().toString();
	    if (tc.getHeaderValue().toString() == "Compound") {
	    } else {
		fields.add(colName);
	    }
	}
	return fields.toArray(new String[0]);
    }
    
    ActivityCellRenderer acr = null;
    
    protected HashMap<String, String> getCompoundFields(EntityTable table, int row, CompoundAdapter adapter) {
	TableColumnModel tcm = table.getColumnModel();
	Object molecule = null;
	HashMap<String, String> fields = new HashMap<String,String>();
	for (int col=0; col<tcm.getColumnCount(); col++) {
	    TableColumn tc = tcm.getColumn(col);
	    String colName = tc.getHeaderValue().toString();
	    if (tc.getHeaderValue().toString() == "Compound") {
		molecule = table.getValueAt(row, col);
	    } else if (tc instanceof EntityTableColumn) {
		EntityTableColumn etc = (EntityTableColumn)tc;
		Object value = table.getValueAt(row, etc.getModelIndex());
		TableCellRenderer tcr = tcm.getColumn(col).getCellRenderer();
		if (tcr instanceof TokenCellEditorRenderer) {
		    Value[] va = ((TokenCellEditorRenderer)tcr).parseValue(value);
		    if (va.length == 1) {
		        if  (va[0].getValue() == null) 
		            fields.put(colName,  "");
		        else
		            fields.put(colName, va[0].getValue().toString());
		    } else if (va.length > 1) {
			StringBuffer sb = new StringBuffer();
			for (Value entry: va) {
			    if (sb.length() > 0) sb.append("; ");
			    if  (entry.getValue() != null) 
			        sb.append(entry.getValue().toString());
			}
			fields.put(colName, sb.toString());
		    }
		} else if (tcr instanceof MolCellRenderer && value instanceof Compound) {
		    // value is a molecule
		    adapter.setCompound((Compound)value);
		    fields.put(colName, adapter.toFormat(MolecularData.Format.SMI).toString());
		} else {
		    // treat value as literal
		    fields.put(colName, value.toString());
		}
	    } else if (tc instanceof BioAssayTableColumn) {
		Object obj = table.getModel().getValueAt(row, tc.getModelIndex());
		ArrayList<Value> activities = ((BioAssayTableColumn)tc).getActivity(obj);
		StringBuffer sb = new StringBuffer();
		if (activities != null) {
		    if (acr == null) acr = new ActivityCellRenderer();
		    acr.setActivities(activities.toArray(new Value[0]));
		    fields.put(colName, acr.getLabel());
		}
	    } else {
		Object value = listView.getTable().getValueAt(row, tc.getModelIndex());
		fields.put(colName, value.toString());
	    }
	}
	if (molecule instanceof Compound)
	    adapter.setCompound((Compound)molecule);
	else
	    adapter.setCompound(null);
	
	return fields;
    }
    
    protected void printTxtCompound(CompoundAdapter adapter, Object[] fieldValues, PrintStream ps, FileFilter ff) {
	char sep = ff == FILTER_SMI ? '\t' : ',';
	String smiles = adapter.getStructureSMILES();
	String name = adapter.getName();
	if (name == null || name.equals("")) {
	    if (adapter.getCompound().getId() instanceof Value) name = ((Value)adapter.getCompound().getId()).getValue().toString();
	    else name = adapter.getCompound().getId().toString();
	}
	ps.print(smiles);
	for (int i=0; i<fieldValues.length; i++) {
	    ps.print(sep);
	    String value = "";
	    if (fieldValues[i] == null) {
		value = "";
	    } else if (fieldValues[i] instanceof String[]) {
		int j = 0;
		ps.print("\"");
		for (String s : (String[])fieldValues[i]) {
		    if (j++ > 0) ps.print(";");
		    ps.print(s);
		}
		ps.print("\"");
		
	    } else {
		value = fieldValues[i].toString();
	    }
	    boolean hasSep = value.indexOf(sep) > -1;
	    if (hasSep) value = "\"" + value + "\"";
	    ps.print(value);
	}
	ps.println("");
    }
    
    protected void printSDFCompound(CompoundAdapter adapter, String[] fieldNames, Object[] fieldValues, PrintStream ps) {
	String mol = adapter.getStructureMOL();
	mol = mol.substring(0, mol.indexOf("$$$$"));
	ps.print(mol);
	for (int i=0; i<fieldNames.length; i++) {
	    if (fieldValues[i] != null && fieldValues[i].toString().length() > 0) {
		ps.print (">  <"+fieldNames[i]+">\n");
		if (fieldValues[i] instanceof String[]) {
		    for (String entry: (String[])fieldValues[i])
			ps.print(entry+"\n");
		} else {
		    ps.print(fieldValues[i].toString()+"\n");
		}
		ps.print("\n");
	    }
	}
	ps.print("$$$$\n");
    }
    

    public void exportData (FileFilter format, File file) throws IOException {
	State<?> state = getState();
	if (state == null) return;

	EventList<? extends Entity> entities = state.getEntities ();
	logger.info("Entity count "+entities.size()
		    +"; selections="+state.getSelectionModel().getMaxSelectionIndex());
	boolean exportNonLoadedEntities = true;
	if (state.getSelectionModel().getMaxSelectionIndex() > -1) {
	    // if we have selection, then only save those!
	    EventList<Entity> selectedEntities = new BasicEventList<Entity>();
	    ListSelectionModel lsm = state.getSelectionModel();
	    {
		if (lsm.getMinSelectionIndex() > 0 || lsm.getMaxSelectionIndex()+1 < entities.size())
		    exportNonLoadedEntities = false;
		for (int i=lsm.getMinSelectionIndex(); i<lsm.getMaxSelectionIndex()+1; i++)
		    if (i<entities.size())
			if (lsm.isSelectedIndex(i))
			    selectedEntities.add(entities.get(i));
			else exportNonLoadedEntities = false;
	    }
	    if (!exportNonLoadedEntities) entities = selectedEntities; 
	}

	if (exportNonLoadedEntities) {
	    exportLimit = MAX_EXPORT;
	    int expectedCount = Math.max(entities.size(), state.size());
	    if (getResource() instanceof Value)
		try {
		    int resourceCount = Integer.valueOf(((Value)getResource()).getChild("count").getValue().toString());
		    expectedCount = Math.max(expectedCount, resourceCount);
		} catch (Exception e) {;}
	    if (expectedCount > exportLimit) {
		if (JOptionPane.showConfirmDialog(null, "Exports are limited to "+MAX_EXPORT+" entries, OK to truncate?", 
			"Collection size exceeds export limit", 
			JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.CANCEL_OPTION)
		    return;
		expectedCount = exportLimit;

	    }    
		
	    // load rest of entities to export
	    while (entities.size() < exportLimit && state.fetch()) {
		final int progressCurrent = entities.size();
		final int progressMax = expectedCount;
		SwingUtilities.invokeLater(new Runnable () {
			public void run () {
			    updateProgressDialog(progressCurrent, progressMax);
			}
		});
		// wait some time for things to load
		try {

		    Thread.sleep(500);
		}
		catch (InterruptedException e) {
		    logger.severe(e.getMessage());
		}

	    }
	}
	    
	OutputStream os = new FileOutputStream (file);
	PrintStream ps = new PrintStream (os, true);
	CompoundAdapter adapter = new CompoundAdapter();
	int index = 0;
	entities.getReadWriteLock().readLock().lock();
	try {
	    if (listView != null && listView.getTable() != null) {
		EntityTable table = listView.getTable();
		String[] fieldNames = getFieldNames(table);
		String[] fieldValues = new String[fieldNames.length];
		HashMap<String, String> fields;
		if (format == FILTER_CSV || format == FILTER_SMI)
		    printHeader(fieldNames, ps, format);
		while (index < table.getRowCount()) {
		    fields = getCompoundFields(table, index, adapter);
		    for (int i=0; i<fieldNames.length; i++)
			if (fields.containsKey(fieldNames[i]))
			    fieldValues[i] = fields.get(fieldNames[i]);
			else
			    fieldValues[i] = "";
			    
		    if (format == FILTER_SDF) {
			printSDFCompound (adapter, fieldNames, fieldValues, ps);
		    }
		    else {
			printTxtCompound (adapter, fieldValues, ps, format);
		    }
		    index++;
		}
	    } else {
		String[] fieldNames = {"Name", "PubChem CID", "Synonyms"};
		Object[] fieldValues = new String[fieldNames.length];
		if (format == FILTER_CSV || format == FILTER_SMI)
		    printHeader(fieldNames, ps, format);
		while (index < entities.size()) {
		    Compound c = (Compound)entities.get(index);
		    adapter.setCompound(c);
		    fieldValues[0] = c.getName();
		    fieldValues[1] = adapter.getPubChemCID().toString();
		    fieldValues[2] = adapter.getSynonyms();
		    if (format == FILTER_SDF) {
			printSDFCompound (adapter, fieldNames, fieldValues, ps);
		    }
		    else {
			printTxtCompound(adapter, fieldValues, ps, format);
		    }
		    index++;
		}
	    }
	    logger.info(entities.size() + " Compound(s) exported!");
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't export data", ex);
	}
	finally {
	    entities.getReadWriteLock().readLock().unlock();
	    updateProgressDialog(index, 0);
	    os.close();
	}
    }

    public boolean isImportSupported () {return true;}
    @Override
    public FileFilter[] getImportFileFilters () {
	return new FileFilter[] {
	    FILTER_SMI, FILTER_SDF//, FILTER_CSV
	};
    }
    
    public void addDataColumns (Map<String, String> colFields) {
	int modelIndex = 1;
	EntityTableModel model = (EntityTableModel)listView.getTable().getModel();
	for (final String field: colFields.keySet()) {
	    String type = colFields.get(field);

	    EntityTableColumn column = new EntityTableColumn (field, modelIndex, model);
	    TableCellRenderer renderer;
	    renderer = new TokenCellEditorRenderer () {
		@Override
		public Value[] parseValue (Object value) {
		    Entity e = (Entity)value;
		    return e.getValues(field).toArray(new Value[0]);
		}
	    };
//	    if (type.equals("Structure")) {
//		renderer = new MolCellRenderer ();
//		((MolCellRenderer)renderer).setLabelVisible(true);
//	    }
	    column.setCellRenderer(renderer);
	    column.setCellEditor((TableCellEditor)renderer);
	    
	    listView.getTable().addColumn(column);
	}
    }

    public List<String> getDataColumns() {
        ArrayList<String> dataColumns = new ArrayList<String>();
        if (listView != null && listView.getTable() != null) {
            for (int i=0; i<listView.getTable().getColumnCount(); i++) {
                TableColumn tc = listView.getTable().getColumnModel().getColumn(i);
                if (tc instanceof EntityTableColumn && ((EntityTableColumn)tc).getEntityField() instanceof String)
                    dataColumns.add((String)((EntityTableColumn)tc).getEntityField());
            }
        }
        return dataColumns;
    }
}
