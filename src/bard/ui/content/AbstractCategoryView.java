// $Id$

package bard.ui.content;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;

import ca.odell.glazedlists.EventList;

import bard.core.Entity;
import bard.ui.common.*;
import bard.ui.explodingpixels.macwidgets.BottomBar;
import bard.ui.util.UIUtil;
import bard.util.DataGrouping;
import bard.ui.icons.IconFactory;

public abstract class AbstractCategoryView 
    implements CategoryView, PropertyChangeListener, ChangeListener {

    static private final Logger logger = Logger.getLogger
	(AbstractCategoryView.class.getName());

    private String group;
    private String name;
    private Grid grid;
    private Content content;
    private DefaultBottomBar bottomBar;
    private BoundedRangeModel range;

    private JComponent component;
    private boolean busy = false;

    protected AbstractCategoryView () {
    }

    public AbstractCategoryView (String group, String name) {
	this.group = group;
	this.name = name;

	grid = new Grid ();
	JPanel pane = new JPanel (new BorderLayout ());
	pane.add(UIUtil.iTunesScrollPane(grid));

	bottomBar = new DefaultBottomBar();
	
	component = pane;

	// default to be busy... 
	setBusy (true);
    }

    public String getGroup () { return group; }
    public void setGroup (String group) { this.group = group; }
    public String getName () { return name; }
    public void setName (String name) { this.name = name; }
    public Component getComponent () { 
	return component; 
    }
    public void setContent (Content content) {
	this.content = content;
	//content.addPropertyChangeListener(this);
    }
    public Content getContent () { return content; }
    public Type getType () { return Type.Grid; }
    public Icon getIcon () { return IconFactory.View.Grid.getIcon(); }
    public String getDescription () { return "Grid view"; }

    public void propertyChange (PropertyChangeEvent e) {
	String prop = e.getPropertyName();
	if (prop.equals("state") && !content.isBusy()) {
	    // content finished loading... so do our stuff 
	}
    }

    public void setBusy (boolean busy) {
	this.busy = busy;
    }
    public boolean isBusy () { return busy; }

    public void stateChanged (ChangeEvent e) {
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    grid.setCellSize(range.getValue());
		}
	    });	    
    }

    public BoundedRangeModel getBoundedRangeModel () { 
	return range;
    }
    public void setBoundedRangeModel (BoundedRangeModel range) {
	if (this.range != null) {
	    this.range.removeChangeListener(this);
	}
	this.range = range;
	range.addChangeListener(this);
	bottomBar.setSliderRangeModel(range);
    }
    public BottomBar getBottomBar () { return bottomBar; }
    
    public Grid getGridComponent () { return grid; }
}
