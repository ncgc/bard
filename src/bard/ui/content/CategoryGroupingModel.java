// $Id$

package bard.ui.content;

public interface CategoryGroupingModel {
    public String[] getCategoryGroups ();
    public CategoryView[] getCategories (String group);
}
