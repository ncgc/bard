package bard.ui.content;

import bard.core.*;
import bard.core.adapter.ProjectAdapter;
import bard.ui.common.EntityTableColumn;
import bard.ui.common.EntityTableColumnFactory;
import bard.ui.common.EntityTableModel;
import bard.ui.common.TokenCellEditorRenderer;
import bard.ui.event.ContentChangeEvent;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;

import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

public class ProjectContent extends AbstractContent<Project> {
    private static final Logger logger = Logger.getLogger
            (ProjectContent.class.getName());

    private static final Preferences PREFS =
            Preferences.userNodeForPackage(ProjectContent.class);
    private static final Preferences NODE;

    static {
        Preferences node = PREFS;
        try {
            node = PREFS.node(ProjectContent.class.getSimpleName());
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Can't get preference node for "
                    + ProjectContent.class, ex);
        }
        NODE = node;
    }

    protected ProjectContent() {
        setName("Projects");
    }

    public ProjectContent(ProjectService service) {
        super(service);
    }

    public ProjectContent(ProjectService service, Object etag) {
        super(service, etag);
    }

    protected Preferences getPreferences() {
        return NODE;
    }

    public Icon getIcon() {
        return IconFactory.Content.Project.getIcon();
    }

    public Class<Project> getEntityClass() {
        return Project.class;
    }

    public String getName() {
        return "Projects";
    }

    public View.Type[] getSupportedViewTypes() {
        return new View.Type[]{
        	View.Type.Record, View.Type.List 
        };
    }

    protected ListViewPanel listView;
    protected ProjectRecordViewPanel recordView;

    /*
    * Views
    */
    @Override
    protected View createListView() {
        if (listView == null) {
            listView = new ProjectListViewPanel();
        }
        return listView;
    }

    @Override
    protected View createRecordView() {
        if (recordView == null) {
            recordView = new ProjectRecordViewPanel(this);
        }
        return recordView;
    }

    /**
     * List view
     */
    class ProjectListViewPanel extends ListViewPanel 
    	implements EntityTableColumnFactory, 
    	EventSubscriber<ContentChangeEvent>, 
    	PropertyChangeListener, HyperlinkListener {
        Entity[] selections;

        ProjectListViewPanel() {
            super(ProjectContent.this);
            getTable().setEntityColumnFactory(Project.class, this);
            State state = ProjectContent.this.getState();
            setListSource(Project.class, state);

            //getTable().setRowHeight(150);
            EventBus.subscribe(ContentChangeEvent.class, this);
        }

        public void propertyChange(PropertyChangeEvent e) {
            String prop = e.getPropertyName();
            if (!"action".equals(prop)) {
                return;
            }
            final Project project = (Project) e.getNewValue();
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                }
            });
        }

        /*
         * EntityTableColumnFactory interface
         */
        public EntityTableColumn[] createColumns(int modelIndex, EntityTableModel model) {
            Class clazz = model.getColumnClass(modelIndex);
            if (!clazz.isAssignableFrom(Project.class)) {
                return new EntityTableColumn[0];
            }

            ArrayList<EntityTableColumn> columns = new ArrayList<EntityTableColumn>();
            EntityTableColumn column;

            column = new EntityTableColumn("Project ID", modelIndex, model);
            TokenCellEditorRenderer renderer = new TokenCellEditorRenderer() {
                @Override
                public Value[] parseValue(Object value) {
                    Project e = (Project) value;
                    return new Value[]{
                            new StringValue((DataSource) null, "Project ID", e.getId().toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
            columns.add(column);

            column = new EntityTableColumn("Source", modelIndex, model);
            renderer = new TokenCellEditorRenderer() {
		JLabel sourceLabel = new JLabel();
                public Component getTableCellRendererComponent(JTable table, Object value,
                                                               boolean isSelected, boolean hasFocus, int row, int column) {
                    String source = null;
                    setField("laboratory name");
                    Value val = ((Project) value).getValue("laboratory name");
                    if (val != null) source = val.getValue().toString().trim();
                    ImageIcon icon = null;
                    IconFactory.CenterIcon.checkHeight(table.getRowHeight());
                    for (IconFactory.CenterIcon center : IconFactory.CenterIcon.values()) {
                        if (center.getCenter().equals(source) || center.getFullName().equals(source)) {
                            icon = center.getIcon();
			    source = center.getFullName();
			    break;
                        }
                    }
		    if (icon != null) {
			sourceLabel.setIcon(icon);
			sourceLabel.setText(null);
			sourceLabel.setToolTipText(source);
		    } else {
			sourceLabel.setIcon(null);
			sourceLabel.setText(source);
			sourceLabel.setToolTipText(source);
		    }
		    return sourceLabel;
                }
            };
            column.setCellRenderer(renderer);
            columns.add(column);

            column = new EntityTableColumn("Name", modelIndex, model);
            renderer = new TokenCellEditorRenderer() {
                @Override
                public Value[] parseValue(Object value) {
                    Entity e = (Entity) value;
                    return new Value[]{
                            new StringValue
                                    ((DataSource) null, "Name", e.getName())
                    };
                }
            };
            column.setCellRenderer(renderer);
            columns.add(column);

            column = new EntityTableColumn("Experiments", modelIndex, model);
            renderer = new TokenCellEditorRenderer() {
                @Override
                public Value[] parseValue(Object value) {
                    Entity e = (Entity) value;
                    Value nexptValue = e.getValue(Project.NumberOfExperimentsValue);
                    Integer nexpt = 0;
                    if (nexptValue != null) nexpt = (Integer) nexptValue.getValue();
                    return new Value[]{
                            new IntValue((DataSource) null, "Experiments", nexpt )
                    };
                }
            };
            column.setCellRenderer(renderer);
            columns.add(column);

            column = new EntityTableColumn("Probes", modelIndex, model);
            renderer = new TokenCellEditorRenderer() {
                @Override
                public Value[] parseValue(Object value) {
                    Entity e = (Entity) value;
                    Value nprobeValue = e.getValue(Project.NumberOfProbesValue);
                    Integer nexpt = 0;
                    if (nprobeValue != null) nexpt = (Integer) nprobeValue.getValue();
                    return new Value[]{
                            new IntValue((DataSource) null, "Probes", nexpt )
                    };
                }
            };
            column.setCellRenderer(renderer);
            columns.add(column);


//            column = new EntityTableColumn("Biology", modelIndex, model);
//            TableCellRenderer jeptcr = new TableCellRenderer() {
//                JEditorPane jep = new JEditorPane();
//                
//                public Component getTableCellRendererComponent(JTable arg0,
//                        Object entity, boolean arg2, boolean arg3, int arg4,
//                        int arg5) {
//                    
//                    Project e = (Project) entity;
//                    StringBuffer string = new StringBuffer();
//                    for (Value context: e.getValues("contexts")) {
//                        if ("biology".equals(context.getValue()))
//                            for (Value v : context.getChildren()) {
//                                String val, display, url;
//                                val = v.getValue().toString();
//                                if (v instanceof StringDisplayValue) {
//                                    display = ((StringDisplayValue) v).getDisplay();
//                                } else {
//                                    display = "";
//                                }
//                                url = v.getURL();
//                                if (url != null)
//                                    string.append("<a href=\""+url+"\">"+display+"</a><br>\n");
//                                else string.append(display+"<br>\n");
//                            }
//                    }
//                    jep.setContentType("text/html");
//                    jep.setEditable(false);
//                    jep.setText(string.toString());
//                    jep.addHyperlinkListener(ProjectListViewPanel.this);
//                     
//                    return jep;
//                }
//                
//            };
//            column.setCellRenderer(jeptcr);
//            columns.add(column);

            column = new EntityTableColumn ("Biology", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object entity) {
//            TableCellRenderer jeptcr = new TableCellRenderer() {
//                JEditorPane jep = new JEditorPane();
//                
//                public Component getTableCellRendererComponent(JTable arg0,
//                        Object entity, boolean arg2, boolean arg3, int arg4,
//                        int arg5) {
//                    
                    Project e = (Project) entity;
                    StringBuffer string = new StringBuffer();
                    for (Value context: e.getValues(Biology.class.toString())) {
                        Biology b = (Biology)e.getEntityFromValue((EntityValue)context);
                        if (b != null) {
                            String display, url;
                            display = b.getName();
                            //url = null;
                            //if (url != null)
                            //    string.append("<a href=\""+url+"\">"+display+"</a><br>\n");
                            //else string.append(display+"<br>\n");
                            string.append(string.length() > 0 ? "; " : "");
                            string.append(display);
                        }
                    }
//                    jep.setContentType("text/html");
//                    jep.setEditable(false);
//                    jep.setText(string.toString());
//                    jep.addHyperlinkListener(AssayListViewPanel.this);
//                     
//                    return jep;
                    return new Value[] {
                            new StringValue 
                            ((DataSource)null, "Biology", string.toString())
                        };
                }
                
            };
            column.setCellRenderer(renderer);
//            column.setCellEditor(renderer);
            columns.add(column);

            // KEGG disease annotations
            column = new EntityTableColumn("Disease Area", modelIndex, model);
            renderer = new TokenCellEditorRenderer() {
                @Override
                public Value[] parseValue(Object value) {
                    Project e = (Project) value;
                    ProjectAdapter pa = new ProjectAdapter(e);
                    Collection<Value> annos = pa.getAnnotations();
                    Set<String> dcatNames = new HashSet<String>();
                    for (Value v : annos) {
                        if (v.getSource().getName().equals(EntityNamedSources.KEGGDiseaseCategoryAnnotationSource)) {
                            dcatNames.add(v.getValue().toString());
                        }
                    }
                    Value[] values = new Value[dcatNames.size()];
                    int i = 0;
                    for (String dcatName : dcatNames) {
                        values[i++] =  new StringValue((DataSource) null, "Disease Areas", dcatName);
                    }
                    return values;
                }
            };
            column.setCellRenderer(renderer);
            columns.add(column);

            return columns.toArray(new EntityTableColumn[0]);
        }

        public void onEvent(ContentChangeEvent e) {
            if (e.getContent() != ProjectContent.this) {
                return;
            }
//            logger.info("name=" + e.getName()
//                    + " old=" + e.getOldValue() + " new=" + e.getNewValue());
            if (e.getName().equals(ContentChangeEvent.STATE)) {
                State<Project> state = (State<Project>) e.getNewValue();
        	setListSource(Project.class, state);
            }
        }

        public void hyperlinkUpdate (HyperlinkEvent e) {
            //URL url = e.getURL();
            java.net.URI uri = UIUtil.getLinkURI(e);
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                logger.info("launching "+uri.toString());
                try {
                    //new BrowserLauncher().openURLinBrowser(uri.toString());
                    EventBus.publish(e);
                }
                catch (Exception ex) {
                    logger.log(Level.SEVERE, 
                               "Can't launch external browser "+uri, ex);
                }
            }
            else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
                JComponent c = (JComponent)e.getSource();
                c.setToolTipText(uri != null ? uri.toString() : null);
            }
        }
    }

}
