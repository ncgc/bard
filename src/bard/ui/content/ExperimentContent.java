package bard.ui.content;

import bard.core.Assay;
import bard.core.AssayValues;
import bard.core.Biology;
import bard.core.Compound;
import bard.core.DataSource;
import bard.core.Entity;
import bard.core.Experiment;
import bard.core.ExperimentService;
import bard.core.ExperimentValues;
import bard.core.IntValue;
import bard.core.StringValue;
import bard.core.Value;
import bard.ui.common.EntityTableColumn;
import bard.ui.common.EntityTableColumnFactory;
import bard.ui.common.EntityTableModel;
import bard.ui.common.TokenCellEditorRenderer;
import bard.ui.event.ContentChangeEvent;
import bard.ui.icons.IconFactory;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;


public class ExperimentContent extends AbstractContent<Experiment> {
    private static final Logger logger = Logger.getLogger
        (ExperimentContent.class.getName());

    private static final Preferences PREFS =
	Preferences.userNodeForPackage(ExperimentContent.class);
    private static final Preferences NODE;
    static {
	Preferences node = PREFS;
	try {
	    node = PREFS.node(ExperimentContent.class.getSimpleName());
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't get preference node for "
		       +ExperimentContent.class, ex);
	}
	NODE = node;
    }

    protected ExperimentContent() {
        setName ("Experiments");
    }

    public ExperimentContent(ExperimentService service) {
        super (service);
    }

    public ExperimentContent(ExperimentService service, Object etag) {
        super (service, etag);
    }

    protected Preferences getPreferences () { return NODE; }
    public Icon getIcon () { return IconFactory.Content.Assay.getIcon(16); }
    public Class<Experiment> getEntityClass () { return Experiment.class; }
    public String getName () { return "Experiment"; }
    public View.Type[] getSupportedViewTypes () {
        return new View.Type[]{
        	View.Type.List//, View.Type.Record//, View.Type.Grid
        };
    }

//    protected ListViewPanel listView;
//    protected CategoryGroupingPanel gridView;
//    protected AssayRecordViewPanel recordView;

    /*
     * Views
     */
//    @Override
//    protected View createListView () {
//        if (listView == null) {
//            listView = new ExperimentListViewPanel ();
//        }
//        return listView;
//    }
    
//    @Override
//    protected View createGridView () {
//        if (gridView == null) {
//            gridView = new CompoundCategoryGroupingPanel ();
//        }
//        return gridView;
//    }

//    @Override
//    protected View createRecordView () {
//        if (recordView == null) {
//            recordView = new AssayRecordViewPanel (this);
//        }
//        return recordView;
//    }

}
