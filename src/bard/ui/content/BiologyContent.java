package bard.ui.content;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.prefs.Preferences;

import javax.swing.*;
import bard.core.Biology;
import bard.core.BiologyService;
import bard.core.EntityServiceManager;
import bard.ui.icons.IconFactory;

public class BiologyContent extends AbstractContent<Biology> {
    private static final Logger logger = Logger.getLogger
        (BiologyContent.class.getName());

    private static final Preferences PREFS = 
	Preferences.userNodeForPackage(BiologyContent.class);
    private static final Preferences NODE;
    static {
	Preferences node = PREFS;
	try {
	    node = PREFS.node(BiologyContent.class.getSimpleName());
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't get preference node for "
		       +ProjectContent.class, ex);
	}
	NODE = node;
    }

    protected BiologyContent () {
        setName ("Biology");
    }

    public BiologyContent (BiologyService service) {
        super (service);
    }

    public BiologyContent (BiologyService service, Object etag) {
        super (service, etag);
    }

    protected Preferences getPreferences () { return NODE; }

    public Class<Biology> getEntityClass () { return Biology.class; }
    public View.Type[] getSupportedViewTypes () {
        return new View.Type[0];
    }
}
