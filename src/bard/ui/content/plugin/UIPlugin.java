package bard.ui.content.plugin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JSplitPane;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.bushe.swing.event.EventBus;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import bard.core.Entity;
import bard.ui.explodingpixels.macwidgets.MacFontUtils;
import bard.ui.util.UIUtil;

abstract public class UIPlugin<T extends Entity> implements HyperlinkListener {

    static private Logger logger = Logger.getLogger
	    (UIPlugin.class.getName());
    
    boolean busy = false;

    static final Border BORDER = 
	BorderFactory.createLineBorder(new Color (0xa5a5a5));

    T anchor = null; // anchor entity
    Container taskpane = null; // container JXTaskPane gets added to

    public void update (T entity, JXTaskPaneContainer taskpane) {
	//logger.info(anchor+":"+entity.getId());
	if (entity!=null && !entity.equals(anchor)) {
	    this.anchor = entity; // current anchor entity
	    this.taskpane = taskpane;
	    setBusy (true);
	    new PluginRecordLoader<T> (this, entity).execute();
	    logger.info(entity.getId() + " Update plugin record view ");
	}
    }

    JXTaskPane createTaskPane (String title) {
	JXTaskPane tp = new JXTaskPane ();
	tp.setLayout(new BorderLayout ());
	tp.setTitle(title);
	tp.setCollapsed(true);
	tp.setScrollOnExpand(true);
	((JComponent)tp.getContentPane()).setBackground(Color.white);
	return tp;
    }

    public void hyperlinkUpdate (HyperlinkEvent e) {
	//URL url = e.getURL();
        java.net.URI uri = UIUtil.getLinkURI(e);
	if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
	    logger.info("launching "+uri.toString());
	    try {
		//new BrowserLauncher().openURLinBrowser(uri.toString());
		EventBus.publish(e);
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Can't launch external browser "+uri, ex);
	    }
	}
	else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
	    JComponent c = (JComponent)e.getSource();
	    c.setToolTipText(uri != null ? uri.toString() : null);
	}
    }
    
    JEditorPane createEP (String title) {
	JEditorPane ep = new JEditorPane ();
	ep.setContentType("text/html");
	ep.setEditable(false);
	ep.setOpaque(false);
	ep.addHyperlinkListener(this);
	if (title != null) {
	    ep.setBorder(BorderFactory.createTitledBorder
			 (BORDER, title, TitledBorder.LEADING, 
			  TitledBorder.DEFAULT_POSITION, 
			  MacFontUtils.ITUNES_TABLE_HEADER_FONT));
	}
	    /*
	    ep.setBorder
		(BorderFactory.createCompoundBorder
		 (BorderFactory.createLineBorder(new Color (0xa5a5a5)),
		  BorderFactory.createEmptyBorder(2,1,1,1)));
	    */
	return ep;
    }

    static JSplitPane createSplitPane (int which) {
	JSplitPane split = new JSplitPane (which);
	split.setDividerSize(4);
        split.setContinuousLayout(true);
        split.setBorder(BorderFactory.createEmptyBorder());
	return split;
    }

    abstract Object[] getPluginResponse (T entity);
    
    abstract JXTaskPane responseTaskPane (Object[] retVal);

    void createTaskPane(Object[] retVal) {
	taskpane.add(responseTaskPane(retVal));
    }
    
    public void setBusy (boolean busy) {
	this.busy = busy;
    }
    public boolean isBusy () { return busy; }

}

class PluginRecordLoader<T extends Entity> extends SwingWorker<Object[], Void> {
    static private Logger logger = Logger.getLogger
	    (UIPlugin.class.getName());

    UIPlugin<T> related;
    T entity;

    PluginRecordLoader (UIPlugin<T> related, T entity) {
	this.related = related;
	this.entity = entity;
    }

    @Override
    protected Object[] doInBackground () {
	return related.getPluginResponse(entity);
    }

    @Override
    protected void done () {
	try {
	    Object[] retVal = get ();
	    related.createTaskPane(retVal);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't run plugin", ex);
	}
	related.setBusy(false);
    }
}
