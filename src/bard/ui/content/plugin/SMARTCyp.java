package bard.ui.content.plugin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jdesktop.swingx.JXTaskPane;

import com.jidesoft.dialog.JideOptionPane;

import chemaxon.formats.MolImporter;
import chemaxon.sss.search.MolSearch;
import chemaxon.struc.MDocument;
import chemaxon.struc.Molecule;
import chemaxon.struc.graphics.MFont;

import bard.core.Compound;
import bard.core.MolecularData;
import bard.core.MolecularValue;
import bard.ui.common.Grid;
import bard.ui.common.GridCellAnnotator;
import bard.ui.common.MolCellEditor;
import bard.ui.common.MolCellRenderer;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import bard.util.BardHttpClient;

public class SMARTCyp extends UIPlugin<Compound> implements GridCellAnnotator {

    int index = 0;

    Grid grid;
    JEditorPane ep;
    MolSearch molSearch = new MolSearch();

    static String[] cyps = {"3A4", "2D6", "2C9"};
    String structure = null;
    ArrayList<JsonNode> cyp3a4 = null;
    
    Object[] getPluginResponse (Compound cmpd) {        
	if (!(cmpd instanceof Compound)) {
	    throw new IllegalArgumentException
	    ("Entity is not of type Compound!");
	}
	BardHttpClient httpclient = new BardHttpClient("SMARTCyppluginResponse");
	try {
	    String url = "";
	    if (cmpd.getValue(Compound.PubChemCIDValue) == null) {
		String cid = cmpd.getValue(Compound.PubChemCIDValue).getValue().toString();
		url = "http://bard.nih.gov/api/straw/plugins/smartcyp/cid/"+cid;
	    } else {
		String smiles = ((MolecularValue)cmpd.getValue(Compound.MolecularValue)).toFormat(MolecularData.Format.SMILES).toString();
		if (smiles == null || smiles.length() == 0)
		    return null;
		url = "http://bard.nih.gov/api/straw/plugins/smartcyp/?smiles="+URLEncoder.encode(smiles);
	    }
	    HttpGet get = new HttpGet(url);
	    HttpResponse response = httpclient.execute(get);
	    HttpEntity entity = response.getEntity();
	    if (entity != null && response.getStatusLine()
		    .getStatusCode() == 200) {
		InputStream is = entity.getContent();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(is);
		if (root == null) return null;
		
		Object[] retVal = new Object[cyps.length+1];

		String structure = root.get("structure").getTextValue();
		retVal[0] = structure;
		for (int i=0; i<cyps.length; i++) {
		    JsonNode cypNode = root.get(cyps[i]);
		    ArrayList<JsonNode> entries = new ArrayList<JsonNode>();
		    for (int j=0; j<cypNode.size(); j++) {
			entries.add(cypNode.get(j));
		    }
		    retVal[i+1] = entries;
		}
		return retVal;        	    
	    }
	} catch (Exception ex) {ex.printStackTrace();} 
	finally { 
	    httpclient.close();
	}
	return null;
    }

    @SuppressWarnings("unchecked")
    JXTaskPane responseTaskPane (Object[] retVal) {
	if (retVal == null) {
	    JXTaskPane tp = createTaskPane("SMARTCyp");
	    tp.setCollapsed(false);
	    tp.add(new JLabel("SMARTCyp analysis not available for this compound, sorry!"));
	    return tp;
	}
	structure = (String)retVal[0];
	cyp3a4 = (ArrayList<JsonNode>)retVal[1];
	index = 0;

	JXTaskPane tp = createTaskPane("SMARTCyp");
	tp.setCollapsed(false);
	tp.add(initUI());
	updateToIndex();
	return tp;
    }

    protected Grid createMolGrid () {
	Grid grid = new Grid ();
	MolCellRenderer mcr = new MolCellRenderer();
	mcr.setAtomMapVisible(true);
	mcr.setIgnoreEZ(true);
	grid.setCellRenderer(mcr);
	MolCellEditor mce = new MolCellEditor ();
	mce.setAtomMapVisible(true);
	mce.setIgnoreEZ(true);
	mce.instrumentContextMenu();
	grid.setCellEditor(mce);
	//grid.setUseDefaultBackground(false);

	//grid.setCellSize(300);
	return grid;
    }

    Component initUI() {
	grid = createMolGrid ();
	grid.setCellAnnotator(this);
        grid.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                JComponent c = (JComponent) e.getSource();
                Rectangle r = c.getBounds();
                grid.setCellSize
                        ((int) (Math.max(r.width, r.height) * .8 + 0.5));
            }
        });

	JPanel toggle = new JPanel();
	JButton but1 = new JButton(IconFactory.getImageIcon("arrow-left-24.png", 16));
	but1.addActionListener(new java.awt.event.ActionListener() {    
	    public void actionPerformed(java.awt.event.ActionEvent e) {
		previous();
	    }
	});

	JButton but2 = new JButton(IconFactory.getImageIcon("arrow-right-24.png", 16));
	but2.addActionListener(new java.awt.event.ActionListener() {    
	    public void actionPerformed(java.awt.event.ActionEvent e) {
		next();
	    }
	});

	JButton but3 = new JButton(IconFactory.getImageIcon("question-octagon-frame.png", 16));
	but3.addActionListener(new java.awt.event.ActionListener() {    
	    public void actionPerformed(java.awt.event.ActionEvent e) {
		JideOptionPane.showMessageDialog(grid,
			"BADAPPLE, the BARD Promiscuity Plugin, from UNM; returns evidence-based promiscuity scores.\n VERSION: 0.9alpha (Jan 2013)\n UNM BARD Team: Jeremy Yang, Anna Waller, Cristian Bologa, Oleg Ursu, Steve Mathias, Tudor Oprea, Larry Sklar\n database: carlsbad.health.unm.edu:5432:openchord:badapple2\n note: (none)");
	    }
	});

	toggle.add(but1);
	toggle.add(but2);

	JPanel bottomControl = new JPanel();
	bottomControl.add(but3, BorderLayout.LINE_START);
	bottomControl.add(toggle, BorderLayout.CENTER);

	JPanel panel = new JPanel(new BorderLayout());
	panel.add(grid, BorderLayout.CENTER);
	panel.add(bottomControl, BorderLayout.SOUTH);

	JSplitPane split = createSplitPane (JSplitPane.HORIZONTAL_SPLIT);
	split.setLeftComponent(panel);

	ep = createEP (null);
	split.setRightComponent(ep);
	split.setResizeWeight(.4);

	return split;
    }

    void next() {
	index = index + 1;
	if (index > cyp3a4.size() - 1) index = 0;
	updateToIndex();
    }

    void previous() {
	index = index - 1;
	if (index < 0) index = cyp3a4.size() - 1;
	updateToIndex();
    }

    void updateToIndex() {
	grid.clear();

	double val = cyp3a4.get(index).get("score").getDoubleValue();
	String atomName = cyp3a4.get(index).get("id").getTextValue();
	int atom = Integer.valueOf(atomName.substring(atomName.indexOf(".")+1))-1;

	try {
	    Molecule cmpd = MolImporter.importMol(structure);
	    Color color = Color.GREEN;
	    if (val < 50)
		color = Color.RED;
	    else if (val < 100)
		color = Color.YELLOW;
	    
//	    MDocument mdoc = new MDocument(cmpd);
//	    mdoc.setAtomSetColorMode(1,  MDocument.SETCOLOR_SPECIFIED);
//	    mdoc.setAtomSetRGB(1, color.getRGB());
//	    mdoc.setAtomSetFont(1, new MFont("Helvetica", MFont.BOLD, 36));
	    
	    cmpd.getAtom(atom).setSelected(true);
	    cmpd.getAtom(atom).setAtomMap(atom+1);
//	    cmpd.getAtom(atom).setAliasstr(cmpd.getAtom(atom).getSymbol());
//	    cmpd.getAtom(atom).setSetSeq(1);
	    cmpd.setName(null);
	    grid.addValue(cmpd);
	    grid.setSelectionColor(color);
	    grid.setCellSelected(0, true);
	} catch (Exception ex) {ex.printStackTrace();}

	
	StringBuilder sb = new StringBuilder (UIUtil.CSS);

	String comment = "<font bgcolor=\"00FF00\">(low)</font>";
	if (val < 50) comment = "<font bgcolor=\"FF0000\">(high)</font>";
	else if (val < 100) comment = "<font bgcolor=\"FFFF00\">(medium)</font>";

	sb.append("<b>Score: "+val+"</b> &nbsp&nbsp&nbsp&nbsp&nbsp "+comment+" <br><br>");
	Iterator<String> si = cyp3a4.get(index).getFieldNames();
	ArrayList<String> sia = new ArrayList<String>();
	while (si.hasNext()) {
	    sia.add(si.next());
	}
	String[] sta = sia.toArray(new String[0]);
	Arrays.sort(sta);
	for (String field: sta) {
	    String entry = cyp3a4.get(index).get(field).asText();
	    sb.append(field+": "+entry+"<br>");
	}
	ep.setText(sb.toString());
	return;
    }

    @Override
    public String getGridCellBadgeAnnotation(Grid g, Object value, int cell) {
	// TODO Auto-generated method stub
	return null;
    }

    public String getGridCellAnnotation (Grid g, Object v, int c) {
	if (v == null) {
	    return null;
	}
	Molecule cmpd = (Molecule)v;

	String name = cmpd.getName();
	return name;
    }

}
