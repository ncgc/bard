package bard.ui.content.plugin;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jdesktop.swingx.JXTaskPane;

import com.jidesoft.dialog.JideOptionPane;

import chemaxon.formats.MolImporter;
import chemaxon.sss.search.MolSearch;
import chemaxon.struc.Molecule;

import bard.core.Compound;
import bard.core.MolecularData;
import bard.core.MolecularValue;
import bard.ui.common.Grid;
import bard.ui.common.GridCellAnnotator;
import bard.ui.common.MolCellEditor;
import bard.ui.common.MolCellRenderer;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import bard.util.BardHttpClient;

public class BadAppleUNM extends UIPlugin<Compound> implements GridCellAnnotator {

    ArrayList<Integer> score = new ArrayList<Integer>();
    ArrayList<JsonNode> info = new ArrayList<JsonNode>();
    int index = 0;

    Grid grid;
    JEditorPane ep;
    MolSearch molSearch = new MolSearch();

    Object[] getPluginResponse (Compound cmpd) {        
	if (!(cmpd instanceof Compound)) {
	    throw new IllegalArgumentException
	    ("Entity is not of type Compound!");
	}
	ArrayList<Integer> score = new ArrayList<Integer>();
	ArrayList<JsonNode> info = new ArrayList<JsonNode>();
	BardHttpClient httpclient = new BardHttpClient("BadApplepluginResponse");
	//	    DefaultHttpClient httpclient = new DefaultHttpClient();
	try {
	    if (cmpd.getValue(Compound.PubChemCIDValue) == null) return null;
	    String cid = cmpd.getValue(Compound.PubChemCIDValue).getValue().toString();
	    String url = "http://bard.nih.gov/api/latest/plugins/badapple/prom/cid/"+cid;
	    HttpGet get = new HttpGet(url);
	    HttpResponse response = httpclient.execute(get);
	    HttpEntity entity = response.getEntity();
	    if (entity != null && response.getStatusLine()
		    .getStatusCode() == 200) {
		InputStream is = entity.getContent();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(is);
		if (root == null) return null;

		JsonNode scaffIds = root.get("hscafs");
		if (scaffIds == null) return null;

		for (int i=0; i <scaffIds.size() && i<5; i++) {
		    int scaf = scaffIds.get(i).get("scafid").asInt();
		    int val = scaffIds.get(i).get("pScore").asInt();
		    score.add(val);
		    String url2 = "https://bard.nih.gov/api/latest/plugins/badapple/prom/scafid/"+scaf;
		    HttpGet get2 = new HttpGet(url2);
		    HttpResponse response2 = httpclient.execute(get2);
		    if (response2 != null) {
		        InputStream is2 = response2.getEntity().getContent();
		        JsonNode node = mapper.readTree(is2);
		        info.add(node);
		    }
		}
		if (score.size() == 0) return null;

		Object[] retVal = {score, info};
		return retVal;        	    
	    }
	} catch (Exception ex) {ex.printStackTrace();} 
	finally { 
	    httpclient.close();
	    //        	httpclient.getConnectionManager().shutdown();
	}
	return null;
    }

    JXTaskPane responseTaskPane (Object[] retVal) {
	index = 0;
	if (retVal == null) {
	    score = new ArrayList<Integer>();
	    info = new ArrayList<JsonNode>();
	    JXTaskPane tp = createTaskPane("BADAPPLE, the BARD Promiscuity Plugin, from UNM; returns evidence-based promiscuity scores.");
	    tp.setCollapsed(false);
	    tp.add(new JLabel("BARDAPPLE analysis not available for this compound, sorry!"));
	    return tp;
	}

	score = (ArrayList<Integer>)retVal[0];
	info = (ArrayList<JsonNode>)retVal[1];
	JXTaskPane tp = createTaskPane("BADAPPLE, the BARD Promiscuity Plugin, from UNM; returns evidence-based promiscuity scores.");
	tp.setCollapsed(false);
	tp.add(initUI());
	updateToIndex();
	return tp;
    }

    protected Grid createMolGrid () {
	Grid grid = new Grid ();
	grid.setCellRenderer(new MolCellRenderer ());
	MolCellEditor mce = new MolCellEditor ();
	mce.instrumentContextMenu();
	grid.setCellEditor(mce);
	//grid.setUseDefaultBackground(false);

	grid.setPreferredSize(new Dimension (200, 200));
	return grid;
    }

    Component initUI() {
	grid = createMolGrid ();
	grid.setCellAnnotator(this);

	JPanel toggle = new JPanel();
	JButton but1 = new JButton(IconFactory.getImageIcon("arrow-left-24.png", 16));
	but1.addActionListener(new java.awt.event.ActionListener() {    
	    public void actionPerformed(java.awt.event.ActionEvent e) {
		previous();
	    }
	});

	JButton but2 = new JButton(IconFactory.getImageIcon("arrow-right-24.png", 16));
	but2.addActionListener(new java.awt.event.ActionListener() {    
	    public void actionPerformed(java.awt.event.ActionEvent e) {
		next();
	    }
	});

	JButton but3 = new JButton(IconFactory.getImageIcon("question-octagon-frame.png", 16));
	but3.addActionListener(new java.awt.event.ActionListener() {    
	    public void actionPerformed(java.awt.event.ActionEvent e) {
		JideOptionPane.showMessageDialog(grid,
			"BADAPPLE, the BARD Promiscuity Plugin, from UNM; returns evidence-based promiscuity scores.\n VERSION: 0.9alpha (Jan 2013)\n UNM BARD Team: Jeremy Yang, Anna Waller, Cristian Bologa, Oleg Ursu, Steve Mathias, Tudor Oprea, Larry Sklar\n database: carlsbad.health.unm.edu:5432:openchord:badapple2\n note: (none)");
	    }
	});

	toggle.add(but1);
	toggle.add(but2);

	JPanel bottomControl = new JPanel();
	bottomControl.add(but3, BorderLayout.LINE_START);
	bottomControl.add(toggle, BorderLayout.CENTER);

	if (score.size() < 2) but1.setEnabled(false);
	if (score.size() < 2) but2.setEnabled(false);

	JPanel panel = new JPanel(new BorderLayout());
	panel.add(grid, BorderLayout.CENTER);
	panel.add(bottomControl, BorderLayout.SOUTH);

	JSplitPane split = createSplitPane (JSplitPane.HORIZONTAL_SPLIT);
	split.setLeftComponent(panel);

	ep = createEP (null);
	split.setRightComponent(ep);
	split.setResizeWeight(.4);

	return split;
    }

    void next() {
	index = index + 1;
	if (index > info.size() - 1) index = 0;
	updateToIndex();
    }

    void previous() {
	index = index - 1;
	if (index < 0) index = info.size() - 1;
	updateToIndex();
    }

    void updateToIndex() {
	grid.clear();
	try {
	    Molecule cmpd = (Molecule)((MolecularValue)anchor.getValue(Compound.MolecularValue)).toFormat(MolecularData.Format.NATIVE);
	    cmpd.clean(2, null);
	    cmpd.setDim(2);
	    Molecule clone = cmpd.cloneMolecule();
	    clone.aromatize();
	    molSearch.setTarget(clone);
	    Molecule scaffold = MolImporter.importMol(info.get(index).get("scafsmi").asText());
	    scaffold.aromatize();
	    scaffold.clean(2, null);
	    scaffold.setDim(2);
	    molSearch.setQuery(scaffold);
	    int[] matches = molSearch.findFirst();
	    if (matches != null)
		for (int match: matches) {
		    cmpd.getAtom(match).setAtomMap(match);
		}
	    //MolHandler mh = new MolHandler();
	    //mh.setMolecule(scaffold);
	    //mh.align(cmpd, matches);
	    cmpd.setName("Match");
	    scaffold.setName("Scaffold");
	    grid.addValue(cmpd);
	    grid.addValue(scaffold);
	} catch (Exception ex) {ex.printStackTrace();}

	int val = score.get(index);
	StringBuilder sb = new StringBuilder (UIUtil.CSS);

	String comment = "<font bgcolor=\"00FF00\">(low)</font>";
	if (val > 99) comment = "<font bgcolor=\"FFFF00\">(medium)</font>";
	if (val > 299) comment = "<font bgcolor=\"FF0000\">(high)</font>";


	sb.append("<b>Score: "+val+"</b> &nbsp&nbsp&nbsp&nbsp&nbsp "+comment+" <br><br>");
	Iterator<String> si = info.get(index).getFieldNames();
	ArrayList<String> sia = new ArrayList<String>();
	while (si.hasNext()) {
	    sia.add(si.next());
	}
	String[] sta = sia.toArray(new String[0]);
	Arrays.sort(sta);
	for (String field: sta) {
	    String entry = info.get(index).get(field).asText();
	    sb.append(field+": "+entry+"<br>");
	}
	ep.setText(sb.toString());
	return;
    }

    @Override
    public String getGridCellBadgeAnnotation(Grid g, Object value, int cell) {
	// TODO Auto-generated method stub
	return null;
    }

    public String getGridCellAnnotation (Grid g, Object v, int c) {
	if (v == null) {
	    return null;
	}
	Molecule cmpd = (Molecule)v;

	String name = cmpd.getName();
	return name;
    }

}
