
package bard.ui.content;

import bard.core.Entity;
import bard.util.DataGrouping;

public interface CategoryView extends View {
    // return the parent group for this category
    public String getGroup (); 
    public boolean isBusy ();
    public DataGrouping getDataGrouping ();
    // the associated Content
    public void setContent (Content content);
}
