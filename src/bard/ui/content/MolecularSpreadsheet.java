package bard.ui.content;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.AbstractButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.BorderFactory;

import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.DataSource;
import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityValue;
import bard.core.Project;
import bard.core.StringDisplayValue;
import bard.core.StringValue;
import bard.core.Value;
import bard.ui.common.BioAssayTableColumn;
import bard.ui.common.DefaultBottomBar;
import bard.ui.common.EntityTableColumn;
import bard.ui.common.EntityTableColumnFactory;
import bard.ui.common.EntityTableModel;
import bard.ui.common.MolCellRenderer;
import bard.ui.common.TokenCellEditorRenderer;
import bard.ui.explodingpixels.macwidgets.MacIcons;
import bard.ui.explodingpixels.macwidgets.MacButtonFactory;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.SearchResultEvent;
import bard.ui.event.StateChangeEvent;
import bard.ui.icons.IconFactory;

public class MolecularSpreadsheet extends ListViewPanel 
    implements EntityTableColumnFactory, 
    EventSubscriber,
    ActionListener,
    PropertyChangeListener {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = 
	    Logger.getLogger(CompoundContent.class.getName());
    private JPopupMenu popup = new MolSpreadsheetPopupMenu();
    private JPopupMenu addDataMenu = new JPopupMenu();

    private Content<Assay> assayContent = null; //For composite ETags
    
    MolecularSpreadsheet (CompoundContent content) {
	super (content);
	getTable().setEntityColumnFactory(Compound.class, this);
	State state = content.getState();
	if (state != null) {
	    setListSource (Compound.class, state);
	}

	// check for Composite ETag content; pull in assays
	if (content.getResource() instanceof Value) {
	    Value etag = (Value)content.getResource();
	    Value type = etag.getChild("type");
	    if (type != null && type.getValue().toString().endsWith("ETag")) {
	        EventBus.subscribe(StateChangeEvent.class, this);
	        ContentManager manager = content.getContentManager();
	        assayContent = manager.add(Assay.class, etag);
	        onEvent(new StateChangeEvent(assayContent.getState(), "count", 0, 0));
	    }
	}
	
	//getTable().setRowHeight(150);

	updateBottomBar();
	
	EventBus.subscribe(ContentChangeEvent.class, this);
    }

    class MolSpreadsheetPopupMenu extends JPopupMenu {
	private static final long serialVersionUID = 1L;
	String addMenuName = "Add columns ...";
	String removeMenuName = "Remove columns ...";
	JMenuItem addMenu = new JMenuItem (addMenuName);
	JMenuItem removeMenu = new JMenuItem (removeMenuName);
	
	public MolSpreadsheetPopupMenu() {
	    init();
	}
	
	private void init () {
	    addMenu.setActionCommand("add");
	    addMenu.setToolTipText("add col to worksheet");
	    addMenu.addActionListener(MolecularSpreadsheet.this);
	    removeMenu.setActionCommand("remove");
	    removeMenu.setToolTipText("remove col from worksheet");
	    removeMenu.addActionListener(MolecularSpreadsheet.this);
	    removeMenu.setEnabled(false);
	    add(addMenu);
	    add(removeMenu);
	    JMenuItem cancelMenu = new JMenuItem("Cancel");
	    cancelMenu.setActionCommand("cancel");
	    cancelMenu.addActionListener(MolecularSpreadsheet.this);
	    add(cancelMenu);
	}

	@Override
	public void show (Component c, int x, int y) {
	    super.show(c, x, y);
	}
    }

    public void propertyChange (PropertyChangeEvent e) {
	String prop = e.getPropertyName();
	if (!"action".equals(prop)) {
	    return;
	}
	final Compound cmpd = (Compound)e.getNewValue();
	//logger.info(cmpd.toString());

	SwingUtilities.invokeLater(new Runnable () {
	    public void run () {
	    }
	});
    }

    /*
     * EntityTableColumnFactory interface
     */
    public EntityTableColumn[] createColumns 
    (int modelIndex, EntityTableModel model) {
	Class clazz = model.getColumnClass(modelIndex);
	if (!clazz.isAssignableFrom(Compound.class)) {
	    return new EntityTableColumn[0];
	}

	ArrayList<EntityTableColumn> columns = 
		new ArrayList<EntityTableColumn>();
	EntityTableColumn column;

	column = new EntityTableColumn ("Compound", modelIndex, model);
	MolCellRenderer molRenderer = 
		new MolCellRenderer ();
	molRenderer.setLabelVisible(true);
	column.setCellRenderer(molRenderer);
	columns.add(column);

	column = new EntityTableColumn ("Class", modelIndex, model);
	TokenCellEditorRenderer renderer = new TokenCellEditorRenderer (Compound.CompoundClass);
	column.setCellRenderer(renderer);
	columns.add(column);

	column = new EntityTableColumn ("Name", modelIndex, model);
	renderer = new TokenCellEditorRenderer () {
	    @Override
	    public Value[] parseValue (Object value) {
		Entity e = (Entity)value;
		return new Value[] {
			new StringValue 
			((DataSource)null, "Name", e.getName())
		};
	    }
	};
	column.setCellRenderer(renderer);
	column.setCellEditor(renderer);
	columns.add(column);

	column = new EntityTableColumn ("Active/Tested Assays", modelIndex, model);
	renderer = new TokenCellEditorRenderer () {
	    @Override
	    public Value[] parseValue (Object value) {
		Entity e = (Entity)value;
		Value numActive = e.getValue(Compound.NumActiveAssays);
		Value numTested = e.getValue(Compound.NumAssays);
		if (numActive != null && numTested != null)
		    return new Value[] {
			new StringDisplayValue 
			((DataSource)null, Compound.NumActiveAssays, numActive.getValue() + "/" + numTested.getValue(), numActive.toString())
		};
		else return new Value[] {};
	    }
	};
	column.setComparator(new Comparator() {
	    double[] parseValue(Entity e) {
		Value numActive = e.getValue(Compound.NumActiveAssays);
		Value numTested = e.getValue(Compound.NumAssays);
		int na = 0, nt = 0;
		double ratio = 0.;
		if (numActive != null && numTested != null) {
		    na = Integer.valueOf(numActive.getValue().toString());
		    nt = Integer.valueOf(numTested.getValue().toString());
		    ratio = 0;
		    if (nt > 0)
			ratio = (double)na / (double)nt;
		}
		return new double[] {ratio, na, -nt};
	    }
	    
	    public int compare(Object o1, Object o2) {
		if (o2 == null) {
		    if (o1 == null)
			return 0;
		    return 1;
		} else if (o1 == null) {
		    return -1;
		}
		if (o1 instanceof Entity && o2 instanceof Entity) {
		    double[] da1 = parseValue((Entity)o1);
		    double[] da2 = parseValue((Entity)o2);
		    for (int i=0; i<3; i++)
			if (da1[i] < da2[i]) return -1;
			else if (da1[i] > da2[i]) return 1;
		    return 0;
		}
		else return o1.toString().compareTo(o2.toString());
	    }
	});
	column.setCellRenderer(renderer);
	column.setCellEditor(renderer);
	columns.add(column);
	
	column = new EntityTableColumn ("IUPAC", modelIndex, model);
	renderer = new TokenCellEditorRenderer (Compound.IUPACNameValue);
	column.setCellRenderer(renderer);
	column.setCellEditor(renderer);
	columns.add(column);

//	column = new EntityTableColumn ("SID", modelIndex, model);
//	renderer = new TokenCellEditorRenderer
//		(Compound.PubChemSIDValue, true);
//	column.setPreferredWidth(80);
//	column.setCellRenderer(renderer);
//	column.setCellEditor(renderer);
//	columns.add(column);

	return columns.toArray(new EntityTableColumn[0]);
    }

    public void onEvent (Object ev) {
        if (ev instanceof ContentChangeEvent) {
            onEvent ((ContentChangeEvent)ev);
        }
        else if (ev instanceof StateChangeEvent) {
            onEvent ((StateChangeEvent)ev);
        }
    }

    public void onEvent (StateChangeEvent e) {  // for delayed loading of assays in composite ETags especially
        if (assayContent == null || e.getState() != assayContent.getState()) {
            return;
        }
        
        ArrayList<String> assayList = new ArrayList<String>();
        for (int i=0; i<getTable().getColumnCount();i++)
            if (getTable().getColumnModel().getColumn(i) instanceof BioAssayTableColumn)
                assayList.add(((BioAssayTableColumn)getTable().getColumnModel().getColumn(i)).getAssay().getId().toString());
        for (Assay assay: assayContent.getState().getEntities()) {
            if (assayList.indexOf(assay.getId().toString()) == -1)
                getTable().addColumn(
                        new BioAssayTableColumn(assay, (CompoundContent)content, getTable()));
        }
    }

    public void onEvent (ContentChangeEvent e) {
	if (e.getContent() != content) {
	    return;
	}
	/*
            logger.info("name="+e.getName()
                        +" old="+e.getOldValue()+" new="+e.getNewValue());
	 */
	if (e.getName().equals(ContentChangeEvent.STATE)) {
	    State<Compound> state = (State<Compound>)e.getNewValue();
	    logger.info("## STATE: "+content.getName()
		    +" old="+e.getOldValue()
		    +" new="+e.getNewValue()
		    +" size="+state.size());
	    setListSource (Compound.class, state);
	}
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	popup.setVisible(false);

	if (e.getActionCommand().equals("Add Data")) {
	    Component source = (Component)e.getSource();
	    addDataMenu.show(source, source.getX(), source.getY());
	} else if (e.getActionCommand().equals("Add Specific Assay ...")) {
	    String assayId = JOptionPane.showInputDialog("Enter Assay ID to Add", 1053);
	    if (assayId == null || assayId.length()==0) return;
	    Assay assay = (Assay)content.getContentManager().getServiceManager()
		    .getService(Assay.class).get(assayId);
	    // http://bard.nih.gov/api/latest/experiments/1865/etag/4542b5ad815102c4/exptdata
	    getTable().addColumn(
		    new BioAssayTableColumn(assay, (CompoundContent)content, getTable()));
	} else if (e.getActionCommand().equals("Add Assays from Project ...")) {
	    String projectId = JOptionPane.showInputDialog("Enter Project ID to Add", 449);
	    if (projectId == null || projectId.length()==0) return;
	    Project project = (Project)content.getContentManager().getServiceManager()
		    .getService(Project.class).get(projectId);
	    for (Value value: project.getValues(Assay.class.toString())) {
		EntityValue ev = (EntityValue)value;
		Assay assay = (Assay)project.getEntityFromValue(ev);
		getTable().addColumn(
			new BioAssayTableColumn(assay, (CompoundContent)content, getTable()));
	    }
	} else if (e.getActionCommand().equals("Add Assays from Collection ...")) {
	    HashMap<String, Content> collections = new HashMap<String, Content>();
	    EntityService es = content.getService();
	    if (es != null) {
		ContentManager cm = ContentManager.getInstance(content.getService().getServiceManager());
		for (Content assayContent: cm.getContents()) {
		    String name = assayContent.getName();
		    if (assayContent != this.content && assayContent.getEntityClass() == Assay.class && !"Assays".equals(name)) {
			collections.put(name, assayContent);
		    }
		}
	    }
	    if (collections.size() == 0) return;
	    String collectionId = (String)JOptionPane.showInputDialog((Component)e.getSource(), "Select Assay Collection", "Collection Chooser", 
		    JOptionPane.QUESTION_MESSAGE, null, collections.keySet().toArray(new String[0]), null);
	    if (collectionId == null || collectionId.length()==0) return;
	    Content assayContent = collections.get(collectionId);
	    for (Object assay: assayContent.getEntities()) {
		getTable().addColumn(
			new BioAssayTableColumn((Assay)assay, (CompoundContent)content, getTable()));
	    }
	}
	
    }
    
    protected void updateBottomBar() {
	super.updateBottomBar();

	addDataMenu = new JPopupMenu();
	JMenuItem addProjectAssays = new JMenuItem("Add Assays from Project ...");
	addProjectAssays.addActionListener(this);
	JMenuItem addAssaySet = new JMenuItem("Add Assays from Collection ...");
	addAssaySet.addActionListener(this);
	JMenuItem addAssayByID = new JMenuItem("Add Specific Assay ...");
	addAssayByID.addActionListener(this);
	
	addDataMenu.add(addProjectAssays);
	addDataMenu.add(addAssaySet);
	addDataMenu.add(addAssayByID);

        /*
	JButton addCol = new JButton
            //(IconFactory.getImageIcon("plus24.png", 16));
            (MacIcons.PLUS);
        */
        AbstractButton addCol = 
            (AbstractButton) MacButtonFactory.createGradientButton
            (MacIcons.PLUS, this);
        addCol.setBorder(BorderFactory.createEmptyBorder());
	addCol.setActionCommand("Add Data");
	addCol.setToolTipText("Add BioAssay data to worksheet");
	//addCol.addActionListener(this);
	bottomBar.addComponentToLeft(addCol);
    }
}


