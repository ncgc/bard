package bard.ui.content;

import bard.core.Assay;
import bard.core.AssayService;
import bard.core.AssayValues;
import bard.core.Biology;
import bard.core.DataSource;
import bard.core.Entity;
import bard.core.EntityValue;
import bard.core.Experiment;
import bard.core.ExperimentValues;
import bard.core.IntValue;
import bard.core.Project;
import bard.core.StringDisplayValue;
import bard.core.StringValue;
import bard.core.Value;
import bard.ui.common.EntityTableColumn;
import bard.ui.common.EntityTableColumnFactory;
import bard.ui.common.EntityTableModel;
import bard.ui.common.TokenCellEditorRenderer;
import bard.ui.content.ProjectContent.ProjectListViewPanel;
import bard.ui.event.ContentChangeEvent;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;

import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;


public class AssayContent extends AbstractContent<Assay> {
    private static final Logger logger = Logger.getLogger
        (AssayContent.class.getName());

    private static final Preferences PREFS = 
	Preferences.userNodeForPackage(AssayContent.class);
    private static final Preferences NODE;
    static {
	Preferences node = PREFS;
	try {
	    node = PREFS.node(AssayContent.class.getSimpleName());
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't get preference node for "
		       +AssayContent.class, ex);
	}
	NODE = node;
    }

    protected AssayContent () {
        setName ("Assays");
    }

    public AssayContent (AssayService service) {
        super (service);
    }

    public AssayContent (AssayService service, Object etag) {
        super (service, etag);
    }

    protected Preferences getPreferences () { return NODE; }
    public Class<Assay> getEntityClass () { return Assay.class; }
    public String getName () { return name; }
    public View.Type[] getSupportedViewTypes () {
        return new View.Type[]{
        	View.Type.List, View.Type.Record//, View.Type.Grid
        };
    }

    protected ListViewPanel listView;
    protected CategoryGroupingPanel gridView;
    protected AssayRecordViewPanel recordView;

    /*
     * Views
     */
    @Override
    protected View createListView () {
        if (listView == null) {
            listView = new AssayListViewPanel ();
        }
        return listView;
    }
    
//    @Override
//    protected View createGridView () {
//        if (gridView == null) {
//            gridView = new CompoundCategoryGroupingPanel ();
//        }
//        return gridView;
//    }

    @Override
    protected View createRecordView () {
        if (recordView == null) {
            recordView = new AssayRecordViewPanel (this);
        }
        return recordView;
    }

    /**
     * List view
     **/
    class AssayListViewPanel extends ListViewPanel 
	implements EntityTableColumnFactory, 
		   EventSubscriber<ContentChangeEvent>,
		   PropertyChangeListener, HyperlinkListener {

	AssayListViewPanel () {
	    super (AssayContent.this);
	    getTable().setEntityColumnFactory(Assay.class, this);
	    State state = AssayContent.this.getState();
	    if (state != null) {
		setListSource (Assay.class, state);
	    }
	    //getTable().setRowHeight(150);
	    EventBus.subscribe(ContentChangeEvent.class, this);
	}

	public void propertyChange (PropertyChangeEvent e) {
	    String prop = e.getPropertyName();
	    if (!"action".equals(prop)) {
		return;
	    }
	    final Assay assay = (Assay)e.getNewValue();
	    //logger.info(cmpd.toString());

	    SwingUtilities.invokeLater(new Runnable () {
		    public void run () {
		    }
		});
	}
	
	/*
	 * EntityTableColumnFactory interface
	 */
	public EntityTableColumn[] createColumns 
	    (int modelIndex, EntityTableModel model) {
	    Class clazz = model.getColumnClass(modelIndex);
	    if (!clazz.isAssignableFrom(Assay.class)) {
		return new EntityTableColumn[0];
	    }
            
	    ArrayList<EntityTableColumn> columns = 
		new ArrayList<EntityTableColumn>();
	    EntityTableColumn column;

	    column = new EntityTableColumn ("PubChem AID", modelIndex, model); //AssayGrant, assay component role, protein, species name
	    TokenCellEditorRenderer renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer aids = new StringBuffer();
                    for (Value expt: e.getValues(Experiment.class.toString())) {
                        Experiment exp = (Experiment)e.getEntityFromValue((EntityValue)expt);
                        if (exp != null) {
                            if (aids.length() > 0) aids.append(", ");
                            aids.append(exp.getPubchemAid());
                        }
                    }
                    return new Value[] {
                            new StringValue 
                            ((DataSource)null, "PubChem AID", e.getId()+":"+aids)//+e.getValue(AssayValues.AssayPubChemAIDValue).getValue().toString()) !!!!
                    };
                }
            };
            column.setCellRenderer(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Source", modelIndex, model); //AssaySource, AssayGrant, assay component role, protein, species name
	    renderer = new TokenCellEditorRenderer () {
		JLabel sourceLabel = new JLabel();
		public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column)
		{
		    String source = null;
		    if (((Assay)value).getValue("AssaySource") != null)
			source = ((Assay)value).getValue("AssaySource").getValue().toString();
		    ImageIcon icon = null;
		    IconFactory.CenterIcon.checkHeight(table.getRowHeight());
		    for (IconFactory.CenterIcon center: IconFactory.CenterIcon.values()) {
			if (center.getCenter().equals(source) || center.getFullName().equals(source)) {
			    icon = center.getIcon();
			    source = center.getFullName();
			    break;
			}
		    }

		    if (icon != null) {
			sourceLabel.setIcon(icon);
			sourceLabel.setText(null);
			sourceLabel.setToolTipText(source);
		    } else {
			sourceLabel.setIcon(null);
			sourceLabel.setText(source);
			sourceLabel.setToolTipText(source);
		    }
		    return sourceLabel;
		}
	    };
            column.setCellRenderer(renderer);
	    columns.add(column);

            /*
	    column = new EntityTableColumn ("Probe", modelIndex, model);
            renderer = new TokenCellEditorRenderer (Compound.ProbeIDValue);
            column.setCellRenderer(renderer);
	    columns.add(column);
            */

	    column = new EntityTableColumn ("Name", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                    @Override
                    public Value[] parseValue (Object value) {
                        Entity e = (Entity)value;
                        return new Value[] {
                            new StringValue 
                            ((DataSource)null, "Name", e.getName())
                        };
                    }
                };
            column.setCellRenderer(renderer);
	    //column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Biology", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object entity) {
//            TableCellRenderer jeptcr = new TableCellRenderer() {
//                JEditorPane jep = new JEditorPane();
//                
//                public Component getTableCellRendererComponent(JTable arg0,
//                        Object entity, boolean arg2, boolean arg3, int arg4,
//                        int arg5) {
//                    
                    Assay e = (Assay) entity;
                    StringBuffer string = new StringBuffer();
                    for (Value context: e.getValues(Biology.class.toString())) {
                        Biology b = (Biology)e.getEntityFromValue((EntityValue)context);
                        if (b != null) {
                            String display, url;
                            display = b.getName();
                            url = null;
                            //if (url != null)
                            //    string.append("<a href=\""+url+"\">"+display+"</a><br>\n");
                            string.append(string.length() > 0 ? "; " : "");
                            string.append(display);
                            
                        }
                    }
//                    jep.setContentType("text/html");
//                    jep.setEditable(false);
//                    jep.setText(string.toString());
//                    jep.addHyperlinkListener(AssayListViewPanel.this);
//                     
//                    return jep;
                    return new Value[] {
                            new StringValue 
                            ((DataSource)null, "Biology", string.toString())
                        };
                }
                
            };
            column.setCellRenderer(renderer);
//            column.setCellEditor(renderer);
            columns.add(column);

	    column = new EntityTableColumn ("Format", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer string = new StringBuffer();
                    for (Value v: e.getValues(Assay.AssayMinimumAnnotation.assayFormat.name())) {
                	if (string.indexOf(v.getValue().toString()) == -1) {
                	    if (string.length() > 0) string.append(",");
                	    string.append(v.getValue().toString());
                	}
                    }
                    if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Format", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    //column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Type", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer string = new StringBuffer();
                    for (Value v: e.getValues(Assay.AssayMinimumAnnotation.assayType.name())) {
                	if (string.indexOf(v.getValue().toString()) == -1) {
                	    if (string.length() > 0) string.append(",");
                	    string.append(v.getValue().toString());
                	}
                    }
                    if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Type", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    //column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Kit or Cell Name", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer string = new StringBuffer();
                    for (Value v: e.getValues("cultured cell name")) {
                	if (string.indexOf(v.getValue().toString()) == -1) {
                	    if (string.length() > 0) string.append(",");
                	    string.append(v.getValue().toString());
                	}
                    }
                    if (string.length() > 0) 
                        string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Kit or Cell Name", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Species", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer string = new StringBuffer();
                    for (Value v: e.getValues("species name")) {
                	if (string.indexOf(v.getValue().toString()) == -1) {
                	    if (string.length() > 0) string.append(",");
                	    string.append(v.getValue().toString());
                	}
                    }
                    if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Species", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Detection", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer string = new StringBuffer();
                    for (Value v: e.getValues(Assay.AssayMinimumAnnotation.detectMethod.name())) {
                	if (string.length() > 0) string.append(",");
                	string.append(v.getValue().toString());
                    }
                    if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Detection", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Instrument", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer string = new StringBuffer();
                    for (Value v: e.getValues(Assay.AssayMinimumAnnotation.detectInstrument.name())) {
                	if (string.length() > 0) string.append(",");
                	string.append(v.getValue().toString());
                    }
                    if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Instrument", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Footprint", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    StringBuffer string = new StringBuffer();
                    for (Value v: e.getValues(Assay.AssayMinimumAnnotation.footprint.name())) {
                	if (string.length() > 0) string.append(",");
                	string.append(v.getValue().toString());
                    }
                    if (string.length() > 0) string.setCharAt(0, Character.toUpperCase(string.charAt(0)));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Footprint", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    column.setCellEditor(renderer);
	    columns.add(column);

	    column = new EntityTableColumn ("Wavelengths", modelIndex, model);
            renderer = new TokenCellEditorRenderer () {
                @Override
                public Value[] parseValue (Object value) {
                    Assay e = (Assay)value;
                    ArrayList<String> list = new ArrayList<String>();
                    for (Value v: e.getValues("excitation wavelength")) {
                	list.add("EX:"+v.getValue().toString());
                    }
                    for (Value v: e.getValues("emission wavelength")) {
                	list.add("EM:"+v.getValue().toString());
                    }
                    for (Value v: e.getValues("absorbance wavelength")) {
                	list.add("AB:"+v.getValue().toString());
                    }
                    for (Value v: e.getValues("measurement wavelength")) {
                	list.add("Mes:"+v.getValue().toString());
                    }
                    for (int i=list.size()-1; i>-1; i--) {
                        if (list.indexOf(list.get(i)) < i)
                            list.remove(i);
                    }
                    StringBuffer string = new StringBuffer();
                    if (list.size() > 0) string.append(list.get(0));
                    for (int i=1; i<list.size(); i++)
                        string.append(", "+list.get(i));
                    return new Value[] {
                        new StringValue 
                        ((DataSource)null, "Wavelengths", string.toString())
                    };
                }
            };
            column.setCellRenderer(renderer);
	    column.setCellEditor(renderer);
	    columns.add(column);

//	    column = new EntityTableColumn ("Type", modelIndex, model);
//            renderer = new TokenCellEditorRenderer () {
//                @Override
//                protected Value[] parseValue (Object value) {
//                    Assay e = (Assay)value;
//                    String type = "";
//                    for (ExperimentValues.ExperimentType at: ExperimentValues.ExperimentType.values()) {
//                	boolean hasType = false;
//                	for (Experiment expt: e.getExperiments())
//                	    if (at.equals(expt.getType()))
//                		hasType = true;
//                	if (hasType) {
//                	    if (type.length() > 0) type += ", ";
//                	    type += at.toString();
//                	}               	
//                    }
//                    return new Value[] {
//                        new StringValue 
//                        ((DataSource)null, "Type", type)
//                    };
//                }
//            };
//            column.setCellRenderer(renderer);
//	    column.setCellEditor(renderer);
//	    columns.add(column);
//
//	    column = new EntityTableColumn ("Classification", modelIndex, model);
//            renderer = new TokenCellEditorRenderer () {
//                @Override
//                protected Value[] parseValue (Object value) {
//                    Assay e = (Assay)value;
//                    String type = "";
//                    for (ExperimentValues.ExperimentRole ar: ExperimentValues.ExperimentRole.values()) {
//                	boolean hasType = false;
//                	for (Experiment expt: e.getExperiments())
//                	    if (ar.equals(expt.getRole()))
//                		hasType = true;
//                	if (hasType) {
//                	    if (type.length() > 0) type += ", ";
//                	    type += ar.toString();
//                	}               	
//                    }
//                    return new Value[] {
//                        new StringValue 
//                        ((DataSource)null, "Classification", type)
//                    };
//                }
//            };
//            column.setCellRenderer(renderer);
//	    column.setCellEditor(renderer);
//	    columns.add(column);
//
//	    column = new EntityTableColumn ("Tested Compounds", modelIndex, model);
//            renderer = new TokenCellEditorRenderer () {
//                @Override
//                protected Value[] parseValue (Object value) {
//                    Assay e = (Assay)value;
//                    int testedNum = 0;
//                    for (Experiment expt: e.getExperiments())
//                	testedNum += ((IntValue)expt.getValue(Experiment.ExperimentCompoundCountValue)).getValue();
//                    return new Value[] {
//                        new IntValue 
//                        ((DataSource)null, "Compounds Tested", testedNum)
//                    };
//                }
//            };
//            column.setCellRenderer(renderer);
//	    column.setCellEditor(renderer);
//	    columns.add(column);

	    return columns.toArray(new EntityTableColumn[0]);
	}

	public void onEvent (ContentChangeEvent e) {
	    if (e.getContent() != AssayContent.this) {
		return;
	    }
            logger.info("name="+e.getName()
                        +" old="+e.getOldValue()+" new="+e.getNewValue());
            if (e.getName().equals(ContentChangeEvent.STATE)) {
                State<Assay> state = (State<Assay>)e.getNewValue();
                setListSource (Assay.class, state);
            }
	}

        public void hyperlinkUpdate (HyperlinkEvent e) {
            //URL url = e.getURL();
            java.net.URI uri = UIUtil.getLinkURI(e);
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                logger.info("launching "+uri.toString());
                try {
                    //new BrowserLauncher().openURLinBrowser(uri.toString());
                    EventBus.publish(e);
                }
                catch (Exception ex) {
                    logger.log(Level.SEVERE, 
                               "Can't launch external browser "+uri, ex);
                }
            }
            else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
                JComponent c = (JComponent)e.getSource();
                c.setToolTipText(uri != null ? uri.toString() : null);
            }
        }
    }
}
