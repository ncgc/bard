
package bard.ui.content;

import java.beans.PropertyChangeListener;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.swing.Icon;
import org.jdesktop.application.Application;
import org.jdesktop.swingworker.SwingWorker;

import bard.ui.explodingpixels.macwidgets.*;
import bard.ui.icons.IconFactory;

/**
 * TODO: This class should be rework to support persistence so
 *  as to allow it to be saved and rerun
 */
public abstract class AbstractTask extends SourceListItem {
    private static final Logger logger = 
	Logger.getLogger(AbstractTask.class.getName());

    static final Icon Run = IconFactory.App.Run.getIcon();
    static final Icon Err = IconFactory.App.Err.getIcon();
    static final Icon OK = IconFactory.App.OK.getIcon();

    class TaskWorker extends SwingWorker<Throwable, String> {
	Throwable ex;

	@Override
	protected Throwable doInBackground () {
	    try {
		doWork ();
	    }
	    catch (Exception ex) {
		return ex;
	    }
	    return null;
	}

	@Override
	protected void done () {
	    try {
		ex = get ();
		stop ();
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "TaskWorker thread interrupted", ex);
	    }
	}

	@Override
	protected void process (String... mesg) {
	}

	Throwable getException () { return ex; }
    }

    TaskWorker worker = new TaskWorker ();
    boolean removeOnSuccess;
    String mesg;

    public AbstractTask (String name) {
	this (name, false);
    }

    public AbstractTask (String name, boolean removeOnSuccess) {
	super (name, Run);
	this.removeOnSuccess = removeOnSuccess;
    }

    public String getName () { return getText (); }
    public void setName (String name) {	setText (name); }
    protected void setMessage (String mesg) {
	this.mesg = mesg;
    }
    public String getMessage () { return mesg; }

    public boolean getRemoveOnSuccess () { return removeOnSuccess; }
    public void setRemoveOnSuccess (boolean removeOnSuccess) {
	this.removeOnSuccess = removeOnSuccess;
    }

    public void start () {
	if (worker.getState() == SwingWorker.StateValue.DONE) {
	    worker = new TaskWorker ();
	}
	setBusy (true);
	worker.execute();
    }

    public void stop () {
	if (stopped ()) {
	    setIcon (worker.getException() != null ? Err : OK);
	}
	else {
	    worker.cancel(true);
	}
	setBusy (false);
    }

    public boolean stopped () { 
	return worker.isDone() || worker.isCancelled(); 
    }
    public boolean isDone () { return worker.isDone(); }
    public boolean isCanceled () { return worker.isCancelled(); }
    public Throwable getException () { return worker.getException(); }

    // define by subclass to do actual work in background thread
    protected abstract void doWork () throws Exception;
}
