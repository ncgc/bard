package bard.ui.content;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.CompoundService;
import bard.core.Entity;
import bard.core.Project;
import bard.core.Value;
import bard.core.adapter.CompoundAdapter;
import bard.ui.common.AnnotationTree;
import bard.ui.common.Grid;
import bard.ui.common.GridCellAnnotator;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import bard.util.ChemblTargetClass;
import chemaxon.struc.Molecule;
import com.jidesoft.swing.JideTabbedPane;
import edu.stanford.ejalbert.BrowserLauncher;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXImagePanel;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.statistics.HistogramDataset;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CompoundRecordViewPanel extends RecordViewPanel 
    implements GridCellAnnotator, ChangeListener {

    static private Logger logger = Logger.getLogger
	(CompoundRecordViewPanel.class.getName());

    static final String PC_URL1 = 
	"http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?cid=";
    static final String PC_URL2 = 
	"http://www.ncbi.nlm.nih.gov/sites/entrez?db=pcsubstance&term=";

    static final String CHEMIDPLUS_URL = 
	"http://chem.sis.nlm.nih.gov/chemidplus/direct.jsp?result=advanced&regno=";

    static final Icon PLUS_SMALL_ICON = IconFactory.Misc.Plus16.getIcon();
    static final Icon MINUS_SMALL_ICON = IconFactory.Misc.Minus16.getIcon();

    private JLabel titleLabel;

    class SynonymLoader extends SwingWorker<Collection<Value>, Void> {
        CompoundAdapter compound;
        DefaultListModel model;
        UIRecordPanel ui;
        
        SynonymLoader (CompoundAdapter c, 
                       DefaultListModel model, 
                       UIRecordPanel ui) {
            compound = c;
            this.model = model;
            this.ui = ui;
        }

        @Override
        protected Collection<Value> doInBackground () {
            Collection<Value> syns = null;
            try {
                CompoundService cs = 
                    (CompoundService)getContent().getService();
                syns = cs.getSynonyms
                    (compound.getCompound());
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            return syns;
        }

        protected void process (Collection<Value> syns) {
            for (Value s : syns) {
                compound.getCompound().add(s);
                model.addElement(s.getValue());
            }
        }

        @Override
        protected void done () {
            ui.setBusy(false);
            try {
                process(get ());              
            }
            catch (Exception ex) {
        	ex.printStackTrace();
            }
        }
    }

    class SIDLoader extends SwingWorker<Throwable, Value> {
        CompoundAdapter compound;
        DefaultListModel model;
        UIRecordPanel ui;
        SIDLoader (CompoundAdapter c,  DefaultListModel model,  UIRecordPanel ui) {
            compound = c;
            this.model = model;
            this.ui = ui;
        }

        @Override
        protected Throwable doInBackground () {
            try {
                Collection<Value> sids = compound.getCompound().getSubstances();
                publish(sids.toArray(new Value[0]));
            }
            catch (Exception ex) {
                return ex;
            }
            return null;
        }

        @Override
        protected void process (Value... syn) {
            for (Value s : syn) {
                compound.getCompound().add(s);
                model.addElement(s);
            }
        }

        @Override
        protected void done () {
            ui.setBusy(false);
            try {
                Throwable t = get ();
            } catch (Exception ex) {
            }
        }
    }

    class BioactivitySummaryLoader extends SwingWorker<Throwable, Map<String, Object>> {
        Compound compound;
        BioactivitySummaryRecord ui;
        Map<String, Object> map;

        BioactivitySummaryLoader(Compound c, UIRecordPanel ui) {
            compound = c;
            this.ui = (BioactivitySummaryRecord) ui;
        }

        @Override
        protected Throwable doInBackground() {
            try {
                Map<String, Object> summary = compound.getSummary();
                publish(summary);
            } catch (Exception ex) {
                return ex;
            }
            return null;
        }

        protected void process(Map<String, Object>... maps) {
            this.map = maps[0];
        }

        @Override
        protected void done() {
            if (ui.getRecordPanel().getComponentCount() > 0 || map == null) return;

            List<Double> testScores = (List<Double>) map.get("testedScores");
            if (testScores == null) testScores = new ArrayList<Double>();
            List<Double> testPotencies = (List<Double>) map.get("testedPotencies");
            if (testPotencies == null) testPotencies = new ArrayList<Double>();

            double meanPotency = 0.0, sdPotency = 0.0;
            if (testPotencies != null) {
                for (Double x : testPotencies) meanPotency += x;
                meanPotency /= testPotencies.size();

                double tmp = 0.0;
                for (Double x : testPotencies) tmp += (x - meanPotency) * (x - meanPotency);
                tmp /= testPotencies.size();
                sdPotency = Math.sqrt(tmp);
            }

            List<String> targets = (List<String>) map.get("testedTargets");
            if (targets == null) targets = new ArrayList<String>();
            int nunique = (new HashSet<String>(targets)).size();
            String targetPhrase = "";
            if (nunique > 0) targetPhrase = " (covering " + nunique + " unique targets)";

            DecimalFormat df = new DecimalFormat("#.00");
            StringBuilder sb = new StringBuilder(UIUtil.CSS);
            sb.append("Tested in ").append(map.get("ntest")).append(" assays ");
            sb.append(targetPhrase);
            sb.append(" and active in ");
            sb.append(map.get("nhit")).append(".</p><br><br>");
            //sb.append("The average tested potency was ").append(df.format(meanPotency)).append(" (S.D. = ").append(df.format(sdPotency)).append(")");
            ui.getEditorPane().setText(sb.toString());
            ui.getRecordPanel().add(ui.getEditorPane());

            // target summary
            ChemblTargetClass ctc = new ChemblTargetClass();
            Map<String, Integer> tcounts = new HashMap<String, Integer>();
            for (String target : targets) {
                String klass = ctc.getTargetClass(target, 1);
                if (klass == null) continue;
                if (tcounts.containsKey(klass)) tcounts.put(klass, tcounts.get(klass)+1);
                else tcounts.put(klass, 1);
            }
            if (tcounts.keySet().size() > 0) {
                DefaultPieDataset pieData = new DefaultPieDataset();
                for (String key : tcounts.keySet()) {
                    int value = tcounts.get(key);
                    pieData.setValue(key + " = " + value, new Double(value));
                }
                JFreeChart targetPieChart = ChartFactory.createPieChart(null, pieData, false, true, false);
                targetPieChart.setTitle("Tested Target Classes");
                PiePlot plot = (PiePlot) targetPieChart.getPlot();
                plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 10));
                plot.setCircular(false);
                plot.setLabelGap(0.02);
                ui.getRecordPanel().add(new ChartPanel(targetPieChart));
            } else {
                JPanel p = new JPanel(new BorderLayout());
                p.add(new JLabel("No target classification available"), BorderLayout.CENTER);
                ui.getRecordPanel().add(p);
            }

            // scores histogram
            double[] avals = new double[testScores.size()];
            for (int i = 0; i < testScores.size(); i++) avals[i] = testScores.get(i);
            if (avals.length > 0) {
        	HistogramDataset scoreData = new HistogramDataset();
        	scoreData.addSeries("Histogram", avals, 10);
        	JFreeChart testedScoresHistogram = ChartFactory.createHistogram(
        		"Distribution of Scores (Tested)",
        		"Score", "Frequency",
        		scoreData,
        		PlotOrientation.VERTICAL, false, false, false);
        	((XYBarRenderer)((XYPlot) testedScoresHistogram.getPlot()).getRenderer()).setDrawBarOutline(true);
        	((XYBarRenderer)((XYPlot) testedScoresHistogram.getPlot()).getRenderer()).setBarPainter(new StandardXYBarPainter());
        	((XYPlot) testedScoresHistogram.getPlot()).getRenderer().setPaint(new Color(255,0,0));

        	ui.getRecordPanel().add(new ChartPanel(testedScoresHistogram));
            }

            avals = new double[testPotencies.size()];
            int npos = 0;
            for (int i = 0; i < testPotencies.size(); i++) {
        	double potency = testPotencies.get(i);
                avals[i] = Math.log(potency > 0 ? potency : 100);
            }
            if (avals.length > 0) {
        	HistogramDataset potencyData = new HistogramDataset();
        	potencyData.addSeries("Histogram", avals, 10);
        	JFreeChart testedPotenciesHistogram = ChartFactory.createHistogram(
                    "Distribution of Potencies (Tested)",
                    "log [Potency uM]", "Frequency",
                    potencyData,
                    PlotOrientation.VERTICAL, false, false, false);
        	((XYBarRenderer)((XYPlot) testedPotenciesHistogram.getPlot()).getRenderer()).setDrawBarOutline(true);
        	((XYBarRenderer)((XYPlot) testedPotenciesHistogram.getPlot()).getRenderer()).setBarPainter(new StandardXYBarPainter());
        	((XYPlot) testedPotenciesHistogram.getPlot()).getRenderer().setPaint(new Color(255,0,0));
        	//final NumberAxis domainAxis = new LogarithmicAxis("Potency [uM]");
        	//((XYPlot) testedPotenciesHistogram.getPlot()).setDomainAxis(domainAxis);

        	ui.getRecordPanel().add(new ChartPanel(testedPotenciesHistogram));
            }

            ui.setBusy(false);
            try {
                Throwable t = get();
            } catch (Exception ex) {
            }
        }
    }


    class AnnotationLoader extends SwingWorker<Throwable, Value> {
        CompoundAdapter compound;
        UIRecordPanel ui;
        CollectionTree tree;
        AnnotationTree atree;
        JEditorPane namePane;

        AnnotationLoader(CompoundAdapter c, CollectionTree tree, AnnotationTree atree, JEditorPane namePane, UIRecordPanel ui) {
            compound = c;
            this.tree = tree;
            this.atree = atree;
            this.ui = ui;
            this.namePane = namePane;
        }

        @Override
        protected Throwable doInBackground() {
            try {
                Collection<Value> sids = compound.getCompound().getAnnotations();
                publish(sids.toArray(new Value[0]));
            } catch (Exception ex) {
                return ex;
            }
            return null;
        }

        @Override
        protected void process(Value... annots) {

            // save the annotations
            for (Value s : annots) {
                compound.getCompound().add(s);
            }

            // set up the collections tree
            if (tree != null) tree.setCompound(compound);

            // set up the annotations tree
            if (atree != null) atree.setEntity(compound);

            // update the compound name with linkout
            if (namePane != null) {
                String name = compound.getName();
                StringBuilder sb = new StringBuilder(UIUtil.CSS);
                Collection<Value> anno = compound.getCompound().getAnnotations();
                Value url = compound.getCompound().getValue(Compound.URLValue);
                if (url == null) {
                    for (Value v : anno) {
                        if (v.getId().equals("URL")) {
                            url = v;
                            break;
                        }
                    }
                }
                if (url != null) {
                    sb.append("<a href=\"").append(url.getValue()).append("\">").append(name).append("</a>");
                } else {
                    sb.append(name);
                }
                namePane.setText(sb.toString());
                namePane.setCaretPosition(0);
            }
        }

        @Override
        protected void done() {
            ui.setBusy(false);
            try {
                Throwable t = get();
            } catch (Exception ex) {
            }
        }
    }

    static class FilterTreeModel extends DefaultTreeModel {
	Compound compound;

	FilterTreeModel () {
	    super (new DefaultMutableTreeNode ()); // empty root
	}

	void setFilters (Compound compound) {
	    DefaultMutableTreeNode root = (DefaultMutableTreeNode)getRoot ();
	    root.removeAllChildren();
	    this.compound = compound;
	    reload();
	}

	Compound getCompound () { return compound; }

	void clear () {
	    compound = null;
	    ((DefaultMutableTreeNode)getRoot()).removeAllChildren();
	    reload ();
	}
    }

    static class FilterTreeCellRenderer extends DefaultTreeCellRenderer {

	static final Icon LEAF_ICON  = IconFactory.Misc.Leaf.getIcon();
	static final Icon FILTER_ICON = IconFactory.Misc.Funnel.getIcon();

	FilterTreeCellRenderer () {
	    setLeafIcon (LEAF_ICON);
	    setOpenIcon (FILTER_ICON);
	    setClosedIcon (FILTER_ICON);
	}

	@Override
	public Component getTreeCellRendererComponent
	    (JTree tree, Object value, boolean sel, boolean expanded, 
	     boolean leaf, int row, boolean hasFocus) {
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
	    if (value == null) {
	    }
	    else if (leaf) {
		Object obj = node.getUserObject();
		if (obj instanceof Molecule) {
		    Molecule mol = (Molecule)node.getUserObject();
		    if (mol != null) {
			value = mol.getProperty("FILTER")
			    +": "+mol.getProperty("VALUE");
		    }
		    else {
			value = null;
		    }
		}
		else {
		    value = obj;
		}
	    }

	    return super.getTreeCellRendererComponent
		(tree, value, sel, expanded, leaf, row, hasFocus);
	}
    }


    // also the same as FilterTreeModel... refactor? sigh
    static class CollectionTreeModel extends DefaultTreeModel {
	Compound compound;

	CollectionTreeModel () {
	    super(new DefaultMutableTreeNode());
	}

	void setCompound (CompoundAdapter compound) {
	    DefaultMutableTreeNode root = (DefaultMutableTreeNode)getRoot ();
	    root.removeAllChildren();

            Map<String, java.util.List<String>> anno = 
                new HashMap<String, java.util.List<String>>();

            // first pass grab all relevant annotations
            for (Value v : compound.getAnnotations()) {
                if (v.getId().equals("CompoundSpectra")) {
                    java.util.List<String> values = anno.get("QC spectra");
                    if (values == null) {
                        anno.put("QC spectra", 
                                 values = new ArrayList<String>());
                    }
                    values.add((String)v.getValue());
                }
                else if (v.getId().equals("CLINICALTRIALS")) {
                    java.util.List<String> values = 
                        anno.get("ClinicalTrials.gov");
                    if (values == null) {
                        anno.put("ClinicalTrials.gov", 
                                 values = new ArrayList<String>());
                    }
                    values.add((String)v.getValue());
                }
                else if (v.getId().equals("CompoundDrugLabelRx")
                         || v.getId().equals("CompoundDrugLabelOtc")) {
                    java.util.List<String> values = 
                        anno.get("FDA DailyMed");
                    if (values == null) {
                        anno.put("FDA DailyMed", 
                                 values = new ArrayList<String>());
                    }
                    String[] sv = ((String)v.getValue()).split(";");
                    values.add(sv[1]+" "+sv[2]+", "+sv[4]);
                }
            }

            // second pass match them with the collection
            for (Value v : compound.getAnnotations()) {
                if (v.getId().equals("COLLECTION")) {
                    addNode (root, anno, (String)v.getValue());
                }
            }

	    reload();
	    this.compound = compound.getCompound();
	}

        void addNode (DefaultMutableTreeNode root, 
                      Map<String, java.util.List<String>> anno, 
                      String sv) {

            String[] child = sv.split("\\|");
            DefaultMutableTreeNode n;
            if (child.length > 0) {
                n = new DefaultMutableTreeNode (child[0]);
                for (int i = 1; i < child.length; ++i) {
                    String[] c = child[i].split(":");
                    if (c.length > 0) {
                        DefaultMutableTreeNode p = null;
                        for (Enumeration en = n.children();
                             en.hasMoreElements(); ) {
                            DefaultMutableTreeNode k = 
                                (DefaultMutableTreeNode)en.nextElement();
                            if (c[0].equals(k.getUserObject())) {
                                p = k;
                                break;
                            }
                        }
                        if (p == null) {
                            n.add(p = new DefaultMutableTreeNode
                                  (c[0]));
                        }

                        for (int j = 1; j < c.length; ++j) {
                            p.add(new DefaultMutableTreeNode (c[j]));
                        }
                    }
                    else {
                        n.add(new DefaultMutableTreeNode (child[i]));
                    }
                }
            }
            else {
                n = new DefaultMutableTreeNode (sv);
            }

            java.util.List<String> vals = anno.get((String)n.getUserObject());
            if (vals != null) {
                for (String v : vals) {
                    n.add(new DefaultMutableTreeNode (v));
                }
            }

            if ("Community curation".equals(n.getUserObject())) {
                root.insert(n, 0);
            }
            else if ("ChemIDPlus diffs".equals(n.getUserObject())) {
                // ignore this bogus collection
            }
            else {
                root.add(n);
            }
        }

	Compound getCompound () { return compound; }

	void clear () {
	    compound = null;
	    ((DefaultMutableTreeNode)getRoot()).removeAllChildren();
	    reload ();
	}	    
    }

    static class CollectionTreeCellRenderer extends DefaultTreeCellRenderer {

	static final Icon LEAF_ICON = IconFactory.Misc.Brightness.getIcon();
	static final Icon COLLECTION_ICON = IconFactory.Misc.Tag.getIcon();
	static final Icon STAR_ICON = IconFactory.Misc.Star.getIcon();
	static final Icon RAINBOW_ICON = IconFactory.Misc.Rainbow.getIcon();
        static final Icon PILL_ICON = IconFactory.Misc.Pill.getIcon();

	CollectionTreeCellRenderer () {
	    setLeafIcon (LEAF_ICON);
	    //setOpenIcon (IconFactory.Misc.ArrowMergeDown.getIcon());
	    //setClosedIcon (IconFactory.Misc.ArrowMergeLeft.getIcon());
            setOpenIcon (null);
            setClosedIcon (null);
	}

	@Override
	public Component getTreeCellRendererComponent
	    (JTree tree, Object value, boolean sel, boolean expanded, 
	     boolean isLeaf, int row, boolean hasFocus) {
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
	    if (value == null) {
	    }
	    else {
		value = node.getUserObject();
                String par = "";
                if (node.getParent() != null) {
                    par = (String)((DefaultMutableTreeNode)node
                                   .getParent()).getUserObject();
                    if (par == null) {
                        par = "";
                    }
                }

                Icon leaf = LEAF_ICON;
                if ("Community curation".equals(value)
                    || "Community curation".equals(par)) {
                    leaf = STAR_ICON;
                }
                else if ("QC spectra".equals(value) 
                         || "QC spectra".equals(par)) {
                    leaf = RAINBOW_ICON;
                }
                else if ("ClinicalTrials.gov".equals(value) 
                         || "ClinicalTrials.gov".equals(par)) {
                    leaf = IconFactory.Content.ClinicalTrial2.getIcon();
                }
                else if (value != null) {
                    String s = ((String)value).toLowerCase();
                    if (s.indexOf("approved") >= 0
                        || s.indexOf("fda") >= 0) {
                        //leaf = PILL_ICON;
                    }
                    else if (par.toLowerCase().indexOf("approved") >= 0
                             || par.toLowerCase().indexOf("fda") >= 0) {
                        leaf = PILL_ICON;
                    }
                }
                setLeafIcon (leaf);
	    }
	    return super.getTreeCellRendererComponent
		(tree, value, sel, expanded, isLeaf, row, hasFocus);
	}
    }
    
    class CollectionTree extends JTree implements TreeSelectionListener {
	CollectionTreeModel model = new CollectionTreeModel ();

	CollectionTree () {
	    setModel (model);
	    setRootVisible (false);
	    setCellRenderer (new CollectionTreeCellRenderer ());
	    /*
              getSelectionModel().setSelectionMode
              (TreeSelectionModel.SINGLE_TREE_SELECTION);
              addTreeSelectionListener (this);
	    */
	}

	public void setCompound (CompoundAdapter compound) {
	    model.setCompound(compound);
	    DefaultMutableTreeNode root = 
		(DefaultMutableTreeNode)model.getRoot();
	    for (Enumeration en = root.depthFirstEnumeration ();
		 en.hasMoreElements(); ) {
		DefaultMutableTreeNode leaf = 
		    (DefaultMutableTreeNode)en.nextElement();
		TreePath tp = new TreePath (leaf.getPath());
		expandPath (tp);
	    }
	}

	public void clear () {
	    clearSelection ();
	    model.clear();
	}

	public void valueChanged (TreeSelectionEvent e) {
	    if (model.getCompound() == null) {
		return;
	    }

	    TreePath path = getSelectionPath ();
	    if (path != null) {
		DefaultMutableTreeNode node = 
		    (DefaultMutableTreeNode)path.getLastPathComponent();
	    }
	}
    }


    class AnnotationsRecord extends UIRecordPanel<CompoundAdapter> {
        AnnotationTree<Compound> annoTree;

        AnnotationsRecord() {
            super(new BorderLayout(0, 5));
            annoTree = new AnnotationTree<Compound>();
            add(createScrollPane(annoTree));
        }

        public void clear() {
            annoTree.clear();
        }

        public void update(CompoundAdapter c) {
            setBusy(true);

            if (c == null) {
                clear();
                return;
            }
            annoTree.clear();
            // TODO right now we always retrieve annotations on an update - ideally we should check whether we already have them and then update the UI
            new AnnotationLoader(c, null, annoTree, null, this).execute();
        }
    }

    class IdentifierRecord extends UIRecordPanel<CompoundAdapter> {
        CollectionTree collectionTree;
        JList synList; // synonym list
        JList subList; // substance list
        JEditorPane nameEP;
        JEditorPane cidEP;

        IdentifierRecord() {
            super(new BorderLayout(0, 5));

            JPanel pane = new JPanel(new GridLayout(1, 2, 0, 0));
            pane.setOpaque(false);
            pane.add(nameEP = createEP("Name"));
            pane.add(cidEP = createEP("CID"));
            add(pane, BorderLayout.NORTH);
            add(createTab());

            setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        }

        JComponent createTab() {
            JideTabbedPane tab = new JideTabbedPane();
            tab.setTabShape(JideTabbedPane.SHAPE_BOX);
            tab.setTabPlacement(JTabbedPane.BOTTOM);
            tab.setOpaque(false);

            collectionTree = new CollectionTree();

            synList = createList();
            subList = createList();

            tab.addTab("Synonyms", createScrollPane(synList));
            tab.addTab("Collections", createScrollPane(collectionTree));
            tab.addTab("Substances", createScrollPane(subList));

            return tab;
        }

        public void clear() {
            nameEP.setText(null);
            nameEP.setToolTipText(null);
            cidEP.setText(null);
            collectionTree.clear();
            ((DefaultListModel) synList.getModel()).clear();
        }

        public void update(CompoundAdapter c) {
            setBusy(true);

            clear();
            if (c == null) {
                return;
            }

            Object id = c.getCompound().getId();
            if (id == null) id = "not available";
            String sb = UIUtil.CSS + "<a href=\"" + PC_URL1 + id + "\">" + id + "</a>";
            cidEP.setText(sb.toString());
            cidEP.setCaretPosition(0);

            titleLabel.setText(c.getCompound().getName()+" [ CID "+id.toString()+" ]");

            // annotations
            loadAnnotations(c);

            // substances
	    ((DefaultListModel)subList.getModel()).clear();
	    if (c.getPubChemCID() == null) {
		System.out.println("SDFS");
		return;
	    }
	    Long[] sids = c.getPubChemSIDs();
	    if (sids != null) {
		for (Long s : sids) {
		    ((DefaultListModel)subList.getModel()).addElement(s);
		}
	    }

            // synonyms
            loadSynonyms(c);
        }

        void loadAnnotations(CompoundAdapter c) {
            collectionTree.clear();
            nameEP.setText("");
            nameEP.setCaretPosition(0);

            // TODO right now we always retrieve annotations on an update - ideally we should check whether we already have them and then update the UI
            new AnnotationLoader(c, collectionTree, null, nameEP, this).execute();
        }

        void loadSubstances(CompoundAdapter c) {
            DefaultListModel model = (DefaultListModel) subList.getModel();
            model.clear();
//            Long[] sids = c.getPubChemSIDs((CompoundService)getContent().getService());
//            if (sids.length == 0) {
                new SIDLoader(c, model, this).execute();
//            } else {
//                for (Long sid : sids) model.addElement(sid);
//                setBusy(false);
//            }
        }

        void loadSynonyms(CompoundAdapter c) {
            DefaultListModel model = (DefaultListModel) synList.getModel();
            model.clear();

            String[] syns = c.getSynonyms();
            if (syns.length == 0) {
                new SynonymLoader(c, model, this).execute();
            } else {
                for (String s : syns) {
                    if (!model.contains(s))
                	model.addElement(s);
                }
                setBusy(false);
            }
        }
    }

    class TherapeuticsRecord extends UIRecordPanel<CompoundAdapter> {
        JEditorPane indicationEP, moaEP;

        TherapeuticsRecord() {
            super(new MigLayout
                    ("", "[grow,fill]",
                            "[min:50,grow 50,fill]unrel[min:50,grow 50,fill]"));
            add(createScrollPane("Indication", indicationEP = createEP()), "spanx,grow,wrap");
            add(createScrollPane("Mechanism of Action", moaEP = createEP()), "spanx,grow");
        }

        public void clear() {
            indicationEP.setText(null);
            moaEP.setText(null);
        }

        public void update(CompoundAdapter c) {
            clear();
            if (c == null || c.getCompound() == null) {
                return;
            }

            Collection<Value> anno = c.getCompound().getAnnotations();
            Value moa = null, ind = null;
            for (Value v : anno) {
                if (v.getId().equals("CompoundMOA")) {
                    moa = v;
                } else if (v.getId().equals("CompoundIndication")) {
                    ind = v;
                }
            }
            if (moa != null) {
                moaEP.setText(UIUtil.CSS + moa.getValue());
                moaEP.setCaretPosition(0);
            }
            if (ind != null) {
                indicationEP.setText(UIUtil.CSS + ind.getValue());
                indicationEP.setCaretPosition(0);
            }
        }
    }


    class BioactivitySummaryRecord extends UIRecordPanel<Compound> {
        JLabel label;
        JPanel recordPanel;
        JEditorPane ep;

        BioactivitySummaryRecord() {
            super(new BorderLayout(5,5));
            ep = createEP("Activity overview");
            GridLayout gridLayout = new GridLayout(2,2);
            gridLayout.setHgap(5);
            gridLayout.setVgap(5);
            recordPanel = new JPanel(gridLayout);
            add(recordPanel);
        }

        @Override
        public void clear() {
            recordPanel.removeAll();
            recordPanel.revalidate();
        }

        public void update(Compound c) {
            if (c == null){
                return;
            }
            clear();
            setBusy(true);
            new BioactivitySummaryLoader(c, this).execute();
        }

        public JLabel getLabel() {
            return label;
        }

        public JPanel getRecordPanel() {
            return recordPanel;
        }

        public JEditorPane getEditorPane() {
            return ep;
        }
    }

    class PropertiesRecord extends UIRecordPanel<CompoundAdapter>
	implements ActionListener {

	JTextField mw, mass, formula, tpsa, stereocenters, xlogp;
	JTextArea smi, iupac;
	JXHyperlink href;
	JXImagePanel img;
        

	PropertiesRecord () {
	    super (new MigLayout ("wrap", "[right][grow,fill]", ""));
            add (new JLabel ("IUPAC Name"), "top");
	    add (createSimpleScrollPane (iupac = new JTextArea ()), "grow");
	    iupac.setOpaque(false);
	    iupac.setRows(3);
	    iupac.setEditable(false);
	    iupac.setLineWrap(true);

	    add (new JLabel ("Molecular Weight"));
	    add (mw = field ());
	    add (new JLabel ("Exact Mass"));
	    add (mass = field ());
            add (new JLabel ("LogP"));
            add (xlogp = field ());
            add (new JLabel ("TPSA"));
            add (tpsa = field ());
	    add (new JLabel ("Molecular Formula"));
	    add (formula = field ());
	    add (new JLabel ("Stereocenters"));
	    add (stereocenters = field ());

	    add (new JLabel ("SMILES"), "top");
	    add (createSimpleScrollPane (smi = new JTextArea ()), "grow");
	    smi.setOpaque(false);
	    smi.setRows(3);
	    smi.setEditable(false);
	    smi.setLineWrap(true);

	    href = new JXHyperlink ();
	    href.setClickedColor(href.getUnclickedColor());
	    href.setText("Click for additional properties...");
	    href.addActionListener(this);
	    add (href, "spanx,grow");
	}

	public void clear () {
	    mw.setText(null);
	    mass.setText(null);
	    formula.setText(null);
	    smi.setText(null);
	    stereocenters.setText(null);
            iupac.setText(null);
            tpsa.setText(null);
            xlogp.setText(null);
            href.setToolTipText(null);
            href.putClientProperty("_target", null);
	}

	public void update (CompoundAdapter c) {
	    if (c == null) {
		clear ();
		return;
	    }

            Integer stereo = c.stereocenters();
            Integer def = c.definedStereo();
            if (stereo != null) {
                stereocenters.setText((def!= null ? def : 0)+"/"+stereo);
            }
            else {
                stereocenters.setText(null);
            }

            Value v = c.getCompound().getValue(Compound.IUPACNameValue);
            if (v != null) {
                String name = (String)v.getValue();
                iupac.setText(name);
                iupac.setCaretPosition(0);
                iupac.setToolTipText(name);
            }
            else {
                iupac.setToolTipText(null);
            }

            if (c.mwt() != null) {
                mw.setText(String.format("%1$.3f", c.mwt()));
            }
            if (c.exactMass() != null) {
                mass.setText(String.format("%1$.3f", c.exactMass()));
            }
            if (c.TPSA() != null) {
                tpsa.setText(String.format("%1$.1f", c.TPSA()));
            }
            if (c.logP() != null) {
                xlogp.setText(String.format("%1$.1f", c.logP()));
            }
            formula.setText(c.formula());
            smi.setText(c.getStructureSMILES());
            smi.setToolTipText(c.getStructureSMILES());

	    try {
                String smiles = smi.getText();
		String url = "http://www.chemicalize.org/?mol="
		    + java.net.URLEncoder.encode(smiles, "ISO-8859-1");
		href.setToolTipText("http://chemicalize.org/?mol="+smiles);
		href.putClientProperty("_target", url);
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Bogus URI constructed", ex);
	    }
	}

	public void actionPerformed (ActionEvent e) {
	    if (e.getSource() == href) {
		String url = (String)href.getClientProperty("_target");
		if (url != null) {
		    try {
			new BrowserLauncher().openURLinBrowser(url);
		    }
		    catch (Exception ex) {
			logger.log(Level.SEVERE, 
				   "Can't launch external browser " +url, ex);
		    }
		}
	    }
	}
    }

    class DrugLabelPane extends UIRecordPanel<CompoundAdapter> {
	JEditorPane ep = createEP ();
	String title;

	DrugLabelPane () {
	    super (new BorderLayout ());
	    add (ep);
	}

	DrugLabelPane (String label) {
	    super (new BorderLayout ());
	    add (ep);
	    setLabel (label);
	}

	/*
	 * a label has the following format
	 * 53808-0811;ZIAGEN;tablet, film coated;ABACAVIR SULFATE;300 mg;J220T4J9Q2 WR2TIP26VS
	 */
	public void setLabel (String label) {
	    String[] toks = label.split(";");

	    StringBuilder sb = new StringBuilder (HTML);
	    sb.append("<table>");
	    sb.append("<tr valign=\"top\">"
		      +"<td align=\"right\"><b>NDC</b></td><td>"
		      +"<a target=\"_tripod\" href=\"http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?ndc="+toks[0]+"\">"+toks[0]+"</a></td></tr>");
	    sb.append("<tr valign=\"top\"><td align=\"right\">"
		      +"<b>Active Ingredient</b></td><td>"+toks[3]+"</td></tr>");
	    sb.append("<tr valign=\"top\"><td align=\"right\">"
		      +"<b>Strength</b></td><td>"+toks[4]+"</td></tr>");
	    String[] uniis = toks[toks.length-1].split("[\\s]+");
	    sb.append("<tr valign=\"top\"><td align=\"right\">"
		      +"<b>UNII</b></td><td>");
	    for (String u : uniis) {
		sb.append("<a target=\"_tripod\" href=\"http://chem.sis.nlm.nih.gov/chemidplus/direct.jsp?name=UNII-"+u+"&result=advanced\">"+u+"</a>&nbsp;");
	    }
	    sb.append("</td></tr>");
	    sb.append("</table>");

	    ep.setText(sb.toString());
	    title = toks[1]+" "+toks[2] + ", "+toks[4];
	}

	public String getTitle () { return title; }
	public void clear () { ep.setText(null); }
	public void update (CompoundAdapter c) {}
    }

    private Grid grid; // main compound grid
    private Compound compound; // current compound
    private CompoundAdapter compoundAdapter;

    private java.util.List<UIRecord<CompoundAdapter>> forms;
    private BoundedRangeModel range;

    public CompoundRecordViewPanel (CompoundContent content) {
	super (content);

	if (!content.getEntities().isEmpty()) {
	    updateView (0);
//	} else {
//	    updateView(-1);
	}
    }

    public String getGridCellAnnotation (Grid g, Object v, int c) {
	if (v == null) {
	    return null;
	}
	Compound cmpd = (Compound)v;

	String name = cmpd.getName();
	if (name == null || name.length() == 0) {
	    //String[] syn = cmpd.getSynonyms();
	    //name = syn != null && syn.length > 0 ? syn[0] : null;
	}
	return name;
    }

    public String getGridCellBadgeAnnotation (Grid g, Object v, int c) {
        return null;
    }

    public void stateChanged (ChangeEvent e) {
    }
    
    @Override
    public BoundedRangeModel getBoundedRangeModel () { 
	if (range == null) {
	    range = new DefaultBoundedRangeModel (120, 0, 50, 400);
	    range.addChangeListener(this);
	}
	return range;
    }

    @Override
    protected void show (Entity e) {
	if (e == null) {
	    clear ();
	    return;
	}
	else if (!(e instanceof Compound)) {
	    throw new IllegalArgumentException
		("Entity is not a compound instance!");
	}
        compound = (Compound)e;
	compoundAdapter = new CompoundAdapter (compound, getContent().getServiceManager());

	grid.clear();
	grid.addValue(compound);
	for (UIRecord<CompoundAdapter> f : forms) {
	    f.update(compoundAdapter);
	}

        updateRelatedContent (e);
	//repaint ();
    }


    @Override
    protected void clear () {
	for (UIRecord<CompoundAdapter> f : getForms ()) {
	    f.clear();
	}

        JTabbedPane tab = getRelatedContentTab ();
        for (int i = 0; i < tab.getTabCount(); ++i) {
            String title = tab.getTitleAt(i);
            Component c = tab.getComponentAt(i);
            if (c instanceof UIRelatedRecordPanel) {
                UIRelatedRecordPanel form = 
                    (UIRelatedRecordPanel)c;
                form.clear();
                //tab.setTitleAt(i, form.getRelatedContent().getName());
            }
	}

	grid.clear();
    }

    // show related entities
    CompoundAdapter adapter = new CompoundAdapter();
    protected void updateRelatedContent (Entity entity) {
	JTabbedPane tab = getRelatedContentTab ();
	for (int i = 0; i < tab.getTabCount(); ++i) {
	    String title = tab.getTitleAt(i);
            Component comp = tab.getComponentAt(i);
            if (comp instanceof UIRecordPanel) {
                UIRecordPanel form = (UIRecordPanel)comp;
                try {
                    form.getClass().getMethod("update", Compound.class);
                    form.update(entity);
                } catch (Exception e) {
                    try {
                        form.getClass().getMethod("update", Entity.class);
                        form.update(entity);
                    } catch (Exception ex) {
                	adapter.setCompound((Compound)entity);
                	form.update(adapter);
                    }
                }
                /*
                tab.setTitleAt
                    (i, form.getRelatedContent().getName()
                     +" ("+form.getRelatedCount()+")");
                */
	    }
	}
    }

    @Override
    protected JComponent createContentPane() {

        // get structure depiction
        grid = createMolGrid();
        grid.setCellAnnotator(this);
        grid.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                JComponent c = (JComponent) e.getSource();
                Rectangle r = c.getBounds();
                grid.setCellSize
                        ((int) (Math.min(r.width, r.height) * .8 + 0.5));
            }
        });

        relatedContentTab = new JideTabbedPane();
        ((JideTabbedPane) relatedContentTab)
                .setTabShape(JideTabbedPane.SHAPE_BOX);
        initRelatedContentTab(relatedContentTab);

        // make a panel to contain the title for this record
        JPanel titlePanel = new JPanel(new BorderLayout());
        titleLabel = new JLabel();
        titlePanel.add(titleLabel, BorderLayout.CENTER);
        titlePanel.setBorder(new CompoundBorder(new SoftBevelBorder(BevelBorder.LOWERED), new EmptyBorder(5, 5, 5, 5)));

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(titlePanel, BorderLayout.NORTH);
        panel.add(relatedContentTab, BorderLayout.CENTER);

        JSplitPane split = createSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        split.setLeftComponent(grid);
        split.setRightComponent(panel);
        split.setResizeWeight(.25);

        return split;
    }


//    @Override
//    protected JComponent createView() {
//        JSplitPane split = createSplitPane(JSplitPane.VERTICAL_SPLIT);
//        split.setBackground(Color.white);
//        split.setBottomComponent(contentPane = createContentPane());
//        split.setTopComponent(createRelatedContentPane());
//        split.setDividerLocation(601);
//        return split;
//    }

    JComponent createContentComponent (Content c, Entity e) {
	return new JLabel (e.getClass().toString());
    }

    synchronized java.util.List<UIRecord<CompoundAdapter>> getForms () {
	if (forms == null) {
	    forms = new ArrayList<UIRecord<CompoundAdapter>>();
	}
	return forms;
    }


//    @Override
//    protected JComponent createContentPane() {
//        grid = createMolGrid();
//        grid.setCellAnnotator(this);
//        grid.addComponentListener(new ComponentAdapter() {
//            public void componentResized(ComponentEvent e) {
//                JComponent c = (JComponent) e.getSource();
//                Rectangle r = c.getBounds();
//                grid.setCellSize
//                        ((int) (Math.min(r.width, r.height) * .8 + 0.5));
//            }
//        });
//
//        //grid.setUseDefaultBackground(false);
//        return createCompoundInfoPane();
//    }


//    JComponent createCompoundInfoPane() {
//        //pane.setBackground(Color.white);
//
//        JideTabbedPane tab = new JideTabbedPane();
//        tab.setTabShape(JideTabbedPane.SHAPE_BOX);
//        //tab.setOpaque(false);
//
//        UIRecord<CompoundAdapter> form;
//        getForms().add(form = new IdentifierRecord());
//        tab.addTab("Identifiers", form.component());
//
//        //getForms().add(form = new RegulatoryRecord ());
//        //tab.addTab("Regulatory Status", form.component());
//
//        getForms().add(form = new TherapeuticsRecord());
//        tab.addTab("Therapeutics", form.component());
//
//        getForms().add(form = new PropertiesRecord());
//        tab.addTab("Properties", form.component());
//
//        getForms().add(form = new AnnotationsRecord());
//        tab.addTab("Annotations", form.component());
//        JXTaskPane taskPane = new JXTaskPane();
//        taskPane.setTitle("Detailed compound information");
//        taskPane.setCollapsed(true);
//        taskPane.add(tab);
//
////        JPanel pane = new JPanel(new BorderLayout());
//        JXTaskPaneContainer pane = new JXTaskPaneContainer();
//        pane.add(taskPane);
//        return pane;
//    }

    @Override
    protected void initRelatedContentTab(JTabbedPane tab) {
        super.initRelatedContentTab(tab);
        
        int size = getBoundedRangeModel().getValue();
        Content content = getContent();

        tab.addTab("Activity Summary", IconFactory.Misc.Sticky.getIcon(),
                new BioactivitySummaryRecord());

        tab.addTab("Assays", ContentManager.getIcon(Assay.class),
                new UIRelatedAssayRecordPanel(content));


        tab.addTab("Projects", ContentManager.getIcon(Project.class),
                new UIRelatedProjectRecordPanel(content));

        tab.addTab("Plug-ins", IconFactory.Misc.Sticky.getIcon(),
                new UIPluginsRecordPanel(content));

        UIRecord<CompoundAdapter> form;
        getForms().add(form = new IdentifierRecord());
        tab.addTab("Identifiers", form.component());

        //getForms().add(form = new RegulatoryRecord ());
        //tab.addTab("Regulatory Status", form.component());

        getForms().add(form = new TherapeuticsRecord());
        tab.addTab("Therapeutics", form.component());

        getForms().add(form = new PropertiesRecord());
        tab.addTab("Properties", form.component());

        getForms().add(form = new AnnotationsRecord());
        tab.addTab("Annotations", form.component());

    }
}
