package bard.ui.content;

import java.util.Collection;

import javax.swing.ListSelectionModel;

import bard.core.Entity;
import bard.core.SearchParams;
import bard.core.Value;
import ca.odell.glazedlists.EventList;

/**
 * represent the state of a content
 */
public interface State<T extends Entity> {
    // parent Content<T>
    Content<T> getContent ();
    SearchParams getQuery();

    /*
     * facets associated with this state
     */
    Collection<Value> getFacets ();

    /*
     * selection
     */
    ListSelectionModel getSelectionModel();
    
    /*
     * entity buffer
     */
    EventList<T> getEntities ();
    T contains (Object id);
    T getEntity (); // current entity
    void setEntity (T entity);
    int size ();
    public Value getETag();

    /*
     * return true if the next batch is available; otherwise false.
     */
    boolean fetch (int size); // fetch the next entity batch
    boolean fetch (); // using default size

    /*
     * cleanup 
     */
    void clean (); // clean up this state
}
