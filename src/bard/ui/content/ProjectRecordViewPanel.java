// $Id$

package bard.ui.content;

import bard.core.*;
import bard.core.adapter.AssayAdapter;
import bard.core.adapter.EntityAdapter;
import bard.core.adapter.ProjectAdapter;
import bard.ui.common.Grid;
import bard.ui.common.GridCellAnnotator;
import bard.ui.explodingpixels.macwidgets.MacFontUtils;
import bard.ui.icons.IconFactory;
import bard.ui.icons.IconFactory.CenterIcon;
import bard.ui.util.UIUtil;
import com.jidesoft.swing.JideTabbedPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ProjectRecordViewPanel extends RecordViewPanel implements GridCellAnnotator, ChangeListener {

    static private Logger logger = Logger.getLogger(ProjectRecordViewPanel.class.getName());

    static final Icon PLUS_SMALL_ICON = IconFactory.Misc.Plus16.getIcon();
    static final Icon MINUS_SMALL_ICON = IconFactory.Misc.Minus16.getIcon();

    static class FilterTreeModel extends DefaultTreeModel {
        Assay assay;

        FilterTreeModel() {
            super(new DefaultMutableTreeNode()); // empty root
        }

        void setFilters(Assay assay) {
            DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();
            root.removeAllChildren();
            this.assay = assay;
            reload();
        }

        Assay getAssay() {
            return assay;
        }

        void clear() {
            assay = null;
            ((DefaultMutableTreeNode) getRoot()).removeAllChildren();
            reload();
        }
    }

    static class FilterTreeCellRenderer extends DefaultTreeCellRenderer {

        static final Icon LEAF_ICON = IconFactory.Misc.Leaf.getIcon();
        static final Icon FILTER_ICON = IconFactory.Misc.Funnel.getIcon();

        FilterTreeCellRenderer() {
            setLeafIcon(LEAF_ICON);
            setOpenIcon(FILTER_ICON);
            setClosedIcon(FILTER_ICON);
        }

        @Override
        public Component getTreeCellRendererComponent
                (JTree tree, Object value, boolean sel, boolean expanded,
                 boolean leaf, int row, boolean hasFocus) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            if (value == null) {
            } else if (leaf) {
                Object obj = node.getUserObject();
                value = obj;
            }

            return super.getTreeCellRendererComponent
                    (tree, value, sel, expanded, leaf, row, hasFocus);
        }
    }


    // also the same as FilterTreeModel... refactor? sigh
    static class CollectionTreeModel extends DefaultTreeModel {
        Assay assay;

        CollectionTreeModel() {
            super(new DefaultMutableTreeNode());
        }

        void setAssay(AssayAdapter assay) {
            DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();
            root.removeAllChildren();

            Map<String, java.util.List<String>> anno =
                    new HashMap<String, java.util.List<String>>();

            // first pass grab all relevant annotations
            for (Value v : assay.getAnnotations()) {
                if (v.getId().equals("CompoundSpectra")) {
                    java.util.List<String> values = anno.get("QC spectra");
                    if (values == null) {
                        anno.put("QC spectra",
                                values = new ArrayList<String>());
                    }
                    values.add((String) v.getValue());
                } else if (v.getId().equals("CLINICALTRIALS")) {
                    java.util.List<String> values =
                            anno.get("ClinicalTrials.gov");
                    if (values == null) {
                        anno.put("ClinicalTrials.gov",
                                values = new ArrayList<String>());
                    }
                    values.add((String) v.getValue());
                } else if (v.getId().equals("CompoundDrugLabelRx")
                        || v.getId().equals("CompoundDrugLabelOtc")) {
                    java.util.List<String> values =
                            anno.get("FDA DailyMed");
                    if (values == null) {
                        anno.put("FDA DailyMed",
                                values = new ArrayList<String>());
                    }
                    String[] sv = ((String) v.getValue()).split(";");
                    values.add(sv[1] + " " + sv[2] + ", " + sv[4]);
                }
            }

            // second pass match them with the collection
            for (Value v : assay.getAnnotations()) {
                if (v.getId().equals("COLLECTION")) {
                    addNode(root, anno, (String) v.getValue());
                }
            }

            reload();
            this.assay = assay.getAssay();
        }

        void addNode(DefaultMutableTreeNode root,
                     Map<String, java.util.List<String>> anno,
                     String sv) {

            String[] child = sv.split("\\|");
            DefaultMutableTreeNode n;
            if (child.length > 0) {
                n = new DefaultMutableTreeNode(child[0]);
                for (int i = 1; i < child.length; ++i) {
                    String[] c = child[i].split(":");
                    if (c.length > 0) {
                        DefaultMutableTreeNode p = null;
                        for (Enumeration en = n.children();
                             en.hasMoreElements(); ) {
                            DefaultMutableTreeNode k =
                                    (DefaultMutableTreeNode) en.nextElement();
                            if (c[0].equals(k.getUserObject())) {
                                p = k;
                                break;
                            }
                        }
                        if (p == null) {
                            n.add(p = new DefaultMutableTreeNode
                                    (c[0]));
                        }

                        for (int j = 1; j < c.length; ++j) {
                            p.add(new DefaultMutableTreeNode(c[j]));
                        }
                    } else {
                        n.add(new DefaultMutableTreeNode(child[i]));
                    }
                }
            } else {
                n = new DefaultMutableTreeNode(sv);
            }

            java.util.List<String> vals = anno.get((String) n.getUserObject());
            if (vals != null) {
                for (String v : vals) {
                    n.add(new DefaultMutableTreeNode(v));
                }
            }

            if ("Community curation".equals(n.getUserObject())) {
                root.insert(n, 0);
            } else if ("ChemIDPlus diffs".equals(n.getUserObject())) {
                // ignore this bogus collection
            } else {
                root.add(n);
            }
        }

        Assay getAssay() {
            return assay;
        }

        void clear() {
            assay = null;
            ((DefaultMutableTreeNode) getRoot()).removeAllChildren();
            reload();
        }
    }

    static class CollectionTreeCellRenderer extends DefaultTreeCellRenderer {

        static final Icon LEAF_ICON = IconFactory.Misc.Brightness.getIcon();
        static final Icon COLLECTION_ICON = IconFactory.Misc.Tag.getIcon();
        static final Icon STAR_ICON = IconFactory.Misc.Star.getIcon();
        static final Icon RAINBOW_ICON = IconFactory.Misc.Rainbow.getIcon();
        static final Icon PILL_ICON = IconFactory.Misc.Pill.getIcon();

        CollectionTreeCellRenderer() {
            setLeafIcon(LEAF_ICON);
            //setOpenIcon (IconFactory.Misc.ArrowMergeDown.getIcon());
            //setClosedIcon (IconFactory.Misc.ArrowMergeLeft.getIcon());
            setOpenIcon(null);
            setClosedIcon(null);
        }

        @Override
        public Component getTreeCellRendererComponent
                (JTree tree, Object value, boolean sel, boolean expanded,
                 boolean isLeaf, int row, boolean hasFocus) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            if (value == null) {
            } else {
                value = node.getUserObject();
                String par = "";
                if (node.getParent() != null) {
                    par = (String) ((DefaultMutableTreeNode) node
                            .getParent()).getUserObject();
                    if (par == null) {
                        par = "";
                    }
                }

                Icon leaf = LEAF_ICON;
                if ("Community curation".equals(value)
                        || "Community curation".equals(par)) {
                    leaf = STAR_ICON;
                } else if ("QC spectra".equals(value)
                        || "QC spectra".equals(par)) {
                    leaf = RAINBOW_ICON;
                } else if ("ClinicalTrials.gov".equals(value)
                        || "ClinicalTrials.gov".equals(par)) {
                    leaf = IconFactory.Content.ClinicalTrial2.getIcon();
                } else if (value != null) {
                    String s = ((String) value).toLowerCase();
                    if (s.indexOf("approved") >= 0
                            || s.indexOf("fda") >= 0) {
                        //leaf = PILL_ICON;
                    } else if (par.toLowerCase().indexOf("approved") >= 0
                            || par.toLowerCase().indexOf("fda") >= 0) {
                        leaf = PILL_ICON;
                    }
                }
                setLeafIcon(leaf);
            }
            return super.getTreeCellRendererComponent
                    (tree, value, sel, expanded, isLeaf, row, hasFocus);
        }
    }

    class CollectionTree extends JTree implements TreeSelectionListener {
        CollectionTreeModel model = new CollectionTreeModel();

        CollectionTree() {
            setModel(model);
            setRootVisible(false);
            setCellRenderer(new CollectionTreeCellRenderer());
            /*
                 getSelectionModel().setSelectionMode
                 (TreeSelectionModel.SINGLE_TREE_SELECTION);
                 addTreeSelectionListener (this);
           */
        }

        public void setAssay(AssayAdapter assay) {
            model.setAssay(assay);
            DefaultMutableTreeNode root =
                    (DefaultMutableTreeNode) model.getRoot();
            for (Enumeration en = root.depthFirstEnumeration();
                 en.hasMoreElements(); ) {
                DefaultMutableTreeNode leaf =
                        (DefaultMutableTreeNode) en.nextElement();
                TreePath tp = new TreePath(leaf.getPath());
                expandPath(tp);
            }
        }

        public void clear() {
            clearSelection();
            model.clear();
        }

        public void valueChanged(TreeSelectionEvent e) {
            if (model.getAssay() == null) {
                return;
            }

            TreePath path = getSelectionPath();
            if (path != null) {
                DefaultMutableTreeNode node =
                        (DefaultMutableTreeNode) path.getLastPathComponent();
            }
        }
    }


    class IdentifierRecord extends UIRecordPanel<ProjectAdapter> {
        JEditorPane description, statistics;
        JEditorPane nameEP;
        JLabel sourceLabel;

        ChartPanel chartPanel = null;

        JideTabbedPane tab;

        IdentifierRecord() {
            super(new BorderLayout(0, 5));

            JPanel pane = new JPanel(new BorderLayout());
            pane.setOpaque(false);

            pane.add(nameEP = createEP("Name"), BorderLayout.CENTER);
            sourceLabel = new JLabel();
            sourceLabel.setOpaque(false);
            sourceLabel.setBorder(BorderFactory.createTitledBorder
                    (BORDER, "Source", TitledBorder.LEADING,
                            TitledBorder.DEFAULT_POSITION,
                            MacFontUtils.ITUNES_TABLE_HEADER_FONT));
            pane.add(sourceLabel, BorderLayout.LINE_START);
            add(pane, BorderLayout.NORTH);
            add(createTab());

            setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        }

        JComponent createTab() {
            tab = new JideTabbedPane();
            tab.setTabShape(JideTabbedPane.SHAPE_BOX);
            tab.setTabPlacement(JTabbedPane.BOTTOM);
            tab.setOpaque(false);

            description = createEP("Description");
            tab.addTab("Description", createScrollPane(description));

            statistics = createEP("Statistics");
            tab.addTab("Statistics", createScrollPane(statistics));

            chartPanel = new ChartPanel(null);
            tab.addTab("Experiments", createScrollPane(chartPanel));

            return tab;
        }

        public void clear() {
            nameEP.setText(null);
            nameEP.setToolTipText(null);
            sourceLabel.setText(null);
            sourceLabel.setIcon(null);
            description.setText(null);
            statistics.setText(null);
        }

        public void update(ProjectAdapter a) {
            setBusy(true);

            if (a == null) {
                clear();
                return;
            }

            StringBuilder sb = new StringBuilder(UIUtil.CSS);
            sb.append(a.getProject().getName());
            nameEP.setText(sb.toString());
            nameEP.setCaretPosition(0);

            sb = new StringBuilder(a.getProject().getValue(Project.SourceValue).getValue().toString());
            ImageIcon icon = null;
            for (IconFactory.CenterIcon center : IconFactory.CenterIcon.values()) {
                if (center.getCenter().equals(sb.toString()) || center.getFullName().equals(sb.toString())) {
                    icon = center.getIcon();
		    sb = new StringBuilder(center.getFullName());
		    break;
                }
            }
            if (icon != null) {
                sourceLabel.setIcon(icon);
                sourceLabel.setText(null);
                sourceLabel.setToolTipText(sb.toString());
                if (icon.getIconWidth() < 50)
                    sourceLabel.setPreferredSize(new Dimension(50, 50));
                else sourceLabel.setPreferredSize(null);
                //sourceLabel.validate();
            } else {
                sourceLabel.setIcon(null);
                sourceLabel.setText(sb.toString());
                sourceLabel.setToolTipText(sb.toString());
                sourceLabel.setPreferredSize(null);
                //sourceLabel.validate();
            }

            String desc = a.getProject().getDescription();
            desc = desc.replaceAll("\n", "<br>");
            desc = desc.replaceAll("<br><br>", "<P>");
            description.setText(UIUtil.CSS+desc);
            description.setCaretPosition(0);

            Map<String, Object> projectSummary = a.getEntity().getProjectSummary();

            sb = new StringBuilder(UIUtil.CSS);
            sb.append("<table>");
            sb.append("<tr><td><b>Assay Count</b></td><td>").append(projectSummary.get("assay_count")).append("</td></tr>");
            sb.append("<tr><td><b>Experiment Count</b></td><td>").append(projectSummary.get("assay_count")).append("</td></tr>");
            sb.append("<tr><td><b>Purchased compounds</b></td><td>").append(projectSummary.get("cmpd_purchase_count")).append("</td></tr>");
            sb.append("<tr><td><b>Synthesized compounds</b></td><td>").append(projectSummary.get("cmpd_synthesis_count")).append("</td></tr>");
            sb.append("</table");

            statistics.setText(sb.toString());
            statistics.setCaretPosition(0);

            DefaultPieDataset pieData = new DefaultPieDataset();
            Map<String, Integer> ecls = (Map<String, Integer>) projectSummary.get("experiment_class");
            if (ecls != null)
        	for (String key : ecls.keySet()) {
        	    int value = ecls.get(key);
        	    pieData.setValue(key+" = "+value, new Double(value));
        	}
            JFreeChart chart = ChartFactory.createPieChart(null, pieData, true, true, false);
            PiePlot plot = (PiePlot) chart.getPlot();
            plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
            plot.setCircular(false);
            plot.setLabelGap(0.02);
            chartPanel.setChart(chart);
            chartPanel.repaint();

            setBusy(false);
        }

    }


    //    private Grid grid; // main project grid  !!! update with my own project component
    private Project project; // current project
    private ProjectAdapter projectAdapter;

    private java.util.List<UIRecord<ProjectAdapter>> forms;
    private JList annoList; // annotations list
    private BoundedRangeModel range;

    public ProjectRecordViewPanel(ProjectContent content) {
        super(content);
        project  = content.getEntity();
        if (project != null) { // && !content.getEntities().isEmpty()) { !!!
            updateView(0);
//        } else {
//            updateView(-1);
        }
    }

    public String getGridCellAnnotation(Grid g, Object v, int c) {
        if (v == null) {
            return null;
        }
        Project project = (Project) v;

        String name = project.getName();
        if (name == null || name.length() == 0) {
            //String[] syn = project.getSynonyms();
            //name = syn != null && syn.length > 0 ? syn[0] : null;
        }
        return name;
    }

    public String getGridCellBadgeAnnotation(Grid g, Object v, int c) {
        return null;
    }

    public void stateChanged(ChangeEvent e) {
    }

    @Override
    public BoundedRangeModel getBoundedRangeModel() {
        if (range == null) {
            range = new DefaultBoundedRangeModel(120, 0, 50, 400);
            range.addChangeListener(this);
        }
        return range;
    }

    @Override
    protected void show(Entity e) {
        if (e == null) {
            clear();
            return;
        } else if (!(e instanceof Project)) {
            throw new IllegalArgumentException
                    ("Entity is not a project instance!");
        }
        project = (Project) e;
        projectAdapter = new ProjectAdapter(project);

//	grid.clear();
//	grid.addValue(project);
        for (UIRecord<ProjectAdapter> f : forms) {
            f.update(projectAdapter);
        }

        try {
            updateRelatedContent(projectAdapter);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Can't show related contents", ex);
        }
        //repaint ();
    }


    @Override
    protected void clear() {
        for (UIRecord<ProjectAdapter> f : getForms()) {
            f.clear();
        }

        JTabbedPane tab = getRelatedContentTab();
        for (int i = 0; i < tab.getTabCount(); ++i) {
            String title = tab.getTitleAt(i);
            Component c = tab.getComponentAt(i);
            if (c instanceof UIRelatedRecordPanel) {
                UIRelatedRecordPanel form =
                        (UIRelatedRecordPanel) c;
                form.clear();
                //tab.setTitleAt(i, form.getRelatedContent().getName());
            }
        }

//	grid.clear();
    }

    // show related entities
    protected void updateRelatedContent(EntityAdapter entityAdapter) throws Exception {
        logger.info("Updating related content for " + ((ProjectAdapter)entityAdapter).getProject().getId());
        //logger.info(getContent().getName()+" "+entity.getId());
        //EntityRegistry reg = getContent().getRegistry();


        Project project = ((ProjectAdapter) entityAdapter).getEntity();
        JTabbedPane tab = getRelatedContentTab();
        for (int i = 0; i < tab.getTabCount(); ++i) {
            String title = tab.getTitleAt(i);
            Component comp = tab.getComponentAt(i);
            if (comp instanceof UIRelatedRecordPanel) {
                UIRelatedRecordPanel form = (UIRelatedRecordPanel) comp;
                form.update(entityAdapter.getEntity());
            } else if ("Annotations".equals(title)) {
                DefaultListModel model = (DefaultListModel)annoList.getModel();
                model.clear();
                // make sure annotations have been loaded
                project.getAnnotations();
                ArrayList<String> prefix = new ArrayList<String>();
                for (Value v: project.getValues()) {
                    recurseAnnotations(model, prefix, v);
                }
            }
        }
    }

    JComponent createContentComponent(Content c, Entity e) {
        return new JLabel(e.getClass().toString());
    }

    synchronized java.util.List<UIRecord<ProjectAdapter>> getForms() {
        if (forms == null) {
            forms = new ArrayList<UIRecord<ProjectAdapter>>();
        }
        return forms;
    }


    @Override
    protected JComponent createContentPane() {
//	grid = createMolGrid ();
//	grid.setCellAnnotator(this);
//	grid.addComponentListener(new ComponentAdapter () {
//		public void componentResized (ComponentEvent e) {
//		    JComponent c = (JComponent)e.getSource();
//		    Rectangle r = c.getBounds();
//		    grid.setCellSize
//			((int)(Math.min(r.width, r.height)*.8+0.5));
//		}
//	    });
//
//	//grid.setUseDefaultBackground(false);

        JSplitPane split = createSplitPane(JSplitPane.HORIZONTAL_SPLIT);
//	split.setLeftComponent(grid);
        split.setRightComponent(createProjectInfoPane());
        split.setResizeWeight(.4);

        return split;
    }


    JComponent createProjectInfoPane() {
        JPanel pane = new JPanel(new BorderLayout());
        //pane.setBackground(Color.white);

        JideTabbedPane tab = new JideTabbedPane();
        tab.setTabShape(JideTabbedPane.SHAPE_BOX);
        //tab.setOpaque(false);

        UIRecord<ProjectAdapter> form;
        getForms().add(form = new IdentifierRecord());
        tab.addTab("Summary", form.component());

        //getForms().add(form = new RegulatoryRecord ());
        //tab.addTab("Regulatory Status", form.component());

        //getForms().add(form = new TherapeuticsRecord ());
        //tab.addTab("Therapeutics", form.component());

        //getForms().add(form = new PropertiesRecord ());
        //tab.addTab("Properties", form.component());

        initRelatedContentTab (tab);
        
        pane.add(tab);
        return pane;
    }

    @Override
    protected void initRelatedContentTab(JTabbedPane tab) {
        super.initRelatedContentTab(tab);

        int size = getBoundedRangeModel().getValue();

        createAssayTab(tab);
        createExperimentTab(tab);
//        createBiologyTab(tab);
        createProbeTab(tab);
        createAnnotationTab(tab);
    }

    void createAssayTab(JTabbedPane tab) {
        tab.addTab("Assays", ContentManager.getIcon(Assay.class),
                   new UIRelatedAssayRecordPanel (getContent()));
    }

    void createExperimentTab(JTabbedPane tab) {
        tab.addTab("Experiments", ContentManager.getIcon(Experiment.class),
                   new UIRelatedExperimentRecordPanel(getContent()));
    }


    void createProbeTab(JTabbedPane tab) {
        tab.addTab("Probes", ContentManager.getIcon(Compound.class),
                   new UIRelatedCompoundRecordPanel(getContent()));
    }


    void createBiologyTab (JTabbedPane tab) {
        Content<Biology> content = getContent().getContentManager().getContent(Biology.class);
        Component pane = new UIRelatedBiologyRecordPanel(content);
        tab.addTab(content.getName(), content.getIcon(), pane);
    }

    void createAnnotationTab(JTabbedPane tab) {
        //JPanel pane = new JPanel (new BorderLayout ());
        annoList = createList();
        annoList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting())
                    return;

                String entry = (String) annoList.getSelectedValue();
                if (entry != null && entry.indexOf("http://") > -1) {
                    entry = entry.substring(entry.indexOf("http://"));
                    try {
                        URL url = new URL(entry.split("\\s")[0]);
                        hyperlinkUpdate(new HyperlinkEvent(entry, HyperlinkEvent.EventType.ACTIVATED, url));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            }
        });

        tab.addTab("Annotations", IconFactory.Misc.Sticky.getIcon(), createScrollPane(annoList));
    }
}
