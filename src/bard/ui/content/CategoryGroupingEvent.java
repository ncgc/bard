// $Id$
package bard.ui.content;

import java.util.EventObject;

public class CategoryGroupingEvent extends EventObject {
    private boolean ungroup;
    private String group;
    private CategoryView category;

    public CategoryGroupingEvent (CategoryGroupingPanel source, 
				  boolean ungroup) {
	super (source);
	this.ungroup = ungroup;
    }

    public CategoryGroupingEvent (CategoryGroupingPanel source,
				  String group, CategoryView category) {
	super (source);
	this.group = group;
	this.category = category;
    }

    public boolean isUngroup () { return ungroup; }
    public CategoryView getCategory () { return category; }
    public String getGroup () { return group; }
}
