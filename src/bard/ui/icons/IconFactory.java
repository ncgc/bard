// $Id$

package bard.ui.icons;

import javax.swing.ImageIcon;
import java.awt.Image;

public class IconFactory {
    /**
     * view icons
     */
    public enum View {
        List ("NSListViewTemplate.png"),
            Grid ("NSIconViewTemplate.png"),
            Record ("NSFlowViewTemplate.png"),
            Column ("NSColumnViewTemplate.png");

        final ImageIcon icon;
        View (String name) {
            icon = getImageIcon (name);
        }
        public ImageIcon getIcon () { return icon; }
        public ImageIcon getIcon (int size) { return scale (icon, size); }
    }

    /**
     * content icons
     */
    public enum Content {
        Compound ("pccompound.gif"),
            Substance ("pcsubstance.gif"),
            Fragment ("pcbioactivity.gif"),
            Assay ("pcbioassay.gif"),
            Target ("Structure.gif"),
            Document ("PubMedCentral.gif"),
            Drug ("pcdrug.gif"),
            Gene ("Gene.gif"),
            Pathway ("biosystems2.png"),
            Disease ("gensat.gif"),
            ClinicalTrial ("gap.gif"),
            Project ("block.png"),
            ClinicalTrial2 ("clinicalTrial_sponsors.png")
            ;

        final ImageIcon icon;
        Content (String name) {
            icon = getImageIcon (name);
        }
        public ImageIcon getIcon () { return icon; }
        public ImageIcon getIcon (int size) { return scale (icon, size); }
    }

    /**
     * status icons
     */
    public enum Status {
        Available ("NSStatusAvailable.png"),
            None ("NSStatusNone.png"),
            Partial ("NSStatusPartiallyAvailable.png"),
            Unavailable ("NSStatusUnavailable.png");

        final ImageIcon icon;
        Status (String name) {
            icon = getImageIcon (name);
        }
        public ImageIcon getIcon () { return icon; }
        public ImageIcon getIcon (int size) { return scale (icon, size); }
    }

    public enum Toolbar {
        Refresh ("arrow-circle-double.png"),
            Backward ("arrow24-180.png"),
            Forward ("arrow24.png"),
            Find ("find.png"),
            Search ("magnifier-left24.png")
            ;
        final ImageIcon icon;
        Toolbar (String name) {
            icon = getImageIcon (name);
        }
        public ImageIcon getIcon () { return icon; }
        public ImageIcon getIcon (int size) { return scale (icon, size); }
    }

    /**
     * app icons
     */
    public enum App {
        Home ("home.png"),
            Search ("search.png"),
            Search2 ("magnifier-left.png"),
            Clear ("clear.png"),
            Back ("arrow-curve-180-left.png"),
            Forward ("arrow-curve.png"),
            Balloon ("balloon.png"),
            Reload ("bin24.png"),
            SmallLeftTriangle ("NSRightFacingTriangleTemplate.png"),
            SmallRightTriangle ("NSLeftFacingTriangleTemplate.png"),

            Run ("application-run.png"),
            Err ("application--exclamation.png"),
            OK ("tick.png");

        final ImageIcon icon;
        App (String name) {
            icon = getImageIcon (name);
        }
        public ImageIcon getIcon () { return icon; }
        public ImageIcon getIcon (int size) { return scale (icon, size); }
    }

    public enum Misc {
        Forward ("arrow-curve.png"),
            Backward ("arrow-curve-180-left.png"),
            StarEmpty ("star-empty.png"),
            Star ("star.png"),
            Leaf ("leaf.png"),
            Funnel ("funnel.png"),
            Tick ("tick.png"),
            Tag ("tag.png"),
            TagPlus ("tag--plus.png"),
            Plus16 ("plus-small.png"),
            Minus16 ("minus-small.png"),
            Minus ("minus.png"),
            Flask ("flask.png"),
            DBImport ("database-import.png"),
            DBExport ("database-export.png"),
            Flag ("flag.png"),
            Folder ("blue-folder.png"),
            Pill ("pill.png"),
            Sticky ("sticky-notes-text.png"),
            Rainbow ("rainbow.png"),
            ArrowMergeLeft ("arrow-merge-000-left.png"),
            ArrowMergeDown ("arrow-merge-270.png"),
            Brightness ("brightness-small-low.png")
            ;

        final ImageIcon icon;
        Misc (String name) {
            icon = getImageIcon (name);
        }
        public ImageIcon getIcon () { return icon; }
        public ImageIcon getIcon (int size) { return scale (icon, size); }
    }

    /**
     * splash
     */
    public enum Splash {
        BARD ("BARD_logo_small.png");
        
        final ImageIcon icon;
        Splash (String name) {
            //icon = getImageIcon (name);
            icon = new ImageIcon (IconFactory.class.getResource
                    ("/bard/ui/main/resources/"+name));
        }
        public ImageIcon getIcon () { return icon; }
        public ImageIcon getIcon (int size) { return scale (icon, size); }
    }

    public static enum CenterIcon {
        Broad ("Broad Institute", "centers/Broad.png", 94, 24),
        Burnham ("Burnham Center for Chemical Genomics", "Sanford-Burnham Center for Chemical Genomics", "centers/Burnham.png", 135, 29),
        JHICC ("Johns Hopkins Ion Channel Center", "centers/JHICC.png", 28, 28),
        NCGC ("NCGC", "NIH Chemical Genomics Center", "centers/NCGC.png", 61, 30),
        Scripps ("The Scripps Research Institute Molecular Screening Center", "centers/Scripps.png", 32, 32),
        UNM ("NMMLSC", "New Mexico Molecular Libraries Screening Center", "centers/UNM.png", 92, 25),
        Vanderbilt ("Vanderbilt Screening Center for GPCRs, Ion Channels and Transporters", "centers/Vanderbilt.png", 99, 30),
        Vanderbilt2 ("Vanderbilt Specialized Chemistry Center", "centers/Vanderbilt.png", 99, 30),
        PCMD ("PCMD", "The Penn Center for Molecular Discovery", "centers/PCMD.png", 25, 25);
    
        private String center;
        private String fullName;
        private String url;
        private int size1, size2;
        private ImageIcon icon;
        static private int height = 38;
        CenterIcon(String center, String url, int size1, int size2) {
            this.center = center;
            this.fullName = center;
            this.url = url;
            this.size1 = size1;
            this.size2 = size2;
            updateIcon(38);
        }
        CenterIcon(String center, String fullName, String url, int size1, int size2) {
            this.center = center;
            this.fullName = fullName;
            this.url = url;
            this.size1 = size1;
            this.size2 = size2;
            updateIcon(38);
        }
        public String getCenter() {return center;}
        public String getFullName() {return fullName;}
        public ImageIcon getIcon() {return icon;}
        public ImageIcon updateIcon(int height) {
            this.icon = getImageIcon(url, height/38*size1, height/38*size2);
            return this.icon;
        }
        static public void checkHeight(int rowHeight) {
            if (rowHeight != height && rowHeight > 37) {
        	height = rowHeight;
        	for (CenterIcon center: CenterIcon.values()) {
        	    center.updateIcon(height);
        	}
            }
            
        }
    }

    static public ImageIcon getImageIcon (String name) {
        return getImageIcon (name, 0);
    }

    static public ImageIcon getImageIcon (String name, int size) {
        ImageIcon icon = new ImageIcon (IconFactory.class.getResource
                                        ("resources/"+name));
        if (size > 0) {
            return scale (icon, size);
        }
        return icon;
    }
    
    static public ImageIcon getImageIcon (String name, int size1, int size2) {
        ImageIcon icon = new ImageIcon (IconFactory.class.getResource
                                        ("resources/"+name));

        return new ImageIcon (icon.getImage().getScaledInstance
                (size1, size2, Image.SCALE_SMOOTH));
    }
    
    static public ImageIcon scale (ImageIcon icon, int size) {
        if (size > 0) {
            return new ImageIcon (icon.getImage().getScaledInstance
                                  (size, size, Image.SCALE_SMOOTH));
        }
        return icon;
    }
}
