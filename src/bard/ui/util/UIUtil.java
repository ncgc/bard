// $Id$

package bard.ui.util;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.io.IOException;
import java.net.URL;
import java.net.URI;
import java.awt.*;
import java.awt.geom.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.AttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.border.Border;

import com.jgoodies.looks.plastic.PlasticFileChooserUI;
import org.jdesktop.swingx.painter.BusyPainter;

import bard.ui.explodingpixels.macwidgets.IAppWidgetFactory;
import bard.ui.icons.IconFactory;
import bard.core.*;

import bard.ui.content.View;
import bard.ui.content.Content;

/**
 * UI-related utilities
 */
public class UIUtil {
    private static final Logger logger = 
        Logger.getLogger(UIUtil.class.getName());

    public static final Border EMPTY_BORDER =
        BorderFactory.createEmptyBorder(2,2,2,2);

    public static final Border BORDER = 
	BorderFactory.createLineBorder(new Color (0xa5a5a5));

    public static final String CSS = 
	"<html><body style=\"font: 10px 'Lucida Grande',"
	+"Verdana, Helvetica, Arial, Geneva, sans-serif; color: #333;\">";

    private static LookAndFeel systemLnF;
    private static JFileChooser fileChooser;

    private UIUtil () {}

    public static ImageIcon createImageIcon (String name) {
	return new ImageIcon (UIUtil.class.getResource(name));
    }

    // center component c on screen
    public static void centerComponent (Component c) {
	Dimension size = c.getSize();
	Dimension screenSize = c.getToolkit().getScreenSize();
        c.setLocation((screenSize.width  - size.width) / 2,
		      (int) ((screenSize.height - size.height) / 2));
    }

    // center component child respect to parent component
    public static void centerComponent (Component parent, Component child) {
	Dimension s0 = parent.getSize();
	Dimension s1 = child.getSize();
	child.setLocation(parent.getX() + (s0.width - s1.width)/2, 
			  parent.getY() + (s0.height - s1.height)/2);
    }

    public static void setupSplashScreen (URL imageURL) throws IOException {
	final SplashScreen splash = SplashScreen.getSplashScreen();
	if (splash == null) {
	    return;
	}
	splash.setImageURL(imageURL);
	
	final Graphics2D g = splash.createGraphics();
	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
			   RenderingHints.VALUE_ANTIALIAS_ON);
	g.setRenderingHint(RenderingHints.KEY_RENDERING, 
			   RenderingHints.VALUE_RENDER_QUALITY);
	
	final int size = 21;
	Rectangle r = splash.getBounds();
	g.translate(r.width - size - 5, r.height - size - 5);
	
	final BusyPainter painter = new BusyPainter
	    (new Rectangle2D.Float(0,0,5.5f,2.5f),
	     new Ellipse2D.Float(3.0f,3.0f,15.0f,15.0f));
	painter.setTrailLength(4);
	painter.setPoints(9);
	painter.setFrame(0);
	painter.setAntialiasing(true);
	
	final java.util.Timer timer = new java.util.Timer();
	timer.scheduleAtFixedRate
	    (new TimerTask () {
		    public void run () {
			if (splash.isVisible()) {
			    painter.setFrame
				((painter.getFrame()+1)%painter.getPoints());
			    painter.paint(g, null, size, size);
			    splash.update();
			}
			else {
			    timer.cancel();
			}
		    }
		}, 0, 100);
    }

    public static JScrollPane iTunesScrollPane (Component c) {
	return iTunesScrollPane (c, true, false);
    }

    public static JScrollPane iTunesScrollPane
	(Component c, boolean horizontalScrollbar, boolean border) {
	JScrollPane sp = new JScrollPane (c);
	IAppWidgetFactory.makeIAppScrollPane(sp);
	if (border) {
	    //sp.setBorder(BorderFactory.createEmptyBorder());
	    sp.setBorder(BORDER);
	}
	if (!horizontalScrollbar) {
	    sp.setHorizontalScrollBarPolicy
		(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);	    
	}
	return sp;
    }

    public static class BusyPane 
	extends JComponent implements ActionListener {

	javax.swing.Timer timer;
	BusyPainter painter;
	int frame = 0;
	AffineTransform txn = new AffineTransform ();
	int size = 14;
        int count;

	public BusyPane () {
	    setPreferredSize (new Dimension (size+2, size+2));

	    painter = new BusyPainter
		(new RoundRectangle2D.Float(0.f, 0.f,5.f, 2.f, 4.f, 4.f),
		 new Ellipse2D.Float(2.f,2.f,10.0f,10.0f));
	    painter.setTrailLength(6);
	    painter.setPoints(9);
	    painter.setFrame(0);

	    timer = new javax.swing.Timer (125, this);
	}

	public void start () { 
            if (!timer.isRunning()) {
                timer.start();
            }
            ++count;
	}
	public void stop () { 
            --count;
            if (timer.isRunning()) {
                if (count <= 0) {
                    timer.stop();
                    repaint ();
                }
            }
	}

	public boolean isBusy () { return timer.isRunning(); }

	@Override
	protected void paintComponent (Graphics g) {
	    if (isBusy ()) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle r = getBounds ();

                g2.translate((r.width-size)/2, (r.height-size)/2);
                painter.paint(g2, this, size, size);
	    }
	}

	public void actionPerformed (ActionEvent e) {
	    frame = (frame+1) % 8;
	    painter.setFrame(frame);
	    repaint ();
	}
    }

    // bard://compounds/2722[?view=(Record|Grid|List)]
    public static class BARDUri {
        String content;
        View.Type view;
        String[] argv;
        URI uri;

        public BARDUri (URI uri) {
            if (!uri.getScheme().equals("bard")) {
                throw new IllegalArgumentException ("Bogus BARD URI "+uri);
            }
            this.uri = uri;
            content = uri.getHost();

            String query = uri.getQuery();
            if (query == null)
        	view = View.Type.List;
            else
        	for (String q : query.split("&")) {
        	    String[] p = q.split("=");
        	    if (p.length == 2) {
        		if (p[0].equalsIgnoreCase("view")) {
        		    view = View.Type.valueOf(p[1]);
        		}
        	    }
        	    else {
        		logger.warning("Parameter \""+q+"\" not parsed!");
        	    }
        	}

            java.util.List<String> a = new ArrayList<String>();
            for (String p : uri.getPath().split("/")) {
                if (p != null) {
                    a.add(p);
                }
            }
            argv = a.toArray(new String[0]);
        }

        public <E extends Entity> boolean check (Content<E> c) {
            return UIUtil.getUriName
                (c.getEntityClass()).equalsIgnoreCase(content);
        }

        public String content () { return content; }
        public View.Type view () { return view; }
        public String[] argv () { return argv; }

        public String toString () {
            return getClass()+"{content="+content+",view="
                +view+",argv="+argv.length+",uri="+uri+"}";
        }
    }


    public static BusyPane createBusyPane () {
	return new BusyPane ();
    }

    public static Icon getIcon (Class<? extends Entity> clazz) {
        return getIcon (clazz, 0);
    }

    public static Icon getIcon (Class<? extends Entity> clazz, int size) {
        if (Compound.class.isAssignableFrom(clazz)) {
            return IconFactory.Content.Compound.getIcon(size);
        }
        if (Assay.class.isAssignableFrom(clazz)) {
            return IconFactory.Content.Assay.getIcon(size);
        }
        if (Project.class.isAssignableFrom(clazz)) {
            return IconFactory.Content.Project.getIcon(size);
        }
        return null;
    }

    public static String getUriName (Class<? extends Entity> clazz) {
        return clazz.getSimpleName().toLowerCase()+"s";
    }

    public static LookAndFeel getSystemLookAndFeel () {
        if (systemLnF == null) {
            try {
                String os = System.getProperty("os.name");
                if (os.startsWith("Mac")) {
                    // use default l&f for Mac OSX
                    System.setProperty("apple.laf.useScreenMenuBar", "true");
                    
                    Class clz = Class.forName
                        (UIManager.getSystemLookAndFeelClassName(), 
                         true, Thread.currentThread().getContextClassLoader());
                    
                    systemLnF = (LookAndFeel)clz.newInstance();
                }
                
                UIManager.setLookAndFeel
                    ("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
            }
            catch (Exception ex) {
                logger.log(Level.SEVERE, "Can't set l&f", ex);
            }
        }
        return systemLnF;
    }

    public static java.net.URI getLinkURI (HyperlinkEvent e) {
        String uri = "";
        if (e.getURL() != null)
            uri = e.getURL().toString();
        if (e.getSourceElement() != null) {
            AttributeSet attrs = (AttributeSet)e.getSourceElement()
                    .getAttributes().getAttribute(HTML.Tag.A);
            uri = (String)attrs.getAttribute(HTML.Attribute.HREF);
        }
        try {
            return new java.net.URI(uri);
        }
        catch (Exception ex) {
            throw new IllegalArgumentException ("Not a valid URI: "+uri);
        }
    }

    public static JFileChooser getFileChooser () {
        if (fileChooser == null) {
            fileChooser = new JFileChooser ();
            new PlasticFileChooserUI (fileChooser);
            fileChooser.updateUI();
        }

        return fileChooser;
    }

    public static void main (String[] argv) throws Exception {
	JFrame frame = new JFrame ();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.pack();
	frame.setSize(800, 600);
	frame.setVisible(true);
    }
}
