package bard.ui.explodingpixels.data;

public interface RatingChangeListener {

    void ratingChanged(Rating newRating);

}
