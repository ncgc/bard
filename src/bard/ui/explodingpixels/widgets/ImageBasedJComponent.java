package bard.ui.explodingpixels.widgets;

import bard.ui.explodingpixels.painter.ImagePainter;
import bard.ui.explodingpixels.swingx.EPPanel;

import java.awt.Dimension;
import java.awt.Image;

public class ImageBasedJComponent extends EPPanel {

    private final ImagePainter fPainter;

    public ImageBasedJComponent(Image image) {
        fPainter = new ImagePainter(image);
        setBackgroundPainter(fPainter);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(fPainter.getImage().getWidth(null),
                fPainter.getImage().getHeight(null));
    }
}
