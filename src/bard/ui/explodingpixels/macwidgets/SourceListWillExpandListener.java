package bard.ui.explodingpixels.macwidgets;

public interface SourceListWillExpandListener {
    void sourceListWillCollapse (SourceListItem item);
    void sourceListWillExpand (SourceListItem item);
    void sourceListWillCollapse (SourceListCategory category);
    void sourceListWillExpand (SourceListCategory category);
}
