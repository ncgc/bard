package bard.ui.explodingpixels.macwidgets;

import bard.ui.explodingpixels.macwidgets.plaf.SourceListTreeUI;
import bard.ui.explodingpixels.widgets.TreeUtils;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.EventObject;

import java.awt.geom.*;
import org.jdesktop.swingx.StackLayout;
import org.jdesktop.swingx.painter.BusyPainter;


/**
 * An implementation of an OS X Source List. For a full descrption of what a Source List is, see the
 * <a href="http://developer.apple.com/documentation/UserExperience/Conceptual/AppleHIGuidelines/XHIGWindows/chapter_18_section_4.html#//apple_ref/doc/uid/20000961-CHDDIGDE">Source Lists</a>
 * section of Apple's Human Interface Guidelines.
 * <p/>
 * This component provides the two basic sytles of Source List: focusble and non-focusable.
 * As the name implies, focusable Source Lists and recieve keyboard focus, and thus can be navigated
 * using the arrow keys. Non-focusable, cannot receive keyboard focus, and thus cannot be
 * navigated via the arrow keys. The two styles of {@code SourceList} are pictured below:
 * <br>
 * <table>
 * <tr><td align="center"><img src="../../../../graphics/iTunesSourceList.png"></td>
 * <td align="center"><img src="../../../../graphics/MailSourceList.png"></td></tr>
 * <tr><td align="center"><font size="2" face="arial"><b>Focusable SourceList<b></font></td>
 * <td align="center"><font size="2" face="arial"><b>Non-focusable SourceList<b></font></td></tr>
 * </table>
 * <br>
 * Here's how to create a simple {@code SourceList} with one item:
 * <pre>
 * SourceListModel model = new SourceListModel();
 * SourceListCategory category = new SourceListCategory("Category");
 * model.addCategory(category);
 * model.addItemToCategory(new SourceListItem("Item"), category);
 * SourceList sourceList = new SourceList(model);
 * </pre>
 * <p>
 * To install a selection listener on the {@code SourceList}, add a
 * {@link SourceListSelectionListener}.
 * </p>
 * <p/>
 * To install a context-menu provider, call
 * {@link #setSourceListContextMenuProvider(SourceListContextMenuProvider)} with an implementation
 * of {@link SourceListContextMenuProvider}.
 */
public class SourceList {

    private SourceListModel fModel;

    private final SourceListModelListener fModelListener = createSourceListModelListener();

    private final List<SourceListSelectionListener> fSourceListSelectionListeners =
            new ArrayList<SourceListSelectionListener>();

    private DefaultMutableTreeNode fRoot = new DefaultMutableTreeNode("root");

    private DefaultTreeModel fTreeModel = new DefaultTreeModel(fRoot);

    private JTree fTree = new CustomJTree(fTreeModel);

    private JScrollPane fScrollPane;

    private final JPanel fComponent = new JPanel(new BorderLayout());
    private final JPanel fBusyPane = new JPanel (new StackLayout ());

    private TreeSelectionListener fTreeSelectionListener = createTreeSelectionListener();
    private TreeWillExpandListener fTreeWillExpandListener = createTreeWillExpandListener();

    private MouseListener fMouseListener = createMouseListener();

    private SourceListControlBar fSourceListControlBar;

    private SourceListContextMenuProvider fContextMenuProvider =
            new EmptySourceListContextMenuProvider();

    private List<SourceListClickListener> fSourceListClickListeners =
            new ArrayList<SourceListClickListener>();
    private final List<SourceListWillExpandListener> 
	fSourceListWillExpandListeners = 
	new ArrayList<SourceListWillExpandListener>();
    private SourceListToolTipProvider fToolTipProvider = new EmptyToolTipProvider();
    private Map<Object, BusyPane> fBusyPanes = new HashMap<Object, BusyPane>();

    static class BusyPane extends JComponent implements ActionListener {
	javax.swing.Timer timer;
	BusyPainter painter;
	int frame = 0;
	JTree tree;
	TreePath path;
	AffineTransform txn = new AffineTransform ();
	int size = 14;

	public BusyPane (JTree tree, TreePath path) {
	    this.tree = tree;
	    this.path = path;
	    painter = new BusyPainter
		(new RoundRectangle2D.Float(0.f, 0.f,5.f, 2.f, 4.f, 4.f),
		 new Ellipse2D.Float(2.f,2.f,10.0f,10.0f));
	    painter.setTrailLength(6);
	    painter.setPoints(9);
	    painter.setFrame(0);

	    timer = new javax.swing.Timer (250, this);
	}

	public void start () { timer.start(); }
	public void stop () { timer.stop(); }

	public boolean isRunning () { return timer.isRunning(); }

	@Override
	protected void paintComponent (Graphics g) {
	    if (!tree.isVisible(path)) {
		return;
	    }

	    Graphics2D g2 = (Graphics2D)g;
	    Rectangle clip = g.getClipBounds();

	    Rectangle r = tree.getPathBounds(path);
	    if (r != null && clip.intersects(r)) {
		int x = r.x + (clip.width - size - 10);
		int y = r.y + (r.height - size)/2;
		g2.translate(x, y);
		painter.paint(g2, this, size, size);
	    }
	}

	public void actionPerformed (ActionEvent e) {
	    frame = (frame+1) % 8;
	    painter.setFrame(frame);
	    repaint ();
	}
    }

    /**
     * Creates a {@code SourceList} with an empty {@link SourceListModel}.
     */
    public SourceList() {
        this(new SourceListModel());
    }

    /**
     * Creates a {@code SourceList} with the given {@link SourceListModel}.
     *
     * @param model the {@code SourceListModel} to use.
     */
    public SourceList(SourceListModel model) {
        if (model == null) {
            throw new IllegalArgumentException("Groups cannot be null.");
        }

        fModel = model;
        fModel.addSourceListModelListener(fModelListener);

        initUi();

        // add each category and its sub-items to backing JTree.
        for (int i = 0; i < model.getCategories().size(); i++) {
            doAddCategory(model.getCategories().get(i), i);
        }
    }

    private void initUi() {
	fBusyPane.add(fTree, StackLayout.TOP);
	fScrollPane = MacWidgetFactory.createSourceListScrollPane(fBusyPane);
	fScrollPane.getViewport().addComponentListener
	    (new ComponentAdapter () {
		public void componentResized (ComponentEvent e) {
		    Component c = (Component)e.getSource();
		    Dimension dim = fTree.getPreferredSize();
		    dim.width = c.getWidth();
		    fBusyPane.setPreferredSize(dim);
		}
	    });

        fComponent.add(fScrollPane);
        fTree.addTreeSelectionListener(fTreeSelectionListener);
	fTree.addTreeWillExpandListener(fTreeWillExpandListener);
        fTree.addMouseListener(fMouseListener);
    }

    /**
     * Installs the given {@link SourceListControlBar} at the base of this {@code SourceList}. This
     * method can be called only once, and should generally be called during creation of the
     * {@code SourceList}.
     *
     * @param sourceListControlBar the {@link SourceListControlBar} to add.
     * @throws IllegalStateException    if a {@code SourceListControlBar} has already been installed
     *                                  on this {@code SourceList}.
     * @throws IllegalArgumentException if the {@code SourceListControlBar} is null.
     */
    public void installSourceListControlBar(SourceListControlBar sourceListControlBar) {
        if (fSourceListControlBar != null) {
            throw new IllegalStateException("A SourceListControlBar has already been installed on" +
                    " this SourceList.");
        }
        if (sourceListControlBar == null) {
            throw new IllegalArgumentException("SourceListControlBar cannot be null.");
        }
        fSourceListControlBar = sourceListControlBar;
        fComponent.add(fSourceListControlBar.getComponent(), BorderLayout.SOUTH);
    }

    /**
     * True if there is a {@link SourceListControlBar} installed on this {@code SourceList}.
     *
     * @return true if there is a {@link SourceListControlBar} installed on this {@code SourceList}.
     */
    public boolean isSourceListControlBarInstalled() {
        return fSourceListControlBar != null;
    }

    /**
     * Sets the {@link SourceListContextMenuProvider} to use for this {@code SourceList}.
     *
     * @param contextMenuProvider the {@link SourceListContextMenuProvider} to use for this
     *                            {@code SourceList}.
     * @throws IllegalArgumentException if the {@code SourceListContextMenuProvider} is null.
     */
    public void setSourceListContextMenuProvider(SourceListContextMenuProvider contextMenuProvider) {
        if (contextMenuProvider == null) {
            throw new IllegalArgumentException("SourceListContextMenuProvider cannot be null.");
        }
        fContextMenuProvider = contextMenuProvider;
    }

    /**
     * Uninstalls any listeners that this {@code SourceList} installed on creation, thereby allowing
     * it to be garbage collected.
     */
    public void dispose() {
        fModel.removeSourceListModelListener(fModelListener);
    }

    /**
     * Gets the selected {@link SourceListItem}.
     *
     * @return the selected {@code SourceListItem}.
     */
    public SourceListItem getSelectedItem() {
        SourceListItem selectedItem = null;
        if (fTree.getSelectionPath() != null
                && fTree.getSelectionPath().getLastPathComponent() != null) {
            DefaultMutableTreeNode selectedNode =
                    (DefaultMutableTreeNode) fTree.getSelectionPath().getLastPathComponent();
            assert selectedNode.getUserObject() instanceof SourceListItem
                    : "Only SourceListItems can be selected.";
            selectedItem = (SourceListItem) selectedNode.getUserObject();
        }
        return selectedItem;
    }

    /**
     * Selects the given {@link SourceListItem} in the list.
     *
     * @param item the item to select.
     * @throws IllegalArgumentException if the given item is not in the list.
     */
    public void setSelectedItem(SourceListItem item) {
        getModel().validateItemIsInModel(item);
        DefaultMutableTreeNode treeNode = getNodeForObject(fRoot, item);
        fTree.setSelectionPath(new TreePath(treeNode.getPath()));
    }

    public void startEditingItem (SourceListItem item) {
        getModel().validateItemIsInModel(item);
        DefaultMutableTreeNode treeNode = getNodeForObject(fRoot, item);
	fTree.startEditingAtPath(new TreePath(treeNode.getPath()));
    }

    /**
     * Clears the current selection, if there is one.
     */
    public void clearSelection() {
        fTree.clearSelection();
    }

    /**
     * Sets whether this {@code SourceList} can have focus. When focusable and this
     * {@code SourceList} has focus, the keyboard can be used for navigation.
     *
     * @param focusable true if this {@code SourceList} should be focusable.
     */
    public void setFocusable(boolean focusable) {
        fTree.setFocusable(focusable);
    }

    /**
     * Installs iApp style scroll bars on this {@code SourceList}.
     *
     * @see IAppWidgetFactory#makeIAppScrollPane
     */
    public void useIAppStyleScrollBars() {
        IAppWidgetFactory.makeIAppScrollPane(fScrollPane);
    }

    /**
     * Gets the {@link SourceListColorScheme} that this {@code SourceList} uses.
     *
     * @return the {@link SourceListColorScheme} that this {@code SourceList} uses.
     */
    public SourceListColorScheme getColorScheme() {
        return ((SourceListTreeUI) fTree.getUI()).getColorScheme();
    }

    /**
     * Sets the {@link SourceListColorScheme} that this {@code SourceList} uses.
     *
     * @param colorScheme the {@link SourceListColorScheme} that this {@code SourceList} uses.
     */
    public void setColorScheme(SourceListColorScheme colorScheme) {
        ((SourceListTreeUI) fTree.getUI()).setColorScheme(colorScheme);
    }

    /**
     * Set's the {@link TransferHandler} that this {@code SourceList} should use
     * during drag and drop operations. If the given handler not null, then
     * dragging will be turned on for the {@code SourceList}. If the handler is
     * null, then dragging will be turned off.
     *
     * @param transferHandler the {@code TransferHandler} for this
     *                        {@code SourceList}. Can be null.
     */
    public void setTransferHandler(TransferHandler transferHandler) {
        fTree.setDragEnabled(transferHandler != null);
        fTree.setTransferHandler(transferHandler);
    }

    /**
     * Scrolls the given {@link SourceListItem} to be visible.
     *
     * @param item the {@code SourceListItem} to scroll to visible.
     */
    public void scrollItemToVisible(SourceListItem item) {
        getModel().validateItemIsInModel(item);
        DefaultMutableTreeNode treeNode = getNodeForObject(fRoot, item);
        fTree.scrollPathToVisible(new TreePath(treeNode.getPath()));
    }

    /**
     * Sets the expanded state of the given {@link SourceListCategory}.
     *
     * @param category the category to set the expanded state on.
     * @param expanded true if the given category should be expanded, false if it should be
     *                 collapsed.
     * @throws IllegalArgumentException if the given {@code SourceListCategory} is not part of the
     *                                  associated {@link SourceListModel}.
     */
    public void setExpanded(SourceListCategory category, boolean expanded) {
        DefaultMutableTreeNode categoryNode = getNodeForObject(category);
        checkCategoryNodeNotNull(categoryNode);
        TreeUtils.setExpandedOnEdt(fTree, new TreePath(categoryNode.getPath()), expanded);
    }

    /**
     * Sets the expanded state of the given {@link SourceListItem}.
     *
     * @param item     the item to set the expanded state on.
     * @param expanded true if the given item should be expanded, false if it should be
     *                 collapsed.
     * @throws IllegalArgumentException if the given {@code SourceListItem} is not part of the
     *                                  associated {@link SourceListModel}.
     */
    public void setExpanded(SourceListItem item, boolean expanded) {
        DefaultMutableTreeNode itemNode = getNodeForObject(item);
        checkItemNodeNotNull(itemNode);
        TreeUtils.setExpandedOnEdt(fTree, new TreePath(itemNode.getPath()), expanded);
    }

    private DefaultMutableTreeNode getNodeForObject(Object userObject) {
        return getNodeForObject(fRoot, userObject);
    }

    private static DefaultMutableTreeNode getNodeForObject(DefaultMutableTreeNode parentNode,
                                                           Object userObject) {
        if (parentNode.getUserObject().equals(userObject)) {
            return parentNode;
        } else if (parentNode.children().hasMoreElements()) {
            for (int i = 0; i < parentNode.getChildCount(); i++) {
                DefaultMutableTreeNode childNode =
                        (DefaultMutableTreeNode) parentNode.getChildAt(i);
                DefaultMutableTreeNode retVal =
                        getNodeForObject(childNode, userObject);
                if (retVal != null) {
                    return retVal;
                }
            }
        } else {
            return null;
        }

        return null;
    }

    /**
     * Gets the user interface component representing this {@code SourceList}. The returned
     * {@link JComponent} should be added to a container that will be displayed.
     *
     * @return the user interface component representing this {@code SourceList}.
     */
    public JComponent getComponent() {
        return fComponent;
    }

    /**
     * Gets the {@link SourceListModel} backing this {@code SourceList}.
     *
     * @return the {@code SourceListModel} backing this {@code SourceList}.
     */
    public SourceListModel getModel() {
        return fModel;
    }

    public void setModel (SourceListModel model) {
	if (fModel != null) {
	    dispose ();
	}
	fRoot.removeAllChildren();
        fModel = model;
        fModel.addSourceListModelListener(fModelListener);
        for (int i = 0; i < model.getCategories().size(); i++) {
            doAddCategory(model.getCategories().get(i), i);
        }
	fTreeModel.reload();
    }	

    /**
     * Sets the {@link SourceListToolTipProvider} to use.
     *
     * @param toolTipProvider the {@code SourceListToolTipProvider to use.
     */
    public void setToolTipProvider(SourceListToolTipProvider toolTipProvider) {
        if (toolTipProvider == null) {
            throw new IllegalArgumentException("SourceListToolTipProvider cannot be null.");
        }
        fToolTipProvider = toolTipProvider;
    }

    private void doAddCategory(SourceListCategory category, int index) {
        DefaultMutableTreeNode categoryNode = new DefaultMutableTreeNode(category);
        fTreeModel.insertNodeInto(categoryNode, fRoot, index);
        // add each of the categories child items to the tree.
        for (int i = 0; i < category.getItems().size(); i++) {
            doAddItemToCategory(category.getItems().get(i), category, i);
        }

        TreeUtils.expandPathOnEdt(fTree, new TreePath(categoryNode.getPath()));
    }

    private void doRemoveCategory(SourceListCategory category) {
        DefaultMutableTreeNode categoryNode = getNodeForObject(fRoot, category);
        checkCategoryNodeNotNull(categoryNode);
        fTreeModel.removeNodeFromParent(categoryNode);
    }

    private void doAddItemToCategory(SourceListItem itemToAdd, SourceListCategory category, int index) {
        DefaultMutableTreeNode categoryNode = getNodeForObject(fRoot, category);
        checkCategoryNodeNotNull(categoryNode);
        doAddItemToNode(itemToAdd, categoryNode, index);
    }

    private void doRemoveItemFromCategory(SourceListItem itemToRemove, SourceListCategory category) {
        DefaultMutableTreeNode categoryNode = getNodeForObject(fRoot, category);
        checkCategoryNodeNotNull(categoryNode);
        DefaultMutableTreeNode itemNode = getNodeForObject(categoryNode, itemToRemove);
        checkCategoryNodeNotNull(itemNode);
        fTreeModel.removeNodeFromParent(itemNode);
    }

    private void doAddItemToItem(SourceListItem itemToAdd, SourceListItem parentItem, int index) {
        DefaultMutableTreeNode parentItemNode = getNodeForObject(fRoot, parentItem);
        checkCategoryNodeNotNull(parentItemNode);
        doAddItemToNode(itemToAdd, parentItemNode, index);
    }

    private void doRemoveItemFromItem(SourceListItem itemToRemove, SourceListItem parentItem) {
        DefaultMutableTreeNode parentNode = getNodeForObject(fRoot, parentItem);
        checkCategoryNodeNotNull(parentNode);
        DefaultMutableTreeNode itemNode = getNodeForObject(parentNode, itemToRemove);
        checkCategoryNodeNotNull(itemNode);
        fTreeModel.removeNodeFromParent(itemNode);
    }

    private void doAddItemToNode(SourceListItem itemToAdd, DefaultMutableTreeNode parentNode, int index) {
        DefaultMutableTreeNode itemNode = new DefaultMutableTreeNode(itemToAdd);
        fTreeModel.insertNodeInto(itemNode, parentNode, index);
        // add each of the newly added item's children nodes.
        for (int i = 0; i < itemToAdd.getChildItems().size(); i++) {
            doAddItemToItem(itemToAdd.getChildItems().get(i), itemToAdd, i);
        }
        // if the parent node is a new node, expand it. thus the default behavior is to expand a
        // parent node.
        if (parentNode.getChildCount() == 1) {
            TreeUtils.expandPathOnEdt(fTree, new TreePath(parentNode.getPath()));
        }
    }

    private void doItemChanged(SourceListItem item) {
        DefaultMutableTreeNode itemNode = getNodeForObject(fRoot, item);
        checkItemNodeNotNull(itemNode);
        fTreeModel.nodeChanged(itemNode);

	BusyPane bp = fBusyPanes.get(item);
	if (item.isBusy()) {
	    if (bp == null) {
		bp = new BusyPane (fTree, new TreePath (itemNode.getPath()));
		bp.start();
		fBusyPane.add(bp);
		fBusyPanes.put(item, bp);
	    }
	}
	else if (bp != null) {
	    bp.stop();
	    fBusyPane.remove(bp);
	    fBusyPanes.remove(item);
	}
    }

    private void doShowContextMenu(MouseEvent event) {
        // grab the item or category under the mouse events point if there is
        // there is an item or category under this point.
        Object itemOrCategory = getItemOrCategoryUnderPoint(event.getPoint());

        // if there was no item under the click, then call the generic contribution method.
        // else if there was a SourceListItem under the click, call the corresponding contribution
        //         method.
        // else if there was a SourceListCategory under the click, call the corresponding contribution
        //         method.
        JPopupMenu popup = null;
        if (itemOrCategory == null) {
            popup = fContextMenuProvider.createContextMenu();
        } else if (itemOrCategory instanceof SourceListItem) {
            popup = fContextMenuProvider.createContextMenu((SourceListItem) itemOrCategory);
        } else if (itemOrCategory instanceof SourceListCategory) {
            popup = fContextMenuProvider.createContextMenu((SourceListCategory) itemOrCategory);
        }

        // only show the context-menu if menu items have been added to it.
        if (popup != null && popup.getComponentCount() > 0) {
            popup.show(fTree, event.getX(), event.getY());
        }
    }

    private void doSourceListClicked(MouseEvent event) {
        // grab the item or category under the mouse events point if there is
        // there is an item or category under this point.
        Object itemOrCategory = getItemOrCategoryUnderPoint(event.getPoint());

        SourceListClickListener.Button button =
                SourceListClickListener.Button.getButton(event.getButton());
        int clickCount = event.getClickCount();

        if (itemOrCategory == null) {
            // do nothing.
        } else if (itemOrCategory instanceof SourceListItem) {
            fireSourceListItemClicked((SourceListItem) itemOrCategory, button, clickCount);
        } else if (itemOrCategory instanceof SourceListCategory) {
            fireSourceListCategoryClicked((SourceListCategory) itemOrCategory, button, clickCount);
        }
	TreePath path = fTree.getPathForLocation(event.getX(), event.getY());
    }

    private Object getItemOrCategoryUnderPoint(Point point) {
        // grab the path under the given point.
        TreePath path = fTree.getPathForLocation(point.x, point.y);
        // if there is a tree item under that point, cast it to a DefaultMutableTreeNode and grab
        // the user object which will either be a SourceListItem or SourceListCategory.
        return path == null
                ? null : ((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject();
    }

    private TreeSelectionListener createTreeSelectionListener() {
        return new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                fireSourceListItemSelected(getSelectedItem());
            }
        };
    }

    private TreeWillExpandListener createTreeWillExpandListener () {
	return new TreeWillExpandListener () {
		public void treeWillCollapse (TreeExpansionEvent e) {
		    TreePath path = e.getPath();
		    Object itemOrCategory = 
			((DefaultMutableTreeNode)path.getLastPathComponent()).getUserObject();
		    if (itemOrCategory instanceof SourceListItem) {
			fireSourceListWillCollapse 
			    ((SourceListItem)itemOrCategory);
		    }
		    else if (itemOrCategory instanceof SourceListCategory) {
			fireSourceListWillCollapse 
			    ((SourceListCategory)itemOrCategory);
		    }
		}

		public void treeWillExpand (TreeExpansionEvent e) {
		    TreePath path = e.getPath();
		    Object itemOrCategory = 
			((DefaultMutableTreeNode)path.getLastPathComponent()).getUserObject();
		    if (itemOrCategory instanceof SourceListItem) {
			fireSourceListWillExpand
			    ((SourceListItem)itemOrCategory);
		    }
		    else if (itemOrCategory instanceof SourceListCategory) {
			fireSourceListWillExpand
			    ((SourceListCategory)itemOrCategory);
		    }
		}
	    };
    }

    private SourceListModelListener createSourceListModelListener() {
        return new SourceListModelListener() {
            public void categoryAdded(SourceListCategory category, int index) {
                doAddCategory(category, index);
            }

            public void categoryRemoved(SourceListCategory category) {
                doRemoveCategory(category);
            }

            public void itemAddedToCategory(SourceListItem item, SourceListCategory category, int index) {
                doAddItemToCategory(item, category, index);
            }

            public void itemRemovedFromCategory(SourceListItem item, SourceListCategory category) {
                doRemoveItemFromCategory(item, category);
            }

            public void itemAddedToItem(SourceListItem item, SourceListItem parentItem, int index) {
                doAddItemToItem(item, parentItem, index);
            }

            public void itemRemovedFromItem(SourceListItem item, SourceListItem parentItem) {
                doRemoveItemFromItem(item, parentItem);
            }

            public void itemChanged(SourceListItem item) {
                doItemChanged(item);
            }
        };
    }

    private MouseListener createMouseListener() {
        return new MouseAdapter() {
            // TODO there is an interesting point of contention here: should
            // TODO the context menu always be shown as if it were on a Mac (on
            // TODO mouse press) or based on the platform on which it is running.
            // TODO always doing the same thing would actually be harder,
            // TODO because we wouldn't be able to rely on the isPopupTrigger
            // TODO method and there is no way to determine when the 
            // TODO popup-menu-trigger button is.
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    doShowContextMenu(e);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    doShowContextMenu(e);
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                doSourceListClicked(e);
            }
        };
    }

    // SourceListClickListener support. ///////////////////////////////////////    

    private void fireSourceListItemClicked(
            SourceListItem item, SourceListClickListener.Button button,
            int clickCount) {
        for (SourceListClickListener listener : fSourceListClickListeners) {
            listener.sourceListItemClicked(item, button, clickCount);
        }
    }

    private void fireSourceListCategoryClicked(
            SourceListCategory category, SourceListClickListener.Button button,
            int clickCount) {
        for (SourceListClickListener listener : fSourceListClickListeners) {
            listener.sourceListCategoryClicked(category, button, clickCount);
        }
    }

    /**
     * Adds the {@link SourceListClickListener} to the list of listeners.
     *
     * @param listener the {@code SourceListClickListener} to add.
     */
    public void addSourceListClickListener(SourceListClickListener listener) {
        fSourceListClickListeners.add(listener);
    }

    /**
     * Removes the {@link SourceListClickListener} to the list of listeners.
     *
     * @param listener the {@code SourceListClickListener} to remove.
     */
    public void removeSourceListClickListener(SourceListClickListener listener) {
        fSourceListClickListeners.remove(listener);
    }

    // SourceListSelectionListener support. ///////////////////////////////////

    private void fireSourceListItemSelected(SourceListItem item) {
        for (SourceListSelectionListener listener : fSourceListSelectionListeners) {
            listener.sourceListItemSelected(item);
        }
    }

    /**
     * Adds the {@link SourceListSelectionListener} to the list of listeners.
     *
     * @param listener the {@code SourceListSelectionListener} to add.
     */
    public void addSourceListSelectionListener(SourceListSelectionListener listener) {
        fSourceListSelectionListeners.add(listener);
    }

    /**
     * Removes the {@link SourceListSelectionListener} from the list of listeners.
     *
     * @param listener the {@code SourceListSelectionListener} to remove.
     */
    public void removeSourceListSelectionListener(SourceListSelectionListener listener) {
        fSourceListSelectionListeners.remove(listener);
    }

    public void addSourceListWillExpandListener 
	(SourceListWillExpandListener listener) {
	fSourceListWillExpandListeners.add(listener);
    }

    public void removeSourceListWillExpandListener 
	(SourceListWillExpandListener listener) {
	fSourceListWillExpandListeners.remove(listener);
    }

    private void fireSourceListWillExpand (SourceListCategory category) {
	for (SourceListWillExpandListener listener : 
		 fSourceListWillExpandListeners) {
	    listener.sourceListWillExpand(category);
	}
    }

    private void fireSourceListWillCollapse (SourceListCategory category) {
	for (SourceListWillExpandListener listener : 
		 fSourceListWillExpandListeners) {
	    listener.sourceListWillCollapse(category);
	}
    }

    private void fireSourceListWillExpand (SourceListItem item) {
	for (SourceListWillExpandListener listener : 
		 fSourceListWillExpandListeners) {
	    listener.sourceListWillExpand(item);
	}
    }

    private void fireSourceListWillCollapse (SourceListItem item) {
	for (SourceListWillExpandListener listener : 
		 fSourceListWillExpandListeners) {
	    listener.sourceListWillCollapse(item);
	}
    }



    // Utility methods. ///////////////////////////////////////////////////////////////////////////

    private static void checkCategoryNodeNotNull(MutableTreeNode node) {
        if (node == null) {
            throw new IllegalArgumentException("The given SourceListCategory " +
                    "does not exist in this SourceList.");
        }
    }

    private static void checkItemNodeNotNull(MutableTreeNode node) {
        if (node == null) {
            throw new IllegalArgumentException("The given SourceListItem " +
                    "does not exist in this SourceList.");
        }
    }

    // EmptySourceListContextMenuProvider implementation. /////////////////////////////////////////

    private static class EmptySourceListContextMenuProvider implements SourceListContextMenuProvider {
        public JPopupMenu createContextMenu() {
            return null;
        }

        public JPopupMenu createContextMenu(SourceListItem item) {
            return null;
        }

        public JPopupMenu createContextMenu(SourceListCategory category) {
            return null;
        }
    }

    // Custom JTree implementation that always returns SourceListTreeUI delegate. /////////////////
    private class CustomTreeCellEditor 
	extends DefaultTreeCellEditor implements Icon {
	Icon realIcon;

	CustomTreeCellEditor (JTree tree) {
	    super (tree, null);
	    offset = 55; // hack!
	    editingIcon = this;
	}

	@Override
	public Component getTreeCellEditorComponent
	    (JTree tree, Object value, boolean isSelected,
	     boolean expanded, boolean leaf, int row) {
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;

	    realIcon = null;
	    if (node != null) {
		if (node.getUserObject() instanceof SourceListItem) {
		    SourceListItem item = (SourceListItem)node.getUserObject();
		    realIcon = item.getIcon();
		}
	    }
	    return super.getTreeCellEditorComponent
		(tree, value, isSelected, expanded, leaf, row);
	}

	@Override
	protected void determineOffset(JTree tree, Object value,
				       boolean isSelected, boolean expanded,
				       boolean leaf, int row) {
	    // do nothing
	}

	@Override
	public Object getCellEditorValue () {
	    Object value = super.getCellEditorValue();
	    DefaultMutableTreeNode node = 
		(DefaultMutableTreeNode) lastPath.getLastPathComponent();
	    Object obj = node.getUserObject();
	    if (obj instanceof SourceListItem) {
		SourceListItem item = (SourceListItem)obj;
		item.setText((String)value);
	    }
	    return obj;
	}

	@Override
	public boolean isCellEditable (EventObject e) {
	    if (super.isCellEditable(e)) {
		// now check that its 
		DefaultMutableTreeNode leaf = 
		    (DefaultMutableTreeNode)lastPath.getLastPathComponent();
		Object obj = leaf.getUserObject();
		if (obj instanceof SourceListItem) {
		    return ((SourceListItem)obj).isEditable();
		}
	    }
	    return false;
	}

	/**
	 * Icon interface
	 */
	public int getIconHeight () { 
	    return realIcon != null ? realIcon.getIconHeight() : 0; 
	}
	public int getIconWidth () {
	    return realIcon != null ? realIcon.getIconWidth() : 0;
	}
	public void paintIcon (Component c, Graphics g, int x, int y) {
	    if (realIcon != null) {
		// another HACK!
		realIcon.paintIcon(c, g, 35, y);
	    }
	}
    }

    private class CustomJTree extends JTree implements CellEditorListener {
        public CustomJTree(TreeModel newModel) {
            super(newModel);
	    setEditable (true);
	    setCellEditor (new CustomTreeCellEditor (this));
	    ToolTipManager.sharedInstance().registerComponent(this);
        }

        @Override
        public void updateUI() {
            setUI(new SourceListTreeUI());
            invalidate();
        }

	@Override 
	public void collapsePath (TreePath path) {
	    DefaultMutableTreeNode node = 
		(DefaultMutableTreeNode)path.getLastPathComponent();
	    Object obj = node.getUserObject();
	    if (obj instanceof SourceListCategory) {
		// not allowed to be closed
		SourceListCategory cat = (SourceListCategory)obj;
		if (!cat.isCollapsable()) {
		    // don't collapse
		}
		else {
		    super.collapsePath(path);
		}
	    }
	    else {
		super.collapsePath(path);
	    }
	    // since the parent uses the StackLayout, we force it to
	    //  get the original size
	    getParent().setPreferredSize(null);
	}

        @Override
        public String getToolTipText(MouseEvent event) {
            TreePath path = getPathForLocation(event.getX(), event.getY());
            Object userObject = path == null
                    ? null : ((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject();
            String toolTipText = null;
            if (userObject instanceof SourceListCategory) {
                toolTipText = fToolTipProvider.getTooltip((SourceListCategory) userObject);
            } else if (userObject instanceof SourceListItem) {
                toolTipText = fToolTipProvider.getTooltip((SourceListItem) userObject);
            }
            return toolTipText;
        }

	@Override
	public void expandPath (TreePath path) {
	    super.expandPath(path);
	    getParent().setPreferredSize(null);
	}

	public void editingCanceled (ChangeEvent e) {
	    // do nothing
	}
	public void editingStopped (ChangeEvent e) {
	    Object value = getCellEditor().getCellEditorValue();
	    TreePath path = getEditingPath ();
	    DefaultMutableTreeNode leaf = 
		(DefaultMutableTreeNode)path.getLastPathComponent();
	    System.err.println("** "+e.getSource()+" old: "+leaf.getUserObject()+" new:"+value);
	}
    }

    // Empty SourceListTooltipProvider. ///////////////////////////////////////////////////////////

    private static class EmptyToolTipProvider implements SourceListToolTipProvider {
        public String getTooltip(SourceListCategory category) {
            return null;
        }

        public String getTooltip(SourceListItem item) {
            return null;
        }
    }
}
