package bard.ui.explodingpixels.macwidgets;

public class SourceListWillExpandAdapter 
    implements SourceListWillExpandListener {
    public void sourceListWillCollapse (SourceListItem item) {}
    public void sourceListWillCollapse (SourceListCategory category) {}
    public void sourceListWillExpand (SourceListItem item) {}
    public void sourceListWillExpand (SourceListCategory category) {}
}
