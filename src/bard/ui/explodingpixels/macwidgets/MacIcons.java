package bard.ui.explodingpixels.macwidgets;

import javax.swing.ImageIcon;

public class MacIcons {

    public static ImageIcon PLUS =
            new ImageIcon(MacIcons.class.getResource("images/plus.png"));

    public static ImageIcon MINUS =
            new ImageIcon(MacIcons.class.getResource("images/minus.png"));

    public static ImageIcon GEAR =
            new ImageIcon(MacIcons.class.getResource("images/gear.png"));
}
