package bard.ui.explodingpixels.macwidgets;

import bard.ui.explodingpixels.util.PlatformUtils;
import bard.ui.explodingpixels.widgets.WindowDragger;
import bard.ui.explodingpixels.widgets.WindowUtils;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;

public class HudPanel extends JPanel {
    private static final int ROUNDED_RECT_DIAMETER = 16;

    private static final Color HIGHLIGHT = new Color(255, 255, 255, 59);
    private static final Color HIGHLIGHT_BOTTOM = new Color(255, 255, 255, 25);
    private static final Color BACKGROUND = new Color(30, 30, 30, 216);

    private int roundedDiameter = ROUNDED_RECT_DIAMETER;

    public HudPanel() {
	setLayout(new BorderLayout());
	setOpaque(false);
    }

    public void setRoundedDiameter (int diameter) {
	this.roundedDiameter = diameter;
    }

    public int getRoundedDiameter () { return roundedDiameter; }

    @Override
    protected void paintBorder(Graphics g) {
	// create a copy of the graphics object and turn on anti-aliasing.
	Graphics2D graphics2d = (Graphics2D) g.create();
	graphics2d.setRenderingHint(
				    RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	// paint a border around the window that fades slightly to give a more pleasnt highlight
	// to the window edges.
	GradientPaint paint = new GradientPaint(0, 0, HIGHLIGHT, 0, getHeight(), HIGHLIGHT_BOTTOM);
	graphics2d.setPaint(paint);
	graphics2d.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 
				 roundedDiameter, roundedDiameter);

	graphics2d.dispose();
    }

    @Override
     protected void paintComponent(Graphics g) {
	// create a copy of the graphics object and turn on anti-aliasing.
	Graphics2D graphics2d = (Graphics2D) g.create();
	graphics2d.setRenderingHint(
				    RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	graphics2d.setComposite(AlphaComposite.Src);

	// draw the rounded rectangle background of the window.
	graphics2d.setColor(BACKGROUND);
	graphics2d.fillRoundRect(0, 0, getWidth(), getHeight(),
				 roundedDiameter, roundedDiameter);
	// tell the shadow to revalidate.
	getRootPane().putClientProperty("apple.awt.windowShadow.revalidateNow", new Object());

	graphics2d.dispose();
    }

}
