package bard.ui.explodingpixels.macwidgets;

public interface SourceListBadgeContentProvider {

    int getCounterValue();

}
