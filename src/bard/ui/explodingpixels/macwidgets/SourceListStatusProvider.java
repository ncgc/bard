package bard.ui.explodingpixels.macwidgets;

public interface SourceListStatusProvider {
    void setBusy (boolean busy);
    boolean isBusy ();
}
