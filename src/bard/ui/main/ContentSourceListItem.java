// $Id$

package bard.ui.main;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.SwingUtilities;

import ca.odell.glazedlists.EventList;
import bard.ui.explodingpixels.macwidgets.SourceListItem;

import org.bushe.swing.event.EventSubscriber;
import org.bushe.swing.event.EventBus;

import bard.ui.content.Content;
import bard.ui.content.State;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.StateChangeEvent;

public class ContentSourceListItem 
    extends SourceListItem implements EventSubscriber {

    private static final Logger logger = 
	Logger.getLogger(ContentSourceListItem.class.getName());

    private Content content;
    private boolean loaded = false;

    protected ContentSourceListItem (String name, Icon icon) {
	super (name, icon);
    }

    public ContentSourceListItem (Content content) {
	super (content.getName(), content.getIcon());
	this.content = content;

        setCounterValue ((int)content.getTotalSize());
        //startTimer ();

	EventBus.subscribe(ContentChangeEvent.class, this);
	EventBus.subscribe(StateChangeEvent.class, this);
    }

    void startTimer () {
        new javax.swing.Timer(5000, new ActionListener () {
                public void actionPerformed (ActionEvent e) {
//                    setCounterValue (0);
                }
            }).start();
    }

    public void onEvent (Object e) {
        if (e instanceof StateChangeEvent) {
            onEvent ((StateChangeEvent)e);
        }
        else if (e instanceof ContentChangeEvent) {
            onEvent ((ContentChangeEvent)e);
        }
    }

    public void onEvent (StateChangeEvent e) {
        if (e.getSource() != content.getState()) {
            return;
        }
        //setCounterValue ((int)content.getTotalSize()); //!!! should we display final counts or what we actually have on hand?
    }

    public void onEvent (ContentChangeEvent e) {
	if (content != e.getContent()) {
	    return;
	}

        /*
	logger.info(content+" "+content.getName()+": "+e.getName()
                    +" old="+e.getOldValue() + " new="+e.getNewValue());
        */

	String prop = e.getName();
	if (prop.equals(ContentChangeEvent.BUSY)) {
	    setBusy ((Boolean)e.getNewValue());
	}
	else if ( prop.equals(ContentChangeEvent.STATE) ) {
	    //logger.severe("Reset counter to: "+((State)e.getNewValue()).size()+" for class "+((State)e.getNewValue()).getContent().getEntityClass());
	    setCounterValue (((State)e.getNewValue()).size());
	}
	else if (
		(prop.equals(ContentChangeEvent.SIZE) && getCounterValue() == 0) ||  //!!! should we display final counts or what we actually have on hand?
		prop.equals(ContentChangeEvent.NEWCOUNT)) {
	    setCounterValue ((Integer)e.getNewValue());
	}
    }

    public Content getContent () { return content; }
    public void reload () { 
        content.reload();
    }
    public void prev () {
    	content.pop();
    }
    public void next () {
    	content.push();
    }
    public void setLoaded (boolean loaded) {
	this.loaded = loaded;
    }
    public boolean isLoaded () { return loaded; }
}
