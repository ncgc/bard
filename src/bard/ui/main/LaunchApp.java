// $Id$
package bard.ui.main;

import java.util.List;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;
import org.jdesktop.application.Application;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingworker.SwingWorker;

import java.util.logging.Level;
import java.util.logging.Logger;
import bard.ui.util.UIUtil;

public class LaunchApp extends SwingWorker<Throwable, Void> {
    private static Logger logger = Logger.getLogger(LaunchApp.class.getName());

    String[] argv;
    Class<? extends Application> app;

    public LaunchApp (Class<? extends Application> app, 
		      String splash, String[] argv) {
	this.argv = argv;
	this.app = app;
	try {
//	    UIUtil.setupSplashScreen
//		(LaunchApp.class.getResource(splash));
	    SwingUtilities.invokeAndWait(new Runnable () {
		    public void run () {
			showSplash ();
		    }
		});
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't initialize splash screen", ex);
	}
    }

    @Override
    protected Throwable doInBackground () {
	try {
	    Application.launch(app, argv);
	}
	catch (Exception ex) {
	    return ex;
	}
	return null;
    }

    void showSplash () {
    	SplashScreen splash = SplashScreen.getInstance();
    	splash.setText("Loading client...");
    	splash.setMin(0);
    	splash.setMax(100);
    	splash.setValue(10);
    	splash.setVisible(true);
    }

    @Override
    protected void done () {
	//SplashScreen.getInstance().setVisible(false);
	try {
	    Throwable t = get ();
	    if (t != null) {
		JXErrorPane.showDialog(t);
		System.exit(1);
	    }
	}
	catch (Exception ex) {
	    JXErrorPane.showDialog(ex);
	}
    }
}
