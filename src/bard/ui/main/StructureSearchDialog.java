
package bard.ui.main;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.*;
import org.jdesktop.swingworker.SwingWorker;

import chemaxon.marvin.beans.MViewPane;
import chemaxon.marvin.beans.MSketchPane;
import chemaxon.struc.*;
import chemaxon.formats.MolImporter;
import chemaxon.formats.MolFormatException;
import chemaxon.util.MolHandler;

import net.miginfocom.swing.MigLayout;
import org.bushe.swing.event.EventBus;

import bard.core.SearchParams;
import bard.core.StructureSearchParams;
import bard.ui.event.SearchEvent;
import bard.ui.util.UIUtil;


public class StructureSearchDialog extends JDialog 
    implements ActionListener, PropertyChangeListener {

    private static final Logger logger = Logger.getLogger
	(StructureSearchDialog.class.getName());
    
    private MSketchPane msketch;
    private JRadioButton subRb, simRb, exactRb, supRb;
    private JTextField tanimotoField;
    private JTextField maxField;
    private RoundTextField statusField;
    private UIUtil.BusyPane busy;
    private JButton searchBtn, cancelBtn;

    protected SearchParams query;

    public StructureSearchDialog (Frame owner) {
	super (owner, false);
	initDialog ();
	setTitle ("BARD Structure Search");
    }

    protected void initDialog () {
	JPanel pane = new JPanel 
	    (new MigLayout ("", "[right]unrel[grow,fill]", 
			    "unrel[][][top,grow,fill][]unrel[]unrel[]"));

	final JLabel tanLabel = new JLabel ("Tanimoto");
	tanLabel.setEnabled(false);
	final JLabel maxLabel = new JLabel ("Top N");
	maxLabel.setEnabled(false);
	ActionListener action = new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    Object source = e.getSource();
		    if (source == subRb
			|| source == exactRb
			|| source == supRb) {
			tanimotoField.setEnabled(false);
			tanLabel.setEnabled(false);
			maxField.setEnabled(false);
			maxLabel.setEnabled(false);
		    }
		    else if (source == simRb) {
			tanimotoField.setEnabled(true);
			tanLabel.setEnabled(true);
			maxField.setEnabled(true);
			maxLabel.setEnabled(true);
		    }
		}
	    };

	msketch = new MSketchPane ();
	msketch.setCloseEnabled(false);
	msketch.addPropertyChangeListener(this);

        JMenuBar menubar = msketch.getJMenuBar();
        ArrayList<JMenu> remove = new ArrayList<JMenu>();
        for (int i = 0; i < menubar.getMenuCount(); ++i) {
            JMenu menu = menubar.getMenu(i);
            if (menu.getText().equals("Help") 
                || menu.getText().equals("Tools")) {
                remove.add(menu);
            }
        }
        for (JMenu m : remove) {
            menubar.remove(m);
        }

	//pane.add(new JLabel ("<html><b>Structure"), "top");
	pane.add(msketch, "growx, span, wrap");

	pane.add(new JLabel ("<html><b>Search Type"), "top");
	Box box = Box.createHorizontalBox();
	ButtonGroup bg = new ButtonGroup ();
	box.add(subRb = new JRadioButton ("Substructure"));
	subRb.setToolTipText
	    ("Search for compounds with the specified substructure");
	subRb.addActionListener(action);
	subRb.setSelected(true);
	bg.add(subRb);
	box.add(Box.createHorizontalStrut(10));
        box.add(Box.createHorizontalGlue());

	box.add(supRb = new JRadioButton ("Superstructure"));
	supRb.setToolTipText
	    ("Search for all compounds that are superstructure of the query");
	supRb.addActionListener(action);
	bg.add(supRb);
	box.add(Box.createHorizontalStrut(10));
        box.add(Box.createHorizontalGlue());

	box.add(exactRb = new JRadioButton ("Exact"));
	exactRb.setToolTipText("Search for exact compound");
	exactRb.addActionListener(action);
	bg.add(exactRb);
	box.add(Box.createHorizontalStrut(10));
        box.add(Box.createHorizontalGlue());

	box.add(simRb = new JRadioButton ("Similarity"));
	simRb.setToolTipText
	    ("Search for similar compounds (in the Tanimoto sense)");
	simRb.addActionListener(action);
	bg.add(simRb);

	pane.add(box, "growx, span, top");

	box = Box.createHorizontalBox();
	box.add(tanLabel);
	box.add(Box.createHorizontalStrut(5));
	box.add(tanimotoField = new RoundTextField (5));
	tanimotoField.setEnabled(false);
	tanimotoField.setText("0.85");
	tanimotoField.setToolTipText("Specify minimum Tanimoto cutoff");
	box.add(Box.createHorizontalStrut(10));
	box.add(maxLabel);
	box.add(Box.createHorizontalStrut(5));
	box.add(maxField = new RoundTextField (5));
	maxField.setEnabled(false);
	maxField.setText(String.valueOf(1000));
	maxField.setToolTipText
	    ("Specify only the top N compounds are returned");
	pane.add(box, "span, growx, wrap 10");

	box = Box.createHorizontalBox();
	box.add(statusField = new RoundTextField ());
	box.add(Box.createHorizontalStrut(5));
	box.add(busy = new UIUtil.BusyPane());
	statusField.setEditable(false);
	pane.add(box, "span, growx, wrap");
	pane.add(new JSeparator (), "growx, span, wrap");

	JPanel control = new JPanel (new BorderLayout ());
	control.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
	control.add(pane);

	JPanel bpane = new JPanel (new GridLayout (1, 2, 2, 0));
	JButton btn;
	bpane.add(btn = new JButton ("Search"));
	btn.addActionListener(this);
	btn.setEnabled(false);
	searchBtn = btn;
	bpane.add(btn = new JButton ("Close"));
	btn.addActionListener(this);

	JPanel cpane = new JPanel ();
	cpane.add(bpane);

	control.add(cpane, BorderLayout.SOUTH);
	getContentPane().add(control);

	pack ();
    }

    public void setStatus (String status) {
        statusField.setText(status);
    }

    public void setBusy (boolean busy) {
        if (busy) this.busy.start();
        else this.busy.stop();
    }

    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();

	if (cmd.equalsIgnoreCase("close")) {
	    setVisible (false);
	    return;
	}

	StructureSearchParams query = null;
        Molecule mol = msketch.getMol();
        mol.aromatize();
	if (subRb.isSelected()) {
	    String s = mol.toFormat("cxsmarts:s");
	    query = new StructureSearchParams 
                (s, StructureSearchParams.Type.Substructure);
	}
        else if (exactRb.isSelected()) {
            String s = mol.toFormat("smiles:q");
	    query = new StructureSearchParams 
                (s, StructureSearchParams.Type.Exact);            
        }
        else if (supRb.isSelected()) {
            String s = mol.toFormat("smiles:s");
	    query = new StructureSearchParams 
                (s, StructureSearchParams.Type.Superstructure);
        }
	else {
	    try {
		for (MolAtom a : mol.getAtomArray()) {
		    int atno = a.getAtno();
		    if (a.isQuery() 
			|| atno == MolAtom.LIST 
			|| atno == MolAtom.ANY) {
			throw new Exception ("Query molecule");
		    }
		}

		for (MolBond b : mol.getBondArray()) {
		    if (b.isQuery() || b.getType() == MolBond.ANY) {
			throw new Exception ("Query molecule");
		    }
		}
	    }
	    catch (Exception ex) {
		JOptionPane.showMessageDialog
		    (this, "Markush structure not supported "
		     +"for the selected search type!", "Error", 
		     JOptionPane.ERROR_MESSAGE);
		return;
	    }

	    if (simRb.isSelected()) {
		String sim = tanimotoField.getText();
		try {
		    query = new StructureSearchParams
			(mol.toFormat("smiles:a"), 
                         StructureSearchParams.Type.Similarity);
		    query.setThreshold(Double.parseDouble(sim));
		}
		catch (NumberFormatException ex) {
		    JOptionPane.showMessageDialog
			(this, "Invalid Tanimoto value: "+sim, 
			 "Error", JOptionPane.ERROR_MESSAGE);
		    return;
		}
		String max = maxField.getText();
		try {
		    long maxNum = Long.valueOf(max);
		    if (maxNum > 1000) throw new NumberFormatException();
		    query.setTop(Long.valueOf(max));		    
		} catch (NumberFormatException ex) {
		    JOptionPane.showMessageDialog
			(this, "Invalid max number to return: "+max, 
			 "Error", JOptionPane.ERROR_MESSAGE);
		    return;
		}
	    }
	}
	
        this.query = query;
        setVisible (false);

        search ();
    }

    public void search () {
        EventBus.publish(new SearchEvent (this, query));
    }

    public void propertyChange (PropertyChangeEvent e) {
	String name = e.getPropertyName();
	logger.info(name+": old="+e.getOldValue()+" new="+e.getNewValue());
        /*
	if ("doc0".equals(name)) {
	    statusField.setText(null);
	    searchBtn.setEnabled(mview.getM(0).getAtomCount() > 0);
	}
        */
        if ("mol".equals(name)) {
	    statusField.setText(null);
	    searchBtn.setEnabled(msketch.getMol().getAtomCount() > 0); 
        }
    }

    public void setSearch (String text) {
	if (text == null) {
	    return;
	}

	String[] toks = text.split("[\\s\\t]+");
	for (String s : toks) {
	    if (s.startsWith("sub:")) {
		subRb.doClick();
		msketch.setMol(s.substring(4));
	    }
	    else if (s.startsWith("sim:")) {
		simRb.doClick();
		msketch.setMol(s.substring(4));
	    }
	    else if (s.startsWith("tan:")) {
		tanimotoField.setText(s.substring(4));
	    }
	    else if (s.startsWith("max:")) {
		maxField.setText(s.substring(4));
	    }
	}
    }


    public static void main (String[] argv) throws Exception {
	StructureSearchDialog sd = new StructureSearchDialog (null);
	sd.pack();
	sd.setVisible(true);
    }
}
