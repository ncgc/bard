package bard.ui.main;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;

import net.miginfocom.swing.MigLayout;

import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.JXHyperlink;

import bard.core.Entity;
import bard.ui.content.Content;
import bard.ui.content.ContentManager;
import bard.ui.util.UIUtil;
import bard.util.BardHttpClient;

import chemaxon.formats.MolImporter;
import chemaxon.struc.Molecule;

public class ImportDialog extends JDialog 
    implements ActionListener, CaretListener, PropertyChangeListener {

    private static final Logger logger = 
	Logger.getLogger(ImportDialog.class.getName());

    enum FieldEnum {
	None (null),
	Name ("Name"),
	Text ("Text"),
	Number ("Number"),
	Structure ("Structure"),
	BioActivity ("BioActivity");

	final String field;
	FieldEnum (String field) {
	    this.field = field;
	}

	String getField () { return field; }
    }

    static class FieldImport {
	String name; // name of field
	FieldEnum field; // which field does this name map to?

	FieldImport (String name, FieldEnum field) {
	    this.name = name;
	    this.field = field;
	}
    }

    class ScanDataSource extends SwingWorker<Throwable, String> {
	String source;
	int count; // number of entries to scan
	Set<String> fields = new TreeSet<String>();

	ScanDataSource (String source, int count) {
	    this.source = source;
	    this.count = count;
	}

	@Override
	protected Throwable doInBackground () {
	    busy.start();
	    importBtn.setEnabled(false);
	    scanLink.setEnabled(false);

	    try {
		int pos = source.lastIndexOf('.');
		String ext = "";
		if (pos > 0) {
		    ext = source.substring(pos+1);
		}

		if (ext.equalsIgnoreCase("csv")) {
		    scanCSV ();
		}
		else {
		    scanSDF ();
		}
	    }
	    catch (Exception ex) {
		return ex;
	    }
	    return null;
	}

	@Override
	protected void done () {
	    importBtn.setEnabled(true);
	    scanLink.setEnabled(true);
	    busy.stop();

	    try {
		Throwable t = get ();
		if (t != null) {
		    statusField.setText("Error: "+t.getMessage());
		}
		else {
		    statusField.setText
			(fields.size() + " unique fields found!");
		    ((FieldTableModel)fieldTable.getModel()).setFields(fields);
		}
	    }
	    catch (Exception ex) {
		statusField.setText("Error: "+ex.getMessage());
	    }
	    busy.stop();
	}

	@Override
	protected void process (String... mesg) {
	    for (String s : mesg) {
		statusField.setText(s);
	    }
	}

	void scanSDF () throws Exception {
	    MolImporter mi = new MolImporter (BardHttpClient.getURLInputStream(new URL (source)));
	    int cnt = 0;
	    for (Molecule mol = new Molecule (); 
		 cnt < count && mi.read(mol); ++cnt) {
		for (int i = 0; i < mol.getPropertyCount(); ++i) {
		    String prop = mol.getPropertyKey(i);
		    fields.add(prop);
		}
		publish ("Scanning entry "+(cnt+1)+"...");
	    }
	    mi.close();
	}

	void scanCSV () throws Exception {
	    BufferedReader br = new BufferedReader 
		(new InputStreamReader (BardHttpClient.getURLInputStream(new URL (source))));
	    String header = br.readLine();
	    br.close();
	    for (String column : header.split(",")) {
		fields.add(column);
	    }
	}
    } // ScanDataSource

    class FieldTableModel extends DefaultTableModel {
	String[] columns = new String[] {
	    "Field",
	    "If matches...",
	    "Import as..."
	};

	FieldTableModel () {
	}

	public int getColumnCount () { return columns.length; }
	public String getColumnName (int col) {
	    return columns[col];
	}
	public boolean isColumnEditable (int col) {
	    return col > 0;
	}
	public Class getColumnClass (int col) {
	    if (col == 2) 
		return FieldEnum.class;
	    return String.class;
	}

	void setFields (Set<String> fields) {
	    setRowCount (0);
	    for (String f : fields) {
		addRow (new Object[]{f, null, FieldEnum.None});
	    }
	}

	Map<String, String[]> getPropertyMapping () {
	    Map<String, String[]> map = new TreeMap<String, String[]>();
	    for (int r = 0; r < getRowCount (); ++r) {
		String prop = (String)getValueAt (r, 0);
		String cons = (String)getValueAt (r, 1);
		FieldEnum field = (FieldEnum)getValueAt (r, 2);
		logger.info("** property="+ prop
			    +" constraint="+cons+" field="+field);
		if (field != FieldEnum.None 
		    || (cons != null && cons.length() > 0)) {
		    map.put(prop, new String[]{field.getField(), cons});
		}
	    }
	    return map;
	}
    }

    JTextField statusField;
    UIUtil.BusyPane busy;

    JTextField sourceField;
    JTextField nameField;
    JTextArea descArea;
    JButton importBtn, closeBtn;
    JXHyperlink scanLink;
    JTable fieldTable;
    JCheckBox standardizeCb, saltStripCb;

    Content<? extends Entity> content;

    public ImportDialog (Frame owner) {
	super (owner, true); // modal dialog
	initDialog ();
    }

    public ImportDialog (Frame owner, boolean modal) {
	super (owner, modal); // modal dialog
    }

    protected void initDialog () {
	setTitle ("Import Data");

	JPanel pane = new JPanel 
	    (new MigLayout ("", "[right][grow,fill]", 
			    "unrel[][][]unrel[grow,fill]unrel[]"));

	pane.add(new JLabel ("Source"), "gap 10");
	sourceField = new JTextField (30);
	JPanel bp = new JPanel (new BorderLayout (5, 0));
	bp.add(sourceField);
	JButton btn = new JButton ("...");
	btn.setMargin(new Insets (0, 0, 0, 0));
	btn.setToolTipText("Open file chooser");
	btn.addActionListener(this);
	bp.add(btn, BorderLayout.WEST);
	pane.add(bp, "span, growx, wrap 10");
	sourceField.setToolTipText("Data source; can be a file name or URL.");
	sourceField.addCaretListener(this);

	pane.add(new JLabel ("Name"), "gap 10");
	pane.add(nameField = new JTextField (30), 
		 "span, growx, wrap 10");
	nameField.setToolTipText("Specify the name of this import; "
				      +"please choose a meaningful name.");
	nameField.addCaretListener(this);

	pane.add(new JLabel ("Description"), "gap 10");
	pane.add(new JScrollPane (descArea = new JTextArea (10, 30)),
		"span, growx, wrap 10");
	descArea.addCaretListener(this);

	pane.add(new JLabel ("Options"), "gap 10");
	Box box = Box.createHorizontalBox();
	box.add(standardizeCb = new JCheckBox ("Standardize"));
	standardizeCb.setToolTipText("Standardize and normalize structures");
	standardizeCb.setSelected(true);
	box.add(Box.createHorizontalStrut(5));
	box.add(saltStripCb = new JCheckBox ("Salt stripping"));
	saltStripCb.setToolTipText
	    ("Remove salt and/or solvent from structures");
	saltStripCb.setSelected(true);
	box.add(Box.createHorizontalGlue());
	pane.add(box, "span, growx, wrap 10");

	pane.add(createOptionPane (), "span, growx,wrap 10");
	pane.add(new JSeparator (), "span, growx, wrap");

	JPanel sp = new JPanel (new BorderLayout (5, 0));
	sp.add(busy = UIUtil.createBusyPane(), BorderLayout.WEST);
	sp.add(statusField = new JTextField ());
	pane.add(sp, "span, growx");
	statusField.setEditable(false);

	JPanel top = new JPanel (new BorderLayout (0, 5));
	top.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
	top.add(pane);

	bp = new JPanel (new GridLayout (1, 4, 3, 0));
	bp.add(btn = new JButton ("Import"));
	btn.setToolTipText("Import data");
	btn.addActionListener(this);
	btn.setEnabled(false);
	importBtn = btn;

	bp.add(btn = new JButton ("Close"));
	btn.addActionListener(this);
	closeBtn = btn;
	JPanel bbp = new JPanel ();
	bbp.add(bp);
	top.add(bbp, BorderLayout.SOUTH);
	
	getContentPane().add(top);
	pack ();
    }

    JComponent createOptionPane () {
	JPanel pane = new JPanel (new BorderLayout (0, 5));
	pane.setBorder(BorderFactory.createCompoundBorder
		       (BorderFactory.createTitledBorder(""),
			BorderFactory.createEmptyBorder(2,2,2,2)));

	scanLink = new JXHyperlink ();
	scanLink.setText("Scan data source for import fields...");
	scanLink.setClickedColor(scanLink.getUnclickedColor());
	scanLink.setToolTipText("Click to start scanning...");
	scanLink.setActionCommand("Scan");
	scanLink.addActionListener(this);
	scanLink.setEnabled(false);

	fieldTable = new JTable (new FieldTableModel ());
	fieldTable.setDefaultEditor
	    (FieldEnum.class, new DefaultCellEditor 
	     (new JComboBox (FieldEnum.values())));
	fieldTable.setShowVerticalLines(false);
	fieldTable.getTableHeader().setReorderingAllowed(false);
	fieldTable.setEnabled(false);
	fieldTable.setRowHeight(20);

	pane.add(scanLink, BorderLayout.NORTH);
	pane.add(new JScrollPane (fieldTable));

	return pane;
    }

    public void setContent (Content<? extends Entity> content) {
	this.content = content;
    }
    public Content<? extends Entity> getImportClass () { return content; }

    public void setName (String name) {
	nameField.setText(name);
    }
    public String getName () { return nameField.getText(); }

    public void setDescription (String desc) {
	descArea.setText(desc);
    }
    public String getDescription () { return descArea.getText(); }


    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
	JFileChooser fileChooser = 
	    UIUtil.getFileChooser();

	if (content == null) {
	    return;
	}
	logger.info("Action: "+cmd + " class: "+content.getEntityClass().getName());

	if ("...".equals(cmd)) {

	    fileChooser.setDialogTitle("Import "+content.getEntityClass().getName());
	    fileChooser.resetChoosableFileFilters();
	    fileChooser.setFileFilter(null);
	    for (FileFilter f : content.getImportFileFilters()) {
		if (fileChooser.getFileFilter() == null) {
		    fileChooser.setFileFilter(f);
		}
		fileChooser.addChoosableFileFilter(f);
	    }

	    int ans = fileChooser.showOpenDialog(this);	    
	    if (JFileChooser.APPROVE_OPTION == ans) {
		File file = fileChooser.getSelectedFile();
		//sourceField.setText(file.getCanonicalPath());
		sourceField.setText(file.toURI().toString());
		actionPerformed(new ActionEvent(e.getSource(), 0, "scan"));
	    }
	}
	else if ("import".equalsIgnoreCase(cmd)) {

	    //FileFilter format = fileChooser.getFileFilter();
	    try {
		Map<String,Object> options = new HashMap<String,Object> ();
		options.put("standardize", standardizeCb.isSelected());
		options.put("saltstrip", saltStripCb.isSelected());
		options.put("description", getDescription());
		doImport (content, nameField.getText(), 
			  sourceField.getText(), options,
			  ((FieldTableModel)fieldTable
			   .getModel()).getPropertyMapping());
		sourceField.setText(null); // clear this out..
		setVisible (false);
	    }
	    catch (Exception ex) {
		statusField.setText(ex.getMessage());
	    }
	}
	else if ("scan".equalsIgnoreCase(cmd)) {
	    new ScanDataSource (sourceField.getText(), 100).execute();
	}
	else {
	    setVisible (false);
	}
    }

    /*
     * Override by subclass to do actual work
     */
    protected void doImport (Content<? extends Entity> clazz, String name, String source, 
			     Map<String,Object> options, Map<String, String[]> props) {
    }

    void setBusy (boolean busy) {
	if (busy) this.busy.start();
	else this.busy.stop();
	importBtn.setEnabled(!busy);
	closeBtn.setEnabled(!busy);
    }

    public void propertyChange (PropertyChangeEvent e) {
	String name = e.getPropertyName();
    }

    public void caretUpdate (CaretEvent e) {
	String name = nameField.getText().trim();
	String source = sourceField.getText().trim();
	boolean enable = name.length() > 0 && source.length() > 0;
	importBtn.setEnabled(enable);
	scanLink.setEnabled(enable);
	fieldTable.setEnabled(enable);
    }

    public static void main (String[] argv) throws Exception {
	ImportDialog id = new ImportDialog (null);
	id.setVisible(true);
    }

}
