package bard.ui.main;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import java.text.NumberFormat;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import net.miginfocom.swing.MigLayout;
import com.jidesoft.swing.RangeSlider;

import chemaxon.struc.Molecule;

import bard.chem.ChemUtil;
import bard.util.Histogram;

import bard.ui.common.HistogramPlotPane;
import bard.ui.content.*;
import bard.ui.util.UIUtil;


public class FilterPane extends JPanel {

    public static abstract class MolProp {
	String name;
	String desc;
	boolean isdef; // is default?
	Number min, max;
	RangeSlider slider;
	HistogramPlotPane hpp;

	MolProp (String name, String desc, boolean isdef) {
	    this.name = name;
	    this.desc = desc;
	    this.isdef = isdef;
	}
	abstract Number calc (Molecule mol);
	boolean isDefault () { return isdef; }

	double getValue (int x) {
	    double a = min.doubleValue();
	    return a + x*(max.doubleValue()-a)/100.;
	}
	double getSliderLow () {
	    return getValue (slider.getLowValue());
	}
	double getSliderHigh () {
	    return getValue (slider.getHighValue());
	}
    }

    Component createPropPane () {
        props = new MolProp[] {
	};

	JPanel pane = new JPanel (new BorderLayout (0, 5));
	pane.add(UIUtil.iTunesScrollPane(filterPane = new JPanel ()));
	//filterPane.setBackground(Color.white);
	filterPane.setBorder
	    (BorderFactory.createCompoundBorder
	     (BorderFactory.createEmptyBorder(2,2,2,2),
	      BorderFactory.createTitledBorder("Property filters")));
        
	filterPane.setLayout(new MigLayout ("", "[right][grow,fill][]", ""));
	for (MolProp p : props) {
	    if (p.slider == null) {
		p.slider = new RangeSlider ();
		//p.slider.setBackground(Color.white);
		p.slider.setPreferredSize(new Dimension (50, 20));
		p.slider.setPaintTrack(true);
		p.slider.setPaintTicks(true);
		p.slider.setLowValue(0);
		p.slider.setHighValue(100);
		//slider.setPaintLabels(true);
		p.slider.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		final JLabel min = new JLabel ();
		p.slider.putClientProperty("MinLabel", min);
		final JLabel max = new JLabel ();
		p.slider.putClientProperty("MaxLabel", max);
		p.slider.putClientProperty("MolProp", p);
		p.slider.addChangeListener(new ChangeListener () {
			public void stateChanged (ChangeEvent e) {
			    MolProp p = (MolProp)((JComponent)e.getSource())
				.getClientProperty("MolProp");
			    min.setText(String.format
					("%1$.1f", p.getSliderLow()));
			    max.setText(String.format
					("%1$.1f", p.getSliderHigh()));
			}
		    });
		p.hpp = new HistogramPlotPane ();
		p.hpp.getPlot().getDomainAxis().setVisible(false);
		p.hpp.setPreferredSize(new Dimension (100, 50));
	    }

	    addSeparator (filterPane, p.name);
	    filterPane.add(p.hpp, "skip,wrap 10");
	    filterPane.add((Component)p.slider.getClientProperty("MinLabel"));
	    filterPane.add(p.slider);
	    filterPane.add((Component)p.slider.getClientProperty("MaxLabel"),
			   "wrap 10");
	}

	return pane;
    }

    static void addSeparator (JPanel pane, String title) {
	pane.add(new JLabel (title), 
		 "gapbottom 1, span, split 2, aligny center");
	pane.add(new JSeparator (), "gapleft rel,growx");	
    }

    MolProp[] props;
    JPanel filterPane;

    public FilterPane () {
        super (new BorderLayout ());
        add (createPropPane ());
    }

    protected void updateFilterBounds () {
        for (MolProp p : props) {
            RangeSlider slider = p.slider;
            JLabel min = (JLabel)slider.getClientProperty("MinLabel");
            JLabel max = (JLabel)slider.getClientProperty("MaxLabel");
            System.err.println(p.name+" min="+p.min+" max="+p.max);
            if (p.min instanceof Integer) {
                min.setText(p.min.toString());
                max.setText(p.max.toString());
            }
            else {
                min.setText(String.format
                            ("%1$.1f", p.min.doubleValue()));
                max.setText(String.format
                            ("%1$.1f", p.max.doubleValue()));
            }
        }
        filterPane.revalidate();
        filterPane.repaint();            
    }

    public void setContent (Content content) {
    }
}
