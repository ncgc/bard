
package bard.ui.main;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.DataSource;
import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.EntityValue;
import bard.core.IntValue;
import bard.core.MolecularData;
import bard.core.MolecularValue;
import bard.core.Project;
import bard.core.ServiceIterator;
import bard.core.StringValue;
import bard.core.Value;
import bard.core.adapter.CompoundAdapter;
import bard.core.impl.MolecularDataJChemImpl;
import bard.core.rest.RESTCompoundService;
import bard.core.rest.RESTEntityServiceManager;
import bard.ui.common.DefaultBottomBar;
import bard.ui.content.AbstractTask;
import bard.ui.content.CompoundContent;
import bard.ui.content.Content;
import bard.ui.content.ContentManager;
import bard.ui.content.DefaultState;
import bard.ui.content.State;
import bard.ui.content.View;
import bard.ui.event.ContentChangeEvent;
import bard.ui.event.SearchEvent;
import bard.ui.event.ViewChangeEvent;
import bard.ui.explodingpixels.macwidgets.BottomBar;
import bard.ui.explodingpixels.macwidgets.ITunesNavigationHeader;
import bard.ui.explodingpixels.macwidgets.LabeledComponentGroup;
import bard.ui.explodingpixels.macwidgets.MacButtonFactory;
import bard.ui.explodingpixels.macwidgets.MacUtils;
import bard.ui.explodingpixels.macwidgets.MacWidgetFactory;
import bard.ui.explodingpixels.macwidgets.SourceList;
import bard.ui.explodingpixels.macwidgets.SourceListCategory;
import bard.ui.explodingpixels.macwidgets.SourceListClickListener;
import bard.ui.explodingpixels.macwidgets.SourceListContextMenuProvider;
import bard.ui.explodingpixels.macwidgets.SourceListControlBar;
import bard.ui.explodingpixels.macwidgets.SourceListItem;
import bard.ui.explodingpixels.macwidgets.SourceListModel;
import bard.ui.explodingpixels.macwidgets.SourceListSelectionListener;
import bard.ui.explodingpixels.macwidgets.SourceListToolTipProvider;
import bard.ui.explodingpixels.macwidgets.SourceListWillExpandListener;
import bard.ui.explodingpixels.macwidgets.UnifiedToolBar;
import bard.ui.explodingpixels.macwidgets.plaf.HudComboBoxUI;
import bard.ui.explodingpixels.widgets.WindowUtils;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import bard.util.BardHttpClient;
import ca.odell.glazedlists.EventList;
import chemaxon.formats.MolImporter;
import chemaxon.struc.Molecule;
import edu.stanford.ejalbert.BrowserLauncher;
import gov.nih.ncgc.util.MolStandardizer;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;
import org.jdesktop.application.Application;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.LocalStorage;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.JXErrorPane;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BARD extends SingleFrameApplication 
    implements SourceListClickListener, 
	       SourceListSelectionListener, 
	       SourceListWillExpandListener,
               SourceListToolTipProvider,
	       PropertyChangeListener,
	       EventSubscriber, ActionListener {

    private static Logger logger = Logger.getLogger(BARD.class.getName());

    /*
     * client properties
     */
    static final String CLIENT_PROP_COLLECTION = "tag-pane";
    static final String CLIENT_PROP_CONTEXTPANE = "context-pane";
    static final String CLIENT_PROP_INFOSPLIT = "info-split";
    static final String CLIENT_PROP_CURATORPANE = "curator-pane";
    static final String CLIENT_PROP_FILTERSPANE = "filters-pane";

    /*
     * top-level source list categories
     */
    static final String CONTENTS = "Contents";
    static final String SEARCHES = "Searches";
    static final String FACETS = "Facets";
    static final String FILTERS = "Filters";
    static final String TASKS = "Tasks";
    static final String COLLECTIONS = "Collections";

    private FeedbackDialog feedbackDialog;

    private String appTitle;
    //private JLabel statusLabel;
    private JComponent viewPane;
    private JComponent homePane;
    private DefaultBottomBar defaultBottomBar;
    private JPanel bottomBarPanel;
    private ViewControl viewControl;
    //private JSlider slider;
    private JSplitPane infoSplit;
    //private SearchTextField searchControl;

    private AbstractButton homeBtn;
    private AbstractButton reloadBtn;
    private SearchPane searchPane;

    private AbstractButton prevBtn;
    private AbstractButton nextBtn;
    private LinkedList<DisplayState> history = new LinkedList<DisplayState>();
    private LinkedList<DisplayState> queue = new LinkedList<DisplayState>();

    private SourceList sourceList;
    private Map<String, SourceListCategory> categories = 
	new HashMap<String, SourceListCategory>();

    private URI openURI;
    
    private EntityServiceManager esm;
    private int isLoading = 0;
    
    private String localETagsFile = "BARDCollections.xml";
    private boolean localETagsLoaded = false;
    private ArrayList<Object> publicETags = new ArrayList<Object>();
    private ImportDialog importDialog;

    class ItemToolTipProvider implements SourceListToolTipProvider {
	ItemToolTipProvider () {
	}

	public String getTooltip(SourceListCategory category) {
	    return null;
	}

	public String getTooltip (SourceListItem item) {
	    return null;
	}
    }

    class CollectionsLoader extends SwingWorker<Throwable, SourceListItem> {
        ContentSourceListCategory collections;
        SourceListModel sourceList;
        EntityServiceManager esm;
        ContentManager contentManager;
        SourceListItem loading;

        CollectionsLoader (SourceListModel sourceList, 
                           ContentSourceListCategory collections, 
                           EntityServiceManager esm) {
            this.sourceList = sourceList;
            this.collections = collections;
            this.esm = esm;
            loading = new SourceListItem ("Loading...");
            loading.setBusy(true);

            contentManager = ContentManager.getInstance(esm);
            //contentManager = new ContentManager (esm);
            collections.setContentManager(contentManager);
        }

        @Override
        protected Throwable doInBackground () {
            try {
                publish (loading);
                for (EntityService es : esm.getServices()) {
                    ServiceIterator<Value> iter = es.etags(null);
                    ArrayList<SourceListItem> items = 
                            new ArrayList<SourceListItem>();
                    // max of 5 for now
                    for (Value t : iter.next(5)) {
                        Content content = contentManager.add(es, t);
                        items.add(new ContentSourceListItem 
                                (content));
                        BARD.this.publicETags.add(content.getResource()); 
                    }
                    publish (items.toArray(new SourceListItem[0]));
                }
                LocalStorage saveSession = getContext().getLocalStorage();
                try {
                    ArrayList<String> privateETags = (ArrayList<String>)saveSession.load(localETagsFile);
                    if (privateETags == null) {
                        localETagsLoaded = true;
                        return null;
                    }
                    ArrayList<SourceListItem> items = 
                            new ArrayList<SourceListItem>();
                    for (String mark: privateETags) {
                        Value etag = (Value)new ObjectInputStream(new ByteArrayInputStream(bard.util.Base64.decodeFast(mark.toCharArray()))).readObject();
                        Value type = etag.getChild("type");
                        Class clazz = null;
                        if (type != null) {
                            if (type.getValue().toString().endsWith("Compound")) clazz = Compound.class;
                            else if (type.getValue().toString().endsWith("Project")) clazz = Project.class;
                            else if (type.getValue().toString().endsWith("Assay")) clazz = Assay.class;
                            else if (type.getValue().toString().endsWith("ETag")) clazz = Compound.class;
                        }
                        // update with latest version from service (name changes, etc.)
                        Value newEtag = null;
                        if (clazz != null) newEtag = esm.getService(clazz).getETag(etag.getId());

                        if (newEtag != null && newEtag.getChild("type") != null) {
                            ArrayList<String> childIds = new ArrayList<String>();
                            for (Value item: newEtag.getChildren()) {
                                if (!childIds.contains(item.getId()))
                                    childIds.add(item.getId());
                            }
                            for (String childId: childIds) {
                                if (etag.getChildren(childId).size() > 1 || newEtag.getChildren(childId).size() > 1) {
                                    for (Value child: etag.getChildren(childId))
                                        etag.remove(child);
                                    for (Value child: newEtag.getChildren(childId))
                                        etag.add(child);
                                } else {
                                    String itemValue = newEtag.getChild(childId).getValue().toString();
                                    Value origItem = etag.getChild(childId);
                                    String origValue = null;
                                    if (origItem != null) {
                                        origValue = origItem.getValue().toString();
                                        if (!itemValue.equals(origValue))
                                            etag.getChild(childId).setValue(newEtag.getChild(childId));
                                    } else {
                                        etag.add(newEtag.getChild(childId));
                                    }
                                }
                            }
                        } else { // couldn't find etag --- let's try to create a new one with all the objects in old etag
                            ArrayList<String> entities = new ArrayList<String>();
                            if (clazz != null) {
                                for (Value ev : etag.getChildren(clazz.toString())) {
                                    entities.add(ev.getValue().toString());
                                }
                            }
                            if (clazz != null && entities != null && entities.size() > 0) {
                                String name = etag.getChild("name") == null ? null : etag.getChild("name").getValue().toString();
                                JOptionPane.showMessageDialog(getMainFrame(), "Could not find previously saved resource: "+name+"; recreating from local file.", "Unable to open etag "+etag.getId(), JOptionPane.WARNING_MESSAGE);
                                newEtag = esm.getService(clazz).newETag(etag.getChild("name").getValue().toString(), entities);
                                if (type.getValue().toString().endsWith("ETag")) { // create composite etag
                                    entities = new ArrayList<String>();
                                    for (Value ev: etag.getChildren(Assay.class.toString())) {
                                        entities.add(((EntityValue)ev).getValue().toString());
                                    }
                                    Value newEtag2 = esm.getService(Assay.class).newETag(etag.getChild("name").getValue().toString(), entities);
                                    ArrayList<String> etagids = new ArrayList<String>();
                                    etagids.add(newEtag.getId());
                                    etagids.add(newEtag2.getId());
                                    etag = esm.getService(clazz).newCompositeETag(etag.getChild("name").getValue().toString(), etagids);
                                    etag = esm.getService(clazz).getETag(etag.getId());
                                } else {
                                    etag = newEtag;
                                }
                            } else {
                                String name = etag.getChild("name") == null ? null : etag.getChild("name").getValue().toString();
                                JOptionPane.showMessageDialog(getMainFrame(), "Could not find previously saved resource: "+name, "Unable to open etag "+etag.getId(), JOptionPane.WARNING_MESSAGE);
                                etag = null;
                                clazz = null;
                            }
                        }
                        if (clazz != null && etag != null) {
                            Content content = contentManager.add(clazz, etag);
                            if (etag.equals(content.getResource()))
                                try {
                                    EventList el = content.getState().getEntities();
                                    try {
                                        el.getReadWriteLock().writeLock().lock();
                                        HashMap<String, Integer> lookup = new HashMap<String,Integer>();
                                        for (int i=0; i<el.size(); i++)
                                            lookup.put(((Entity)el.get(i)).getId().toString(), i);
                                        int elSize = el.size();
                                        for (Value child: etag.getChildren(content.getEntityClass().toString())) {
                                            EntityValue entValue = (EntityValue)child;
                                            Entity newEnt = (Entity)content.getEntityClass().newInstance();
                                            newEnt.setEntityService(content.getService());
                                            newEnt.setId(entValue.getValue().toString());
                                            if (entValue.getChild("Name") != null)
                                                newEnt.setName(entValue.getChild("Name").getValue().toString());
                                            else if (entValue.getChild("Synonym") != null)
                                                newEnt.setName(entValue.getChild("Synonym").getValue().toString());
                                            if (lookup.containsKey(entValue.getValue())) { // add annotations from loaded compound to new compound
                                                Entity freshEnt = (Entity) el.get(lookup.get(entValue.getValue()));
                                                for (Value value: freshEnt.getValues()) {
                                                    newEnt.add(value);
                                                }
                                                for (Value value: entValue.getChildren()) {
                                                    if (freshEnt.getValue(value.getId()) == null)
                                                        newEnt.add(value);
                                                    else if (value.getId() == Compound.MolecularValue) {
                                                        newEnt.remove(newEnt.getValue(Compound.MolecularValue));
                                                        newEnt.add(value);
                                                    }
                                                }
                                            } else { // add new compound to eventList
                                                for (Value value: entValue.getChildren()) {
                                                    newEnt.add(value);
                                                }
                                            }
                                            el.add(newEnt);
                                        }
                                        for (int i=0; i<elSize; i++) {
                                            el.remove(0);
                                        }
                                        etag.remove(etag.getChild("count"));
                                        new IntValue(etag, "count", el.size());
                                    } catch (Exception e) {e.printStackTrace();}
                                    finally {
                                        el.getReadWriteLock().writeLock().unlock();
                                    }
                                    if (content instanceof CompoundContent) {
                                        java.util.List<String> currentCols = ((CompoundContent)content).getDataColumns();
                                        Map<String, String> colFields = new HashMap<String, String>();
                                        for (Value child: etag.getChildren("DataColumn"))
                                            if (!currentCols.contains(child.getValue().toString()))
                                                colFields.put(child.getValue().toString(), "Text");
                                        ((CompoundContent)content).addDataColumns(colFields);
                                        content.setView(View.Type.List);
                                    }
                                } catch (Exception ex) {ex.printStackTrace();}
                            items.add(new ContentSourceListItem(content));
                        }
                    }
                    publish (items.toArray(new SourceListItem[0]));
                    localETagsLoaded = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            catch (Exception ex) {
                return ex;
            }
            return null;
        }

        @Override
        protected void process (SourceListItem... items) {
            for (SourceListItem i : items) {
                sourceList.addItemToCategory(i, collections);
            }
        }

        @Override
        protected void done () {
            try {
                // now remove loading
                sourceList.removeItemFromCategory(loading, collections);
                if (openURI != null) {
                    setView (openURI);
                }
                Throwable t = get ();
                if (t != null) {
                    JXErrorPane.showDialog(t);
                }
            }
            catch (Exception ex) {
                JXErrorPane.showDialog(ex);
            }
        }
    }

    protected BARD () {
    }

    @Override
    protected void initialize (String[] argv) {
        //"http://bard.nih.gov/api/latest";
        String bard = Application.getInstance()
            .getContext().getResourceMap().getString("BARD.home"); 
        if (argv.length > 0) {
            bard = argv[0];
        }
        logger.info("##### BARD home: "+bard+" ######");
        logger.getParent().setLevel(Level.SEVERE);

        if (argv.length > 1) {
            try {
                openURI = new URI (argv[1]);
                logger.info("##### openURI: "+openURI);
            }
            catch (Exception ex) {
                logger.log(Level.SEVERE, "** Bogus open URI: "+argv[1]);
            }
        }

        esm = new RESTEntityServiceManager (bard);
    }

    protected EntityServiceManager getServiceManager () { 
        return esm;
    }

    protected void setServiceManager (EntityServiceManager esm) {
        this.esm = esm;
    }

    protected ContentManager getContentManager () {
        return ContentManager.getInstance(esm);
    }

    @Override
    protected void ready () {
	FrameView view = getMainView ();
	JFrame frame = view.getFrame();
	appTitle = frame.getTitle();
	String ver = System.getProperty("bard-version", null);
	if (ver != null) {
	    appTitle += " ["+ver+"]";
	    frame.setTitle(appTitle);
	}

        addExitListener (new ExitListener () {
                public boolean canExit (EventObject e) {
                    return true;
                }

                public void willExit (EventObject e) {
                }
            });

	UnifiedToolBar toolbar = createToolBar ();
	toolbar.getComponent().setPreferredSize(new Dimension (0, 48));
	//toolbar.installWindowDraggerOnWindow(frame);

	frame.add(toolbar.getComponent(), BorderLayout.NORTH);
	frame.add(createMainPane ());
	//frame.setIconImage(getImageIcon("Icon.siphonify32").getImage());

        MacUtils.makeWindowLeopardStyle(frame.getRootPane());
        WindowUtils.createAndInstallRepaintWindowFocusListener(frame);

	// now create the menu bar
	//view.setMenuBar(createMenuBar ());

        //!!!!	show (view);
        SplashScreen.getInstance().setText("Loading content ...");
        SplashScreen.getInstance().setValue(15);
        isLoading = 15;
	if (frame.getWidth() < 800 || frame.getHeight() < 600) {
	    //frame.setSize(800, 600);
	}

	EventBus.subscribe(ContentChangeEvent.class, this);
	EventBus.subscribe(ViewChangeEvent.class, this);
	EventBus.subscribe(SearchEvent.class, this);
	EventBus.subscribe(HyperlinkEvent.class, this);

        goHome ();
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    setupSourceList ();
//                    if (openURI != null) {
//                        setView (openURI, true); //!!! this was false, but on initial startup, the content needs to loaded
//                    }
		}
	    });

	new javax.swing.Timer(15000, new ActionListener () {
	    public void actionPerformed (ActionEvent e) {
		if (isLoading < 100) {
		    SplashScreen.getInstance().setVisible(false);
		    show(getMainView());
		    isLoading = 100;
		}
	    }
	}).start();
    
    }

    protected JMenuBar createMenuBar () {
	JMenuBar menubar = new JMenuBar ();
	menubar.setName("mainMenuBar");
	menubar.add(createFileMenu ());
	menubar.add(createEditMenu ());
	menubar.add(createViewMenu ());
	//menubar.add(createWindowMenu ());
	menubar.add(createHelpMenu ());
	return menubar;
    }

    JMenu createFileMenu () {
	JMenu menu = new JMenu ("File");
	JMenuItem item;

        menu.add(item = new JMenuItem ("Exit"));
        item.addActionListener(this);
	
	return menu;
    }

    JMenu createEditMenu () {
	JMenu menu = new JMenu ("Edit");
	JMenuItem item;

	return menu;
    }

    JMenu createViewMenu () {
	JMenu menu = new JMenu ("View");
	JMenuItem item;

	return menu;
    }

    JMenu createWindowMenu () {
	JMenu menu = new JMenu ("Window");
	JMenuItem item;

	return menu;
    }

    JMenu createHelpMenu () {
	JMenu menu = new JMenu ("Help");
	JMenuItem item;
	
	return menu;
    }

    private ArrayList<String> getETags() {
	ArrayList<String> privateETags = new ArrayList<String>();
	for (SourceListCategory slc: BARD.this.sourceList.getModel().getCategories()) {
	    if (slc.getText().equals(BARD.COLLECTIONS))
		for (SourceListItem sli: slc.getItems()) {
		    if (sli instanceof ContentSourceListItem) {
			ContentSourceListItem csli = (ContentSourceListItem)sli;
			Object resource = csli.getContent().getResource();
			if (!publicETags.contains(resource))
			    if (resource instanceof Value) {
			        Value etag = (Value)resource;
                                //                          if (!"de6add5339d1af20".equals(etag.getId()) && !"bee2c650dca19d5f".equals(etag.getId())) {
			        // add in IDs of current objects
			        ContentManager cm = csli.getContent().getContentManager();
			        for (Content content: cm.getContents()) {
			            if (etag.equals(content.getResource()))
			                try {
			                    for (Value child: etag.getChildren(content.getEntityClass().toString()))
			                        etag.remove(child);
			                    for (Object ent: content.getEntities()) {
			                        if (Entity.class.isAssignableFrom(ent.getClass())) {
			                            EntityValue ev = new EntityValue(etag, ent.getClass().toString(), ((Entity)ent).getId().toString());
			                            //etag.add(ev); this is redundant ... EntityValue instantiation/etag inheritance already adds it
			                            new StringValue(ev, "Synonym", ((Entity)ent).getName());
			                            for (Value item: ((Entity)ent).getValues())
			                                ev.add(item);
			                        }
			                    }
			                    if (content instanceof CompoundContent) {
			                        for (Value child: etag.getChildren("DataColumn"))
			                            etag.remove(child);
			                        for (String field: ((CompoundContent)content).getDataColumns())
			                            new StringValue(etag, "DataColumn", field);
			                    }
			                } catch (Exception ex) {ex.printStackTrace();}
			        }
			        try {
			            ByteArrayOutputStream baos = new ByteArrayOutputStream();
			            ObjectOutputStream oos = new ObjectOutputStream(baos);
			            oos.writeObject(etag);
			            char[] stream = bard.util.Base64.encodeToChar(baos.toByteArray(), false);
			            privateETags.add(new String(stream));
			        } catch (IOException e) {
			            e.printStackTrace();
			        }
			        //			    }
			    }
		    }
		}
	}
	return privateETags;
    }

    public void actionPerformed (ActionEvent e) {
        String cmd = e.getActionCommand();
        if (cmd.equalsIgnoreCase("exit")) {
            shutdown ();
        }
    }

    @Override
    protected void shutdown () {
	shutdown (true);
    }

    protected void shutdown (boolean force) {
	LocalStorage saveSession = getContext().getLocalStorage();
	try {
	    if (localETagsLoaded || 
		    JOptionPane.showConfirmDialog(getMainFrame(), 
			    "Overwrite collections from prior BARD session with current set of collections?",
			    "Error saving collections", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)	
		saveSession.save(getETags(), localETagsFile);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	super.shutdown(); // save session
	//logDialog.resetLogHandlers(); 
    }

    /*
    @Override
    protected void end () {
    }
    */

    @Override
    protected void startup () {
	// do nothing
    }

    protected UnifiedToolBar createToolBar () {
        UnifiedToolBar toolbar = new UnifiedToolBar ();
	Box spacer = Box.createHorizontalBox();
	spacer.add(Box.createHorizontalStrut(5));
	toolbar.addComponentToLeft(spacer);

	/*
	spacer = Box.createHorizontalBox();
	spacer.add(Box.createHorizontalStrut(10));
	toolbar.addComponentToLeft(spacer);
	*/

        homeBtn = MacButtonFactory.makeUnifiedToolBarButton
                (new JButton ("", IconFactory.App.Home.getIcon()));
        homeBtn.setToolTipText("Home");
        homeBtn.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                goHome ();
            }
        });
        toolbar.addComponentToLeft(homeBtn);

	prevBtn = MacButtonFactory.makeUnifiedToolBarButton
	    (new JButton ("", IconFactory.Toolbar.Backward.getIcon()));
        prevBtn.setEnabled(false);
        prevBtn.setToolTipText("Go back");
	prevBtn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
			prev();
		}
	    });
	toolbar.addComponentToLeft(prevBtn);

	nextBtn = MacButtonFactory.makeUnifiedToolBarButton
	    (new JButton ("", IconFactory.Toolbar.Forward.getIcon()));
        nextBtn.setEnabled(false);
        nextBtn.setToolTipText("Move forward");
	nextBtn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
			next();
		}
	    });
	toolbar.addComponentToLeft(nextBtn);

	reloadBtn = MacButtonFactory.makeUnifiedToolBarButton
	    (new JButton ("", IconFactory.Toolbar.Refresh.getIcon()));
	reloadBtn.setToolTipText("Reload content");
	reloadBtn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
                    reload ();
		}
	    });
	toolbar.addComponentToLeft(reloadBtn);

	AbstractButton importBtn = MacButtonFactory.makeUnifiedToolBarButton
		(new JButton ("", IconFactory.Misc.DBImport.getIcon()));
	importBtn.setEnabled(true);
	importBtn.setToolTipText("Import data");
        importBtn.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                doImport ();
            }
        });
	toolbar.addComponentToLeft(importBtn);

	AbstractButton exportBtn = MacButtonFactory.makeUnifiedToolBarButton
		(new JButton ("", IconFactory.Misc.DBExport.getIcon()));
	exportBtn.setEnabled(true);
	exportBtn.setToolTipText("Export data");
	exportBtn.addActionListener(new ActionListener () {
	    public void actionPerformed (ActionEvent e) {
		doExport ();
	    }
	});
	toolbar.addComponentToLeft(exportBtn);

	
	spacer = Box.createHorizontalBox();
	spacer.add(Box.createHorizontalStrut(10));
	toolbar.addComponentToLeft(spacer);


	AbstractButton feedbackBtn = MacButtonFactory.makeUnifiedToolBarButton
	    (new JButton ("", IconFactory.App.Balloon.getIcon()));
	feedbackBtn.setToolTipText
	    ("Please provide feedback to help us improve "
	    +"the software");
	feedbackBtn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    if (feedbackDialog == null) {
			feedbackDialog = new FeedbackDialog (getMainFrame ());
			UIUtil.centerComponent(getMainFrame(), feedbackDialog);
		    }
		    feedbackDialog.setVisible(true);
		}
	    });

	viewControl = new ViewControl ();
	viewControl.setEnabled(false);
	viewControl.addPropertyChangeListener
	    ("view", new PropertyChangeListener () {
		    public void propertyChange (final PropertyChangeEvent e) {
		    logger.info(e.getPropertyName() + ": old=" + e.getOldValue()
                    + " new=" + e.getNewValue());
// If this is run later, then on startup from a specific record, the propertyChangeEvent from the initial contentSourceListItem load comes after the specific setView call from loading openURI
//		    SwingUtilities.invokeLater(new Runnable () {
//			    public void run () {

				setView ((bard.ui.content.View.Type)e.getNewValue());
//			    }
//			});
		}
	    });

	toolbar.addComponentToRight
	    (new LabeledComponentGroup ("", viewControl).getComponent());
	toolbar.addComponentToRight(spacer);
	//toolbar.addComponentToRight(spacer);

	//searchControl = new SearchTextField ();

        /*
	toolbar.addComponentToRight
	    (new LabeledComponentGroup ("", searchControl).getComponent());
        */
	//toolbar.addComponentToRight(spacer);
	toolbar.addComponentToRight(feedbackBtn);
	toolbar.addComponentToRight(spacer);

	/*
	AbstractButton infoBtn = MacButtonFactory.makeUnifiedToolBarButton
	    (new JButton ("", getImageIcon ("Icon.information")));
	toolbar.addComponentToRight(infoBtn);
	infoBtn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    bouncing.go();
		    goHome (false);
		    ((CardLayout)homePane.getLayout()).show(homePane, "info");
		}
	    });
	*/

	return toolbar;
    }

    protected void goHome () {
        SourceListCategory contents = categories.get(CONTENTS);
        DisplayState display = new DisplayState(null, null, null);
        if (contents != null)
            for (SourceListItem item : contents.getItems()) {
                ContentSourceListItem csi = (ContentSourceListItem)item;
                display = new DisplayState(csi, null, null); // state set to null so we will know to replace this with first search result
            }
        pushDisplayState(display);
        //System.err.println("Go home!");
	goHome (true);
    }

    protected void goHome (boolean showHome) {
	getMainFrame().requestFocusInWindow();
	viewControl.setEnabled(false);
	sourceList.clearSelection();
	//statusLabel.setText(null);
	getMainFrame().setTitle(appTitle);
	updateBottomBarComponent(defaultBottomBar);
	//slider.setEnabled(false);
	//reloadBtn.setEnabled(false);
	if (showHome) {
	    ((CardLayout)homePane.getLayout()).show(homePane, "home");
	}
    }

    public void onEvent (Object event) {
        if (event instanceof SearchEvent) {
            onEvent ((SearchEvent)event);
        }
        else if (event instanceof ContentChangeEvent) {
            onEvent ((ContentChangeEvent)event);
        }
        else if (event instanceof ViewChangeEvent) {
            ViewChangeEvent vce = (ViewChangeEvent)event;
            viewControl.setView(vce.getNewView());
        }
        else if (event instanceof HyperlinkEvent) {
            URI uri = UIUtil.getLinkURI((HyperlinkEvent)event);
            setView (uri);
        }
    }

    public void onEvent (SearchEvent e) {
    	// forward to the search event
    	searchPane.setQuery(e.getQuery());
    	searchPane.onEvent(e);
    	if (e.getQuery() instanceof bard.core.StructureSearchParams)
            try {
                setView(new java.net.URI("bard://compounds?view=List"));
            } catch (URISyntaxException e1) {
                e1.printStackTrace();
            }
        else
    	    ((CardLayout)homePane.getLayout()).show(homePane, "home");
    }

    public void onEvent (ContentChangeEvent<?> e) {
	Content<?> content = e.getContent();
	if (e.getName().equals(ContentChangeEvent.STATE)) { 		
	    if (isLoading < 100) {
		final State<?> state = (State<?>)e.getNewValue();
		isLoading += 20;
		if (isLoading < 100) {
		    SplashScreen.getInstance().setText("Loading content ..."+state.getContent().getName());
		    SplashScreen.getInstance().setValue(isLoading);
		} else {
		    SplashScreen.getInstance().setVisible(false);
		    show(getMainView());
		}
	    }

	    // It doesn't really matter if selected, as all three contentsources should now be synchronized
	    ContentSourceListItem updateSearch = null;
	    SourceListCategory contents = categories.get(CONTENTS);
	    for (SourceListItem item : contents.getItems()) {
		if (item instanceof ContentSourceListItem) {
		    ContentSourceListItem csi = (ContentSourceListItem)item;
		    if (csi != null && csi.getContent() == content) {
			updateSearch = csi;
		    }
		}
	    }
		
	    final State<?> state = (State<?>)e.getNewValue();
	    if (updateSearch != null) {
                if (prevBtn.isEnabled() != history.size() > 1)
                    prevBtn.setEnabled(history.size()>1);
                if (nextBtn.isEnabled() != queue.size() > 0)
                    nextBtn.setEnabled(queue.size()>0);

                searchPane.setSearch(state);
		
		// also try to replace home in history with actual results
                if (history.peek().content == updateSearch) {
                    if (history.peek().state == null) {
                        history.pop();
                        history.push(new DisplayState(updateSearch, null, state));
                    } else if (history.peek().state == e.getOldValue() && updateSearch.getContent().findState((State)e.getOldValue()) == -1) {
                        pushDisplayState(new DisplayState(updateSearch, null, state));
                    }
		}
	    }
	}
	String prop = e.getName();
	if (prop.equals(ContentChangeEvent.CONTENT) && e.getOldValue() == null) { // new collection
	    ContentSourceListItem item = new ContentSourceListItem(e.getContent());
	    item.setText((String)e.getNewValue());
	    SourceListCategory category = categories.get(COLLECTIONS);
	    sourceList.getModel().addItemToCategory(item, category);
	    sourceList.setSelectedItem(item);
	}
    	
    	if (content != getCurrentContent ()) {
    	    return;
    	}

    	if (prop.equals(ContentChangeEvent.ERROR)) {
    	    Throwable ex = (Throwable)e.getNewValue();
    	    JOptionPane.showMessageDialog
    	    (getMainFrame(), ex.getMessage(), 
    	            "Error", JOptionPane.ERROR_MESSAGE);
    	    return;
    	}
    	else if (prop.equals(ContentChangeEvent.VIEW)) {
    	    viewControl.setView((bard.ui.content.View.Type)e.getNewValue());
    	}
    	else if (prop.equals(ContentChangeEvent.STATE)) {
    	    final State<?> state = (State<?>)e.getNewValue();
    	}
    }

    class CollectionsContextMenu extends JPopupMenu 
	implements SourceListContextMenuProvider, ActionListener {

	private static final long serialVersionUID = 1L;
	ContentSourceListItem xitem;
	JMenuItem renameItem, reloadItem, exportItem, copyItem, delItem;

	CollectionsContextMenu () {
	    JMenuItem item;

	    add (item = new JMenuItem ("Rename"));
	    item.setToolTipText("Rename collection/filter");
	    item.addActionListener(this);
	    renameItem = item;
	    
	    add (item = new JMenuItem ("Reload"));
	    item.setToolTipText("Reload collection/filter");
	    item.addActionListener(this);
	    item.setEnabled(true);
	    reloadItem = item;
	    addSeparator ();

	    add (item = new JMenuItem ("Export"));
	    item.setToolTipText("Export collection/filter");
	    item.addActionListener(this);
	    exportItem = item;
	    addSeparator ();

	    add (item = new JMenuItem ("Copy"));
	    item.setToolTipText("Make a copy of collection/filter");
	    item.addActionListener(this);
	    item.setEnabled(false);
	    copyItem = item;
	    addSeparator ();

	    add (item = new JMenuItem ("Delete"));
	    item.setToolTipText("Delete collection/filter");
	    item.addActionListener(this);
	    delItem = item;
	    
	}

	public JPopupMenu createContextMenu (SourceListItem item) {
	    if (!(item instanceof ContentSourceListItem)) {
		xitem = null;
		return null;
	    }
	    xitem = (ContentSourceListItem)item;
	    return this;
	}

	public void actionPerformed (ActionEvent e) {
	    String cmd = e.getActionCommand();
	    if ("delete".equalsIgnoreCase(cmd)) {
		for (SourceListCategory slc: sourceList.getModel().getCategories())
		    if (slc.getText().equals(BARD.COLLECTIONS))
			sourceList.getModel().removeItemFromCategory(xitem, slc);
	    }
	    else if ("export".equalsIgnoreCase(cmd)) {
		doExport ();
	    }
	    else if ("reload".equalsIgnoreCase(cmd)) {
		xitem.reload();
	    }
	    else if ("copy".equalsIgnoreCase(cmd)) {
		//doCopy ();
	    }
	    else if ("rename".equalsIgnoreCase(cmd)) {
		//sourceList.startEditingItem(xitem);
		String newName = JOptionPane.showInputDialog(sourceList.getComponent(), "Enter new name for collection", "Type name here ...");
		if (newName != null)
		    xitem.setText(newName);
		// need to persist this name change on main server !!!
	    }
	}

	public JPopupMenu createContextMenu (SourceListCategory category) {
	    return null;
	}
	public JPopupMenu createContextMenu () { return null; }

	@Override
	public void show (Component c, int x, int y) {
	    SourceListItem item = sourceList.getSelectedItem();
	    if ((item instanceof ContentSourceListItem)
		&& item == xitem) {
		
		if (BARD.this.publicETags.contains(xitem.getContent().getResource())) {
		    renameItem.setEnabled(false);
		    delItem.setEnabled(false);
		} else {
		    renameItem.setEnabled(true);
		    delItem.setEnabled(true);		    
		}
		
		if (xitem.getContent().isExportSupported())
		    exportItem.setEnabled(true);
		else exportItem.setEnabled(false);
		
		super.show(c, x, y);
	    }
	}
    }

    protected Component createMainPane () {
	sourceList = new SourceList ();
	sourceList.useIAppStyleScrollBars();
	sourceList.addSourceListSelectionListener(this);
	sourceList.addSourceListClickListener(this);
	sourceList.setSourceListContextMenuProvider
	    (new CollectionsContextMenu ());

	SourceListControlBar controlBar = createSourceListControlBar ();
	sourceList.installSourceListControlBar(controlBar);
        /*
	sourceList.setSourceListContextMenuProvider
	    (new CollectionsContextMenu ());
        */
	sourceList.setToolTipProvider(new ItemToolTipProvider ());

	JPanel contentPane = new JPanel (new BorderLayout ());
	contentPane.setBorder(BorderFactory.createEmptyBorder());

	//viewPane = createViewPane ();
	homePane = createHomePane ();
	viewPane = new JPanel (new CardLayout ());

	//JComponent infoPane = createInfoPane ();
	//infoSplit = MacWidgetFactory.createSplitPane(viewPane, infoPane);
	//infoSplit.setContinuousLayout(true);
	//infoSplit.setResizeWeight(.80);
	//infoSplit.setDividerLocation(Integer.MAX_VALUE);

	//homePane.add(infoSplit, "view");
	homePane.add(viewPane, "view");
	contentPane.add(homePane);

	defaultBottomBar = new DefaultBottomBar ();
	defaultBottomBar.setSliderEnabled(false);
	bottomBarPanel = new JPanel(new BorderLayout());
	bottomBarPanel.add(defaultBottomBar.getComponent(), BorderLayout.SOUTH);
	contentPane.add(bottomBarPanel, BorderLayout.SOUTH);
	
	JSplitPane split = MacWidgetFactory.createSplitPaneForSourceList
	    (sourceList, contentPane);
	split.setDividerLocation(200);
	controlBar.installDraggableWidgetOnSplitPane(split);

	return split;
    }
    protected JComponent createInfoPane () {
	JPanel pane = new JPanel (new BorderLayout ());
	pane.setBorder(BorderFactory.createEmptyBorder());
	pane.setMinimumSize(new Dimension (0, 0));

	ITunesNavigationHeader hudPanel = new ITunesNavigationHeader ();
	hudPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
	hudPanel.setLayout(new BorderLayout ());
	hudPanel.setPreferredSize(new Dimension (-1, 30));

	final JPanel hudContent = new JPanel (new CardLayout ());

	JComboBox hudCombo = new JComboBox 
	    (new String[] {"Facets", "Related Contents", "About"});
	hudCombo.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    String item = (String)((JComboBox)e.getSource())
			.getSelectedItem();
		    ((CardLayout)hudContent.getLayout())
			.show(hudContent, item);
		}
	    });
	hudCombo.setUI(new HudComboBoxUI ());
	hudPanel.add(hudCombo);
	
        FilterPane facets = new FilterPane ();
	hudContent.add(facets, "Facets");

	((CardLayout)hudContent.getLayout()).show(hudContent, "Facets");
        pane.putClientProperty(CLIENT_PROP_FILTERSPANE, facets);

	pane.add(hudPanel, BorderLayout.NORTH);
	pane.add(hudContent);
	pane.setPreferredSize(new Dimension (200, -1));

	return pane;
    }

    protected JComponent createHomePane () {
	CardLayout card = new CardLayout ();
	JPanel vpane = new JPanel (card);
	//vpane.setMinimumSize(new Dimension (0, 0));
	//vpane.add(new WelcomePane (), "home");
        searchPane = new SearchPane();
        //searchControl = sp.getSearchField();
        vpane.add(searchPane, "home");
        JPanel p = new JPanel ();
        p.setOpaque(true);
        p.setBackground(Color.white);
        //vpane.add(p, "home");
        //        vpane.add(taskPane = new TaskPane (), "task");
	//vpane.add(entityPane = new EntityPane (), "entity");
	//vpane.add(bouncing = new UIUtil.BouncingHeads(), "info");
	card.show(vpane, "home");
	return vpane;
    }

    private void setSearchPaneContent () {
        SourceListCategory contents = categories.get(CONTENTS);
        Content<?>[] ca = new Content<?>[contents.getItemCount()];
        int index = 0;
        for (SourceListItem item : contents.getItems()) {
            if (item instanceof ContentSourceListItem) {
                ContentSourceListItem csi = (ContentSourceListItem)item;
                ca[index] = csi.getContent();
                index++;
            }
        }
        searchPane.setContents(ca);
	return;
    }

    SourceListControlBar createSourceListControlBar () {
	SourceListControlBar controlBar = new SourceListControlBar ();

	/*
	ActionListener plusActions = new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    String cmd = e.getActionCommand();
		    logger.info("PlusAction: "+cmd);
		    if (cmd.equalsIgnoreCase("data source")) {
		    }
		    else {
			JOptionPane.showMessageDialog
			    (getMainFrame (), "Sorry, currently adding "
			     +cmd+" is not supported!", "Error", 
			     JOptionPane.WARNING_MESSAGE);
		    }
		}
	    };

	Insets margin = new Insets (0,0,0,0);
	PopupMenuCustomizer menu = 
	    new PopupMenuCustomizerUsingStrings 
	    (plusActions, "Compound", "Filter");

	controlBar.createAndAddPopdownButton(MacIcons.PLUS, menu);

	controlBar.createAndAddButton(MacIcons.MINUS, new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    doDelete ();
		}
	    });

	menu = new PopupMenuCustomizerUsingStrings(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    String cmd = e.getActionCommand();
		    if (cmd.equalsIgnoreCase("export data")) {
			doExport ();
		    }
		}
	    }, "Export data");
	controlBar.createAndAddPopdownButton(MacIcons.GEAR, menu);
	*/

	return controlBar;
    }

    protected void setView (bard.ui.content.View.Type view) {
        final Content content = getCurrentContent ();
        
        if (content != null && content.getView().getType() != view) {
            //System.err.println("!set view:"+content.getName()+":"+view);
            ContentSourceListItem csi = (ContentSourceListItem)sourceList.getSelectedItem();
            content.setView(view);
            pushDisplayState(new DisplayState(csi, view, csi.getContent().getState()));
            SwingUtilities.invokeLater(new Runnable () {
                    public void run () {
                        showContent (content);
                    }
                });
        }
    }

    protected void setView (URI uri) {
        UIUtil.BARDUri bard = null;
        try {
            bard = new UIUtil.BARDUri(uri);
        } catch (IllegalArgumentException ex) {;}
        if (bard == null) {
	    try {
		new BrowserLauncher().openURLinBrowser(uri.toASCIIString());
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Can't launch external browser " +uri.toASCIIString(), ex);
	    }
	    return;
        }
        logger.info("## "+bard);

        // etags get added as new contentsourcelistitems if not already loaded
        if (bard.argv().length > 1 && bard.content().toLowerCase().equals("etag")) {
            for (SourceListCategory slc: sourceList.getModel().getCategories())
        	if (slc.getText().equals(BARD.COLLECTIONS))
        	    for (SourceListItem sli: slc.getItems())
        		if (sli instanceof ContentSourceListItem) {
        		    ContentSourceListItem csi = (ContentSourceListItem)sli;
        		    State state = csi.getContent().getState();
        		    if (state != null)
        			if (state.getETag() != null && state.getETag().getId().equals(bard.argv()[1])) {
        	                    readyContentSetView(bard, csi, false);
        			    return;
        			}
        		    Object obj = csi.getContent().getResource();
        		    if (obj instanceof Value && ((Value)obj).getId().equals(bard.argv()[1])) {
        			readyContentSetView(bard, csi, false);
        			return;
        		    }
        		}
            
            // update with latest version from service (name changes, etc.)
            Value etag = esm.getService(Compound.class).getETag(bard.argv()[1]);
            Class clazz = null;
            Value type = etag.getChild("type");
            if (type == null) { // we couldn't find this etag
        	JOptionPane.showMessageDialog(getMainFrame(), "Unable to open "+uri.toString(), "Could not find resource", JOptionPane.WARNING_MESSAGE);
        	return;
            }
            if (type != null && type.getValue().toString().endsWith("Compound")) clazz = Compound.class;
            else if (type != null && type.getValue().toString().endsWith("Project")) clazz = Project.class;
            else if (type != null && type.getValue().toString().endsWith("Assay")) clazz = Assay.class;
            else if (type != null && type.getValue().toString().endsWith("ETag")) clazz = Compound.class;
            Content content = getContentManager().add(clazz, etag);
            ContentSourceListItem csi = new ContentSourceListItem(content);
            for (SourceListCategory slc: sourceList.getModel().getCategories())
        	if (slc.getText().equals(BARD.COLLECTIONS))
        	    sourceList.getModel().addItemToCategory(csi, slc);
            readyContentSetView(bard, csi, false);
            return;
        }
        
        // first check if current state already contains this entity, as with search results
        boolean load = true;
        SourceListCategory contents = categories.get(CONTENTS);
        for (SourceListItem item : contents.getItems()) {
            if (item instanceof ContentSourceListItem) {
        	ContentSourceListItem csi = (ContentSourceListItem)item;
        	if (bard.check(csi.getContent())) {
        	    State state = csi.getContent().getState();
        	    if (bard.argv().length < 2 || bard.argv()[1].length() == 0) {
        		load = false;
        	    }
        	    else if (state != null) {
        		Entity e = state.contains(bard.argv()[1]);
        		if (e != null) {
        		    state.setEntity(e);
        		    load = false;
        		}
        	    }
        	    readyContentSetView(bard, csi, load);
        	}
            }
        }
        
        // if loaded new content above, then blank out other sources to keep state stacks in synch
        if (load)
            for (SourceListItem item : contents.getItems()) {
        	if (item instanceof ContentSourceListItem) {
        	    ContentSourceListItem csi = (ContentSourceListItem)item;
        	    if (!bard.check(csi.getContent())) {
        		csi.getContent().push(new DefaultState(csi.getContent(), new ArrayList()));
        	    }
        	}
            }
    }

    protected void readyContentSetView (UIUtil.BARDUri bard, ContentSourceListItem csi, boolean load) {
        csi.getContent().setView(bard.view());
        sourceList.setSelectedItem(csi); // must set selected item first before state content change event is thrown
        if (load) {
            csi.getContent().open(bard);
        }
        pushDisplayState(new DisplayState(csi, bard.view(), csi.getContent().getState()));
        viewControl.setView(bard.view()); // seems excessive to have to set this, but the direct setView call does not properly enforce this selection
    }

    protected void setContentView (ContentSourceListItem csi, State state, View.Type view) {
        if (csi == null) { // upon initial startup
            SourceListCategory contents = categories.get(CONTENTS);
            for (SourceListItem item : contents.getItems()) {
                ContentSourceListItem content = (ContentSourceListItem)item;
                while (content.getContent().canPop())
                    content.getContent().pop();
            }
            goHome(true);
            return;
        }
        
        if (state != null && csi.getContent().getState() != state) {
            
            boolean mainContent = false;
            SourceListCategory contents = categories.get(CONTENTS);
            for (SourceListItem item : contents.getItems()) {
                ContentSourceListItem content = (ContentSourceListItem)item;
                if (content.equals(csi))
                    mainContent = true;
            }
            
            int steps = csi.getContent().findState(state);
            ArrayList<ContentSourceListItem> csiList = new ArrayList<ContentSourceListItem>();
            if (mainContent) {
                for (SourceListItem item : contents.getItems()) {
                    ContentSourceListItem content = (ContentSourceListItem)item;
                    csiList.add(content);
                }
            } else {
                csiList.add(csi);
            }
            
            for (ContentSourceListItem content: csiList) {
                for (int i=0; i<Math.abs(steps); i++)
                    if (steps > 0)
                        content.getContent().push();
                    else
                        content.getContent().pop();
            }
        }
        if (view != null) // home view has no selected item
            sourceList.setSelectedItem(csi); // must set selected item first before state content change event is thrown

        if (view == null) {
            goHome(true);
            return;
        }
        
        viewControl.setView(view); // seems excessive to have to set this, but the setView call below does not properly enforce this selection
        return;
    }

    protected void updateBottomBarComponent(BottomBar bottomBar) {
	for (Component old: bottomBarPanel.getComponents())
	    bottomBarPanel.remove(old);
	bottomBarPanel.add(bottomBar.getComponent(), BorderLayout.SOUTH);
	bottomBarPanel.repaint();
    }
    
    public void sourceListItemSelected (final SourceListItem item) {
	if (item == null) {
	    goHome (false);
	    return;
	}

	Object parent = item.getParent();
	/*if (parent != null && parent instanceof DataSourceListItem) {
	    DataSourceListItem dsItem = (DataSourceListItem)parent;
	    getMainFrame().setTitle
		(appTitle + " \u2014 "
		 + dsItem.getLocator().getDataSource()
		 .getCredential().getUsername() +"@"
		 + dsItem.getLocator().getURI()+"/"+item.getText());
	}
	else*/ {
	    getMainFrame().setTitle(appTitle + " \u2014 "+item.getText());
	}	    

        if (item instanceof AbstractTask) {
	    //taskPane.setTask((AbstractTask)item);
	    //((CardLayout)homePane.getLayout()).show(homePane, "task");
	    //viewControl.setEnabled(false);
	}

	if (!(item instanceof ContentSourceListItem)) {
	    //statusLabel.setText(null);
	    reloadBtn.setEnabled(false);
	    return;
	}
	reloadBtn.setEnabled(true);
	ContentSourceListItem csi = (ContentSourceListItem)this.sourceList.getSelectedItem();
	if (csi != null) {
            if (prevBtn.isEnabled() != history.size() > 1)
                prevBtn.setEnabled(history.size()>1);
            if (nextBtn.isEnabled() != queue.size() > 0)
                nextBtn.setEnabled(queue.size()>0);
	}


	final ContentSourceListItem citem = (ContentSourceListItem)item;
        /*
	if (!citem.isLoaded()) {
	    new javax.swing.Timer(500, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			citem.load();
		    }
		}).start();
	}
        */

	if (item.getCounterValue() > 0) {
	    new javax.swing.Timer(5000, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
//			item.setCounterValue(0);
		    }
		}).start();
	}

	NumberFormat nf = NumberFormat.getIntegerInstance();
	nf.setGroupingUsed(true);

	final Content content = citem.getContent();

	String title = getMainFrame().getTitle() 
	    + " ("+nf.format(content.getTotalSize()) +" total)"
	    ;
	getMainFrame().setTitle(title);

	//searchControl.setHintText("Search "+content.getName());

	viewControl.setEnabled(true);
	viewControl.setEnabled(content.getSupportedViewTypes());
	viewControl.setView(content.getView().getType());

	// wait for a few seconds before update the last viewed time
	javax.swing.Timer timer = new javax.swing.Timer 
	    (3000, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			if (getCurrentContent () == content) {
			    content.setLastViewed(new Date().getTime());
			}
		    }
		});
	timer.setRepeats(false);
	timer.start();

        //System.err.println("Really where item selected:"+item.getText());
        pushDisplayState(new DisplayState(citem, content.getView().getType(), citem.getContent().getState()));
	showContent (content);
    }

    protected Content getCurrentContent () {
	if (sourceList != null) {
	    SourceListItem item = sourceList.getSelectedItem();
	    return item instanceof ContentSourceListItem 
		? ((ContentSourceListItem)item).getContent() : null;
	}
	return null;
    }

    public void sourceListItemClicked 
	(SourceListItem item, 
	 SourceListClickListener.Button mouseButton, int clickCount) {
	/*
	  if (clickCount == 2) {
	  item.setBusy(!item.isBusy());
	  }
	*/
    }

    /*
     * SourceListWillExpandListener
     */
    public void sourceListWillExpand (SourceListCategory category) {
	logger.info(category + " will expand");
    }

    public void sourceListWillExpand (SourceListItem item) {
        logger.info(item + " will expand");
    }

    public void sourceListWillCollapse (SourceListCategory category) {
    }

    public void sourceListWillCollapse (SourceListItem item) {
    }

    public void sourceListCategoryClicked 
	(SourceListCategory category,
	 SourceListClickListener.Button mouseButton, int clickCount) {
    }

    public String getTooltip (SourceListCategory category) {
        return null;
    }

    public String getTooltip (SourceListItem item) {
        if (item instanceof ContentSourceListItem) {
            return ((ContentSourceListItem)item).getContent().getDescription();
        }
        return null;
    }

    public void propertyChange (PropertyChangeEvent e) {
	Object source = e.getSource();
	Object value = e.getNewValue();

	System.out.println(source.getClass() + ": prop="+e.getPropertyName()
			   + " old="+e.getOldValue() 
			   + " new="+e.getNewValue());
	
	/*
	  for (SourceListItem it : contents.getItems()) {
	  it.setCounterValue(0);
	  }
	*/
    }

    protected void showContent (final Content content) {


        if (content == null) {
            updateBottomBarComponent(defaultBottomBar);
	    //slider.setEnabled(false);
	    reloadBtn.setEnabled(false);
            clearFacets ();
	    return;
        }

        bard.ui.content.View view = content.getView();
        if (view == null) {
            return;
        }

        BottomBar bottomBar = view.getBottomBar();
        if (bottomBar == null) bottomBar = defaultBottomBar;
        updateBottomBarComponent(bottomBar);
//	BoundedRangeModel model = view.getBoundedRangeModel();
//	if (model != null) {
//	    slider.setEnabled(true);
//	    slider.setModel(model);
//	}
//	else {
//	    slider.setEnabled(false);
//	}

	// make sure viewPane is visible
	((CardLayout)homePane.getLayout()).show(homePane, "view");

	viewPane.add(view.getComponent(), view.getName());
	((CardLayout)viewPane.getLayout()).show(viewPane, view.getName());
	view.getComponent().invalidate();

        SwingUtilities.invokeLater(new Runnable () {
                public void run () {
                    updateFacets (content.getFacets());
                }
            });

    }

    protected void setupSourceList  () {
	sourceList.removeSourceListWillExpandListener(this);
        sourceList.setToolTipProvider(this);

	try {
	    /*
	    String datasource = PREFS.get(PREF_DATASOURCE, null);
	    if (datasource != null) {
		DataSource ds = dsManager.getDataSource(datasource);
		if (ds != null) {
		    dsLocator = dsManager.getLocator(ds);
		}
		else {
		    logger.log(Level.WARNING, "Can't find default data "
			       +"source \""+datasource+"\"; revert to local");
		}
	    }
	    */

	    // now configure the source list navigation
	    configureSourceList (sourceList, getServiceManager ());
	    setSearchPaneContent();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't configure source list", ex);
	    JXErrorPane.showDialog(ex);
	}

	// make sure this is added after all pending path expansions
	//   left on the edt queue
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    sourceList.addSourceListWillExpandListener(BARD.this);
		}
	    });
    }

    void configureSourceList (final SourceList srcList, 
			      EntityServiceManager esm)  {

	SourceListModel model = new SourceListModel ();
	srcList.setModel(model);

	/*
	 * CONTENTS
	 */
	ContentSourceListCategory contents = createContents (model, esm);

        /*
         * COLLECTIONS
         */
        ContentSourceListCategory collections = createCollections (model, esm);

        //ContentSourceListCategory searches = 
        //    newCategory (model, SEARCHES, false);

        /*
         * FILTERS
         */
        ContentSourceListCategory facets = 
            newCategory (model, FACETS, false);

	/*
	 * TASKS
	 */
	//SourceListCategory tasks = newCategory (model, TASKS);
	//appctx.setSourceListTask(srcList, tasks);
    }


    ContentSourceListCategory createContents (final SourceListModel model, 
                                              EntityServiceManager esm) {
        ContentSourceListCategory contents =
            newCategory (model, CONTENTS, false);

	ContentManager contentManager = 
            ContentManager.getInstance(esm, null); //openURI);  Initial load should be the whole kaboodle
	if (contentManager.getServiceManager().getService(Project.class).size() == 0) {
            SplashScreen.getInstance().setVisible(false);
	    JOptionPane.showMessageDialog
	    (getMainFrame(), "Sorry, API is down.", 
                "Error", JOptionPane.ERROR_MESSAGE);
	    super.shutdown();
	}

	contents.setContentManager(contentManager);

	for (Content c : contentManager.getContents()) {
	    SourceListItem item = new ContentSourceListItem (c);
            item.setBusy(c.isBusy());
	    model.addItemToCategory(item, contents);
	}
        return contents;
    }

    ContentSourceListCategory createCollections (SourceListModel srcList,
                                                 EntityServiceManager esm) { 
        ContentSourceListCategory collections = 
            newCategory (srcList, COLLECTIONS, false);
        new CollectionsLoader (srcList, collections, esm).execute();
        return collections;
    }

    ContentSourceListCategory newCategory (SourceListModel model, 
					   String name) {
	return newCategory (model, name, true);
    }

    ContentSourceListCategory newCategory (SourceListModel model, 
					   String name, boolean collapsable) {
	ContentSourceListCategory cat = new ContentSourceListCategory (name);
	cat.setCollapsable(collapsable);
	categories.put(name, cat);
	model.addCategory(cat);
	return cat;
    }

    protected void pushDisplayState(DisplayState display) {
        if (display.equals(history.peek())) 
            return;
        history.push(display);
        while (queue.size() > 0)
            queue.pop();
        nextBtn.setEnabled(false);
        prevBtn.setEnabled(history.size() > 1);
    }
    
    protected void prev () {
        if (history.size() < 2) {
            prevBtn.setEnabled(false);
            return;
        }
        queue.push(history.pop());
        DisplayState display = history.peek();
        nextBtn.setEnabled(true);
        prevBtn.setEnabled(history.size() > 1);
        
        if (display.content != null)
            display.content.setBusy(false);
        setContentView (display.content, display.state, display.view);
    }
    
    protected void next () {  	
        if (queue.size() < 1) {
            nextBtn.setEnabled(false);
            return;
        }
        DisplayState display = queue.pop();
        history.push(display);
        nextBtn.setEnabled(queue.size()>0);
        prevBtn.setEnabled(history.size() > 1);
        setContentView (display.content, display.state, display.view);
    }
    
    protected void reload () {
        SourceListCategory contents = categories.get(CONTENTS);
        for (SourceListItem item : contents.getItems()) {
            ContentSourceListItem content = (ContentSourceListItem)item;
            content.reload();
        }
    }

    protected void reload (SourceListCategory cat) {
        
    }

    protected void clearFacets () {
        SourceListCategory category = categories.get(FACETS);
        // clear the current facets (if any)
        ArrayList<SourceListItem> items = new ArrayList<SourceListItem>();
        for (SourceListItem item : category.getItems()) {
            items.add(item);
        }

        for (SourceListItem item : items) {
            sourceList.getModel().removeItemFromCategory(item, category);
        }
    }

    protected void updateFacets (Collection<Value> facets) {
        SourceListCategory category = categories.get(FACETS);

        // clear the current facets (if any)
        clearFacets ();

        // add new facets
        if (facets == null || facets.isEmpty()) {
            return;
        }
        
        for (Value f : facets) {
            SourceListItem item = new FacetSourceListItem (f);
            sourceList.getModel().addItemToCategory(item, category);

            for (Value c : f.getChildren()) {
                if (!"".equals(c.getId())) {
                    sourceList.getModel().addItemToItem
                        (new FacetSourceListItem (c), item);
                }
            }
        }
    }

    void doExport () {
	final Content content = getCurrentContent ();
	if (content == null) {
	    JOptionPane.showMessageDialog
		(getMainFrame(), "No content selected!", 
		 "Info", JOptionPane.PLAIN_MESSAGE);
	    return;
	}

	if (!content.isExportSupported()) {
	    JOptionPane.showMessageDialog
		(getMainFrame(), "Sorry, data export is currently not\n"
		 +"supported by this content/view!", 
		 "Error", JOptionPane.ERROR_MESSAGE);
	    return;
	}
	
	final JFileChooser fileChooser = UIUtil.getFileChooser();
	fileChooser.setDialogTitle("Export "+content.getName());
	fileChooser.resetChoosableFileFilters();
	fileChooser.setFileFilter(null);
	for (javax.swing.filechooser.FileFilter f 
		 : content.getExportFileFilters()) {
	    if (fileChooser.getFileFilter() == null) {
		fileChooser.setFileFilter(f);
	    }
	    fileChooser.addChoosableFileFilter(f);
	}
	while (true) {
	    int opt = fileChooser.showSaveDialog(getMainFrame ());
	    if (JFileChooser.APPROVE_OPTION == opt) {
		final File file = fileChooser.getSelectedFile();
		if (file.exists()) {
		    int ans = JOptionPane.showConfirmDialog
			(getMainFrame(), "File \""+file.getName()
			 +"\" exists; overwrite?", 
			 "WARNING", JOptionPane.YES_NO_OPTION, 
			 JOptionPane.WARNING_MESSAGE);
		    if (ans == JOptionPane.NO_OPTION) {
			continue;
		    }
		}

		Thread exportThread = new Thread() {
		    public void run() {
			OutputStream os;
			try {
			    content.exportData
			    (fileChooser.getFileFilter(), file);
			} catch (FileNotFoundException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			} catch (IOException io) {
			    // TODO Auto-generated catch block
			    io.printStackTrace();
			}
		    }
		};
		exportThread.start();
	    }
	    return;
	}
    }

    void doImport () {
	String[] types = {"Compounds", "Assay", "Projects"};
	Class<?>[] contentTypes = {Compound.class, Assay.class, Project.class};
	
/*	No need to ask about content type yet while we can only do compound (activity) import
	int type = JOptionPane.showOptionDialog(
		getMainFrame(), 
		"Select content type to import", 
		"Import content", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, 
		null, types, types[0]);
	if (type == JOptionPane.CANCEL_OPTION || type == -1) {
	    return;
	}
	
*/	int type = 0;
	
	@SuppressWarnings("unchecked")
	Class<? extends Entity> clazz = (Class<? extends Entity>) contentTypes[type];
	
	Content<? extends Entity> content = getContentManager().getContent(clazz);
	if (!content.isImportSupported()) {
	    JOptionPane.showMessageDialog
		(getMainFrame(), "Sorry, data import is currently not\n"
		 +"supported by this content!", 
		 "Error", JOptionPane.ERROR_MESSAGE);
	    return;
	}

	if (importDialog == null) {
	    importDialog = new BARDImportDialog (getMainFrame());
	    importDialog.setSize(500, 400);
	    UIUtil.centerComponent(getMainFrame (), importDialog);
	}
	importDialog.setName("New dataset");
	importDialog.setContent(content);
	importDialog.setVisible(true);
    }

    class BARDImportDialog extends ImportDialog {

	JDialog progressDialog;
	JProgressBar progressBar;
	
	BARDImportDialog (Frame owner) {
	    super (owner);

	    progressDialog = new JDialog();
	    progressDialog.setModal(false);
	    progressDialog.setTitle("Import Collection");
	    progressBar = new JProgressBar(0, 100);
	    progressDialog.add(BorderLayout.CENTER, progressBar);
	    progressDialog.add(BorderLayout.NORTH, new JLabel("Progress..."));
	    JButton cancelBtn = new JButton("Cancel");
	    cancelBtn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    progressDialog.setVisible(false);
		}
	    });
	    progressDialog.add(BorderLayout.SOUTH, cancelBtn);
	    progressDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    progressDialog.setSize(300, 75);
	}

	@Override
	protected void doImport (final Content<? extends Entity> content, 
				 final String name, 
				 final String source,
				 final Map<String,Object> options,
				 final Map<String, String[]> props) {
		Thread exportThread = new Thread() {
		    public void run() {
			try {
			    importContent (content, name, source, options, props);
			} catch (Exception e) {
			    e.printStackTrace();
			}
		    }
		};
		exportThread.start();
	}

	void importContent (Content<? extends Entity> content, String collectionName, String sourceURI,
			    Map<String, Object> options, Map<String, String[]> props) 
	    throws Exception {
//		Map options = new HashMap ();
//		options.put("standardize", standardizeCb.isSelected());
//		options.put("saltstrip", saltStripCb.isSelected());
//		options.put("description", getDescription());

	    	URI uri = new URI (sourceURI);
	    	InputStream is = BardHttpClient.getURLInputStream(uri.toURL());
	    	// all the Molecules generated via MolImporter
	    	ArrayList<Molecule> mols = new ArrayList<Molecule>();
	    	// synchronized list of compounds
	    	ArrayList<Compound> compounds = new ArrayList<Compound>();
	    	// unsynchronized list of compound ID matches
	        ArrayList<String> cids = new ArrayList<String>();
	        // preferred name to use for molecules
	        String nameField = null;
	        // how many failed to match
	        int noMatchCount = 0;
	        
	        for (String prop: props.keySet()) {
	            for (String type: props.get(prop)) {
	        	if (ImportDialog.FieldEnum.Name.getField().equals(type)) {
	        	    nameField = prop;
	        	    break;
	        	}
	            }
	            if (nameField != null)
	        	break;
	        }
	        
	        MolImporter mi = new MolImporter (is);
	        for (Molecule mol; (mol = mi.read()) != null; ) {
	            mols.add(mol);
	        }
	        progressBar.setMaximum(mols.size());
	        progressBar.setValue(0);
	        progressDialog.setVisible(true);
	        
	        MolStandardizer mstd = new MolStandardizer ();
	        CompoundAdapter adapter = new CompoundAdapter();
        	RESTCompoundService rcs = (RESTCompoundService)content.getService();
	        HashMap<String, Compound> extras = new HashMap<String, Compound>();
	        DataSource ds = new DataSource(sourceURI);

	        for (Molecule mol: mols) {
	            progressBar.setValue(progressBar.getValue()+1);
	            if (Boolean.TRUE.equals(options.get("standardize")))
	        	mstd.standardize(mol);

		    /**
	             * here we get coarse match and test internally
	             */
	            String[] hkeys = MolStandardizer.hashKeyArrayExt(mol);
	            String fullHash = "";
	            for (String hkey: hkeys) fullHash += "|"+hkey;
	            if (extras.containsKey(fullHash)) {
	        	compounds.add(extras.get(fullHash));
	        	cids.add(extras.get(fullHash).getId().toString());
	            } else {
	        	Collection<Compound> matches = rcs.getHashEntity(hkeys[0], 0);
	        	Compound bestMatch = null;
	        	for (Compound match: matches) {
	        	    adapter.setCompound(match);
	        	    Molecule matchMol = mi.importMol(adapter.getStructureSMILES());
	        	    mstd.standardize(matchMol);
	        	    String[] hkeysMatch = MolStandardizer.hashKeyArrayExt(matchMol);
		            String fullHashMatch = "";
		            for (String hkey: hkeysMatch) fullHashMatch += "|"+hkey;
	        	    boolean isMatch = fullHash.equals(fullHashMatch);
	        	    if (isMatch) {
	        		bestMatch = match;
	        	    } else {
	        		extras.put(fullHashMatch, match);
	        	    }
	        	}
	        	if (bestMatch == null) {
	        	    // construct compound from scratch
	        	    Compound c = new Compound();
	        	    noMatchCount++;
	                    c.setId("N/A "+noMatchCount);
	                    //c.add(new LongValue (ds, Compound.PubChemCIDValue, 0L));
	                    c.setName(mol.getName());
	                    MolecularData md = new MolecularDataJChemImpl ();
	                    md.setMolecule(mol);
	                    c.add(new MolecularValue (ds, Compound.MolecularValue, md));
	        	    bestMatch = c;
	        	} else {
	        	    cids.add(bestMatch.getId().toString());
	        	}
	        	// add values that we are transferring to each compound
	        	if (nameField != null && mol.getProperty(nameField) != null) {
	        	    StringBuffer molName = new StringBuffer();
	        	    for (String entry: mol.getProperty(nameField).split("[\t\n\f\r]")) {
	        		if (molName.length() > 0) molName.append("; ");
	        		molName.append(entry);
	        	    }
	        	    bestMatch.setName(molName.toString());
	        	} else if (mol.getName() != null && mol.getName().length() > 0) {
	        	    bestMatch.setName(mol.getName());
	        	}
	        	for (String prop: props.keySet()) {
	        	    String value = mol.getProperty(prop);
	        	    if (value != null && value.length() > 0) {
	        		for (String entry: value.split("[\t\n\f\r]"))
	        		    if (entry.length() > 0)
	        			bestMatch.add(new StringValue(ds, prop, entry));
	        	    }
	        	}
	        	compounds.add(bestMatch);
	            }
	        }
  
	        
	        Object obj = content.getService().newETag(collectionName, cids);
		if (obj != null && obj instanceof Value) {
		    Value etag = (Value)obj;
		    if (etag.getChild("count") != null)
		        etag.remove(etag.getChild("count"));
		    new IntValue(etag, "count", compounds.size());
		    CompoundContent newContent = (CompoundContent)getContentManager().add(content.getEntityClass(), etag);
		    newContent.push(new DefaultState(newContent, compounds));
		    
		    // populate ListView table with existing data
		    HashMap<String,String> colFields = new HashMap<String, String>();
		    for (String prop: props.keySet()) {
			for (String field: props.get(prop)) {
			    if (field != null)
				colFields.put(prop, field);
			}
		    }
		    if (colFields.size() > 0) {
			newContent.addDataColumns(colFields);
			newContent.setView(View.Type.List);
		    }
		    
		    ContentChangeEvent event = new ContentChangeEvent(newContent, ContentChangeEvent.CONTENT, null, collectionName);
		    EventBus.publish(event);
		}
	        progressDialog.setVisible(false);
	}
    }

    public static void main (String[] argv) throws Exception {
        new LaunchApp (
        		BARD.class, 
        		Application.getInstance()
    	    	.getContext().getResourceMap().getString("Application.splash"), 
        		argv
        	).execute();
    }
}

