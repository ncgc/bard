// $Id$

package bard.ui.main;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import java.net.URI;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.geom.AffineTransform;

import javax.swing.*;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.decorator.*;
import org.jdesktop.swingx.painter.BusyPainter;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import java.io.*;
import java.util.prefs.*;
import java.net.ProxySelector;
import org.apache.http.HttpHost;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.routing.HttpRoutePlanner;

import bard.util.ClientPrefs;

public class FeedbackDialog extends JDialog 
    implements ActionListener, CaretListener {

    private static final Logger logger = Logger.getLogger
	(FeedbackDialog.class.getName());

    static final String FEEDBACK_URL = 
	"http://tripod.nih.gov/servlet/feedback/";

    static final String PROP_NAME = "name";
    static final String PROP_EMAIL = "email";
    static final String PROP_PROXY_HOST = "proxy-host";
    static final String PROP_PROXY_PORT = "proxy-port";
    


    protected static Preferences getPrefs () {
	return Preferences.userNodeForPackage(FeedbackDialog.class);
    }

    private static final String APPNAME = Application.getInstance()
	.getContext().getResourceMap().getString("Application.title");

    public static final int TYPE_CONTENT_BUG = 0;
    public static final int TYPE_SOFTWARE_BUG = 1;
    public static final int TYPE_SUGGESTION = 2;
    public static final int TYPE_COMMENT = 3;

    static final String[] TYPES = {
	"Content bug",
	"Software bug",
	"Suggestion",
	"General comment"
    };

    private JTextField clientIdField;
    private JTextField nameField;
    private JTextField emailField;
    private JTextField titleField;
    private JTextField proxyHost;
    private JTextField proxyPort;
    private JComboBox typeCb;
    private JTextArea descArea;
    private JTextField statusField;
    private JButton actionBtn;
    private boolean status = true;
    private BusyPane busy;

    private HttpRoutePlanner routePlanner;


    static class BusyPane extends JComponent implements ActionListener {
	javax.swing.Timer timer;
	BusyPainter painter;
	int frame = 0;
	AffineTransform txn = new AffineTransform ();
	int size = 14;
	int count = 0;

	public BusyPane () {
	    setPreferredSize (new Dimension (size+2, size+2));

	    painter = new BusyPainter
		(new RoundRectangle2D.Float(0.f, 0.f,5.f, 2.f, 4.f, 4.f),
		 new Ellipse2D.Float(2.f,2.f,10.0f,10.0f));
	    painter.setTrailLength(6);
	    painter.setPoints(9);
	    painter.setFrame(0);

	    timer = new javax.swing.Timer (125, this);
	}

	public void start () { 
	    if (count++ == 0) {
		timer.start(); 
	    }
	}
	public void stop () { 
	    if (--count == 0) {
		timer.stop(); 
		repaint (); 
	    }
	}
	public void reset () { count = 0; }

	public boolean isBusy () { return timer.isRunning(); }

	@Override
	protected void paintComponent (Graphics g) {
	    if (isBusy ()) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle clip = g.getClipBounds();
		
		Rectangle r = getBounds ();
		if (r != null && clip.intersects(r)) {
		    int x = r.x + (clip.width - size);
		    int y = r.y + (r.height - size)/2;
		    g2.translate(x, y);
		    painter.paint(g2, this, size, size);
		}
	    }
	}

	public void actionPerformed (ActionEvent e) {
	    frame = (frame+1) % 8;
	    painter.setFrame(frame);
	    repaint ();
	}
    }

    class SendFeedback extends SwingWorker<Throwable, Void> {
	boolean ok = false;
	String feedbackId;
	SendFeedback () {
	}

	@Override
	protected Throwable doInBackground () {
	    try {
		send ();
	    }
	    catch (Exception ex) {
		return ex;
	    }
	    return null;
	}

	protected void done () {
	    try {
		Throwable t = get ();
		if (t != null) {
		    statusField.setText("Error: "+t.getMessage());
		}
		else {
		    ok = true;
		    statusField.setText("Feedback #"+feedbackId+" submitted; "
					+"thank you for your feedback!");
		    clear ();
		}
	    }
	    catch (Exception ex) {
		statusField.setText("Connection timed out; perhaps "
				    +"you're behind a firewall, so "
				    +"make sure your proxy settings "
				    +"are correct!");
		logger.log(Level.SEVERE, "Sending feedback failed", ex);
		actionBtn.setEnabled(true);
	    }
	    busy.stop();
	}

	public boolean isOK () { return ok; }

	void send () throws Exception {
	    statusField.setText("Sending feedback...");

	    DefaultHttpClient httpclient = new DefaultHttpClient();
	    Preferences prefs = getPrefs ();
	    String host = prefs.get(PROP_PROXY_HOST, null);
	    String port = prefs.get(PROP_PROXY_PORT, null);
	    if (host != null  && host.length() > 0 
                && port != null && port.length() > 0) {
		HttpHost proxy = new HttpHost 
                    (host, Integer.parseInt(port), "http");
		httpclient.getParams().setParameter
		    (ConnRoutePNames.DEFAULT_PROXY, proxy);	    
	    }
	    else {
		httpclient.setRoutePlanner(getRoutePlanner ());
	    }

	    try {
		HttpPost post = new HttpPost (FEEDBACK_URL);
		
		List<NameValuePair> nvp = new ArrayList<NameValuePair>();
		nvp.add(new BasicNameValuePair 
			("client-id", clientIdField.getText()));
		nvp.add(new BasicNameValuePair
			("feedback-os", "Java "
			 +System.getProperty("java.version")
			 +" on "+ System.getProperty("os.name")
			 +" "+System.getProperty("os.arch") 
			 +" "+System.getProperty("os.version")));
		String version = APPNAME + " (version: "+System.getProperty
		    ("bard-version", "Unknown") + ")";
		nvp.add(new BasicNameValuePair
			("feedback-version", version));
		nvp.add(new BasicNameValuePair
			("feedback-title", titleField.getText()));
		nvp.add(new BasicNameValuePair
			("feedback-type", (String)typeCb.getSelectedItem()));
		nvp.add(new BasicNameValuePair
			("feedback-name", nameField.getText()));
		nvp.add(new BasicNameValuePair
			("feedback-email", emailField.getText()));
		nvp.add(new BasicNameValuePair
			("feedback-mesg", descArea.getText()));

		post.setEntity(new UrlEncodedFormEntity (nvp, HTTP.UTF_8));

		HttpResponse response = httpclient.execute(post);
		statusField.setText(response.getStatusLine().toString());

		// If the response does not enclose an entity, there is no need
		// to worry about connection release
		HttpEntity entity = response.getEntity();
		if (entity != null) {
		    InputStream instream = entity.getContent();
		    try {
			BufferedReader reader = new BufferedReader
			    (new InputStreamReader(instream));
			// do something useful with the response
			feedbackId = reader.readLine();
		    } 
		    catch (IOException ex) {
			throw ex;
		    }
		    catch (RuntimeException ex) {
			post.abort();
			throw ex;
		    }
		    finally {
			instream.close();
		    }
		}
	    }
	    finally {
		httpclient.getConnectionManager().shutdown();
	    }
	}
    }

    public FeedbackDialog (Frame owner) {
	super (owner, false);
	routePlanner = createRoutePlanner ();

	initDialog ();
    }

    protected HttpRoutePlanner createRoutePlanner () {
	SchemeRegistry scheme = new SchemeRegistry ();
	scheme.register(new Scheme ("http", 80, 
				    PlainSocketFactory.getSocketFactory()));
	return new ProxySelectorRoutePlanner
	    (scheme, ProxySelector.getDefault());
    }
    protected HttpRoutePlanner getRoutePlanner () { return routePlanner; }

    protected void initDialog () {
	setTitle (APPNAME + " feedback");

	JPanel pane = new JPanel (new MigLayout ("", "[right][grow,fill]"));
	//pane.add(new JLabel ("Client Info"), "split, span");
	//pane.add(new JSeparator (), "growx, wrap 10");

	pane.add(new JLabel ("Client Id"), "align right, gap 10");
	pane.add(clientIdField = new RoundTextField (30), 
		 "span, growx, wrap 5");
	clientIdField.setEditable(false);
	clientIdField.setText(ClientPrefs.getClientId());

	Preferences prefs = getPrefs ();
	pane.add(new JLabel ("Name (optional)"), "align right, gap 10");
	pane.add(nameField = new RoundTextField (), "span, growx, wrap 5");
	try {
	    String name = prefs.get(PROP_NAME, null);
	    nameField.setText(name);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Exception in preferences", ex);
	}
	nameField.addCaretListener(this);

	pane.add(new JLabel ("Email (optional)"), "align right, gap 10");
	pane.add(emailField = new RoundTextField (), 
		 "span, growx, wrap 10");
	try {
	    String email = prefs.get(PROP_EMAIL, null);
	    emailField.setText(email);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Exception in preferences", ex);
	}
	emailField.addCaretListener(this);	

	//pane.add(new JLabel ("Feedback Details"), "split, span");
	//pane.add(new JSeparator (), "growx, wrap 10");

	pane.add(new JLabel ("Summary"), "align right, gap 10");
	pane.add(titleField = new RoundTextField (), "span, growx, wrap 5");
	titleField.setToolTipText("A brief summary of the feedback");
	titleField.addCaretListener(this);

	pane.add(new JLabel ("Type"), "align right, gap 10");
	pane.add(typeCb = new JComboBox (TYPES), "span, growx, wrap 5");
	typeCb.setToolTipText("Type of feedback");

	pane.add(new JLabel ("Description"), "align right");
	pane.add(new JScrollPane (descArea = new JTextArea (10, 30)),
		 "span, growx, wrap 10");
	descArea.addCaretListener(this);

	pane.add(new JLabel ("Proxy settings"), "split, span");
	pane.add(new JSeparator (), "growx, wrap 10");
	pane.add(new JLabel ("Host"));
	Box box = Box.createHorizontalBox();
	box.add(proxyHost = new RoundTextField ());
	proxyHost.setToolTipText("Proxy host name/address");
	try {
            String host = prefs.get(PROP_PROXY_HOST, null);
	    proxyHost.setText(host);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Exception in preferences", ex);
	}

	JPanel pp = new JPanel (new BorderLayout (5, 0));
	pp.add(box);
	box = Box.createHorizontalBox();
	box.add(new JLabel ("Port"));
	box.add(Box.createHorizontalStrut(5));
	box.add(proxyPort = new RoundTextField (5));
	proxyPort.setToolTipText("Proxy port");
	try {
	    String port = prefs.get(PROP_PROXY_PORT, null);
            proxyPort.setText(port);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Exception in preferences", ex);
	}

	pp.add(box, BorderLayout.EAST);
	pane.add(pp, "growx, wrap 10");

	pane.add(new JLabel ("Status"), "split, span");
	pane.add(new JSeparator (), "growx, wrap 10");
	JPanel sp = new JPanel (new BorderLayout (5, 0));
	sp.add(busy = new BusyPane (), BorderLayout.WEST);
	sp.add(statusField = new RoundTextField ());
	pane.add(sp, "span, growx");
	statusField.setEditable(false);

	JPanel top = new JPanel (new BorderLayout (0, 5));
	top.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
	top.add(pane);

	JPanel bp = new JPanel (new GridLayout (1, 2, 2, 0));
	JButton btn;
	bp.add(btn = new JButton ("Send"));
	btn.addActionListener(this);
	btn.setEnabled(false);
	actionBtn = btn;

	bp.add(btn = new JButton ("Close"));
	btn.addActionListener(this);
	JPanel bbp = new JPanel ();
	bbp.add(bp);
	top.add(bbp, BorderLayout.SOUTH);
	
	getContentPane().add(top);
	pack ();
    }

    void checkAction () {
	String title = titleField.getText();
	if (title == null || title.length() == 0 ) {
	    actionBtn.setEnabled(false);
	    return;
	}
	    
	String desc = descArea.getText();
	if (desc == null || desc.length() == 0) {
	    actionBtn.setEnabled(false);
	    return;
	}

	statusField.setText(null);
	actionBtn.setEnabled(true);
    }

    public void caretUpdate (CaretEvent e) {
	checkAction ();
    }

    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
	status = false;

	Preferences prefs = getPrefs ();
	String name = nameField.getText();
	try {
            prefs.put(PROP_NAME, name);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, 
		       "Exception in preferences", ex);
	}
	
	String email = emailField.getText();
	try {
            prefs.put(PROP_EMAIL, email);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, 
		       "Exception in preferences", ex);
	}

	String host = proxyHost.getText();
        try {
	    prefs.put(PROP_PROXY_HOST, host);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, 
		       "Exception in preferences", ex);
	}

	String port = proxyPort.getText();
        try {
	    prefs.put(PROP_PROXY_PORT, port);
	}
        catch (Exception ex) {
	    logger.log(Level.SEVERE, 
		       "Exception in preferences", ex);
	}

	if (cmd.equalsIgnoreCase("close")) {
	    setVisible (false);
	}
	else {
	    actionBtn.setEnabled(false);
	    busy.start();

	    final SendFeedback feedback = new SendFeedback ();
	    feedback.execute();

	    java.util.Timer timer = new java.util.Timer();
	    timer.schedule(new java.util.TimerTask () {
		    public void run () {
			if (!feedback.isDone()) {
			    feedback.cancel(true);
			}
		    }
		}, 60000);
	}
    }

    void clear () {
	titleField.setText(null);
	descArea.setText(null);
    }

    public static void main (String[] argv) throws Exception {
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    FeedbackDialog diag = new FeedbackDialog (null);
		    diag.setVisible(true);
		}
	    });
    }
}
