
package bard.ui.main;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.Border;
import javax.swing.border.AbstractBorder;

import org.jdesktop.swingx.painter.Painter;
import org.jdesktop.swingx.painter.CapsulePainter;
import org.jdesktop.swingx.painter.RectanglePainter;
import org.jdesktop.swingx.painter.effects.*;

import com.jidesoft.popup.JidePopup;
import com.jidesoft.swing.JidePopupMenu;
import com.jidesoft.swing.LabeledTextField;

import org.bushe.swing.event.EventSubscriber;
import org.bushe.swing.event.EventBus;

import bard.ui.limewire.ui.swing.painter.BorderPainter;
import bard.ui.explodingpixels.macwidgets.MacColorUtils;

import bard.core.SearchParams;
import bard.core.StructureSearchParams;
import bard.ui.icons.IconFactory;
import static bard.ui.util.ColorUtil.*;
import bard.ui.event.*;

public class SearchTextField 
    extends LabeledTextField implements ActionListener, CaretListener {

    private static final Logger logger = Logger.getLogger
	(SearchTextField.class.getName());


    private RectanglePainter<Component> cPainter; // component painter
    private BorderPainter<Component> bPainter; // border painter
    private SearchParams query = null; // current query

    public SearchTextField () {
        this (20);
    }

    public SearchTextField (int columns) {
	super (IconFactory.App.Search.getIcon());

        getTextField().setPreferredSize(new Dimension (10, 18));
        getTextField().setColumns(columns);
        getTextField().addCaretListener(this);

	Color c = getTextField().getBackground();
	int arcWidth = 23, arcHeight = 23;
	cPainter = new RectanglePainter<Component>();
	cPainter.setInsets(new Insets (0,1,0,0));
	cPainter.setAntialiasing(true);
	cPainter.setRounded(true);
	cPainter.setRoundHeight(arcHeight+2);
	cPainter.setRoundWidth(arcWidth+2);
	cPainter.setBorderPaint(BORDER);
	cPainter.setBorderWidth(2f);
	cPainter.setFillPaint(c);

	bPainter = new BorderPainter<Component>
	    (arcWidth, arcHeight, 
	     BORDER, 
	     BEVEL_LEFT, 
	     BEVEL_TOP1, 
	     BEVEL_TOP2, 
	     BEVEL_RIGHT,
	     BEVEL_BOTTOM,
	     BorderPainter.AccentType.SHADOW);

	//setBorder (BorderFactory.createEmptyBorder(0,0,0,0));

	setBackground (c);
	setHintText ("Search Contents");
        //setLabelText ("Text");

	Border empty = BorderFactory.createEmptyBorder(1,1,1,5);
	Border old = (Border)UIManager.get("TextField.border");
	UIManager.put("TextField.border", empty);
	updateUI ();
	// restore old border
	UIManager.put("TextField.border", old);
    }


    protected JidePopupMenu createContextMenu () {
	JidePopupMenu menu = new JidePopupMenu ();
	//menu.add("Recent searches...");
	//menu.addSeparator();
	//menu.add("Advanced options...");
	JMenuItem item = new JMenuItem ("Structure");
	item.setToolTipText("Search by structure");
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    structureSearchInvoked ();
		}
	    });
	menu.add(item);
	/*
	menu.add(item = new JMenuItem ("CAS"));
	item.setToolTipText("Search by CAS registry");
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    String text = getText ();
		    if (text == null || text.indexOf(":") > 0) {
			text = "";
		    }
		    setText ("cas:"+text);
		}
	    });
	menu.add(item = new JMenuItem ("CID"));
	item.setToolTipText("Search by PubChem CID");
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    String text = getText ();
		    if (text == null || text.indexOf(":") > 0) {
			text = "";
		    }
		    setText ("cid:"+text);
		}
	    });
	*/
	    
	return menu;
    }

    public void caretUpdate (final CaretEvent e) {
        EventBus.publish(new CaretEvent (this) {
                public int getDot () { return e.getDot(); }
                public int getMark () { return e.getMark(); }
            });
    }

    protected void structureSearchInvoked () {
    }

    protected AbstractButton createButton () {
	Icon icon = IconFactory.App.Clear.getIcon();
	JButton btn = new JButton (icon);
	btn.setOpaque(false);
	btn.setToolTipText("Clear search");
	btn.setMargin(new Insets (0, 0, 0, 0));
	btn.setBorder(null);
	btn.setBorderPainted(false);
	btn.setFocusPainted(false);
	btn.setPressedIcon(icon);
	btn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    search (null);
		    //getTextField().setRequestFocusEnabled(false);
		}
	    });
	return btn;
    }

    @Override
    public String getText () {
	String text = super.getText();
	if (text != null) {
	    text = text.trim();
	    if (text.length() == 0) {
		return null;
	    }
	}
	return text;
    }

    @Override
    public void setText (String text) {
        // temporary remove this listener
        getTextField().removeCaretListener(this);
        getTextField().setText(text);
        //getTextField().setCaretPosition(0);
        getTextField().addCaretListener(this);
    }

    public void select (int start, int end) {
        getTextField().select(start, end);
    }

    public void setQuery (SearchParams query) {
	if (query != null) {
	    if (query instanceof StructureSearchParams) {
		StructureSearchParams cq = (StructureSearchParams)query;
		StringBuilder sq = new StringBuilder ();

		switch (cq.getType()) {
		case Substructure:
		    sq.append("sub:"+cq.getQuery());
		    break;

		case Similarity:
		    sq.append("sim:"+cq.getQuery());
		    sq.append(" tan:"+String.format
			      ("%1.3f", cq.getThreshold()));
		    break;
		    
		case Superstructure:
		    sq.append("sup:"+cq.getQuery());
		    break;

		case Exact:
                    sq.append("exact:"+cq.getQuery());
		    break;
		}

		setText (sq.toString());
	    }
	    else {
		setText (query.getQuery());
	    }
	    getTextField().setCaretPosition(0);
	}
	else {
	    setText (null);
	}
	this.query = query;
    }

    public SearchParams getQuery () { return query; }

    public void search (SearchParams query) {
	setQuery (query);
	search ();
    }

    SearchParams parseQuery (String text) {
	if (text == null ||  text.length() == 0) 
            return null;
    
	String origText = text;
	
        SearchParams query = null;
        if (text.startsWith("sub:") || text.startsWith("SUB:")) {
            query = new StructureSearchParams
                (text.substring(4), StructureSearchParams.Type.Substructure);
        }
        else if (text.startsWith("sup:") || text.startsWith("SUP:")) {
            query = new StructureSearchParams
                (text.substring(4), StructureSearchParams.Type.Superstructure);
        }
        else if (text.startsWith("exact:") || text.startsWith("EXACT:")) {
            query = new StructureSearchParams
                (text.substring(6), StructureSearchParams.Type.Exact);
        }
        else if (text.startsWith("sim:") || text.startsWith("SIM:")) {
            StructureSearchParams ssp = new StructureSearchParams ();
		
            String[] toks = text.split("[\\s\\t]+");
            for (String t : toks) {
                if (t.startsWith("sim:")) {
                    ssp.setQuery(t.substring(4));
                }
                else if (t.startsWith("tan:")) {
                    try {
                        ssp.setThreshold
                            (Double.parseDouble(t.substring(4)));
                    }
                    catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            query = ssp;
        }
        else { // just text
            List<String[]> filters = null;
            
            // has filter fields
            logger.info(text);
            while (text.length() > 0 && text.charAt(text.length()-1) == ']' && text.lastIndexOf('[') > -1) {
            	String field = text.substring(text.lastIndexOf('[')+1,text.length()-1);
            	text = text.substring(0, text.lastIndexOf('['));
            	int end = Math.max(text.lastIndexOf(']')+1, text.lastIndexOf('"'));
            	end = Math.max(0,  end);
            	String value = text.substring(end, text.length()).trim();
            	text = text.substring(0, end);
            	filters = new ArrayList<String[]>();
            	String[] filterField = {field, value};
            	filters.add(filterField);
            }
            if (text.length() == 0 && filters.size() > 0)
            	text = filters.get(0)[1];
            if ((text.lastIndexOf(' ') > -1 || text.lastIndexOf('-') > -1) && text.charAt(0) != '\"')
            	text = "\""+text+"\"";
            query = new SearchParams (text, filters);
        }

        query.setOrigText(origText);
        return query;
    }

    public void search () {
	String text = getText(); // could be due to user editing

        // override setQuery
        query = parseQuery (text);

	if (query != null) {
	    logger.info("Search executed: " +query.getQuery());
	}
	else {
	    logger.info("Clear search!");
	}

        EventBus.publish(new SearchEvent (this, query));
    }

    @Override
    public void setEnabled (boolean enabled) {
        //super.setEnabled(enabled);
        getTextField().setEnabled(enabled);
    }


    protected JTextField createTextField () {
	JTextField field = new JTextField ();
	field.setToolTipText("Enter search terms");
	field.addActionListener(this);
	return field;
    }

    protected JLabel createLabel () {
	JLabel label = new JLabel (getIcon ());
	label.setToolTipText("Click for search options");
	// add some extra padding on the left to give space between
	//  the icon and the arc border
	label.setBorder(BorderFactory.createEmptyBorder(0,2,0,0));
	return label;
    }

    public void actionPerformed (ActionEvent e) {
	search ();
    }

    @Override
    protected void paintBorder (Graphics g) {
	bPainter.paint((Graphics2D)g, this, getWidth ()+1, getHeight ()+1);
    }

    @Override
    protected void paintComponent (Graphics g) {
	cPainter.paint((Graphics2D)g, this, getWidth (), getHeight ());
	super.paintComponent(g);
    }
}
