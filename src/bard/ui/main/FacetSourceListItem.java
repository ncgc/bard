package bard.ui.main;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.SwingUtilities;

import ca.odell.glazedlists.EventList;
import bard.ui.explodingpixels.macwidgets.SourceListItem;
import bard.ui.explodingpixels.macwidgets.SourceListModel;

import org.bushe.swing.event.EventSubscriber;
import org.bushe.swing.event.EventBus;
import bard.core.Value;
import bard.core.IntValue;

public class FacetSourceListItem extends SourceListItem {
    private static final Logger logger = 
	Logger.getLogger(FacetSourceListItem.class.getName());

    private Value facet;

    public FacetSourceListItem (Value facet) {
	super (facet.getId());
	this.facet = facet;

        if (facet instanceof IntValue) {
            setCounterValue ((Integer)facet.getValue());
        }
    }

    public Value getFacet () { return facet; }
}
