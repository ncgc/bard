// $Id$

package bard.ui.main;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.awt.Component;
import javax.swing.Icon;

import ca.odell.glazedlists.EventList;
import bard.ui.explodingpixels.macwidgets.SourceListCategory;
import bard.ui.explodingpixels.macwidgets.SourceListItem;

import org.bushe.swing.event.EventSubscriber;
import org.bushe.swing.event.EventBus;

import bard.ui.content.Content;
import bard.ui.content.ContentManager;
import bard.ui.event.ContentChangeEvent;

public class ContentSourceListCategory
    extends SourceListCategory 
    implements EventSubscriber<ContentChangeEvent> {
    private static final Logger logger = 
	Logger.getLogger(ContentSourceListCategory.class.getName());

    private ContentManager contentManager;

    public ContentSourceListCategory (String name) {
	this (name, null);
    }

    public ContentSourceListCategory (String name, 
				      ContentManager contentManager) {
	super (name);
	this.contentManager = contentManager;
	EventBus.subscribe(ContentChangeEvent.class, this);
    }

    public void setContentManager (ContentManager contentManager) {
	this.contentManager = contentManager;
    }
    public ContentManager getContentManager () { return contentManager; }

    public void onEvent (ContentChangeEvent e) {
    }
}
