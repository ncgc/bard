// $Id$

package bard.ui.main;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.Project;
import bard.core.SearchParams;
import bard.core.StructureSearchParams;
import bard.core.SuggestParams;
import bard.core.adapter.CompoundAdapter;
import bard.core.adapter.EntityAdapter;
import bard.core.rest.RESTEntityServiceManager;
import bard.ui.common.Grid;
import bard.ui.common.MolCellEditor;
import bard.ui.common.MolCellRenderer;
import bard.ui.content.Content;
import bard.ui.content.ContentManager;
import bard.ui.content.DefaultState;
import bard.ui.content.State;
import bard.ui.event.SearchEvent;
import bard.ui.explodingpixels.macwidgets.SourceListCategory;
import bard.ui.explodingpixels.macwidgets.SourceListItem;
import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import com.jidesoft.popup.JidePopup;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventSubscriber;
import org.jdesktop.application.Application;
import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXImagePanel;
import org.jdesktop.swingx.JXTaskPane;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SearchPane extends JPanel 
    implements EventSubscriber, ListSelectionListener, HyperlinkListener {

    private final static Logger logger = 
	Logger.getLogger(SearchPane.class.getName());
    
    private static Image logo;
    static {
	try {
	    String splash = Application.getInstance()
		.getContext().getResourceMap().getString("Application.splash");
	    logo = ImageIO.read(SearchPane.class.getResource(splash));
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
    private static final String URL = Application.getInstance()
	.getContext().getResourceMap().getString("Application.homepage");
    
    class SuggestWorker extends SwingWorker<Void, String> {
        String query;
        SuggestWorker (String query) {
            this.query = query;
        }

        @Override
        protected Void doInBackground () {
            //logger.info(Thread.currentThread()+": suggestion...");

            Set<String> suggestions = new TreeSet<String>();
            for (Content<?> content: contents) {
        	EntityService<?> es = content.getService();
                Map<String, java.util.List<String>> suggest = 
                    es.suggest(new SuggestParams (query, 5));

                for (Map.Entry<String, java.util.List<String>> me: 
                         suggest.entrySet()) {
                    for (String s : me.getValue()) {
                        s = s.trim();
                        if (s.length() > 0) {
                            String filter = me.getKey();
                            filter = SearchParams.BARD_SOLR_KEYS.convert(filter);
                            String item = s+" ["+filter+"]";
                            if (!suggestions.contains(item))
                        	suggestions.add(item);
                        }
                    }
                }
            }
            publish (suggestions.toArray(new String[20]));

            return null;
        }

        @Override
        protected void process (String... suggestions) {
            // update popup here in edt 
            DefaultListModel list = 
                (DefaultListModel)suggestList.getModel();
            list.clear();
            boolean addedItem = false;
            for (String item: suggestHistory) {
        	if (item.contains(query) && !list.contains(item)) {
        	    list.addElement(item);
        	    addedItem = true;
        	}
            }
            for (String s : suggestions) {
        	if (s != null && !list.contains(s)) {
        	    list.addElement(s);
        	    if (!suggestHistory.contains(s)) suggestHistory.add(s);
        	    addedItem = true;
        	}
            }
            if (addedItem && isTyping) showTextPopup();
            else popup.hidePopup();
        }

        @Override
        protected void done () {
            busy.stop();
        }
    }

    class SearchWorker extends SwingWorker<Throwable, Entity> 
        implements ListEventListener {
        SearchParams query;
//        ArrayList<State> states = new ArrayList<State>();

        SearchWorker (SearchParams query) {
            this.query = query;
            searchQueue.push(this);
        }

        @Override
        protected Throwable doInBackground () {
            try {
                logger.info("## SearchWorker: "
                            +Thread.currentThread()+" "+query.getQuery());

                for (Content<?> c : contents) {
                    State<?> state = c.search(query);
                    state.getEntities().addListEventListener(this);
                    container.add(state, query);
//                    states.add(state);
                }

                //Thread.currentThread().sleep(10000);
            }
            catch (Exception ex) {
                logger.info("SearchWorker: "
                            +Thread.currentThread()+" canceled!");
                return ex;
            }

            return null;
        }

        @Override
        protected void process (Entity... entities) {
            for (Entity e : entities) {
                //logger.info(e.toString());
                JXTaskPane tp = createSearchResultTaskPane (e, query);
                container.add(tp, e);
                if (!containerClass.contains(e.getClass())) containerClass.add(e.getClass());
                
                SearchPane.this.revalidate();
                SearchPane.this.repaint();
            }
        }

        public void listChanged (ListEvent e) {
            while (e.nextBlock()) {
                switch (e.getType()) {
                case ListEvent.INSERT:
                    break;
                case ListEvent.UPDATE:
                    break;
                case ListEvent.DELETE:
                    break;
                }
            }
            
            EventList<Entity> list = (EventList)e.getSource();        
            for (int i = 0; i < 5 && i < list.size(); ++i) {
                publish (list.get(i));
            }
        }

        @Override
        protected void done () {
            busy.stop();
            /*
            for (State s : states) {
                s.getEntities().removeListEventListener(this);
            }
            */

            search.setEnabled(true);
            if (structureSearchDialog != null)
        	structureSearchDialog.setBusy(false);
            try {
                Throwable t = get ();
                if (t != null) {
                    JOptionPane.showMessageDialog
                        (SearchPane.this, "Search canceled!", "Info", 
                         JOptionPane.INFORMATION_MESSAGE);
                }
                else {
                }
            }
            catch (Exception ex) {
            }
        }
    }

    class StructureSearchWorker extends SearchWorker {

        StructureSearchWorker(SearchParams query) {
            super(query);
        }

         @Override
        protected Throwable doInBackground () {
            try {
                logger.info("## StructureSearchWorker: "
                            +Thread.currentThread()+" "+query.getQuery());
                for (Content<?> c : contents) {
                	if (c.getEntityClass() == Compound.class) {
                		State state = c.search(query);
                		state.getEntities().addListEventListener(this);
                                container.add(state, query);
//                              states.add(state);
                	} else {
                		State state = new DefaultState(c, new ArrayList<Entity>());
                                container.add(state, query);
                		c.push(state);
//                		states.add(state);
                	}
                }

            }
            catch (Exception ex) {
                return ex;
            }
            return null;
        }
    }


    SearchTextField search;
    boolean isTyping = false;
//    JXTaskPaneContainer container;
    TabbedSearchResultPane container;
    JidePopup popup;
    UIUtil.BusyPane busy;
    JList suggestList;
    Vector<String> suggestHistory = new Vector<String>() {
        public boolean add(String item) {
            super.add(item);
            if (this.size() > 100) this.remove(0);
            return true;
        }
    };
    JButton searchBtn;
    JPanel popupPane;
    StructureSearchDialog structureSearchDialog;
    public ArrayList<Class<?>> containerClass = new ArrayList<Class<?>>();

    /*
     * list of all running search threads
     */
    LinkedList<Future> searchQueue = new LinkedList<Future>();

    private Content<?>[] contents;

    public SearchPane () {
        this (new Content<?>[0]);
    }

    public SearchPane (Content<?>[] contents) {
        EventBus.subscribe(CaretEvent.class, this);
        EventBus.subscribe(SearchEvent.class, this);

        setBorder (BorderFactory.createEmptyBorder(30, 0, 0, 0));
        setLayout (new BorderLayout (0, 5));
        setBackground (Color.white);

        JXImagePanel img = new JXImagePanel ();
        img.setBackground(Color.white);
        img.setImage(logo);
        add (img, BorderLayout.NORTH);
        add(createSearchPane ());
    
        this.contents = contents;
    }
    
    public void setContents(Content<?>[] contents) { this.contents=contents;}

//    public void setServiceManager (Content<?>[] contents) {
//        this.esm = esm;
//        if (esm != null) {
//            services = esm.getServices();
//        }
//        else {
//            services = null;
//        }
//    }
//
//    public EntityServiceManager getServiceManager () { return esm; }

    public void onEvent (Object e) {
        if (e instanceof SearchEvent) {
            onEvent ((SearchEvent)e);
        }
        else if (e instanceof CaretEvent) {
            onEvent ((CaretEvent)e);
        }
    }

    public void onEvent (SearchEvent e) {
        Object source = e.getSource();

        if (source == structureSearchDialog || e.getQuery() instanceof StructureSearchParams) {
            logger.info("** search: "+e.getQuery());
            search ((StructureSearchParams)e.getQuery());
            return;
        }

        if (source != search) {
            return;
        }

        isTyping = false;
        SearchParams query = e.getQuery();
        if (query == null || query.getQuery() == null) {
            clearSearch ();
            return;
        }

        String text = query.getQuery();
        logger.info("** search: "+text);
        search (query);
    }

    protected void clearSearch () {
        busy.stop();
        container.removeAll();
        popup.hidePopup();
        containerClass = new ArrayList<Class<?>>();

        if (!searchQueue.isEmpty()) {
            Future<?> f = searchQueue.pop();
            f.cancel(true);
        }
    }

    public String getSearch() {
    	return search.getText();
    }
    
    public void setSearch(State<?> state) {
    	process(state);
    }
    
    public void setQuery(SearchParams query) {
	clearSearch();
    	if (query instanceof StructureSearchParams) {
    	    structureSearchDialog.setSearch(query.getOrigText());  		
    	    search.setText("");
    	} else if (query == null) {
    	    search.setText("");
    	} else {
    	    search.setText(query.getOrigText());
    	}
    }
    
    protected void process (State<?> state) {
	Class clazz = state.getContent().getEntityClass();
	int index = 0;
	if (clazz == Compound.class) {
	    index = 0;
	} else if (clazz == Assay.class) {
	    index = 1;
	} else if (clazz == Project.class) {
	    index = 2;
	}
	
	//container.add(state, query);
	if (container.states.get(index) != state) {
	    String text = "";
	    if (state != null && state.getQuery() != null)
		text = state.getQuery().getOrigText();
	    if (search.getText() == null || !search.getText().equals(text)) {
		setQuery(state.getQuery());
	    }

	    container.states.set(index, state);
	    EventList<Entity> list = (EventList<Entity>)state.getEntities();
	    for (int i=0; i<5; i++) {
		if (i < list.size()) {
		    Entity e = list.get(i);
		    JXTaskPane tp = createSearchResultTaskPane (e, state.getQuery());
		    container.add(tp, e);
		}
	    }
	    container.setTitleAt(index, TabbedSearchResultPane.entities[index]);
	    if (!containerClass.contains(clazz)) 
		containerClass.add(clazz);
	}
	
//	Entity[] newList = new Entity[5];
//	EventList<Entity> list = (EventList)state.getEntities();        
//	for (int i = 0; i < 5 && i < list.size(); ++i) {
//	    if (list.size() > i)
//		newList[i] = list.get(i);
//	}
//	for (Entity e: entities)
//	container.containers.get(index).removeAll();
//	container.setTitleAt(index, TabbedSearchResultPane.entities[index]);
//	container.states.set(index, );
//    	if (entities != null)
//    		for (Entity e : entities) {
//    			if (e != null) {
//    				//logger.info(e.toString());
//				if (!containerClass.contains(e.getClass())) 
//				    containerClass.add(e.getClass());
//    				JXTaskPane tp = createSearchResultTaskPane (e);
//    				container.add(tp, e);
//    				
//    				SearchPane.this.revalidate();
//    				SearchPane.this.repaint();
//    			}
//    		}
	this.revalidate();
	this.repaint();
    }
    
    public void onEvent (CaretEvent e) {
        if (e.getSource() != search) {
            return;
        }

        isTyping = true;
        String text = search.getText();
        logger.info("dot="+e.getDot()+" mark="+e.getMark()+" text="+text);
        if (text != null && e.getDot() == text.length() && text.length() > 3) {
            suggest (text);
        }
        else {
            popup.hidePopup();
        }
    }

    protected void suggest (String text) {
        //showTextPopup (); // not sure why popup's size changed
        busy.start();
        new SuggestWorker (text).execute();
    }

    protected void search (SearchParams query) {
        popup.hidePopup();
        container.removeAll();
        busy.start();
        search.setEnabled(false);

        new SearchWorker (query).execute();
    }

    protected void search (StructureSearchParams params) {
        container.removeAll();
        if (structureSearchDialog == null) {
            Frame root = (Frame)SwingUtilities.getAncestorOfClass
                (Frame.class, SearchPane.this);
            structureSearchDialog = new StructureSearchDialog (root);
            UIUtil.centerComponent(root, structureSearchDialog);
        }
        structureSearchDialog.setBusy(true);
        structureSearchDialog.setSearch("sim:"+params.getQuery());
        new StructureSearchWorker (params).execute();
    }

    protected JXTaskPane createSearchResultTaskPane (Entity e, SearchParams query) {
        EntityAdapter adapter = new EntityAdapter (e);
        JXTaskPane tp = new JXTaskPane ();
        tp.setLayout(new BorderLayout ());
        tp.setTitle(e.getName());
        tp.setCollapsed(false);
        tp.setScrollOnExpand(true);
        tp.setIcon(UIUtil.getIcon(e.getClass(), 16));
        ((JComponent)tp.getContentPane()).setBackground(Color.white);

        JEditorPane ep = new JEditorPane ();
        ep.setContentType("text/html");
        ep.setEditable(false);
        ep.setOpaque(false);
        StringBuilder sb = new StringBuilder(UIUtil.CSS);
        String highlight = adapter.getSearchHighlight(query);
        if (highlight != null)
        	sb.append(highlight);
        sb.append("...&nbsp;<i><a href=\"bard://"
        		+e.getClass().getSimpleName().toLowerCase()+"s"
        		+"/"+e.getId()+"?view="
        		+bard.ui.content.View.Type.Record+"\">more</a></i>");
        ep.setText(sb.toString());
        ep.addHyperlinkListener(this);

        if (e instanceof Compound) {
            Compound c = (Compound)e;
            adapter = new CompoundAdapter (c);

            final Grid grid = new Grid ();
            grid.setCellRenderer(new MolCellRenderer ());
            MolCellEditor mce = new MolCellEditor ();
            mce.instrumentContextMenu();
            grid.setCellEditor(mce);

            grid.setPreferredSize(new Dimension (200, 200));
            grid.setMinimumSize(new Dimension (150, 150)); //!!! shouldn't require this, plus plenty of wasted space
            grid.addValue(c);

//            MolRenderer mview = new MolRenderer ();
//            mview.setEditable(1);
//
//            Molecule m = (Molecule)((CompoundAdapter)adapter).toFormat
//                (MolecularData.Format.NATIVE);
//            MDocument doc = new MDocument (m);
//            doc.setBondSetThickness(0, .125);
//            mview.setColorScheme
//                (m.hasAtomSet() || m.hasBondSet()
//                 ? DispOptConsts.MONO_SCHEME_S 
//                 : DispOptConsts.CPK_SCHEME_S);
//            
//            mview.setDocument(0, doc);

            JSplitPane split = new JSplitPane ();
            split.setOpaque(false);
            split.setLeftComponent(grid);
            split.setRightComponent(ep);
            split.setDividerSize(4);
            split.setContinuousLayout(true);
            split.setBorder(null);

            tp.add(split);
        }
        else {
            tp.add(ep);
        }
        
        return tp;
    }

    public void hyperlinkUpdate (HyperlinkEvent e) {
        java.net.URI uri = UIUtil.getLinkURI(e);
        /*
          for (Enumeration en = attrs.getAttributeNames(); 
          en.hasMoreElements();) {
          Object attr = en.nextElement();
          Object value = attrs.getAttribute(attr);
          System.out.println(attr+"["+attr.getClass()+"]="
          +value+"["+value.getClass()+"]");
          }
        */

        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
        	//logger.info("hyperlink "+uri);
        	try {
        		EventBus.publish(e);
        	}
        	catch (Exception ex) {
        		logger.log(Level.SEVERE, "Invalid hyperlink "+uri, ex);
        	}
        }
        else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
        	JComponent c = (JComponent)e.getSource();
        	c.setToolTipText(uri.toString());
        }
    }

    public void valueChanged (ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            return;
        }
        JList list = (JList)e.getSource();
        String value = (String)list.getSelectedValue();
        if (value != null) {
            logger.info("selection: "+value);
            search.setText(value);
            search.select(0, search.getText().length());
            popup.hidePopup();
            search.search();
        }
    }

    Component createSearchPane () {
        JPanel pane = new JPanel (new BorderLayout (0, 5));
        pane.setOpaque(false);

        JPanel sp = new JPanel (new BorderLayout (5, 0));
        sp.setOpaque(false);
        busy = new UIUtil.BusyPane();
        sp.add(busy, BorderLayout.EAST);
        search = new SearchTextField (40) {
                @Override
                protected void structureSearchInvoked () {
                    showStructureDialog ();
                }
            };
        sp.add(search);

        JPanel vp = new JPanel (new BorderLayout (0, 3));
        vp.setOpaque(false);
        vp.add(sp);

        JXHyperlink link = new JXHyperlink ();
        link.setText("Structure search...");
        link.setToolTipText("Click for structure search dialog");
        link.addActionListener(new ActionListener () {
                public void actionPerformed (ActionEvent e) {
                    showStructureDialog ();
                }
            });
        Box box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        box.add(link);
        vp.add(box, BorderLayout.SOUTH);

        JPanel center = new JPanel ();
        center.setOpaque(false);
        center.add(vp);
        pane.add(center, BorderLayout.NORTH);
        pane.add(createResultPane ());

        setupPopup ();
        return pane;
    }

    void showStructureDialog () {
        if (structureSearchDialog == null) {
            Frame root = (Frame)SwingUtilities.getAncestorOfClass
                (Frame.class, SearchPane.this);
            structureSearchDialog = new StructureSearchDialog (root);
            UIUtil.centerComponent(root, structureSearchDialog);
        }

        structureSearchDialog.setVisible(true);
    }

    void showTextPopup () {
        ((CardLayout)popupPane.getLayout()).show(popupPane, "text");
        popup.setPreferredSize
            (new Dimension (search.getPreferredSize().width-15, 250));
        popup.showPopup(search);
    }

    void showStructurePopup () {
        ((CardLayout)popupPane.getLayout()).show(popupPane, "structure");
        popup.setPreferredSize
            (new Dimension (search.getPreferredSize().width-15, 400));
        popup.showPopup(search);
    }
        
    void setupPopup () {
        suggestList = new JList (new DefaultListModel ());
        suggestList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        suggestList.addListSelectionListener(this);


        searchBtn = new JButton 
            ("Search", IconFactory.App.Search2.getIcon());
        //searchBtn.setEnabled(false);
        JPanel bp = new JPanel ();
        bp.add(searchBtn);
        searchBtn.addActionListener(new ActionListener () {
                public void actionPerformed (ActionEvent e) {
                    search.search();
                }
            });

        popupPane = new JPanel (new CardLayout ());
        popupPane.add(UIUtil.iTunesScrollPane
                      (suggestList, false, false), "text");
        //popupPane.add(new StructureSearchPane (), "structure");

        popup = new JidePopup ();
        popup.setResizable(true);
        popup.setDefaultMoveOperation(JidePopup.MOVE_ON_MOVED);

        JPanel pane = new JPanel (new BorderLayout (0, 2));        
        pane.add(popupPane);
        pane.add(bp, BorderLayout.SOUTH);

        popup.add(pane);
    }

    Component createResultPane () {
        JPanel pane = new JPanel (new BorderLayout ());
        pane.setOpaque(false);
        container = new TabbedSearchResultPane(this);
        pane.add(container);
//        container = new JXTaskPaneContainer ();
//        container.setBackground(Color.white);
//        pane.add(UIUtil.iTunesScrollPane(container));
        return pane;
    }

    public SearchTextField getSearchField () { return search; }

    public static void main (String[] argv) throws Exception {
        final EntityServiceManager esm = new RESTEntityServiceManager 
            (argv.length > 0 ? argv[0] : "http://bard.nih.gov/api/latest");
        final ContentManager cm = ContentManager.getInstance(esm);
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    JFrame f = new JFrame ("SearchPane");
		    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    f.getContentPane().add(new SearchPane (cm.getContents()));
		    f.pack();
		    f.setSize(600, 600);
		    f.setVisible(true);
		}
	    });
    }
}
