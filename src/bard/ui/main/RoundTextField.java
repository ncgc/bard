// $Id$
package bard.ui.main;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Insets;

import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.AbstractBorder;

import org.jdesktop.swingx.painter.Painter;
import org.jdesktop.swingx.painter.CapsulePainter;
import org.jdesktop.swingx.painter.RectanglePainter;
import org.jdesktop.swingx.painter.effects.*;

import bard.ui.limewire.ui.swing.painter.BorderPainter;
import static bard.ui.util.ColorUtil.*;

public class RoundTextField extends JTextField {
    private BorderPainter<Component> bPainter; // border painter
    private RectanglePainter<Component> cPainter; // component painter

    public RoundTextField () {
	init ();
    }

    public RoundTextField (int columns) {
	super (columns);
	init ();
    }

    public RoundTextField (String text) {
	super (text);
	init ();
    }

    void init () {
	int arcWidth = 10, arcHeight = 10;

	bPainter = new BorderPainter<Component>
	    (arcWidth, arcHeight, 
	     BORDER, 
	     BEVEL_LEFT, 
	     BEVEL_TOP1, 
	     BEVEL_TOP2, 
	     BEVEL_RIGHT,
	     BEVEL_BOTTOM,
	     BorderPainter.AccentType.GREEN_SHADOW);

	cPainter = new RectanglePainter<Component>();
	cPainter.setInsets(new Insets (0,1,0,0));
	cPainter.setAntialiasing(true);
	cPainter.setRounded(true);
	cPainter.setRoundHeight(arcHeight);
	cPainter.setRoundWidth(arcWidth);
	cPainter.setBorderPaint(BORDER);
	cPainter.setFillPaint(getBackground ());
    }

    @Override
    protected void paintBorder (Graphics g) {
	bPainter.paint((Graphics2D)g, this, getWidth ()+1, getHeight ()+1);
    }

    @Override
    protected void paintComponent (Graphics g) {
	cPainter.paint((Graphics2D)g, this, getWidth (), getHeight ());
	super.paintComponent(g);
    }
}
