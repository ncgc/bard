package bard.ui.main;

import bard.ui.content.State;
import bard.ui.content.View;

public class DisplayState {
    ContentSourceListItem content;
    View.Type view;
    State state;
    
    public DisplayState(ContentSourceListItem content, View.Type view, State state) {
        this.content = content;
        this.view = view;
        this.state = state;
    }
    
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj instanceof DisplayState) {
            DisplayState compare = (DisplayState)obj;
            if (this.content==compare.content && this.view==compare.view && this.state==compare.state)
                return true;
        }
        return false;
    }
}
