// $Id$

package bard.ui.main;

import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import org.jdesktop.swingx.JXImagePanel;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.hyperlink.*;

import org.jdesktop.application.Application;
import edu.stanford.ejalbert.BrowserLauncher;

public class WelcomePane extends JXImagePanel {
    private final static Logger logger = 
	Logger.getLogger(WelcomePane.class.getName());
    
    private static Image image;
    static {
	try {
	    String splash = Application.getInstance()
		.getContext().getResourceMap().getString("Application.splash");
	    image = ImageIO.read(WelcomePane.class.getResource(splash));
	}
	catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
    private static final String URL = Application.getInstance()
	.getContext().getResourceMap().getString("Application.homepage");
    
	
    public WelcomePane () {
	setLayout (new BorderLayout ());
	setBackground (Color.white);
	setImage (image);

	JXHyperlink link = new JXHyperlink 
	    (new AbstractHyperlinkAction (URL) {
		    public void actionPerformed (ActionEvent e) {
			logger.info("target="+getTarget());
			try {
			    new BrowserLauncher().openURLinBrowser
				((String)getTarget ());
			}
			catch (Exception ex) {
			    logger.log(Level.SEVERE, 
				       "Can't launch external browser "
				       +getTarget (), ex);
			}
		    }
		});
	JPanel lp = new JPanel (new BorderLayout ());
	lp.setOpaque(false);
	lp.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
	lp.add(link);

	JPanel llp = new JPanel (new BorderLayout ());
	llp.setOpaque(false);
	llp.add(lp, BorderLayout.EAST);

	add (llp, BorderLayout.SOUTH);
    }

    public static void main (String[] argv) throws Exception {
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    JFrame f = new JFrame ("WelcomePane");
		    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    f.getContentPane().add(new WelcomePane ());
		    f.pack();
		    f.setSize(600, 600);
		    f.setVisible(true);
		}
	    });
    }
}
