// $Id$

package bard.ui.main;

import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;
import javax.swing.BorderFactory;
import javax.swing.LookAndFeel;
import javax.swing.plaf.ComponentUI;

/*
import org.jdesktop.swingx.painter.Painter;
import org.jdesktop.swingx.painter.RectanglePainter;
*/

import bard.ui.explodingpixels.swingx.EPToggleButton;
import bard.ui.explodingpixels.swingx.EPPanel;
import bard.ui.explodingpixels.macwidgets.MacButtonFactory;
import bard.ui.explodingpixels.painter.Painter;
import bard.ui.icons.IconFactory;
import bard.ui.content.View;
import bard.ui.util.UIUtil;

public class ViewControl extends EPPanel implements ActionListener {
    static final Insets MARGIN = new Insets (3, 6, 3, 6);


    LookAndFeel lnf = UIUtil.getSystemLookAndFeel();

    class ViewButton extends JToggleButton {
	private Painter<Component> fBackgroundPainter
	    = MacButtonFactory.getGradientButtonPainter();

	ViewButton (Icon icon) {
	    super (icon);
	    if (lnf != null) {
		ComponentUI ui = lnf.getDefaults().getUI(this);
		setUI (ui);
	    }
	    else {
		setMargin (MARGIN);
	    }
	    setRolloverEnabled (true);
	    setFocusPainted (false);
	}
	/*
	@Override
	protected void paintComponent(Graphics g) {
	    if (fBackgroundPainter != null) {
		Graphics2D graphics = (Graphics2D) g.create();
		fBackgroundPainter.paint
		    (graphics, this, getWidth(), getHeight());
		graphics.dispose();
	    }
	    
	    super.paintComponent(g);
	}
	*/
    }

    private AbstractButton listBtn;
    private AbstractButton gridBtn;
    private AbstractButton recordBtn;
    private AbstractButton columnBtn;

    private AbstractButton hiddenBtn; // non visible toggle 

    private View.Type view = View.Type.List; // current view
    private Map<View.Type, AbstractButton> viewToggles = 
	new HashMap<View.Type, AbstractButton>();


    public ViewControl () {
	initComponent ();
    }

    protected void initComponent () {
	//setBorder (BorderFactory.createEmptyBorder(1,1,1,1));
	setLayout (new GridLayout (1, 3, -1, 0));
	setOpaque (false);


	AbstractButton btn;
	ButtonGroup bg = new ButtonGroup ();
	bg.add(hiddenBtn = new JToggleButton ());

	add (btn = createButton (IconFactory.View.List.getIcon()));
        btn.putClientProperty("JButton.buttonType", "segmentedTextured");
        btn.putClientProperty("JButton.segmentPosition", "first");
	btn.setToolTipText("Show items in list view");
	bg.add(btn);
	viewToggles.put(View.Type.List, btn);

        /*
	add(btn = createButton
	    (IconFactory.getInstance().getImageIcon(IconFactory.VIEW_COLUMN)));
        btn.putClientProperty("JButton.buttonType", "segmentedTextured");
        btn.putClientProperty("JButton.segmentPosition", "middle");
	btn.setToolTipText("Show items in column view");
	bg.add(btn);
	viewToggles.put(Content.View.Column, btn);
        */

	add (btn = createButton (IconFactory.View.Grid.getIcon()));
        btn.putClientProperty("JButton.buttonType", "segmentedTextured");
        btn.putClientProperty("JButton.segmentPosition", "middle");
	btn.setToolTipText("Show items in group view");
	bg.add(btn);
	viewToggles.put(View.Type.Grid, btn);

	add (btn = createButton (IconFactory.View.Record.getIcon()));
        btn.putClientProperty
	    ("JButton.buttonType", "segmentedTextured");
        btn.putClientProperty("JButton.segmentPosition", "last");
	btn.setToolTipText("Show items in record view");
	bg.add(btn);
	viewToggles.put(View.Type.Record, btn);
    }

    AbstractButton createButton (Icon icon) {
	ViewButton vb = new ViewButton (icon);
	vb.addActionListener(this);
	return vb;
    }

    public void actionPerformed (ActionEvent e) {
	for (Map.Entry<View.Type, AbstractButton> me 
		 : viewToggles.entrySet()) {
	    if (me.getValue() == e.getSource()) {
		firePropertyChange ("view", view, me.getKey());
		this.view = me.getKey();
	    }
	}
    }

    public View.Type getView () { return view; }
    public void setView (View.Type view) {
	if (this.view == view) {
	    return;
	}
	firePropertyChange("view", this.view, view);
	AbstractButton ab = viewToggles.get(view);
        if (ab.isEnabled()) { // only select if it's enabled
            ab.setSelected(true);
            this.view = view;
        }
    }

    public void setEnabled (boolean enabled) {
	for (AbstractButton tb : viewToggles.values()) {
	    tb.setEnabled(enabled);
	}
	if (!enabled) {
	    hiddenBtn.setSelected(true);
	}
	else { // restore the previous state
	    viewToggles.get(view).setSelected(true);
	}
    }

    public void setEnabled (View.Type... views) {
	for (AbstractButton tb : viewToggles.values()) {
	    tb.setEnabled(false);
	}
	for (View.Type v : views) {
	    AbstractButton toggle = viewToggles.get(v);
	    if (toggle != null) {
		toggle.setEnabled(true);
	    }
	}
    }
}
