package bard.ui.main;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.Entity;
import bard.core.Project;
import bard.core.SearchParams;
import bard.ui.content.State;
import bard.ui.content.View;
import bard.ui.util.UIUtil;
import com.jidesoft.swing.JideTabbedPane;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JEditorPane;

/**
 * A one line summary.
 *
 * @author Rajarshi Guha
 */
public class TabbedSearchResultPane extends JideTabbedPane {
    static String[] entities = {"Compounds", "Assays", "Projects"};
    static View.Type[] views = {View.Type.Grid, View.Type.List, View.Type.Record};
    ArrayList<JXTaskPaneContainer> containers = new ArrayList<JXTaskPaneContainer>();
    //compoundContainer, assayContainer, projectContainer;
    public ArrayList<State> states = new ArrayList<State>(); 
    //compoundState, assayState, projectState;
    SearchPane searchPane;
    SearchParams query;

    public TabbedSearchResultPane(SearchPane searchPane) {
	this.searchPane = searchPane;
	for (String entity: entities) {
	    JXTaskPaneContainer container = new JXTaskPaneContainer();
	    container.setBackground(Color.white);
	    containers.add(container);
	    add(entity, UIUtil.iTunesScrollPane(container));
	    states.add(null);
	}
//        compoundContainer = new JXTaskPaneContainer();
//        compoundContainer.setBackground(Color.white);
//
//        assayContainer = new JXTaskPaneContainer();
//        assayContainer.setBackground(Color.white);
//
//        projectContainer = new JXTaskPaneContainer();
//        projectContainer.setBackground(Color.white);

//        add("Compounds", UIUtil.iTunesScrollPane(compoundContainer));
//        add("Assays", UIUtil.iTunesScrollPane(assayContainer));
//        add("Projects", UIUtil.iTunesScrollPane(projectContainer));
    }

    public void removeAll() {
	for (int i=0; i<entities.length; i++) {
	    containers.get(i).removeAll();
	    setTitleAt(i, entities[i]);
	    states.set(i,  null);
	}
//        compoundContainer.removeAll();
//        assayContainer.removeAll();
//        projectContainer.removeAll();
//        setTitleAt(0, "Compounds");
//        setTitleAt(1, "Assays");
//        setTitleAt(2, "Projects");
    }

    public void add(State state, SearchParams query) {
	this.query = query;
	int index = -1;
	if (state != null) {
	    if (state.getContent().getEntityClass() == Compound.class)
		index=0;
	    else if (state.getContent().getEntityClass() == Assay.class)
		index=1;
	    else if (state.getContent().getEntityClass() == Project.class)
		index=2;
	}
	if (index > -1) {
	    states.set(index, state);
	    int hits = state.size();
	    setTitleAt(index, entities[index]+" (" + hits + ")");
	};	
    }
    
    protected void updateSummaryPane(int index, int hits) {
	JXTaskPaneContainer container = containers.get(index);
        if (((JXTaskPane)container.getComponent(0)).getTitle().startsWith("Search ")) {
            return;
        }
        JXTaskPane tp = new JXTaskPane ();
        tp.setLayout(new BorderLayout ());
        tp.setTitle("Search on: "+query.getOrigText());
        tp.setCollapsed(false);
        tp.setScrollOnExpand(true);
        //tp.setIcon(UIUtil.getIcon(state.getContent().getEntityClass(), 16));
        ((JComponent)tp.getContentPane()).setBackground(Color.white);

        JEditorPane ep = new JEditorPane ();
        ep.setContentType("text/html");
        ep.setEditable(false);
        ep.setOpaque(false);
        StringBuilder sb = new StringBuilder(UIUtil.CSS);
        sb.append("...&nbsp;<i><a href=\"bard://"
        		+entities[index].toLowerCase()+"/"
        	//	+states.get.getEntity().getId()
        		+"?view="+views[index]+"\">See all "+hits+" hit "+entities[index].toLowerCase()+"</a></i>");
        ep.setText(sb.toString());
        ep.addHyperlinkListener(searchPane);
        tp.add(ep);
        
        if (((JXTaskPane)container.getComponent(0)).getTitle().startsWith("Search ")) {
            container.remove(container.getComponent(0));
            container.add(tp, null, 0);
        } else {
            container.add(tp, null, 0);
        }
    }
    
    public void add(JXTaskPane taskPane, Entity e) {
	int index = 0;
	if (e instanceof Compound) {
	    index = 0;
	} else if (e instanceof Assay) {
	    index = 1;
	} else if (e instanceof Project) {
	    index = 2;
	}
	JXTaskPaneContainer container = containers.get(index);
	container.add(taskPane);
	State state = states.get(index);
	if (state != null) {
	    int hits = state.size();
	    setTitleAt(index, entities[index]+" (" + hits + ")");
	    if (hits > 1)
		updateSummaryPane(index, hits);
	}
//        if (e instanceof Compound) {
//            compoundContainer.add(taskPane);
//            setTitleAt(0, "Compounds (" + (compoundState == null ? compoundContainer.getComponentCount() : compoundState.size()) + ")");
//        } else if (e instanceof Assay) {
//            assayContainer.add(taskPane);
//            setTitleAt(1, "Assays (" + (assayState == null ? assayContainer.getComponentCount() : assayState.size()) + ")");
//        } else if (e instanceof Project) {
//            projectContainer.add(taskPane);
//            setTitleAt(2, "Projects (" + (projectState == null ? projectContainer.getComponentCount() : projectState.size()) + ")");
//        }
    }

}
