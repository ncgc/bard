package bard.ui.main;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.geom.*;
import javax.swing.SwingUtilities;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingworker.SwingWorker;

import bard.ui.icons.IconFactory;
import bard.ui.util.UIUtil;
import bard.ui.util.ColorUtil;

public class SplashScreen extends JDialog {
    private static final Logger logger = Logger.getLogger
	(SplashScreen.class.getName());

    private static final int TRACK_HEIGHT = 5;
    private static final Color BORDER_COLOR = Color.white;
    private static final int BORDER_WIDTH = 1;

    static class ProgressBar extends JComponent {

	int min = 0, max = 100;
	int value = 0;

	ProgressBar () {
	}

	ProgressBar (int value, int min, int max) {
	    this.min = min;
	    this.max = max;
	    if (value <= max && value >= min) {
		this.value = value;
	    }
	    else {
		throw new IllegalArgumentException ("Value not within range");
	    }
	    setPreferredSize (new Dimension (100, 10));
	}

	@Override
	protected void paintComponent (Graphics g) {
	    Graphics2D g2 = (Graphics2D)g;
	    if (!isEnabled ()) {
		g2.setComposite(AlphaComposite.getInstance
				(AlphaComposite.SRC_OVER, 0.5f));
	    }

	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
				RenderingHints.VALUE_ANTIALIAS_ON);

	    Rectangle bounds = getBounds ();
	    double y = (bounds.getHeight() - TRACK_HEIGHT - 2) / 2.0;
	    double w = (double)(value- min)/(max-min) *bounds.getWidth();
    
	    RoundRectangle2D track = new RoundRectangle2D.Double
		(0, y, bounds.getWidth() - 2, TRACK_HEIGHT, 6, 4);
	    
	    g2.setColor(ColorUtil.LightShadow);
	    g2.fill(track);
	    g2.setColor(Color.black);
	    g2.draw(track);

	    RoundRectangle2D filler = new RoundRectangle2D.Double
		(0, y, w - 2, TRACK_HEIGHT, 6, 4);
	    g2.setPaint(ColorUtil.Default);
	    g2.fill(filler);
	}

	public void setValue (int value) {
	    if (value < min || value > max) {
		throw new IllegalArgumentException ("Value out of range");
	    }
	    this.value = value;
	    repaint ();
	}
	public void setMin (int min) { this.min = min; }
	public int getMin () { return min; }
	public void setMax (int max) { this.max = max; }
	public int getMax () { return max; }
    }

    ProgressBar progress;
    JLabel label;
    JTextArea area;

    private SplashScreen () {
	this (null);
    }

    private SplashScreen (String mesg) {
	this (null, mesg);
    }

    private SplashScreen (Frame owner, String mesg) {
	super (owner, false);
	initWindow (mesg);
    }

    private static final SplashScreen instance = new SplashScreen ();
    public static SplashScreen getInstance () { 
	return instance;
    }

    protected void initWindow (String mesg) {
	setUndecorated (true);
	if (isAlwaysOnTopSupported ()) {
	    setAlwaysOnTop (true);
	}

	JPanel pane = new JPanel (new MigLayout ("", "[grow,fill]"));
	JLabel icon = new JLabel (IconFactory.Splash.BARD.getIcon());
	//pane.add(icon, "growy");
	pane.add(icon, "growy, dock north");

	label = new JLabel (mesg);
	label.setPreferredSize(new Dimension (200, 30));
	label.setBackground(Color.white);

	area = new JTextArea (2, 20);
	area.setBackground(Color.white);
	area.setEditable(false);
	area.setLineWrap(true);
	area.setWrapStyleWord(true);
	area.setText(mesg);

	//pane.add(label, "growx, wrap 2");
	//pane.add(area, "growx, wrap 2");
	pane.add(area, "wrap 2");
	progress = new ProgressBar (0, 0, 100);
	pane.add(progress, "split, span");
	progress.setBackground(label.getBackground());

	pane.setBackground(label.getBackground());
	pane.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

	getContentPane().add(pane);
	pack ();

	//setSize (380, 100);
	UIUtil.centerComponent(this);
    }

    public void setMin (int min) { 
	progress.setMin(min);
    }
    public int getMin () { return progress.getMin(); }
    public void setMax (int max) { 
	progress.setMax(max); 
    }
    public int getMax () { return progress.getMax(); }

    public void setValue (int value) {
	progress.setValue(value);
    }

    public void setText (String text) {
	//label.setText(text);
	area.setText(text);
    }

    public void setTextValue (String text, int value) {
	//label.setText(text);
	area.setText(text);
	progress.setValue(value);
	repaint ();
    }

    public static void main (String[] argv) throws Exception {
	SplashScreen diag = new SplashScreen ("Loading client...");
	diag.setVisible(true);
    }
}
