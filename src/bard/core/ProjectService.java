package bard.core;

import java.util.Map;

public interface ProjectService extends EntityService<Project> {

    Map<String, Object> getProjectSummary(Project project);
}
