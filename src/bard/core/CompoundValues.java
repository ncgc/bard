package bard.core;

public interface CompoundValues extends EntityValues {
    static final String MolecularValue = "CompoundMolecularData";

    /**
     * Identifiers
     */
    static final String PubChemCIDValue = "CompoundPubChemCID"; // long
    static final String PubChemSIDValue = "CompoundPubChemSID"; // long
    static final String ProbeIDValue = "CompoundProbeID"; // string
    static final String CASValue = "CompoundCAS"; // string
    static final String UNIIValue = "CompoundUNII"; // string
    static final String IUPACNameValue = "CompoundIUPACName"; // string
    static final String IUPACInChIValue = "CompoundIUPACInChI"; // string
    static final String IUPACInChIKeyValue = "CompoundIUPACInChIKey"; // string
    static final String TripodHashKeyValue = "CompoundTripodHashKey"; // string
    static final String CompoundClass = "compoundClass";
    static final String NumAssays = "numAssay";
    static final String NumActiveAssays = "numActiveAssays";


    /**
     * Registration properties
     */
    static final String SubstanceCategoryValue = "CompoundSubstanceCategory";
    enum SubstanceCategory {
        Chemical,
            Protein,
            NucleicAcid,
            Polymer,
            Others;
            }

    static final String RegulatoryStatusValue = "CompoundRegulatoryStatus";
    enum RegulatoryStatus {
        Approved,
            Withdrawn,
            Phase1,
            Phase2,
            Phase3;
    }

    static final String RegulatoryAgencyValue = "CompoundRegulatoryAgency";
    enum RegulatoryAgency {
        FDA,
            EMA,
            HealthCanada;
            
            // more needed here...            
    }


    /**
     * Molecular properties
     */
    static final String MolecularTypeValue = "CompoundMolecularType";
    enum MolecularType {
        Drug,
            Probe,
            Tool,
            Naive,
            ClinicalCandidate;
            }
}
