package bard.core;

import java.util.Iterator;
import java.util.List;
import java.util.Collection;

public interface ServiceIterator<E> extends Iterator<E> {
    List<E> next (int size); // paging
    long getConsumedCount (); // count consumed thus far
    long getCount (); // total count (if known)
    void done ();
    Value getETag ();
    Collection<Value> getFacets ();
}
