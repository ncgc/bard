package bard.core;

import javax.jdo.annotations.PersistenceCapable;

@PersistenceCapable(detachable="true")
public class NumericValue extends Value {  
    private static final long serialVersionUID = 0x795fc131bdd311dbl;

    protected Number value;
    protected String unit = null;

    protected NumericValue () {}
    public NumericValue (Value parent) {
        super (parent);
    }
    public NumericValue (Value parent, String id) {
        super (parent, id);
    }
    public NumericValue (Value parent, String id, Number value) {
        super (parent, id);
        this.value = value;
    }

    public NumericValue (Value parent, String id, Number value, String unit) {
        super (parent, id);
        this.value = value;
        this.unit = unit;
    }

    public NumericValue (DataSource source, String id) {
        this (source, id, null);
    }

    public NumericValue (DataSource source, String id, Number value) {
        super (source, id);
        this.value = value;
    }

    public NumericValue (DataSource source, String id, Number value, String unit) {
        super (source, id);
        this.value = value;
        this.unit = unit;
    }

    public void setValue (Number value) {
        this.value = value;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public Number getValue () { return value; }

    public String getUnit () { return unit; }
}
