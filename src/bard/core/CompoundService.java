package bard.core;

import java.util.Collection;
import java.util.Map;

public interface CompoundService extends EntityService<Compound> {
    /*
     * structure searching
     */
    ServiceIterator<Compound> structureSearch (StructureSearchParams params);

    /*
     * return compound synonyms
     */
    Collection<Value> getSynonyms (Compound compound);
    Collection<Value> getSubstances (Compound compound);
    Collection<Assay> getTestedAssays (Compound compound, boolean activeOnly);
    Collection<Value> getPubChemSIDs (Compound compound);

    Map<String, Object> getSummary(Compound compound);
}
