package bard.core;

public interface BiologyValues extends EntityValues {
    enum BiologyType {
        Unknown,
        Protein,
        Gene,
        Pathway,
        Disease,
        CellLine,
        Tissue,
        Organism,
        Sequence,
        Process,
        Component,
        Function,
        NCBI,
        GO;
        
        static public BiologyType getType(String type) {
            type = type == null ? "" : type.toLowerCase();
            for (BiologyType bt: BiologyType.values())
                if (bt.name().toLowerCase().equals(type)) return bt;
            return BiologyType.Unknown;
        }
    }

    static final String ExternalId = "extId";
    static final String ExternalRef = "extRef";
    static final String DictionaryLabel = "dictLabel";
    static final String DictionaryId = "dictId";

    static final String AccessionValue = "Accession";
    static final String GeneIDValue = "GeneID";
    static final String TaxonomyIDValue = "TaxonomyID";
}
