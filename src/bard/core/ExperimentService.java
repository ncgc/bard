package bard.core;

public interface ExperimentService extends EntityService<Experiment> {
    /*
     * return compounds for an experiment
     */
    ServiceIterator<Compound> compounds (Experiment expr);

    /*
     * return experimental values for an experiment
     */
    ServiceIterator<Value> activities (Experiment expr);

    /*
     * Return experimental values for an experiment filtered by an etag.
     * Note that an etag must reference a list of substances or compounds
     */
    ServiceIterator<Value> activities (Experiment expr, Value etag);

    /*
     * Return experimental values for an experiment filtered by an etag.
     * Note that an etag must reference a list of substances or compounds
     */
    ServiceIterator<Value> activities (Experiment expr, Compound compound);

    /*
     * Return experimental values for an experiment filtered by an etag.
     * Note that an etag must reference a list of substances or compounds
     */
    ServiceIterator<Value> activities (String exprId, Value etag);

    /*
     * Return experimental values for a list of experiment IDs and sids
     */
    ServiceIterator<Value> activities (Object[] expts, Object[] sids);
}
