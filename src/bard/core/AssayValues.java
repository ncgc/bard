package bard.core;

public interface AssayValues extends EntityValues {
    static final String AssayCategoryValue = "AssayCategory";
    enum AssayCategory {
        Unknown,
        MLSCN,
        MLPCN,
        MLSCN_AP,
        MLPCN_AP;

        public static AssayCategory valueOf (int i) {
            for (AssayCategory c : values ()) {
                if (i == c.ordinal()) 
                    return c;
            }
            return Unknown;
        }
    }

    static final String AssayRoleValue = "AssayRole";
    enum AssayRole {
        Primary,
        Counterscreen,
        SecondaryConfirmation,
        SecondaryAlternative,
        SecondaryOrthogonal,
        SecondarySelectivity;

        public static AssayRole valueOf (int i) {
            for (AssayRole c : values ()) {
                if (i == c.ordinal()) 
                    return c;
            }
            throw new IllegalArgumentException ("Bogus AssayRole "+i);
        }
    }

    static final String AssayTypeValue = "AssayType";
    enum AssayType {
        Other, //0
        Screening, //1
        Confirmatory, //2
        Summary; //3

        public static AssayType valueOf (int i) {
            for (AssayType t : values ()) {
                if (t.ordinal() == i) {
                    return t;
                }
            }
            throw new IllegalArgumentException ("Bogus AssayType "+i);
        }
    }

    enum AssayMinimumAnnotation {
        footprint ("assay footprint"),
        detectMethod ("detection method type"),
        detectInstrument ("detection instrument name"),
        assayFormat ("assay format"),
        assayType ("assay type");
        
        String index;
      
        AssayMinimumAnnotation(String index) {
            this.index = index;
        }
        
        public String index() {return this.index;}
    }
    
    static final String AssayAltName = "AssayAltName";
    static final String AssayDescription = "description";
    static final String AssaySourceValue = "AssaySource";
    static final String AssayGrantValue = "AssayGrant";
    static final String AssayCapIDValue = "AssayCAPID";
    static final String AssayProtocol = "AssayProtocol";
    static final String AssayComments = "AssayComments";
    static final String AssayBARDID = "AssayBARDID";
}
