package bard.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jdo.annotations.PersistenceCapable;

@PersistenceCapable(detachable="true")
public class Project extends Entity implements ProjectValues {
    private static final long serialVersionUID = 0x6e0a525382fcc15dl;

    protected List<Assay> assays = new ArrayList<Assay>();
    protected List<Experiment> experiments = new ArrayList<Experiment>();
    protected List<Biology> targets = new ArrayList<Biology>();
    protected List<Compound> probes = new ArrayList<Compound>();
    
    protected Map<String, Object> projectSummary;

    public Project () {} // required by clazz.newInstance()
    public Project (ProjectService projService) { this.entityService = projService; }
    public Project (ProjectService projService, String name) {
        super (name);
        this.entityService = projService;
    }

    @Deprecated
    public void add (Assay assay) {
        assays.add(assay);
    }
    @Deprecated
    public boolean remove (Assay assay) {
        return assays.remove(assay);
    }
    @Deprecated
    public Collection<Assay> getAssays () { 
        return Collections.unmodifiableCollection(assays);
    }
    @Deprecated
    public Iterator<Assay> assays () { return getAssays().iterator(); }
    @Deprecated
    public int getAssayCount () { return assays.size(); }
    
    public void add (Experiment expr) {
        experiments.add(expr);
    }
    public boolean remove (Experiment expr) {
        return experiments.remove(expr);
    }
    public Collection<Experiment> getExperiments () {
        return Collections.unmodifiableCollection(experiments);
    }
    public Iterator<Experiment> experiments () { 
        return getExperiments().iterator();
    }
    public int getExperimentCount () { return experiments.size(); }

    public void add (Biology target) {
        targets.add(target);
    }
    public boolean remove (Biology target) {
        return targets.remove(target);
    }
    public Collection<Biology> getTargets () {
        return Collections.unmodifiableCollection(targets);
    }
    public Iterator<Biology> targets () {
        return getTargets().iterator();
    }

    public int getTargetCount () { return targets.size(); }

    public void add (Compound probe) {
        probes.add(probe);
    }
    public boolean remove (Compound probe) {
        return probes.remove(probe);
    }
    public Collection<Compound> getProbes() {
        return Collections.unmodifiableCollection(probes);
    }
    public Iterator<Compound> probes () {
        return getProbes().iterator();
    }

    public int getProbeCount () { return probes.size(); }

    public Map<String, Object> getProjectSummary() { 
	if (projectSummary == null && entityService != null) {
	    projectSummary = ((ProjectService)entityService).getProjectSummary(this);
	}
	return this.projectSummary; 
    }
    
    public String toString () {
        return getClass().getName()+"{assays="+assays.size()
            +",experiments="+experiments.size()+","+super.toString()+"}";
    }
}
