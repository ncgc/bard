package bard.core.rest;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.DataSource;
import bard.core.Entity;
import bard.core.Experiment;
import bard.core.ExperimentService;
import bard.core.HillCurveValue;
import bard.core.IntValue;
import bard.core.LongValue;
import bard.core.NumericValue;
import bard.core.Project;
import bard.core.ServiceIterator;
import bard.core.StringDisplayValue;
import bard.core.StringValue;
import bard.core.Value;
import bard.core.adapter.CompoundAdapter;
import bard.util.BardHttpClient;
import cern.colt.Arrays;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

public class RESTExperimentService
    extends RESTAbstractEntityService<Experiment> 
    implements ExperimentService {

    static final private Logger logger = 
        Logger.getLogger(RESTExperimentService.class.getName());

    static final Value DONE = new Value (DataSource.getCurrent());

    class ActivityIterator implements ServiceIterator<Value>, Runnable {
	volatile Object[] expts;
	volatile Object[] sids;
        volatile Value next;
        volatile Value etag;
        volatile Compound compound;
        
        BlockingQueue<Value> buffer = 
            new ArrayBlockingQueue<Value>(5000);

        Future future;
        AtomicLong consumed = new AtomicLong ();
        AtomicLong count = new AtomicLong ();
        AtomicBoolean done = new AtomicBoolean (false);

        List<Value> facets = new ArrayList<Value>();

        ActivityIterator (Experiment expr) {
            this (expr, (Value)null);
        }

        ActivityIterator (Experiment expr, Value etag) {
            this.expts = new Object[1];
            this.expts[0] = expr.getId();
            this.etag = etag;
            future = getThreadPool().submit(this);
        }

        ActivityIterator (Experiment expr, Compound compound) {
            this.expts = new Object[1];
            this.expts[0] = expr.getId();
            this.compound = compound;
            future = getThreadPool().submit(this);
        }

        ActivityIterator (String exprId, Value etag) {
            this.expts = new Object[1];
            this.expts[0] = exprId;
            this.etag = etag;
            future = getThreadPool().submit(this);
        }

        ActivityIterator (Object[] expts, Object[] sids) {
            this.expts = expts;
            this.sids = sids;
            future = getThreadPool().submit(this);
        }
        
        public void run () {
            // unbounded fetching
            try {
                int a = 5;
                int top = a*a;
                int ratio = a;
                long skip = 0;

                while (true) {
                    List<Value> values;
                    if (compound != null && expts.length == 1) { 
                        values = getValues (expts[0].toString(), compound);
                    } else if (sids != null) {
                	values = getValues (expts, sids);
                    } else {
                        values = getValues (expts[0].toString(), etag, top, skip);
                    }

                    for (Value v : values) {
                        buffer.put(v);
                        count.incrementAndGet();
                        
                        if (Thread.currentThread().isInterrupted()) {
                            buffer.put(DONE);
                            return;
                        }
                    }

                    if (values.size() < top) {
                        break; // we're done
                    }

                    /*
                    long s = streamValues (buffer, expr, etag, top, skip);
                    if (s < top) {
                        break; 
                    }
                    */

                    skip += values.size();

                    // cap this
                    if (skip > 1000) {
                        top = 1000;
                    }
                    else {
                        ratio *= a;
                        top = ratio;
                    }
                }
                logger.info("** "+Thread.currentThread()+" finishes!");
            }
            catch (InterruptedException ex) {
                //logger.info("** "+Thread.currentThread()+" interrupted!");

            }
            buffer.offer(DONE);
        }

        public boolean hasNext () {
            if (done.get()) return false;
            if (next != null) return true;

            try {
                Value v = buffer.take();
                next = v != DONE ? v : null;
            }
            catch (InterruptedException ex) {
                /*
                logger.info("** "+Thread.currentThread().getName()
                            +" interrupted!");
                */
                next = null;
            }
            done.set(next == null);

            return !done.get();
        }

        public Value next () {
            consumed.incrementAndGet();
            Value v = next;
            next = null;
            return v;
        }

        public Value getETag () { return etag; }
        public Collection<Value> getFacets () {
            return facets;
        }

        public void remove () {
            throw new UnsupportedOperationException ("remove not supported!");
        }

        public void done () {
            future.cancel(true);
        }

        public List<Value> next (int size) {
            List<Value> page = new ArrayList<Value>();
            /*
            if (size > 0 && next != null) {
                page.add(next);
                next = null;

                size = buffer.drainTo(page, size-1);
                for (; hasNext () && size-- >= 0; ) {
                    page.add(next ());
                }
            }
            */
            for (int i = 0; i < size && hasNext (); ++i) {
                page.add(next ());
            }

            return page;
        }
        
        public long getConsumedCount () { return consumed.get(); }
        public long getCount () { return count.get(); }
    }

    protected RESTExperimentService 
        (RESTEntityServiceManager srvman, String baseURL) {
        super (srvman, baseURL);
    }

    public Class<Experiment> getEntityClass () { return Experiment.class; }
    public String getResourceContext () { return "/experiments"; }

    protected List<Value> getValues 
        (String exptId, Object etag, long top, long skip) {
	BardHttpClient httpclient = new BardHttpClient("getValues[Expt]");
//	DefaultHttpClient httpclient = new DefaultHttpClient();

        List<Value> values = new ArrayList<Value>();
        try {
            StringBuilder resource = new StringBuilder 
                (getResource (exptId));
            if (etag != null) {
                resource.append("/etag/"+getETagId (etag));
            }
            resource.append("/exptdata");

            if (resource.indexOf("?") < 0) {
                resource.append("?");
            }
            else {
                resource.append("&");
            }
            resource.append("skip="+skip+"&top="+top + "&expand=true");
            //logger.info("** "+Thread.currentThread() +" fetching "+resource);

            HttpGet get = new HttpGet (resource.toString());
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            DataSource source = new DataSource 
                (getResourceContext(), exptId);
            source.setURL(resource.toString());

            if (entity != null && response.getStatusLine()
                .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper ();
		    JsonNode root = mapper.readTree(is);
                    JsonNode node = root.get("collection");
                    ArrayNode array = null;

                    if (node != null && node.isArray()) {
                        array = (ArrayNode)node;
                    }
                    else if (root.isArray()) {
                        array = (ArrayNode)root;
                    }

                    if (array != null) {
                        for (int i = 0; i < array.size(); ++i) {
                            JsonNode n = array.get(i);
                            values.add(getValue (source, n));
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    is.close();
                }
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return values;        
    }


    protected List<Value> getValues (String exptId, Compound compound) {
        List<Value> values = new ArrayList<Value>();
//        if (compound != null) return values; //!!!! debug statement
        
        CompoundAdapter adapter = new CompoundAdapter (compound);
        for (Long sid : adapter.getPubChemSIDs()) {
            BardHttpClient httpclient = new BardHttpClient("getExptPubchemSIDs");
//            DefaultHttpClient httpclient = new DefaultHttpClient();
            try {
                String url = getBaseURL()+"/exptdata/"+exptId+"."+sid;
                //logger.info(">>> "+Thread.currentThread() +" fetching "+url+"...");
                HttpGet get = new HttpGet (url);
                HttpResponse response = httpclient.execute(get);

                HttpEntity entity = response.getEntity();
                DataSource source = new DataSource 
                    (getResourceContext(), exptId);
                source.setURL(url);

                if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                    InputStream is = entity.getContent();
                    try {
                        ObjectMapper mapper = new ObjectMapper ();
                        JsonNode root = mapper.readTree(is);
                        values.add(getValue (source, root));
                        return values; //!!! hack to return eraly to cut down on number of httpclient requests
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    finally {
                        is.close();
                    }
                }
                //logger.info("<<< "+Thread.currentThread() +" fetching "+url+"..."+values.size());
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
            finally {
        	httpclient.close();
//                httpclient.getConnectionManager().shutdown();
            }
        }

        return values;        
    }

    protected List<Value> getValues (Object[] expts, Object[] substances) {
        List<Value> values = new ArrayList<Value>();
	if (substances == null || expts == null) return values;
        
	BardHttpClient httpclient = new BardHttpClient("getExptPubchemSIDs3");
//            DefaultHttpClient httpclient = new DefaultHttpClient();
	try {
	    String url = getBaseURL()+"/exptdata";
	    //logger.info(">>> "+Thread.currentThread() +" fetching "+url+"...");
	    HttpPost post = new HttpPost (url);
	    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	    String eids = Arrays.toString(expts);
	    eids = eids.substring(1,eids.length()-1).replace(" ", "");
	    String sids = Arrays.toString(substances);
	    sids = sids.substring(1,sids.length()-1).replace(" ", "");
	    nameValuePairs.add(new BasicNameValuePair("eids", eids));
	    nameValuePairs.add(new BasicNameValuePair("sids", sids));
	    post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	    HttpResponse response = httpclient.execute(post);

	    HttpEntity entity = response.getEntity();
	    DataSource source = new DataSource 
		    (getResourceContext(), "");
	    source.setURL(url);

	    if (entity != null && response.getStatusLine()
		    .getStatusCode() == 200) {
		InputStream is = entity.getContent();
		try {
		    ObjectMapper mapper = new ObjectMapper ();
		    JsonNode root = mapper.readTree(is);
		    for (int i=0; i<((ArrayNode)root).size(); i++) {
			JsonNode value = root.get(i);
			values.add(getValue (source, value));
		    }
		    return values; //!!! hack to return eraly to cut down on number of httpclient requests
		}
		catch (Exception ex) {
		    ex.printStackTrace();
		}
		finally {
		    is.close();
		}
	    }
	    //logger.info("<<< "+Thread.currentThread() +" fetching "+url+"..."+values.size());
	}
	catch (IOException ex) {
	    ex.printStackTrace();
	}
	finally {
	    httpclient.close();
	    //                httpclient.getConnectionManager().shutdown();
	}

        return values;        
    }

    protected long streamValues (BlockingQueue<Value> queue, 
                                 Experiment expr, Object etag, 
                                 long top, long skip) {
	BardHttpClient httpclient = new BardHttpClient("streamValues");
//	DefaultHttpClient httpclient = new DefaultHttpClient();
        long streamed = 0;
        try {
            StringBuilder resource = new StringBuilder 
                (getResource (expr.getId()));
            if (etag != null) {
                resource.append("/etag/"+getETagId (etag));
            }
            resource.append("/exptdata");

            if (resource.indexOf("?") < 0) {
                resource.append("?");
            }
            else {
                resource.append("&");
            }
            resource.append("top="+top+"&skip="+skip+"&expand=true");

            HttpGet get = new HttpGet (resource.toString());
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            DataSource source = new DataSource 
                (getResourceContext(), expr.getId().toString());
            source.setURL(resource.toString());

            if (entity != null && response.getStatusLine()
                .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper ();
		    JsonNode root = mapper.readTree(is);
                    JsonNode node = root.get("collection");
                    ArrayNode array = null;

                    if (node != null && node.isArray()) {
                        array = (ArrayNode)node;
                    }
                    else if (root.isArray()) {
                        array = (ArrayNode)root;
                    }

                    if (array != null) {
                        for (int i = 0; i < array.size(); ++i) {
                            JsonNode n = array.get(i);
                            queue.put(getValue (source, n));
                            ++streamed;
                        }
                    }
                }
                catch (InterruptedException ex) {
                    queue.put(DONE);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    is.close();
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            queue.offer(DONE);
        }
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return streamed; 
    }

    protected Value parseSP(Value parent, JsonNode node) {
        String dname = node.get("displayName").asText();
        JsonNode value  = node.get("value");
        
        if (node.has("childElements"))
            scanElements((ArrayNode)node.get("childElements"), parent, false);
        Double v = value != null && !value.isNull() ? value.asDouble() : null;
        if (v == null) return null;
        return new StringDisplayValue(parent, "Activity", value.asText(), dname);
    }

    protected Value parseCR_SER (Value parent, JsonNode node) {
        String readoutName = "A Readout";
        JsonNode n = node.get("name");
        if (n != null && !n.isNull()) readoutName = n.getTextValue();

        HillCurveValue hcv = new HillCurveValue (parent, "Activity");

        // see if a potency value is reported. If so, add directly to the parent
        // TODO not sure if this is the correct behavior
        n = node.get("displayName");
        Double value = node.get("value") == null || "NA".equals(node.get("value")) ? null : node.get("value").asDouble();

        n = node.get("concResponseSeries");
        if (n != null && !n.isNull()) {
            JsonNode p;
            JsonNode pn = n.get("concRespParams");
            if (pn != null && !pn.isNull()) {
                p = pn.get("s0");
                if (!p.isNull()) {
                    hcv.setS0(p.asDouble());
                }
                p = pn.get("sInf");
                if (!p.isNull()) {
                    hcv.setSinf(p.asDouble());
                }
                p = pn.get("hillCoef");
                if (!p.isNull()) {
                    hcv.setCoef(p.asDouble());
                }
                p = pn.get("logEc50");
                if (!p.isNull()) {
                    double pot = p.asDouble();
                    if (pot < -4) pot = pot + 6;
                    hcv.setSlope(Math.pow(10, pot));
                }
            }
            p = n.get("testConcUnit");
            if (p != null && !p.isNull()) hcv.setTestUnit(p.asText());
            p = n.get("responseUnit");
            if (p != null && !p.isNull()) hcv.setResponseUnit(p.asText());

            ArrayNode crc = (ArrayNode) n.get("concRespPoints");
            for (int i = 0; i < crc.size(); i++) {
                hcv.add(crc.get(i).get("testConc").asDouble(),
                        crc.get(i).get("value").asDouble());
            }
        } else { // A priorityElement that is not a hill curve value
            String respUnit = textValue(node, "responseUnit");
            String displayName = textValue(node, "displayName");
            StringValue unclass = new StringDisplayValue (parent, "Activity", displayName, value + (respUnit == null ? "" : (" " + respUnit)));
            return null;
        }
        return hcv;
    }

    static private int[] potencies = {956, 952, 959, 965, 997, 1002, 1000, 961, 964, 1015, 963, 993, 917, 966, 960, 1378};
    static private String textValue(JsonNode node, String field) {
	if (node == null || node.isNull())
	    return null;
	node = node.get(field);
	if (node == null || node.isNull())
	    return null;
	String value = node.asText();
	return value;
    }
    
    protected Value parseCR_NO_SER (Value parent, JsonNode node, ArrayNode rootElements) {
	Double slope = null;
	String testUnit = "um";
	String displayName = "";
	int elemId = 0;
	
        JsonNode n = node.get("dictElemId");
        if (n != null && !n.isNull()) {
            elemId = n.getIntValue();
            for (int entry: potencies)
        	if (entry == elemId && node.get("value") != null) {
        	    slope = node.get("value").asDouble();
        	    displayName = node.get("displayName").getTextValue();
        	}
            n = node.get("responseUnit");
            if (n != null && !n.isNull()) {
        	testUnit = n.getTextValue();
            }
        }

        HillCurveValue hcv = new HillCurveValue (parent, "Activity");
        hcv.setTestUnit(testUnit);
        if (elemId == 960)
            slope = Math.pow(10, slope + hcv.getOffset());
        else if (elemId == 1378)
            slope = Math.pow(10, hcv.getOffset() - slope);
        if (slope != null) {
            hcv.setSlope(slope/Math.pow(10, HillCurveValue.lookupOffset(testUnit)));
            hcv.setS0(0.);
            hcv.setSinf(0.);
            hcv.setCoef(1.);
        }
        
        ArrayNode children = (ArrayNode) node.get("childElements");
        if (children != null && !children.isNull()) {
            Iterator<JsonNode> it = children.getElements();
            while (it.hasNext()) {
                JsonNode p  = it.next();
                n = p.get("dictElemId");
                if (n != null && !n.isNull() && p.get("value") != null && !p.get("value").isNull()) {
                    elemId = n.getIntValue();
                    if (elemId == 919)
                        hcv.setCoef(p.get("value").asDouble());
                    if (elemId == 920)
                        hcv.setS0(p.get("value").asDouble());
                    if (elemId == 921)
                        hcv.setSinf(p.get("value").asDouble());
                    if (elemId == 962) {
                        double pot = p.get("value").asDouble();
                        if (pot < -4) pot = pot + 6;
                        hcv.setSlope(Math.pow(10, pot));
                    }
                }
            }
        }
//        for (int i=0; i<rootElements.size(); i++) {
//            n = rootElements.get(i);
//            if (n != null && !n.isNull()) {
//        	try {
//        	    String name = textValue(n, "displayName");
//        	    String value = textValue(n, "value");
//        	    String elem = textValue(n, "dictElemId");
//        	    //String rUnit = textValue(n, "responseUnit");
//        	    //String tUnit = textValue(n, "testConcUnit");
//        	    String tConc = textValue(n, "testConc");
//        	    
//        	    if (value != null && value.length() > 0 && !"null".equals(value)) {
//        		if (tConc != null && tConc.length() > 0 && !"null".equals(tConc)) {
//        		    hcv.add(Double.parseDouble(tConc), Double.parseDouble(value));
//        		} else if ("Hill coefficient".equals(name) || "919".equals(elem)) {
//        		    hcv.setCoef(Double.parseDouble(value));
//        		} else if ("Hill sinf".equals(name) || "921".equals(elem)) {
//        		    hcv.setSinf(Double.parseDouble(value));
//        		} else if ("Hill s0".equals(name) || "920".equals(elem)) {
//        		    hcv.setS0(Double.parseDouble(value));
//        		}
//        	    }
//        	} catch (Exception e) {
//        	    e.printStackTrace();
//        	}
//            }
//        }
        
        return hcv;
    }

     /**

     Examples

     CR_SER http://bard.nih.gov/api/v12/exptdata/3982.17406780
     SP http://bard.nih.gov/api/v12/exptdata/476.16953345
     MULTCONC
     CR_NO_SERIES
     UNCLASS
     */
    protected Value getValue(DataSource source, JsonNode node) {

        // some resources have not been updated to server up new style
        // exptdata JSON directly. So we might have to extract it
        if (node.get("exptDataId") != null) { // non-updated API response
            ObjectMapper mapper = new ObjectMapper();
            try {
                node = mapper.readTree(node.get("resultJson").asText());
            } catch (IOException e) {
                logger.severe("Old style exptdata JSON, but missing a resultJson field");
                return null;
            }
        }

        String responseClass = node.get("responseClass").asText();
        logger.info("Will parse exptdata of class: " + responseClass);

        long eid = node.get("bardExptId").asLong();
        long sid = node.get("sid").asLong();
        Value v = new Value(source, eid+"."+sid);

        new LongValue(v, "eid", eid);
        new LongValue(v, "cid", node.get("cid").asLong());
        new LongValue(v, "sid", sid);
        new LongValue(v, "capExptId", node.get("capExptId").asLong());
        new LongValue(v, "capAssayId", node.get("capAssayId").asLong());
        new StringValue(v, "responseClass", responseClass);
        
        ArrayNode anode = (ArrayNode) node.get("rootElements");
        scanElements(anode, v, false);
        
        String potency = null;
        if (node.get("potency") != null && !node.get("potency").isNull()) {
            potency = node.get("potency").asText();
        } else if (responseClass.equals("UNCLASS")) { 
            scanElements(anode, v, true);
        }

        anode = (ArrayNode) node.get("priorityElements");
        for (int i = 0; i < anode.size(); i++) {
            JsonNode n = anode.get(i);
            if (responseClass.equals("CR_SER")) parseCR_SER(v, n);
            else if (responseClass.equals("CR_NO_SER") || responseClass.equals("MULTCONC")) 
        	parseCR_NO_SER(v, n, (ArrayNode) node.get("rootElements"));
            else if (responseClass.equals("SP")) parseSP(v, n);
            else System.err.println("Unknown class");
        }

        if (potency != null && responseClass.equals("UNCLASS")) { // see if we already have potency
            boolean addPotency = true;
            for (Value value: v.getChildren("Activity")) {
                if (value instanceof StringValue) {
                    if (((StringValue)value).getValue().equals(potency))
                        addPotency = false;
                } else if (value instanceof HillCurveValue)
                    addPotency = false;
            }
            if (addPotency)
                new StringDisplayValue(v, "Activity", potency + " uM", "potency");
        }

        return v;
    }

    void scanElements (ArrayNode anode, Value v, boolean activity) {
        for (int i = 0; i < anode.size(); i++) {
            ObjectNode rnode = (ObjectNode) anode.get(i);

            if (rnode != null && !rnode.isNull()) {
                String displayName = rnode.get("displayName").asText();
                JsonNode value = rnode.get("value");
                String elem = textValue(rnode, "dictElemId");
                String rUnit = textValue(rnode, "responseUnit");
                
                if (value != null) {
                    if (activity)
                        new StringDisplayValue(v, "Activity", displayName, value.asText() + (rUnit == null ? "" : (" " + rUnit)));
                    else
                        new StringValue(v, displayName, value.asText());
                }

                ArrayNode children = (ArrayNode)rnode.get("childElements");
                if (children != null)
                    scanElements (children, v, activity);
            }
        }
    }

    public enum PubChemOutcome {
	Inactive (1),
	Active (2),
	Inconclusive (3),
	Unspecified (4),
	Probe (5);
	
	int score;
	PubChemOutcome (int score) {
	    this.score = score;
	}
	public int score() { return score; }
    }
    
    protected Experiment getEntity(Experiment e, JsonNode node) {
	if (node == null || node.isNull())
	    return null;
	if (e == null) {
            e = new Experiment();
        }

        DataSource ds = getDataSource ();

        e.setId(node.get("bardExptId").asInt());
        e.setName(node.get("name").asText());
        e.add(new StringValue (ds, Entity.EntityDescription, node.get("description").asText()));

        JsonNode n;

        n = node.get("category");
        if (n != null && !n.isNull()) {
            e.setCategory(Experiment.ExperimentCategory.valueOf
                          (n.asInt()));
        }

        n = node.get("type");
        if (n != null && !n.isNull()) {
            e.setType(Experiment.ExperimentType.valueOf
                      (n.asInt()));
        }

        n = node.get("classification");
        if (n != null && !n.isNull()) {
            e.setRole(Experiment.ExperimentRole.valueOf
                      (n.asInt()));
        }

        n = node.get("pubchemAid");
        if (n != null && !n.isNull()) {
            e.setPubchemAid(n.asLong());
        }

        n = node.get("capExptId");
        if (n != null && !n.isNull()) {
            e.add(new LongValue (ds, Experiment.ExperimentCAPIDValue, n.asLong()));
        }

        // we'll always have an expanded form, but we only reutrn a
        // a partially filled Assay object
        n = node.get("bardAssayId");
        if (n != null && !n.isNull()) {
            e.add(new LongValue(ds, Assay.AssayBARDID, n.asLong()));
        }
        n = node.get("assayId");
        if (n != null && n.isArray()) {
            RESTAssayService service = (RESTAssayService)getServiceManager()
                .getService(Assay.class);
            ArrayNode assayNode = (ArrayNode) n;
            for (int i = 0; i < assayNode.size(); i++) {
                n = assayNode.get(i);
                Assay assay = new Assay();
                service.getEntity(assay, n);
                e.setAssay(assay);
            }
        }

        n = node.get("compounds");
        if (n != null && !n.isNull()) {
            e.add(new IntValue 
                  (ds, Experiment.ExperimentCompoundCountValue, n.asInt()));
        }

        n = node.get("substances");
        if (n != null && !n.isNull()) {
            e.add(new IntValue
                  (ds, Experiment.ExperimentSubstanceCountValue, n.asInt()));
        }

        n = node.get("activeCompounds");
        if (n != null && !n.isNull()) {
            e.add(new IntValue
                  (ds, Experiment.ExperimentActiveCompoundsValue, n.asInt()));
        }

        n = node.get("confidenceLevel");
        if (n != null && !n.isNull()) {
            e.add(new IntValue
                  (ds, Experiment.ExperimentConfidenceValue, n.asInt()));
        }

        return e;
    }

    protected Experiment getEntitySearch (Experiment e, JsonNode node) {
        if (e == null) {
            e = new Experiment ();
        }

        return e;
    }

    @Override
    public <T extends Entity> ServiceIterator<T> iterator 
                      (Experiment expr, Class<T> clazz) {
        RESTAbstractEntityService<T> service = 
            (RESTAbstractEntityService)getServiceManager().getService(clazz);

        if (clazz.equals(Project.class)) {
            return service.getIterator 
                (getResource (expr.getId()+"/projects"), null);
        }
        else if (clazz.equals(Compound.class)) {
            return service.getIterator
                (getResource (expr.getId()+"/compounds"), null);
        }
        else {
            throw new IllegalArgumentException 
                ("No related entities available for "+clazz);
        }
    }

    public ServiceIterator<Compound> compounds (Experiment expr) {
        String resource = getResource (expr.getId())+"/compounds";
        RESTCompoundService cs = (RESTCompoundService)
            getServiceManager().getService(Compound.class);
        return cs.getIterator(resource, null);
    }

    public ServiceIterator<Value> activities (Experiment expr) {
        return new ActivityIterator (expr);
    }

    public ServiceIterator<Value> activities (Experiment expr, Value etag) {
        return new ActivityIterator (expr, etag);
    }
    
    public ServiceIterator<Value> activities (String exprId, Value etag) {
        return new ActivityIterator (exprId, etag);
    }
    
    public ServiceIterator<Value> activities (Experiment expr, Compound compound) {
        return new ActivityIterator (expr, compound);
    }
    
    public ServiceIterator<Value> activities (Object[] expts, Object[] sids) {
	return new ActivityIterator (expts, sids);
    }
}
