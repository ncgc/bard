package bard.core.rest;

import java.util.logging.Logger;

import org.codehaus.jackson.JsonNode;

import bard.core.Biology;
import bard.core.BiologyService;
import bard.core.BiologyValues;
import bard.core.DataSource;
import bard.core.IntValue;
import bard.core.StringValue;
import bard.core.Value;


@SuppressWarnings("unchecked")
public class RESTBiologyService extends RESTAbstractEntityService<Biology> 
    implements BiologyService, BiologyValues {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    static final private Logger logger = 
        Logger.getLogger(RESTBiologyService.class.getName());

    protected RESTBiologyService 
        (RESTEntityServiceManager srvman, String baseURL) {
        super (srvman, baseURL);
    }

    public Class<Biology> getEntityClass () { return Biology.class; }
    public String getResourceContext () { return "/biology"; }

    protected Biology getEntity (Biology t, JsonNode entry) {
        if (t == null) {
            t = new Biology ();
        }

        JsonNode v = entry.get("resourcePath");
        DataSource ds = getDataSource ();
        if (v != null && !v.isNull()) {
            ds = getDataSource(v.getTextValue());
        }

        v = entry.get("serial");
	t.setId(v.asInt());
	
	v = entry.get("biology");
	t.setType(BiologyType.getType(v.getTextValue()));
	
	v = entry.get("description");
	if (v != null && !v.isNull()) {
	    t.add(new StringValue 
		    (ds, EntityDescription, entry.asText()));
	}
	
	v = entry.get("name");
	if (v != null && !v.isNull()) {
	    t.setName(v.asText()); 
	} else {
	    v = entry.get("description");
	    if (v != null && !v.isNull()) {
	            t.setName(v.asText());
	    }
	}

	v = entry.get("extId");
        if (v != null && !v.isNull()) {
            Value val = new StringValue
                    (ds, Biology.ExternalId, v.asText());
            t.add(val);
        }
        v = entry.get("extRef");
        if (v != null && !v.isNull()) {
            Value val = new StringValue
                    (ds, Biology.ExternalRef, v.asText());
            t.add(val);
        }
        v = entry.get("dictLabel");
        if (v != null && !v.isNull()) {
            Value val = new StringValue
                    (ds, Biology.DictionaryLabel, v.asText());
            t.add(val);
       }
        v = entry.get("dictId");
        if (v != null && !v.isNull()) {
            Value val = new StringValue
                    (ds, Biology.DictionaryId, v.asText());
            t.add(val);
        }
//	v = entry.get("geneId");
//	if (v != null) {
//	    t.add(new IntValue 
//		    (ds, Biology.GeneIDValue, v.asInt()));
//	}
//	v = entry.get("taxId");
//	if (v != null && !v.isNull()) {
//	    t.add(new IntValue
//		    (ds, Biology.TaxonomyIDValue, v.asInt()));
//	}
//	v = entry.get("acc");
//	if (v != null && !v.isNull()) {
//	    Value val = new StringValue
//		    (ds, Biology.AccessionValue, v.asText());
//	    v = entry.get("url");
//	    if (v != null && !v.isNull()) {
//		val.setURL(v.asText());
//	    }
//
//	}

	return t;
    }

}
