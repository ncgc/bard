package bard.core.rest;

import java.util.Collection;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonNode;

import bard.core.Biology;
import bard.core.BiologyService;
import bard.core.BiologyValues;
import bard.core.DataSource;
import bard.core.Document;
import bard.core.IntValue;
import bard.core.StringValue;

public class RESTDocumentService extends RESTAbstractEntityService<Document> {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    static final private Logger logger = 
        Logger.getLogger(RESTDocumentService.class.getName());

    protected RESTDocumentService 
        (RESTEntityServiceManager srvman, String baseURL) {
        super (srvman, baseURL);
    }

    public Class<Document> getEntityClass () { return Document.class; }
    public String getResourceContext () { return "/documents"; }

    protected Document getEntity (Document t, JsonNode entry) {
        if (t == null) {
            t = new Document ();
        }

        t.setId(entry.get("pubmedId").asLong());
	t.setTitle(entry.get("title").asText());
	t.setDoi(entry.get("doi").asText());
	t.setAbs(entry.get("abs").asText());
	t.setPubmedId(entry.get("pubmedId").asLong());

	return t;
    }

}
