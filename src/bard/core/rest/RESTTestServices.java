package bard.core.rest;

import bard.core.Assay;
import bard.core.AssayService;
import bard.core.Biology;
import bard.core.Compound;
import bard.core.CompoundService;
import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.Experiment;
import bard.core.ExperimentService;
import bard.core.Project;
import bard.core.ProjectService;
import bard.core.Document;
import bard.core.SearchParams;
import bard.core.ServiceIterator;
import bard.core.StructureSearchParams;
import bard.core.Substance;
import bard.core.SubstanceService;
import bard.core.Value;
import bard.core.adapter.AssayAdapter;
import bard.core.adapter.CompoundAdapter;
import bard.core.adapter.EntityAdapter;
import bard.core.adapter.ProjectAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;


public class RESTTestServices {
    private static final Logger logger =
        Logger.getLogger(RESTTestServices.class.getName());

    static void testServices1(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        Random rand = new Random();

        CompoundService cs = esm.getService(Compound.class);
        long size = cs.size();
        System.out.println("** compound size: " + size);
        {
            ServiceIterator<Compound> iter = cs.iterator();
            for (int i = 0; i < 1005 && iter.hasNext(); ++i) {
                Compound c = iter.next();
                if (rand.nextDouble() < 0.05) {
                    System.out.println(c);
                }
            }
            iter.done();
            System.out.println("** " + iter.getConsumedCount()
                    + " compounds(s) consumed!");
        }

        AssayService as = esm.getService(Assay.class);
        size = as.size();
        System.out.println("** assay size: " + size);
        {
            ServiceIterator<Assay> iter = as.iterator();
            while (iter.hasNext()
                    && iter.getConsumedCount() < size) {
                Assay a = iter.next();
                if (iter.getConsumedCount() % 100 == 0) {
                    System.out.println(iter.getConsumedCount() + " " + a);
                }
            }
            iter.done();
            System.out.println("** " + iter.getConsumedCount()
                    + " assay(s) consumed!");
        }

        ExperimentService es = esm.getService(Experiment.class);
        size = es.size();
        System.out.println("** experiment size: " + size);
        {
            ServiceIterator<Experiment> iter = es.iterator();
            while (iter.hasNext()
                    && iter.getConsumedCount() < size) {
                Experiment e = iter.next();
                if (iter.getConsumedCount() % 100 == 0) {
                    System.out.println(iter.getConsumedCount() + " " + e);
                }
            }
            iter.done();
            System.out.println("** " + iter.getConsumedCount()
                    + " experiment(s) consumed!");
        }

        // current projects resource is not consistent with other resources
        ProjectService ps = esm.getService(Project.class);
        size = ps.size();
        System.out.println("** project size: " + size);
        {
            ServiceIterator<Project> iter = ps.iterator();
            while (iter.hasNext() && iter.getConsumedCount() < size) {
                Project proj = iter.next();
                System.out.println(proj.getId() + ": " + proj.getName());
            }
            iter.done();
            System.out.println("** " + iter.getConsumedCount()
                    + " project(s) consumed!");
        }

        esm.shutdown();
    }


    static void testServices2(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        ExperimentService es = esm.getService(Experiment.class);
        Experiment expr = es.get(346);
        System.out.println("** " + expr);

        ServiceIterator<Compound> iter = es.compounds(expr);
        Random rand = new Random();
        while (iter.hasNext()) {
            Compound c = iter.next();
            if (rand.nextDouble() < 0.005) {
                long sleep = rand.nextInt(2000);
                System.out.println("** sleeping for "
                        + sleep + "ms; " + iter.getConsumedCount());
                Thread.currentThread().sleep(sleep);
            }
            //System.out.println(" "+c);
        }
        iter.done();
        System.out.println("** " + iter.getConsumedCount()
                + " compounds consumed!");

        esm.shutdown();
    }

    static void testServices3(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        ExperimentService es = esm.getService(Experiment.class);
        Experiment expr = es.get(460);
        System.out.println("** " + expr);

        ServiceIterator<Value> iter = es.activities(expr);
        while (iter.hasNext()) {
            Value v = iter.next();
            System.out.println(v);
        }
        iter.done();
        System.out.println("** " + iter.getConsumedCount()
                + " activities consumed!");

        esm.shutdown();
    }

    static void dump(Compound cmpd) {
        CompoundAdapter ca = new CompoundAdapter(cmpd);
        System.out.println("** CID: " + ca.getPubChemCID());
        System.out.println("** SMILES: " + ca.getStructureSMILES());
        Collection<Value> sids = cmpd.getSubstances();
        System.out.print("** SIDS: " + sids.size() + "[");
        for (Value sid: sids) {
            System.out.print(sid.getValue());
            System.out.print(",");
        }
        System.out.println("]");
        System.out.println("** Name: " + ca.getName());
        System.out.println("** Formula: " + ca.formula());
        System.out.println("** MolWt: " + ca.mwt());
        System.out.println("** ExactMass: " + ca.exactMass());
        System.out.println("** HbondDonor: " + ca.hbondDonor());
        System.out.println("** HbondAcceptor: " + ca.hbondAcceptor());
        System.out.println("** Rotatable: " + ca.rotatable());
        System.out.println("** DefinedStereo: " + ca.definedStereo());
        System.out.println("** Stereocenters: " + ca.stereocenters());
        System.out.println("** TPSA: " + ca.TPSA());
        System.out.println("** logP: " + ca.logP());
        System.out.print("** Synonyms:");
        for (String syn : ca.getSynonyms()) {
            System.out.print(" \"" + syn + "\"");
        }
        System.out.println();
        System.out.print("** Annotations:");
        for (Value v : ca.getAnnotations()) {
            System.out.print(" (" + v.getId() + "," + v.getValue() + ")");
        }
        System.out.println();
    }

    static void dump(Collection<Value> values) {
        for (Value v : values) {
            System.out.println("$$ " + v.getId());
            for (Iterator<Value> it = v.children(); it.hasNext(); ) {
                Value iv = it.next();
                System.out.println
                        ("  <" + iv.getId() + "," + iv.getValue() + ">");
            }
        }
    }

    static void testServices4(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        CompoundService cs = esm.getService(Compound.class);
        Compound cmpd = cs.get(2722);
        dump(cmpd);
        esm.shutdown();
    }

    static void testServices5(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        CompoundService cs = esm.getService(Compound.class);

        StructureSearchParams params =
                new StructureSearchParams("n1cccc2ccccc12");
        //params.setSkip(1001l).setTop(10l);

        ServiceIterator<Compound> iter = cs.search(params);
        System.out.println("####### Substructure..."+iter.getCount());
        while (iter.hasNext() && iter.getConsumedCount() < 1000) {
            Compound c = iter.next();
            //dump (c);
            System.out.println("--- " + iter.getConsumedCount()
                    + "/" + iter.getCount() + " ---");
        }

        dump(iter.getFacets());

        /*
        for (Compound c : iter.next(1000)) {
            dump (c);
            System.out.println("--- "+iter.getConsumedCount()+" ---");
        }
        */
        iter.done();

        iter = cs.structureSearch
                (new StructureSearchParams
                        ("O=S(*C)(Cc1ccc2ncc(CCNC)c2c1)=O",
                                StructureSearchParams.Type.Superstructure));
        System.out.println("####### Superstructure..."+iter.getCount());
        while (iter.hasNext()) {
            Compound c = iter.next();
            //dump(c);
            System.out.println("--- " + iter.getConsumedCount() + " ---");
        }
        iter.done();

        iter = cs.structureSearch
                (new StructureSearchParams
                        ("CN(C)CCC1=CNC2=C1C=C(CS(=O)(=O)N3CCCC3)C=C2",
                                StructureSearchParams.Type.Exact));
        System.out.println("####### Exact..."+iter.getCount());
        while (iter.hasNext()) {
            Compound c = iter.next();
            //dump(c);
            System.out.println("--- " + iter.getConsumedCount() + " ---");
        }
        iter.done();

        iter = cs.structureSearch
                (new StructureSearchParams
                        ("CN(C)CCC1=CNC2=C1C=C(CS(=O)(=O)N3CCCC3)C=C2",
                                StructureSearchParams.Type.Similarity).setThreshold(.65));
        System.out.println("####### Similarity..."+iter.getCount());
        while (iter.hasNext()) {
            Compound c = iter.next();
            //dump(c);
            System.out.println("--- " + iter.getConsumedCount() + " ---");
        }
        iter.done();
        esm.shutdown();
    }

    static void testServices6(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        CompoundService cs = esm.getService(Compound.class);
        Collection<Compound> cmpds = cs.get(Arrays.asList(
                3235555, 3235556, 3235557,
                3235558, 3235559, 3235560,
                3235561, 3235562, 3235563, 3235564));
        for (Compound c : cmpds) {
            dump(c);
            System.out.println("----");
        }

        ProjectService ps = esm.getService(Project.class);
        Collection<Project> projs = ps.get(Arrays.asList(805, 13413413));

        for (Project p : projs) {
            System.out.println(p);
            System.out.println("----");
        }

        System.out.println("** retrieving bogus project 13413413");
        System.out.println(ps.get(13413413));

        esm.shutdown();
    }

    static void testServices7(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        SearchParams params = new SearchParams("dna repair");
        for (EntityService es : esm.getServices()) {
            try {
                ServiceIterator<? extends Entity> iter = es.search(params);
                System.out.println("### " + es.getEntityClass());
                while (iter.hasNext()) {
                    Entity e = iter.next();
                    EntityAdapter adapter = new EntityAdapter(e);
                    System.out.println("++ [" + e.getId() + "] " + e.getName());
                    System.out.println("## " + adapter.getSearchHighlight(params));
                }
                System.out.println(">> ETag=" + iter.getETag()+" "
                                   +iter.getCount() + " hit(s) found!");
                dump(iter.getFacets());
                iter.done();

                System.out.println("### " + iter.getConsumedCount()
                        + " " + es.getEntityClass());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        esm.shutdown();
    }

    static void testServices8(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        CompoundService cs = esm.getService(Compound.class);
        Value etag = cs.newETag
                ("My awesome compound collection",
                        Arrays.asList(3235555, 3235556, 3235557, 3235558, 3235559,
                                3235560, 3235561, 3235562, 3235563, 3235564,
                                3235565, 3235566, 3235567, 3235568, 3235569,
                                3235570, 3235571));
        System.out.println("## New ETag created: " + etag);
        Collection<Value> facets = cs.getFacets(etag);
        dump(facets);

        // adding a few more id's to this etag
        int cnt = cs.putETag(etag, Arrays.asList(1, 2, 3));
        System.out.println(cnt + " new id's added to " + etag);

        // get updated facets
        dump(cs.getFacets(etag));

        Collection<Compound> cmpds = cs.get(Arrays.asList(658342));
        for (Compound c : cmpds) {
            dump(c);
        }

        esm.shutdown();
    }

    static void testServices9 (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        SubstanceService ss = esm.getService(Substance.class);
        Value etag = ss.newETag
                ("my test substance list",
                        Arrays.asList(4237471, 4237472, 4237473, 4237474, 4237475, 4237476,
                                4237477, 4237478, 4237479, 4237480, 4237481, 4237482));
        System.out.println("## New substance ETag created: " + etag);

        ExperimentService es = esm.getService(Experiment.class);
        Experiment expr = es.get(626);
        System.out.println("** " + expr);

        ServiceIterator<Value> iter = es.activities(expr, etag);
        System.out.println("** Experiment "+expr.getId()+": "+iter.getCount());
        while (iter.hasNext()) {
            Value v = iter.next();
            System.out.println(v.getId()+": "
                               +iter.getConsumedCount()+"/"+iter.getCount());
        }
        iter.done();
        System.out.println("** " + iter.getConsumedCount()
                + " activities consumed!");

        esm.shutdown();
    }

    static void testServices9b (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        ExperimentService es = esm.getService(Experiment.class);
        Experiment expr = es.get(1326);

        ServiceIterator<Value> iter = es.activities(expr);
        System.out.println("** Experiment "+expr.getId()+": "+iter.getCount());
        while (iter.hasNext()) {
            Value v = iter.next();
            System.out.println(v.getId()+": "
                               +iter.getConsumedCount()+"/"+iter.getCount());
        }
        iter.done();
        System.out.println("** " + iter.getConsumedCount()
                + " activities consumed!");

        esm.shutdown();
    }

    static void testServices10(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        ExperimentService es = esm.getService(Experiment.class);

        for (Experiment e : es.get
                (Arrays.asList(519, 520, 440, 441, 863, 805, 724, 725, 723, 699))) {
            System.out.println(e);
        }

        ProjectService ps = esm.getService(Project.class);
        Project proj = ps.get(588636);
        System.out.println(proj);
        esm.shutdown();
    }

    static void testServices11(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        List<String[]> filters = new ArrayList<String[]>();
        filters.add(new String[]{"tpsa", "55.1"});

        SearchParams params = new SearchParams("\"dna repair\"", filters);
        for (EntityService es : esm.getServices()) {
            try {
                ServiceIterator<? extends Entity> iter = es.search(params);
                System.out.println("### " + es.getEntityClass());
                while (iter.hasNext()) {
                    Entity e = iter.next();
                    System.out.println("++ " + es.getEntityClass().getName() + "[" + e.getId() + "] " + e.getName());
                }
                System.out.println(">> " + iter.getCount() + " hit(s) found!");

                iter.done();

                System.out.println("### " + iter.getConsumedCount()
                        + " " + es.getEntityClass());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        esm.shutdown();
    }

    static void testServices12 (String baseURL) throws Exception {
        logger.info("## Executing test...");
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);

        long start = System.currentTimeMillis();

        ExperimentService es = esm.getService(Experiment.class);
        CompoundService cs = esm.getService(Compound.class);

        final Experiment experiment = es.get(346);
        final ServiceIterator<Compound> iter =  es.compounds(experiment);

        List<Compound> compoundList = iter.next(5);
        Value etag = cs.newETag("my compound collection", compoundList);

        ServiceIterator<Value> eiter = es.activities(experiment, etag);

        if (eiter.hasNext()) {
            List<Value> act = eiter.next(10);
            System.out.println("** "+act.size());
        }
        /*
        while (eiter.hasNext())  {
            Value v = eiter.next();
            System.out.println("** "+v);
        }
        */

        eiter.done();

        logger.info("## total time: "
                + String.format("%1$.3fs",
                (System.currentTimeMillis() - start)
                        * 1e-3));
        esm.shutdown();
    }

    static void testServices13(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        AssayService as = esm.getService(Assay.class);
        SearchParams sp = new SearchParams("dna repair");
        //sp.setSkip(new Long(0));
        //sp.setTop(new Long(10));

        final List<String[]> filters = new ArrayList<String[]>();
        filters.add(new String[]{"detection method type", "\"spectrophotometry method\""});
        filters.add(new String[]{"assay component role", "\"target\""});
        //sp.setFilters(filters);
        ServiceIterator<Assay> si = as.search(sp);
        System.out.println("## total = "+si.getCount());
        while (si.hasNext()) {
            Assay a = si.next();
            AssayAdapter ad = new AssayAdapter(a);
            Collection<Value> annos = ad.getAnnotations();
            System.out.println("Got "+annos.size()+" for assay id "+a.getId());
        }
        esm.shutdown();
    }


    static void testServices14 (String baseURL) throws Exception {
        logger.info("## Executing test...");
        long start = System.currentTimeMillis();

        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        ExperimentService es = esm.getService(Experiment.class);

        final Experiment experiment = es.get(1326);
        //System.out.println(experiment);

        ServiceIterator<Value> eiter = es.activities(experiment);
        while (eiter.hasNext())  {
            Value v = eiter.next();
            if (eiter.getConsumedCount() % 5000 == 0) {
                logger.info("## fetching "+eiter.getConsumedCount()+"...");
            }
            //System.out.println("** "+v);
        }
        eiter.done();
        logger.info("## total time: "
                    + String.format("%1$.3fs",
                                    (System.currentTimeMillis()-start)
                                    *1e-3));
        esm.shutdown();
    }

    static void testServices15 (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        AssayService as = esm.getService(Assay.class);
        Assay a = as.get(3563);
        AssayAdapter ad = new AssayAdapter(a);
        System.out.println("ad = " + ad);
        Collection<Value> annos = ad.getAnnotations();
        System.out.println("Got " + annos.size() + " for assay id " + a.getId());

    }

    static void testServices17(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        AssayService as = esm.getService(Assay.class);
        Assay a = as.get(588591);
        int nproj = a.getProjectCount();
        System.out.println("nproj = " + nproj);
        Collection<Value> projs = a.getValues(Project.class.toString()); //a.getProjectIds();
        for (Value val : projs) {
            System.out.println("e.getId() = " + val.getValue());
        }
    }

    static void testServices18(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        AssayService as = esm.getService(Assay.class);
        Collection<Assay> a = as.get(Arrays.asList(3894L, 4174L, 4202L));
        System.out.println("a.size() = " + a.size());
    }

    static void testServices19(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        AssayService as = esm.getService(Assay.class);
        Object etag = as.newETag("foo assay colllection", Arrays.asList(3894L, 4174L, 4202L));
        System.out.println("etag = " + etag);
        Collection<Value> facets = as.getFacets(etag);
        for (Value v : facets) {
            System.out.println(v);
        }
        esm.shutdown();
    }

    static void testServices20(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        CompoundService cs = esm.getService(Compound.class);
        Value etag = cs.newETag("foo cids", Arrays.asList(3233285L, 3234360L, 3235360L));
        ExperimentService es = esm.getService(Experiment.class);
        Experiment e = es.get(883L);
        ServiceIterator<Value> acts = es.activities(e, etag);
        while (acts.hasNext()) {
            Value value = acts.next();
            System.out.println("value = " + value);
        }
        System.out.println("acts.getCount() = " + acts.getCount());
    }

    static void testServices20a(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        CompoundService cs = esm.getService(Compound.class);
        Value etag = cs.newETag("foo cids", Arrays.asList(16001802L));
        ExperimentService es = esm.getService(Experiment.class);
        Experiment e = es.get(2273L);
        ServiceIterator<Value> acts = es.activities(e, etag);
        while (acts.hasNext()) {
            Value value = acts.next();
            System.out.println("value = " + value);
        }
        System.out.println("acts.getCount() = " + acts.getCount());
        esm.shutdown();
    }

    static void testServices20b(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        CompoundService cs = esm.getService(Compound.class);
        Value etag = cs.newETag("foo cids", Arrays.asList(3237462L, 3240101L, 16001802L));
        ExperimentService es = esm.getService(Experiment.class);
        Experiment e = es.get(2273L);
        ServiceIterator<Value> acts = es.activities(e, etag);
        while (acts.hasNext()) {
            Value value = acts.next();
            System.out.println("value = " + value);
        }
        System.out.println("acts.getCount() = " + acts.getCount());
        esm.shutdown();
    }

    static void testServices21(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        ProjectService ps = esm.getService(Project.class);
        Project a = ps.get(279);
        ProjectAdapter pa = new ProjectAdapter(a);
        Collection<Value> annos = pa.getAnnotations();
        System.out.println("Got " + annos.size() + " annotations for project id " + a.getId());
        esm.shutdown();
    }

    static void testServices22(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        AssayService as = esm.getService(Assay.class);
        Assay a = as.get(1L);
        Collection<Value> pubs = a.getValues(Document.class.toString());
        System.out.println("pubs.size() = " + pubs.size());
        System.out.println("a.getCapAssayId() = " + a.getId());
        esm.shutdown();
    }

    static void testServices23(String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        ExperimentService es = esm.getService(Experiment.class);
        Experiment e = es.get(1326L);
        System.out.println("e.getAssay() = " + e.getAssay().getId());
        esm.shutdown();
    }

    static void testServices24 (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);
        CompoundService cs = esm.getService(Compound.class);
        System.out.println("#### public compound etags ######");
        ServiceIterator<Value> iter = cs.etags(null);
        while (iter.hasNext()) {
            Value etag = iter.next();
            System.out.println(iter.getConsumedCount()+": "+etag);
            Collection<Value> facets = cs.getFacets(etag);
            System.out.println("=== FACETS ===");
            System.out.println(facets);
            ServiceIterator<Compound> citer = cs.iterator(etag);
            while (citer.hasNext() && citer.getConsumedCount() < 100) {
                Compound c = citer.next();
                Collection<Value> synonyms = c.getSynonyms();
                System.out.println
                    ("=== "+synonyms.size()+" SYNONYMS for Compound "+c.getId()+" ===");
                //System.out.println(synonyms);
            }
            citer.done();
        }
        iter.done();
        esm.shutdown();
    }

    static void testServices25 (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);
        ProjectService ps = esm.getService(Project.class);
        ServiceIterator<Project> iter = ps.iterator();
        while (iter.hasNext()) {
            Project proj = iter.next();
            System.out.println(iter.getConsumedCount()+": "+proj.getName() 
                               +"; "+proj.getExperiments().size()
                               +" experiment(s)");
        }
        iter.done();
        esm.shutdown();
    }

    static void testServices26a (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);
        CompoundService cs = esm.getService(Compound.class);

        /*
         * iterate over all related entities for this compound
         */
        Compound compound = cs.get(5994);
        ServiceIterator<Experiment> expr = 
            cs.iterator(compound, Experiment.class);
        while (expr.hasNext()) {
            Experiment e = expr.next();
            System.out.println(expr.getConsumedCount()+": "+e.getName());
        }
        expr.done();
        System.out.println("## Compound "+compound.getId()+" has "
                           +expr.getConsumedCount() + " experiments!");

        ServiceIterator<Project> proj = 
            cs.iterator(compound, Project.class);
        while (proj.hasNext()) {
            Project p = proj.next();
            System.out.println(proj.getConsumedCount()+": "+p.getName());
        }
        proj.done();
        System.out.println("## Compound "+compound.getId()+" has "
                           +proj.getConsumedCount() + " projects!");

        ServiceIterator<Assay> assay = 
            cs.iterator(compound, Assay.class);
        while (assay.hasNext()) {
            Assay a = assay.next();
            System.out.println(assay.getConsumedCount()+": "+a.getName());
        }
        assay.done();
        System.out.println("## Compound "+compound.getId()+" has "
                           +assay.getConsumedCount() + " assays!");

        esm.shutdown();
    }


    static void testServices26b (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);

        /*
         * iterate over all related entities for this assay
         */
        AssayService as = esm.getService(Assay.class);
        Assay assay = as.get(958);

        ServiceIterator<Experiment> expr = 
            as.iterator(assay, Experiment.class);
        while (expr.hasNext()) {
            Experiment e = expr.next();
            System.out.println(expr.getConsumedCount()+": "+e.getName());
        }
        expr.done();
        System.out.println("## Assay "+assay.getId()+" has "
                           +expr.getConsumedCount() + " experiments!");

        ServiceIterator<Project> proj = 
            as.iterator(assay, Project.class);
        while (proj.hasNext()) {
            Project p = proj.next();
            System.out.println(proj.getConsumedCount()+": "+p.getName());
        }
        proj.done();
        System.out.println("## Assay "+assay.getId()+" has "
                           +proj.getConsumedCount() + " projects!");
        
        esm.shutdown();
    }


    static void testServices26c (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);

        /*
         * iterate over all related entities for this assay
         */
        ProjectService ps = esm.getService(Project.class);
        Project proj = ps.get(1);

        ServiceIterator<Experiment> expr = ps.iterator(proj, Experiment.class);
        while (expr.hasNext()) {
            Experiment e = expr.next();
            System.out.println(expr.getConsumedCount()+": "+e.getName());
        }
        expr.done();
        System.out.println("## Project "+proj.getId()+" has "
                           +expr.getConsumedCount() + " experiments!");


        ServiceIterator<Assay> assay = ps.iterator(proj, Assay.class);
        while (assay.hasNext()) {
            Assay a = assay.next();
            System.out.println(assay.getConsumedCount()+": "+a.getName());
        }
        assay.done();
        System.out.println("## Project " + proj.getId() + " has "
                + assay.getConsumedCount() + " assays!");
        esm.shutdown();
    }

    static void testServices26d (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);

        ExperimentService es = esm.getService(Experiment.class);
        Experiment expr = es.get(197);
        System.out.println("expr.getPubchemAid() = " + expr.getPubchemAid());
        ServiceIterator<Project> iter = es.iterator(expr, Project.class);
        while (iter.hasNext()) {
            Project proj = iter.next();
            System.out.println(iter.getConsumedCount()+": "+proj.getName());
        }
        iter.done();
        System.out.println("## Experiment "+expr.getId()+" has "
                           +iter.getConsumedCount() + " projects!");
        esm.shutdown();
    }

        
    static void testServices27 (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager(baseURL);
        CompoundService cs = esm.getService(Compound.class);
        Collection<Assay> assays = cs.getTestedAssays(cs.get(3846310L), false);
        System.out.println("assays.size() = " + assays.size());
        for (Assay a : assays) System.out.println("a = " + a.getName());
        int nall = assays.size();

        Collection<Assay> aassays = cs.getTestedAssays(cs.get(3846310L), true);
        int nact = aassays.size();
        System.out.println("All = " + nall + " and Active = " + nact);

        esm.shutdown();
    }

    static void testServices28 (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);
        System.out.println("######## This test exercise public etags ######");
        for (EntityService<? extends Entity> es : esm.getServices()) {
            ServiceIterator<Value> iter = es.etags(null);
            System.out.println("=== "+es.getEntityClass()+" "+iter.getCount());
            while (iter.hasNext()) {
                Value tag = iter.next();
                System.out.println("ETag: "+tag.getId());
                System.out.println("Name: "+tag.getChild("name").getValue());
                Integer size = (Integer)tag.getChild("count").getValue();
                System.out.println("Size: "+size);

                ServiceIterator i = es.iterator(tag);
                while (i.hasNext()) {
                    i.next();
                }
                i.done();

                if (i.getConsumedCount() != size) {
                    System.out.println("** size is bogus; expecting "+size
                                       +" but instead got "
                                       +i.getConsumedCount());
                }
            }
            iter.done();
        }
        esm.shutdown();
    }


    static void testServices29 (String baseURL) throws Exception {
        EntityServiceManager esm = new RESTEntityServiceManager (baseURL);
        ExperimentService es = esm.getService(Experiment.class);
        CompoundService cs = esm.getService(Compound.class);
        Experiment expr = es.get(4324);
        Compound cmpd = cs.get(9893924);

        ServiceIterator<Value> iter = es.activities(expr, cmpd);
        while (iter.hasNext()) {
            Value act = iter.next();
            System.out.println("** "+act);
        }
        iter.done();
        logger.info("## "+iter.getConsumedCount()+" activities!");
        esm.shutdown();
    }


    public static void main(String[] argv) throws Exception {
        String latest = "http://bard.nih.gov/api/latest";
        String local = "http://localhost:8080/bard";

        testServices29 (latest);
    }
}
