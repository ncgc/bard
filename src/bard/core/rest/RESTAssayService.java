package bard.core.rest;

import bard.core.Assay;
import bard.core.AssayService;
import bard.core.AssayValues;
import bard.core.Biology;
import bard.core.Compound;
import bard.core.DataSource;
import bard.core.Document;
import bard.core.Entity;
import bard.core.EntityValue;
import bard.core.Experiment;
import bard.core.IntValue;
import bard.core.Project;
import bard.core.ServiceIterator;
import bard.core.StringValue;
import bard.core.Value;
import org.codehaus.jackson.JsonNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;


@SuppressWarnings("unchecked")
public class RESTAssayService extends RESTAbstractEntityService<Assay> 
    implements AssayService, AssayValues {

    private static final long serialVersionUID = -1545336684442597397L;
    static final private Logger logger = 
        Logger.getLogger(RESTAssayService.class.getName());

    protected RESTAssayService 
        (RESTEntityServiceManager srvman, String baseURL) {
        super (srvman, baseURL);
    }

    public Class<Assay> getEntityClass () { return Assay.class; }
    public String getResourceContext () { return "/assays"; }

    protected Assay getEntity (Assay assay, JsonNode node) {
        if (assay == null) {
            assay = new Assay (this);
        }

        if (node == null) {
            return assay;
        }

        JsonNode n;

        n = node.get("resourcePath");
        DataSource ds = getDataSource ();
        if (n != null && !n.isNull()) {
            ds = getDataSource(n.getTextValue());
        }

        n = node.get("bardAssayId");
        if (n != null && !n.isNull()) {
            assay.setId(n.getLongValue());
        }

        n = node.get("capAssayId");
        if (n != null && !n.isNull()) {
            assay.add(new IntValue (ds, AssayCapIDValue, n.getIntValue()));
        }

        n = node.get("category");
        if (n != null && !n.isNull()) {
            assay.setCategory(AssayCategory.valueOf(n.asInt()));
        }

//        n = node.get("type");
//        if (n != null && !n.isNull()) {
//            assay.setType(AssayType.valueOf(n.asInt()));
//        }
//
        // summary
        
        // assays
        
        n = node.get("classification");
        if (n != null && !n.isNull()) {
            assay.setRole(AssayRole.valueOf(n.asInt()));
        }

        n = node.get("title");
        if (n != null && !n.isNull()) {
            assay.add(new StringValue (ds, AssayAltName, n.asText()));
        }

        n = node.get("designedBy");
        if (n != null && !n.isNull()) {
            assay.add(new StringValue (ds, AssaySourceValue, n.asText()));
        }

       n = node.get("source");
        if (n != null && !n.isNull()) {
            assay.add(new StringValue (ds, AssaySourceValue, n.asText()));
        }

        n = node.get("grantNo");
        if (n != null && !n.isNull()) {
            assay.add(new StringValue (ds, AssayGrantValue, n.asText()));
        }

        n = node.get("name");
        if (n != null && !n.isNull()) {
            assay.setName(n.asText());
        }

        // deposited
        
        // updated
        
        // assayType
        
        // assayStatus
        
        // score
        
        //minimumAnnotations
        n= node.get("minimumAnnotations");
        if (n != null && !n.isNull()) {
            for (AssayMinimumAnnotation annot: AssayMinimumAnnotation.values()) {
                JsonNode m = n.get(annot.index());
                if (m != null && !m.isNull()) {
                    assay.add(new StringValue (ds, annot.name(), m.asText()));
                }
            }
        }
        
//        // kegg_disease_cat
//        DataSource keggDS = getDataSource(EntityNamedSources.KEGGDiseaseCategoryAnnotationSource);
//        n = node.get("kegg_disease_cat");
//        if (n != null && !n.isNull()) {
//            for (JsonNode item: (ArrayNode)n) {
//        	assay.add(new StringValue (keggDS, Assay.KEGGDiseaseCategoryAnnotationSource, item.asText()));
//            }
//        }
//        
//        // kegg_disease_names
//        keggDS = getDataSource(EntityNamedSources.KEGGDiseaseNameAnnotationSource);
//        n = node.get("kegg_disease_names");
//        if (n != null && !n.isNull()) {
//            for (JsonNode item: (ArrayNode)n) {
//        	assay.add(new StringValue (keggDS, Assay.KEGGDiseaseNameAnnotationSource, item.asText()));
//            }
//        }
//        
        // !!! only in expanded record -- description, protocol, comments
        n = node.get("description");
        if (n != null && !n.isNull()) {
            assay.add(new StringValue (ds, AssayDescription, n.asText()));
        }
        
        n = node.get("protocol");
        if (n != null && !n.isNull()) {
            assay.add(new StringValue (ds, AssayProtocol, n.asText()));
        }

        n = node.get("comments");
        if (n != null && !n.isNull()) {
            assay.add(new StringValue (ds, AssayComments, n.asText()));
        }

        addEntityValues(assay, ds, node.get("documents"), Document.class, "pubmedId");
        addEntityValues(assay, ds, node.get("targets"), Biology.class, "serial");
        addEntityValues(assay, ds, node.get("experiments"), Experiment.class, "bardExptId");
        addEntityValues(assay, ds, node.get("projects"), Project.class, "bardProjectId");
        
        //getAnnotations(assay);
        
        return assay;
    }

    class ValueServiceIterator implements ServiceIterator {

	Entity ent;
	Iterator<Value> valIterator;
	int index = 0;
	
	ValueServiceIterator(Entity ent, Iterator<Value> valIterator) {
	    this.ent = ent;
	    this.valIterator = valIterator;
	}
	
	@Override
	public boolean hasNext() {
	    return valIterator.hasNext();
	}

	@Override
	public Object next() {
	    return ent.getEntityFromValue((EntityValue)valIterator.next());
	}

	@Override
	public void remove() {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public List next(int size) {
	    List list = new ArrayList();
	    for (int i=0; i<size && hasNext(); i++)
		list.add(next());
	    return list;
	}

	@Override
	public long getConsumedCount() {
	    // TODO Auto-generated method stub
	    return 0;
	}

	@Override
	public long getCount() {
	    // TODO Auto-generated method stub
	    return 0;
	}

	@Override
	public void done() {
	    // TODO Auto-generated method stub
	}

	@Override
	public Value getETag() {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public Collection getFacets() {
	    // TODO Auto-generated method stub
	    return null;
	}
	
    }
    
    @Override
    public <T extends Entity> ServiceIterator<T> iterator 
                      (Assay assay, Class<T> clazz) {
        RESTAbstractEntityService<T> service = 
            (RESTAbstractEntityService<T>)getServiceManager().getService(clazz);

        if (clazz.equals(Project.class)) {
            return new ValueServiceIterator(assay, 
        	    assay.getValues(clazz.toString()).iterator());
//            return service.getIterator 
//                (getResource (assay.getId()+"/projects"), null);
        }
        else if (clazz.equals(Experiment.class)) {
            return service.getIterator
                (getResource (assay.getId()+"/experiments"), null);
        }
        else if (clazz.equals(Compound.class)) {
            return null; //!!! no compound resource available for assays at moment
        }
        else {
            throw new IllegalArgumentException 
                ("No related entities available for "+clazz);
        }
    }
}
