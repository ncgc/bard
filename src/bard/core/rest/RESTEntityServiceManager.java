package bard.core.rest;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ConcurrentHashMap;

import bard.core.*;
import bard.util.CachingService;

public class RESTEntityServiceManager implements EntityServiceManager {
    EntityService[] services;
    ExecutorService threadPool = Executors.newCachedThreadPool();
    CachingService cachingService = new CachingService();

    public RESTEntityServiceManager (String baseURL) {
        services = new EntityService[]{
            new RESTCompoundService (this, baseURL),
            new RESTProjectService (this, baseURL),
            new RESTAssayService (this, baseURL),
            new RESTExperimentService (this, baseURL),
            new RESTSubstanceService (this, baseURL),
            new RESTBiologyService (this, baseURL),
            new RESTDocumentService (this, baseURL)
        };
    }

    protected ExecutorService getThreadPool () { return threadPool; }

    public <T extends EntityService<? extends Entity>> T getService
                      (Class<? extends Entity> clazz) {
        if (services == null) {
            throw new IllegalStateException
                ("Service manager has already been shutdown!");
        }

        Class c = clazz;
        do {
            for (EntityService<? extends Entity> es : services) {
                if (clazz.equals(es.getEntityClass())) {
                    return (T)es;
                }
            }
            c = c.getSuperclass();
        }
        while (c != null);

        return null;
    }

    public <T extends EntityService<? extends Entity>> List<T> getServices () {
        List<T> srv = new ArrayList<T>();
        for (EntityService<? extends Entity> es : services) {
            srv.add((T)es);
        }
        return srv;
    }

    public void shutdown () {
        threadPool.shutdownNow();
    }
}
