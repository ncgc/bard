package bard.core.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import bard.core.Assay;
import bard.core.AssayValues;
import bard.core.Biology;
import bard.core.Compound;
import bard.core.DataSource;
import bard.core.Document;
import bard.core.Entity;
import bard.core.EntityNamedSources;
import bard.core.EntityValue;
import bard.core.Experiment;
import bard.core.IntValue;
import bard.core.Project;
import bard.core.ProjectService;
import bard.core.ServiceIterator;
import bard.core.StringValue;
import bard.util.BardHttpClient;


public class RESTProjectService extends RESTAbstractEntityService<Project>
    implements ProjectService, AssayValues {

    private static final long serialVersionUID = 1L;
    static final private Logger logger = 
        Logger.getLogger(RESTProjectService.class.getName());

    protected RESTProjectService 
        (RESTEntityServiceManager srvman, String baseURL) {
        super (srvman, baseURL);
    }

    public Class<Project> getEntityClass () { return Project.class; }
    public String getResourceContext () { return "/projects"; }

    protected Project getEntity (Project proj, JsonNode node) {
        if (proj == null) {
            proj = new Project (this);
        }

        if (node == null) {
            return proj;
        }

        // identical to Assay... sigh
        JsonNode n = node.get("bardProjectId");
        if (n != null && !n.isNull()) {
            proj.setId(n.asLong());
        }

        n = node.get("name");
        if (n != null && !n.isNull()) {
            proj.setName(n.asText());
        }
        else {
            System.err.println("### No name project: "+node);
        }

        DataSource ds = getDataSource ();

        n = node.get("description");
        if (n != null && !n.isNull()) {
            proj.add(new StringValue (ds, Entity.EntityDescription, n.asText()));
        }

        /*
        proj.add(new StringValue
                (ds, AssayGrantValue, node.get("grantNo").asText()));
        */

        n = node.get("experimentCount");
        if (n != null) {
            proj.add(new IntValue 
                     (ds, Project.NumberOfExperimentsValue, n.asInt()));
        }
        n = node.get("source");
        if (n != null) proj.add(new StringValue(ds, Project.SourceValue, n.asText()));

        n = node.get("probeIds");
        if (n != null) {
            proj.add(new IntValue(ds, Project.NumberOfProbesValue, n.size()));
        }

        addEntityValues(proj, ds, node.get("targets"), Biology.class, "serial");
        addEntityValues(proj, ds, node.get("aids"), Assay.class, "bardAssayId");
        addEntityValues(proj, ds, node.get("eids"), Experiment.class, "bardExptId");
        addEntityValues(proj, ds, node.get("probes"), Compound.class, "probeId");

        getAnnotations(proj);
        
        return proj;
    }

    protected Project getEntitySearch (Project proj, JsonNode node) {
        if (proj == null) {
            proj = new Project (this);
        }

        JsonNode n = node.get("proj_id");
        if (n != null && !n.isNull()) {
            /*
             * FIXME.. this should be handle by the REST api!
             */
            proj = get (n.asLong()); 
        }

        /*
        proj.setId(.asLong());
        proj.setName(node.get("name").asText());
        proj.setDescription(node.get("description").asText());
        */

        DataSource ds = getDataSource ();
        n = node.get("highlight");
        if (n != null && !n.isNull()) {
            proj.add(new StringValue 
                    (ds, Entity.SearchHighlightValue, n.asText()));
        }

        /*
        n = node.get("num_expt");
        if (n != null) {
            proj.add(new IntValue 
                     (ds, Project.NumberOfExperimentsValue, n.asInt()));
        }

        n = node.get("source");
        if (n != null) proj.add(new StringValue
                                (ds, Project.SourceValue, n.asText()));
        */

        return proj;
    }

    Project addAnnotations(Project project, JsonNode node) {
        // pull in assay annotations - process different source with different DataSource's
        DataSource capds = getDataSource(EntityNamedSources.CAPAnnotationSource);
        if (node.has("ak_dict_label") && !node.get("ak_dict_label").isNull()) {
            ArrayNode keys = (ArrayNode) node.get("ak_dict_label");
            ArrayNode vals = (ArrayNode) node.get("av_dict_label");
            for (int i = 0; i < keys.size(); ++i) {
                String key = keys.get(i).asText();
                String val = vals.get(i).asText();
                project.add(new StringValue(capds, key, val));
            }
        }

        project = setAnnotations(project, node,
                getDataSource(EntityNamedSources.GOBPAnnotationSource),
                "gobp_id", "gobp_term");
        project = setAnnotations(project, node,
                getDataSource(EntityNamedSources.GOMFAnnotationSource),
                "gomf_id", "gomf_term");
        project = setAnnotations(project, node,
                getDataSource(EntityNamedSources.GOCCAnnotationSource),
                "gocc_id", "gocc_term");

        project = setAnnotations(project, node,
                getDataSource(EntityNamedSources.KEGGDiseaseCategoryAnnotationSource),
                "kegg_disease_cat", null);
        project = setAnnotations(project, node,
                getDataSource(EntityNamedSources.KEGGDiseaseNameAnnotationSource),
                "kegg_disease_names", null);

        return project;
    }

    Project setAnnotations(Project a, JsonNode node, DataSource ds, String keyField, String valueField) {
        if (node.has(keyField) && !node.get(keyField).isNull()) {
            ArrayNode keys = (ArrayNode) node.get(keyField);
            ArrayNode vals = (ArrayNode) node.get(valueField);
            for (int i = 0; i < keys.size(); ++i) {
                String key = keys.get(i).asText();

                if (valueField != null) {
                    String val = vals.get(i).asText();
                    a.add(new StringValue(ds, key, val));
                } else a.add(new StringValue(ds, keyField, key));
            }
        }
        return a;
    }

    @Override
    public <T extends Entity> ServiceIterator<T> iterator 
                      (Project proj, Class<T> clazz) {
        @SuppressWarnings("unchecked")
	RESTAbstractEntityService<T> service = 
            (RESTAbstractEntityService<T>)getServiceManager().getService(clazz);

        if (clazz.equals(Assay.class)) {
            return service.getIterator
                    (getResource(proj.getId() + "/assays"), null);
        } else if (clazz.equals(Experiment.class)) {
            return service.getIterator
                    (getResource(proj.getId() + "/experiments"), null);
        } else if (clazz.equals(Compound.class)) {
            return service.getIterator
                    (getResource(proj.getId() + "/probes"), null);
        } else {
            throw new IllegalArgumentException
                    ("No related entities available for " + clazz);
        }
    }

    public Map<String, Object> getProjectSummary(Project proj) {
	Map<String, Object> ret = new HashMap<String, Object>();
	String url = getResource(proj.getId()) + "/summary";
	BardHttpClient httpclient = new BardHttpClient("getProjectSummary");
	//        DefaultHttpClient httpclient = new DefaultHttpClient();
	HttpGet get = new HttpGet(url);
	HttpResponse response = httpclient.execute(get);
	HttpEntity entity = response.getEntity();
	if (entity != null && response.getStatusLine().getStatusCode() == 200) { 
	    try {
		InputStream is = entity.getContent();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(is);

		ret.put("assay_count", root.get("assay_count").asInt());
		ret.put("experiment_count", root.get("experiment_count").asInt());
		ret.put("cmpd_purchase_count", root.get("cmpd_purchase_count").asInt());
		ret.put("cmpd_synthesis_count", root.get("cmpd_synthesis_count").asInt());

		List<String> probereps = new ArrayList<String>();
		ArrayNode anode = (ArrayNode) root.get("probe_reports");
		for (int  i = 0; i < anode.size(); i++) probereps.add(anode.get(i).asText());
		ret.put("probe_reports", probereps);

		Map<String, Integer> exptClasses = new HashMap<String, Integer>();
		Iterator<String> fieldNames = root.get("experiment_class").getFieldNames();
		while (fieldNames.hasNext()) {
		    String fieldName = fieldNames.next();
		    int value = root.get("experiment_class").get(fieldName).asInt();
		    exptClasses.put(fieldName, value);
		}
		ret.put("experiment_class", exptClasses);
		is.close();
	    } catch (Exception e) {e.printStackTrace();} 
	}
	httpclient.close();
	return ret;
    }
}
