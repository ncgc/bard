package bard.core.rest;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.CompoundService;
import bard.core.DataSource;
import bard.core.Entity;
import bard.core.EntityNamedSources;
import bard.core.Experiment;
import bard.core.LongValue;
import bard.core.MolecularData;
import bard.core.MolecularValue;
import bard.core.Project;
import bard.core.SearchParams;
import bard.core.ServiceIterator;
import bard.core.StringValue;
import bard.core.StructureSearchParams;
import bard.core.Value;
import bard.core.impl.MolecularDataJChemImpl;
import bard.util.BardHttpClient;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


public class RESTCompoundService extends RESTAbstractEntityService<Compound> 
    implements CompoundService {

    private static final long serialVersionUID = 1L;

    static final private Logger logger = 
        Logger.getLogger(RESTCompoundService.class.getName());

    static final Compound DONE = new Compound (null);

//    // this pattern is quite general; it should be refactor somewhere else
//    class StructureSearchIterator 
//        implements ServiceIterator<Compound>, Runnable {
//        volatile StructureSearchParams params;
//        volatile String url;
//        volatile Compound next;
//        Map<String, Long> etags = new ConcurrentHashMap<String, Long>();
//
//        BlockingQueue<Compound> buffer = 
//            new ArrayBlockingQueue<Compound>(DEFAULT_BUFSIZ);
//
//        Future future;
//        AtomicLong consumed = new AtomicLong ();
//        //AtomicLong count = new AtomicLong ();
//        long count = -1;
//        final Lock lock = new ReentrantLock ();
//        final Condition countSet = lock.newCondition();
//
//        StructureSearchIterator (StructureSearchParams params) {
//            StringBuilder url = new StringBuilder ();
//            try {
//                url.append(getResource()+"?filter="
//                           +URLEncoder.encode(params.getQuery(), "utf-8")
//                           +"[structure]");
//            }
//            catch (UnsupportedEncodingException ex) {
//                ex.printStackTrace();
//                throw new IllegalArgumentException
//                    ("Bogus query: "+params.getQuery());
//            }
//            
//            switch (params.getType()) {
//            case Substructure: url.append("&type=sub"); break;
//            case Superstructure: url.append("&type=sup"); break;
//            case Exact: url.append("&type=exact"); break;
//            case Similarity: 
//                url.append("&type=sim"); 
//                if (params.getThreshold() != null) {
//                    url.append("&cutoff="+String.format
//                               ("%1$.3f", params.getThreshold()));
//                }
//                else {
//                    throw new IllegalArgumentException
//                        ("No threshold specified for similarity search!");
//                }
//                break;
//            }
//            url.append("&expand=true");
//
//            this.url = url.toString();
//            this.params = params;
//
//            future = getThreadPool().submit(this);
//        }
//
//        public void run () {
//            try {
//                long skip = params.getSkip() != null ? params.getSkip() : 0;
//                long top = params.getTop() != null ? params.getTop() : 100;
//
//                while (true) {
//                    List<Compound> results = search (url, top, skip, etags);
//                    for (Compound c : results) {
//                        buffer.put(c);
//                        if (Thread.currentThread().isInterrupted()) {
//                            buffer.put(DONE);
//                            return;
//                        }
//                    }
//
//                    lock.lock();
//                    try {
//                        if (count < 0) {
//                            count = 0;
//                        }
//                        count += results.size();;
//                        countSet.signal();
//                    }
//                    finally {
//                        lock.unlock();
//                    }
//
//                    if (params.getTop() != null || results.size() < top) {
//                        break;
//                    }
//                    skip += results.size();
//                }
//
//                buffer.put(DONE);
//            }
//            catch (InterruptedException ex) {
//                //logger.info("** "+Thread.currentThread()+" interrupted!");
//                buffer.offer(DONE);
//            }
//        }
//
//        public boolean hasNext () {
//            if (next != null) return true;
//
//            try {
//                Compound c = buffer.take();
//                next = c != DONE ? c : null;
//            }
//            catch (InterruptedException ex) {
//                /*
//                logger.info("** "+Thread.currentThread().getName()
//                            +" interrupted!");
//                */
//                next = null;
//            }
//            return next != null;
//        }
//
//        public Compound next () {
//            consumed.incrementAndGet();
//            Compound n = next;
//            next = null;
//            return n;
//        }
//
//        public Collection<Value> getFacets () {
//            String etag = getParentETag (etags);
//            return RESTCompoundService.this.getFacets(etag);
//        }
//
//        public Object getETag () { return getParentETag (etags); }
//
//        public void remove () {
//            throw new UnsupportedOperationException ("remove not supported!");
//        }
//
//        public void done () {
//            future.cancel(true);
//        }
//
//        public List<Compound> next (int size) {
//            List<Compound> page = new ArrayList<Compound>();
//            /*
//            int s = Math.min(size, buffer.size());
//
//            buffer.drainTo(page, s);
//            for (; hasNext () && s < size; ++s) {
//                page.add(next ());
//            }
//            */
//            for (int i = 0; i < size && hasNext (); ++i) {
//                page.add(next ());
//            }
//            return page;
//        }
//        
//        public long getConsumedCount () { return consumed.get(); }
//        public long getCount () { 
//            lock.lock();
//            try {
//                while (count < 0)
//                    countSet.await();
//                return count;
//            }
//            catch (InterruptedException ex) {
//                ex.printStackTrace();
//                return buffer.size();
//            }
//            finally {
//                lock.unlock();
//            }
//        }
//    }

    protected RESTCompoundService 
        (RESTEntityServiceManager srvman, String baseURL) {
        super (srvman, baseURL);
    }

    public Class<Compound> getEntityClass () { return Compound.class; }
    public String getResourceContext () { return "/compounds"; }

    public ServiceIterator<Compound> structureSearch
        (StructureSearchParams params) {
        return search(params);
    }

    public Collection<Assay> getTestedAssays (Compound compound, 
                                              boolean activeOnly) {
	BardHttpClient httpclient = new BardHttpClient("getTestedAssays");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<Assay> assays = new ArrayList<Assay>();
        RESTAssayService as = getServiceManager().getService(Assay.class);

        try {
            String url = getResource(compound.getId() + "/assays?expand=true");
            if (activeOnly) url += "&filter=active";

            HttpGet get = new HttpGet(url);
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            if (entity != null 
                && response.getStatusLine().getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is).get("collection");
                    if (node != null && node.isArray()) {
                        ArrayNode array = (ArrayNode) node;
                        for (int i = 0; i < array.size(); ++i) {
                            JsonNode n = array.get(i);
                            if (n != null && !n.isNull()) {
                                Assay a = new Assay();
                                as.getEntity(a, n);
                                assays.add(a);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    is.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpclient.close();
        }
        return assays;
    }

    public Collection<Value> getAnnotations(Entity entity) {
        BardHttpClient httpclient = new BardHttpClient("getAnnotations");
        List<Value> annotations = new ArrayList<Value>();
        try {
            String url = getResource(entity.getId() + "/annotations");
            HttpGet get = new HttpGet(url);
            HttpResponse response = httpclient.execute(get);
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null && response.getStatusLine().getStatusCode() == 200) {
                InputStream is = httpEntity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is);
                    if (node != null) {
                        ArrayNode keyNode = (ArrayNode) node.get("anno_key");
                        ArrayNode valNode = (ArrayNode) node.get("anno_val");
                        if (keyNode.size() != valNode.size()) {
                            logger.warning("Length of key array did not match that of value array for CID "+entity.getId());
                            throw new IOException("Length of key array did not match that of value array for CID "+entity.getId());
                        }
                        DataSource ds = getDataSource(EntityNamedSources.AnnotationSource);
                        ds.setURL(url);
                        for (int i = 0; i < keyNode.size(); i++) {
                            String key = keyNode.get(i).asText();
                            String value = valNode.get(i).asText();
                            Value entry = new StringValue(ds, key, value);
                            entity.add(entry);
                            annotations.add(entry);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    is.close();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            httpclient.close();
        }
        return annotations;
    }

    public Collection<Value> getSubstances(Compound compound) {
        BardHttpClient httpclient = new BardHttpClient("getSubstances");
        List<Value> sids = new ArrayList<Value>();
        try {
            String url = getResource(compound.getId() + "/sids");
            HttpGet get = new HttpGet(url);
            HttpResponse response = httpclient.execute(get);
            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine().getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is);
                    if (node != null && node.isArray()) {
                        ArrayNode array = (ArrayNode) node;
                        DataSource ds = getDataSource(Compound.PubchemSidSource);
                        ds.setURL(url);
                        for (int i = 0; i < array.size(); ++i) {
                            JsonNode n = array.get(i);
                            if (n != null && !n.isNull()) {
                                sids.add(new LongValue(ds, Compound.PubChemSIDValue, n.asLong()));
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    is.close();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            httpclient.close();
        }
        return sids;
    }

    public Collection<Value> getSynonyms (Compound compound) {
	BardHttpClient httpclient = new BardHttpClient("getSynonyms");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<Value> syns = new ArrayList<Value>();

        try {
            String url = getResource (compound.getId() + "/synonyms");
            //logger.info("## url="+url);

            HttpGet get = new HttpGet (url);
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is);
                    if (node != null && node.isArray()) {
                        ArrayNode array = (ArrayNode)node;
                        DataSource ds = getDataSource 
                            (EntityNamedSources.PubChemSynonymSource);
                        ds.setURL(url);
                        for (int i = 0; i < array.size(); ++i) {
                            JsonNode n = array.get(i);
                            if (n != null && !n.isNull()) {
                                syns.add(new StringValue 
                                         (ds, Compound.SynonymValue, 
                                          n.asText()));
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    is.close();
                }
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return syns;
    }

    public Collection<Value> getPubChemSIDs (Compound compound) {
	BardHttpClient httpclient = new BardHttpClient("getSIDs");
//      DefaultHttpClient httpclient = new DefaultHttpClient();
	List<Value> sids = new ArrayList<Value>();

        try {
            String url = getResource (compound.getId() + "/sids");
            //logger.info("## url="+url);

            HttpGet get = new HttpGet (url);
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is);
                    if (node != null && node.isArray()) {
                        ArrayNode array = (ArrayNode)node;
                        DataSource ds = getDataSource 
                            (EntityNamedSources.PubChemSynonymSource);
                        ds.setURL(url);
                        for (int i = 0; i < array.size(); ++i) {
                            JsonNode n = array.get(i);
                            if (n != null && !n.isNull()) {
                                sids.add(new LongValue 
                                         (ds, Compound.PubChemSIDValue, 
                                          n.asLong()));
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    is.close();
                }
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }	
	return sids;
    }

    @Override
    public Map<String, Object> getSummary(Compound compound) {
        Map<String, Object> ret = new HashMap<String, Object>();
        String url = getResource(compound.getId()) + "/summary?expand=true";
        BardHttpClient httpclient = new BardHttpClient(this.getClass().getSimpleName()+":getSummary()");
        HttpGet get = new HttpGet(url);
        HttpResponse response = httpclient.execute(get);
        HttpEntity entity = response.getEntity();
        if (entity != null && response.getStatusLine().getStatusCode() == 200) {
            try {
                InputStream is = entity.getContent();
                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(is);

                ret.put("nhit", root.get("nhit").asInt());
                ret.put("ntest", root.get("ntest").asInt());

                List<String> testedTargetAcc = new ArrayList<String>();
                ArrayNode anode = (ArrayNode) root.get("testedAssays");
                for (int i = 0; i < anode.size(); i++) {
                    ObjectNode assay = (ObjectNode) anode.get(i);
                    ArrayNode targetNodes = (ArrayNode) assay.get("targets");
                    for (int j = 0; j < targetNodes.size(); j++) testedTargetAcc.add(targetNodes.get(j).getTextValue());
                }
                ret.put("testedTargets", testedTargetAcc);

                List<String> hitTargetAcc = new ArrayList<String>();
                anode = (ArrayNode) root.get("hitAssays");
                for (int i = 0; i < anode.size(); i++) {
                    JsonNode jnode = anode.get(i);
                    ObjectNode assay;
                    if (jnode instanceof ObjectNode) {
                        assay = (ObjectNode) jnode;
                        ArrayNode targetNodes = (ArrayNode) assay.get("targets");
                        for (int j = 0; j < targetNodes.size(); j++) testedTargetAcc.add(targetNodes.get(j).getTextValue());
                    }
                }
                ret.put("hitTargets", hitTargetAcc);

                List<Double> testedPotencies = new ArrayList<Double>();
                List<Double> testedScores = new ArrayList<Double>();
                anode = (ArrayNode) root.get("testedExptdata");
                for (int i = 0; i < anode.size(); i++) {
                    ObjectNode exptdata = (ObjectNode) anode.get(i);
                    JsonNode potency = exptdata.get("potency");
                    if (potency != null) testedPotencies.add(potency.asDouble());
                    testedScores.add(exptdata.get("score").getDoubleValue());
                }
                ret.put("testedPotencies", testedPotencies);
                ret.put("testedScores", testedScores);

                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        httpclient.close();
        return ret;
    }

    protected List<Compound> search (String resource, long top, long skip) {
        return search (resource, top, skip, null);
    }

    static String getParentETag (Map<String, Long> etags) {
        String mintag = "";
        Long minval = null;
        for (Map.Entry<String, Long> me : etags.entrySet()) {
            if (minval == null || minval > me.getValue()) {
                mintag = me.getKey();
                minval = me.getValue();
            }
        }
        return mintag;
    }

    protected List<Compound> search (String resource, long top, 
                                     long skip, Map<String, Long> etags) {

        List<Compound> results = new ArrayList<Compound>();
	BardHttpClient httpclient = new BardHttpClient("search[Compound]");
//	DefaultHttpClient httpclient = new DefaultHttpClient();

        try {
            String url = resource+"&top="+top+"&skip="+skip;
            //logger.info("** url="+url);

            HttpGet get = new HttpGet (url);
            if (etags != null && !etags.isEmpty()) {
                String etag = getParentETag (etags);
                get.addHeader("If-Match", "\""+etag+"\"");
            }

            HttpResponse response = httpclient.execute(get);
            if (response.containsHeader("ETag") && etags != null) {
                for (Header h : response.getHeaders("ETag")) {
                    int len = h.getValue().length();
                    if (len > 2) {
                        String e = h.getValue().substring(1, len-1);
                        etags.put(e, skip);
                    }
                }
            }

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper ();
                    // why is this different?
                    ArrayNode node = (ArrayNode)mapper.readTree(is);
                    for (int i = 0; i < node.size(); ++i) {
                        JsonNode n = node.get(i);
                        results.add(getEntity (null, n));
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    is.close();
                }
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return results;
    }

    public Collection<Compound> getHashEntity (String hash, int level) {
	String uri = getResource()+hash+"/h"+(level+1)+"?expand=true";
	return getMultiple(uri);	
    }
    
//    protected List<Compound> get (String resource, boolean expand,
//            long top, long skip,
//            List<String> etags, List<Value> facets) {
//        if (resource.startsWith("/etag"))
//            System.err.println("HERE");
//        return super.get(resource,  expand, top, skip, etags, facets);
//    }
//
    protected Compound getEntity (Compound c, JsonNode node) {
        if (c == null) {
            c = new Compound (this);
        }

        if (node.isArray()) {
            ArrayNode array = (ArrayNode) node;
            if (array.size() > 1 || array.size() == 0)
        	logger.severe("Expected single entity in response, got "+array.size()+" entities.");
            for (int i = 0; i < array.size(); ++i) {
        	node = array.get(i);
            }
        }
        
        DataSource ds = getDataSource ();
        JsonNode n = node.get("cid");
        if (n != null) {
            long cid = n.asLong();
            c.setId(cid);
            // redundant
            c.add(new LongValue (ds, Compound.PubChemCIDValue, cid));
        }

        n = node.get("name");
        if (n != null && !n.isNull()) {
            c.setName(n.asText());
        }

        n = node.get("probeId");
        if (n != null && !n.isNull()) {
            c.add(new StringValue (ds, Compound.ProbeIDValue, 
                                   n.asText()));
            String syn = c.getName();
            if (syn != null) {
                //c.add(new StringValue (ds, Compound.SynonymValue, syn));
            }
            // make this the primary id
            c.setName(n.asText());
        }

        n = node.get("url");
        if (n != null && !n.isNull()) {
            c.add(new StringValue (ds, Compound.URLValue, n.asText()));
        }

        n = node.get("iupacName");
        if (n != null && !n.isNull()) {
            c.add(new StringValue (ds, Compound.IUPACNameValue, n.asText()));
        }

        // we should be getting mol format instead of smiles!
        MolecularData md = new MolecularDataJChemImpl ();
        md.setMolecule(node);
        c.add(new MolecularValue (ds, Compound.MolecularValue, md));

        n = node.get("compoundClass");
        if (n != null && !n.isNull()) {
            c.add(new StringValue (ds, Compound.CompoundClass, n.asText()));
        }

        n = node.get("numAssay");
        if (n != null && !n.isNull()) {
            c.add(new StringValue (ds, Compound.NumAssays, n.asText()));
        }

        n = node.get("numActiveAssay");
        if (n != null && !n.isNull()) {
            c.add(new StringValue (ds, Compound.NumActiveAssays, n.asText()));
        }

        return c;
    }

    protected Compound getEntitySearch (Compound c, JsonNode node) {
	return getEntity(c, node);
	
	// now that the search response is equivalent to the full compound response this extra loop is unnecessary
	/*
        if (c == null) {
            c = new Compound (this);
        }

        JsonNode n;
        n = node.get("cid");
        if (n != null && !n.isNull()) {
            // sigh...
            c = get (n.asLong());
        }


        DataSource ds = getDataSource ();

        n = node.get("highlight");
        if (n != null && !n.isNull()) {
            // remove old molecular value
            for (Value v : c.getValues(Compound.MolecularValue)) {
                c.remove(v);
            }

            c.add(new StringValue 
                  (ds, Compound.SearchHighlightValue, n.asText()));

            MolecularData md = new MolecularDataJChemImpl ();
            md.setMolecule(n.asText());
            c.add(new MolecularValue (ds, Compound.MolecularValue, md));
        }
        */

        /*
        MolecularData md = new MolecularDataJChemImpl ();
        md.setMolecule(node.get("iso_smiles").asText());
        c.add(new MolecularValue (ds, Compound.MolecularValue, md));

        ds = getDataSource (EntityNamedSources.AnnotationSource);
        ArrayNode keys = (ArrayNode)node.get("anno_key");
        ArrayNode vals = (ArrayNode)node.get("anno_val");
        for (int i = 0; i < keys.size(); ++i) {
            String key = keys.get(i).asText();
            String val = vals.get(i).asText();
            c.add(new StringValue (ds, key, val));
        }
        */

        /*return c;*/
    }

    public ServiceIterator<Compound> search (SearchParams params) {
        //if (params instanceof StructureSearchParams) {
        //    return structureSearch ((StructureSearchParams)params);
        //}
        return super.search(params);
    }

    @Override
    public <T extends Entity> ServiceIterator<T> iterator 
                      (Compound compound, Class<T> clazz) {
        RESTAbstractEntityService<T> service = 
            (RESTAbstractEntityService)getServiceManager().getService(clazz);

        if (clazz.equals(Project.class)) {
            return service.getIterator 
                (getResource (compound.getId()+"/projects"), null);
        }
        else if (clazz.equals(Assay.class)) {
            return service.getIterator
                (getResource (compound.getId()+"/assays"), null);
        }
        else if (clazz.equals(Experiment.class)) {
            return service.getIterator
                (getResource (compound.getId()+"/experiments"), null);
        }
        else {
            throw new IllegalArgumentException 
                ("No related entities available for "+clazz);
        }
    }

}
