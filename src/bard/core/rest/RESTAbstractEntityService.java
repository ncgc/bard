package bard.core.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import bard.core.Assay;
import bard.core.Biology;
import bard.core.Compound;
import bard.core.DataSource;
import bard.core.Document;
import bard.core.Entity;
import bard.core.EntityNamedSources;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.EntityValue;
import bard.core.Experiment;
import bard.core.FilterParams;
import bard.core.IntValue;
import bard.core.Project;
import bard.core.SearchParams;
import bard.core.ServiceIterator;
import bard.core.ServiceParams;
import bard.core.StringDisplayValue;
import bard.core.StringValue;
import bard.core.StructureSearchParams;
import bard.core.SuggestParams;
import bard.core.Value;
import bard.util.BardHttpClient;


public abstract class RESTAbstractEntityService<E extends Entity>
        implements EntityService<E> {
    private static final long serialVersionUID = 0xe6685070fd7a917el;

    static final Logger logger = Logger.getLogger
            (RESTAbstractEntityService.class.getName());

    static final int DEFAULT_BUFSIZ = 200;
    static final int DEFAULT_TIMEOUT = 2000; // 2s

    long totalSize;
    
    
    
    /*
     * TODO: This class looks too close to JDOAbstractEntityService!
     * A refactor is needed!
     */
    protected class RESTServiceIterator
        implements ServiceIterator<E>, Runnable {

        BlockingQueue buffer;
        int bufferSize;

        AtomicLong consumed = new AtomicLong();
        long count = -1;
        final Lock lock = new ReentrantLock ();
        final Condition countSet = lock.newCondition();        

        ServiceParams params;
        String resource;
        Class<? extends Entity> clazz;
        Future thread;
        List<Value> facets = new ArrayList<Value>();

        volatile String etagId;
        volatile E next;
        volatile Object done;

        RESTServiceIterator(Class<? extends Entity> clazz,
                            String resource, ServiceParams params) {
            this(DEFAULT_BUFSIZ, clazz, resource, params);
        }

        RESTServiceIterator(int bufferSize, Class<? extends Entity> clazz,
                            String resource, ServiceParams params) {
            this.bufferSize = bufferSize;
            this.buffer = new ArrayBlockingQueue (bufferSize);
            this.params = params;
            this.clazz = clazz;
            this.resource = resource;
            try {
                this.done = clazz.newInstance();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            thread = threadPool.submit(this);
        }

        public void run() {
            //logger.info("** "+Thread.currentThread()+": resource="+resource+" params="+params);

            long top = bufferSize;
            long skip = 0;
            List<String> etags = new ArrayList<String> ();
            try {
                if (params != null) {
                    if (params.getSkip() != null) {
                        skip = params.getSkip();
                    }

                    if (params.getTop() != null) {
                        top = params.getTop();
                    }

                    List<E> entities = get
                        (resource, true, top, skip, etags, facets);

                    if (etagId == null && !etags.isEmpty()) {
                        // for now just assume there is only one etag returned
                        etagId = etags.iterator().next();
                    }

                    lock.lock();
                    try {
                        Integer cnt = getHitCount (facets);
                        if (cnt != null) {
                            count = cnt;
                        }
                        else {
                            count = entities.size();
                        }
                        countSet.signal();
                    }
                    finally {
                        lock.unlock();
                    }

                    for (E e : entities) {
                        buffer.put(e);
                    }
                } 
                else {
                    // unbounded fetching with geometric progression
                    int a = 5;
                    top = a*a;
                    int ratio = a;
                    while (true) {
                        facets.clear();
                        List<E> entities = get
                            (resource, true, top, skip, etags, facets);
                        
                        if (etagId == null && !etags.isEmpty()) {
                            // for now just assume there is only one 
                            //  etag returned
                            etagId = etags.iterator().next();
                        }
                        
                        Integer cnt = getHitCount (facets);

                        for (E e : entities) {
                            buffer.put(e);
                            
                            if (Thread.currentThread().isInterrupted()) {
                                buffer.put(done);
                                return;
                            }
                        }

                        lock.lock();
                        try {
                            if (cnt != null) {
                                count = cnt;
                            }
                            else {
                                if (count < 0) 
                                    count = 0;
                                count += entities.size();
                            }
                            countSet.signal();
                        }
                        finally {
                            lock.unlock();
                        }

                        if (entities.size() < top) {
                            break;
                        }

                        skip += entities.size();
                        // geometric progression cap out at DEFAULT_BUFSIZ
                        if (skip > 150) {
                            top = 100;
                        }
                        else {
                            ratio *= a;
                            top = ratio; 
                        }
                    }
                }
                buffer.put(done);
            } 
            catch (InterruptedException ex) {
                logger.info("** " + Thread.currentThread() + " interrupted!");
                buffer.offer(done);
            }
        }

        Integer getHitCount(List<Value> facets) {
            Value hv = null;
            for (Value v : facets) {
                if (v.getId().equals("_HitCount_")) {
                    hv = v;
                    break;
                }
            }

            Integer cnt = null;
            if (hv != null) {
                facets.remove(hv);
                cnt = (Integer) hv.getValue();
            }
            return cnt;
        }

        public E next () {
            consumed.incrementAndGet();
            E e = next;
            next = null;
            return e;
        }

        public List<E> next (int size) {
            List<E> page = new ArrayList<E>();
            /*
            int s = Math.min(size, buffer.size());
            buffer.drainTo(page, s);
            if (s < size) {
                for (; hasNext() && s < size; ++s) {
                    page.add(next());
                }
            }
            */
            for (int i = 0; hasNext () && i < size; ++i) {
                page.add(next ());
            }

            return page;
        }

        public Collection<Value> getFacets () {
            return facets;
        }

        public boolean hasNext() {
            if (next != null) return true;

            try {
                Object obj = buffer.take();
                next = obj == done ? null : (E) obj;
            } 
            catch (InterruptedException ex) {
                /*
                logger.info("** " + Thread.currentThread().getName()
                        + " interrupted!");
                */
                next = null;
            }

            return next != null;
        }

        public Value getETag () { 
            if (etagId == null)
        	return null;
            return RESTAbstractEntityService.this.getETag(etagId);
        }
        
        public void done() {
            thread.cancel(true);
        }

        public long getConsumedCount() {
            return consumed.get();
        }

        public long getCount() {
            lock.lock();
            try {
                while (count < 0)
                    countSet.await();
                return count;
            }
            catch (InterruptedException ex) {
                return buffer.size();
            }
            finally {
                lock.unlock();
            }
        }

        public void remove() {
            throw new UnsupportedOperationException("Method not supported");
        }
    }

    class ETagIterator implements ServiceIterator<Value>, Runnable {
        BlockingQueue<Value> buffer = new ArrayBlockingQueue<Value> (100);

        AtomicLong consumed = new AtomicLong ();
        long count = -1;
        final Lock lock = new ReentrantLock ();
        final Condition countSet = lock.newCondition();        

        Principal principal;
        volatile Value next;

        Value done = new Value (getDataSource ());
        Future thread;

        ETagIterator (Principal principal) {
            this.principal = principal;
            thread = threadPool.submit(this);        
        }

        public void run () {
            int a = 5;
            int top = a*a;
            int ratio = a;
            int skip = 0;
            try {
                while (true) {
                    lock.lock();
                    try {
                        if (count < 0) {
                            count = 0;
                        }

                        List<Value> etags = getETags (top, skip, principal);
                        for (Value e : etags) {
                            buffer.put(e);
                            ++count;

                            if (Thread.currentThread().isInterrupted()) {
                                buffer.put(done);
                                return;
                            }
                        }
                        countSet.signal();
                        
                        if (etags.size() < top) {
                            break;
                        }
                    
                        skip += etags.size();
                    }
                    finally {
                        lock.unlock();
                    }
                    
                    // geometric progression cap out at DEFAULT_BUFSIZ
                    if (skip > 1000) {
                        top = 2000;
                    }
                    else {
                        ratio *= a;
                        top = ratio; 
                    }
                }
                buffer.put(done);
            } 
            catch (InterruptedException ex) {
                buffer.offer(done);
            }
        }

        public Value next () {
            consumed.incrementAndGet();
            Value v = next;
            next = null;
            return v;
        }

        public List<Value> next (int size) {
            List<Value> page = new ArrayList<Value>();
            /*
            int s = Math.min(size, buffer.size());
            buffer.drainTo(page, s);
            if (s < size) {
                for (; hasNext() && s < size; ++s) {
                    page.add(next());
                }
            }
            */

            for (int i = 0; i < size && hasNext (); ++i) {
                page.add(next ());
            }

            return page;
        }

        public boolean hasNext() {
            if (next != null) return true;

            try {
                Value obj = buffer.take();
                next = obj == done ? null : obj;
            } 
            catch (InterruptedException ex) {
                logger.info("** " + Thread.currentThread().getName()
                        + " interrupted!");
                next = null;
            }

            return next != null;
        }

        public Value getETag () { return null; }
        public List<Value> getFacets () { return null; }

        public void done() {
            thread.cancel(true);
        }

        public long getConsumedCount() {
            return consumed.get();
        }

        public long getCount () {
            lock.lock();
            try {
                while (count < 0)
                    countSet.await();
                return count;
            }
            catch (InterruptedException ex) {
                return buffer.size();
            }
            finally {
                lock.unlock();
            }
        }

        public void remove() {
            throw new UnsupportedOperationException("Method not supported");
        }
    }

    protected final String baseURL;
    protected EntityServiceManager srvman;
    protected ExecutorService threadPool;
    protected Ehcache cache;

    // base URL; should have no trailing slash!
    protected RESTAbstractEntityService
        (RESTEntityServiceManager srvman, String baseURL) {
        this.srvman = srvman;
        this.baseURL = baseURL;
        this.threadPool = srvman.getThreadPool();
        cache = srvman.cachingService.getCache(this.getClass().toString());
    }

    public String getBaseURL() {
        return baseURL;
    }

    // relative to the base URL
    public abstract String getResourceContext();

    // construct an entity from a json response
    protected abstract E getEntity(E e, JsonNode node);

//    // construct an entity from a json search response
//    protected abstract E getEntitySearch(E e, JsonNode node);

    protected int parseFacets(List<Value> facets, JsonNode node, List<E> values) {
        int nhits = -1;

        JsonNode meta = node.get("metaData");
        if (meta != null) {
            JsonNode n = meta.get("nhit");
            if (n != null) {
                nhits = n.asInt();
            }

            DataSource ds = getDataSource(EntityNamedSources.FacetSource);
            n = meta.get("facets");
            if (n != null) {
                ArrayNode array = (ArrayNode) n;
                for (int i = 0; i < array.size(); ++i) {
                    facets.add(parseFacet(ds, array.get(i)));
                }
            }
            
            n = meta.get("matchingFields");
            if (n != null && !n.isNull()) {
        	Iterator<String> it = n.getFieldNames();
        	while (it.hasNext()) {
        	    String entName = it.next();
        	    if (entName == null)
        	        logger.severe("whao!");
        	    JsonNode child = n.get(entName);
        	    Iterator<String> it2 = child.getFieldNames();
        	    while (it2.hasNext()) {
        		String fieldName = it2.next();
        		if (child.get(fieldName) != null && !child.get(fieldName).isNull()) {
        		    for (E ent: values) {
        		        if (ent.getId() == null)
        		            logger.severe("whao!!!");
        		        if (entName.equals(ent.getId().toString())) {
        		            ent.add(new StringValue(ds, fieldName, child.get(fieldName).asText()));
        		            ent.add(new StringValue(ds, Entity.SearchHighlightField, fieldName));
        		        }
        		    }
        		}
        	    }
        	}
            }
        }

        return nhits;
    }

    protected Value parseFacet(DataSource ds, JsonNode n) {
        String id = n.get("facetName").asText();
        Value facet = new Value(ds, id);

        n = n.get("counts");
        for (Iterator<Map.Entry<String, JsonNode>>
                     iter = n.getFields(); iter.hasNext(); ) {
            Map.Entry<String, JsonNode> me = iter.next();
            int count = me.getValue().asInt();
            if (count > 0) {
                // doesn't make sense to include facets that have zero counts
                new IntValue(facet, me.getKey(), count);
            }
        }

        return facet;
    }


    protected E getEntity(JsonNode node) {
        return getEntity(null, node);
    }

//    protected E getEntitySearch(JsonNode node) {
//        return getEntitySearch(null, node);
//    }

    protected String getResource(Object resource) {
        return baseURL + getResourceContext() + "/" + resource;
    }

    protected String getResource() {
        return baseURL + getResourceContext() + "/";
    }

    protected String getStructureSearchResource(StructureSearchParams params) {
        StringBuilder url = new StringBuilder ();
        try {
            url.append(getResource()+"?filter="
                       +URLEncoder.encode(params.getQuery(), "utf-8")
                       +"[structure]");
        }
        catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            throw new IllegalArgumentException
                ("Bogus query: "+params.getQuery());
        }
        
        switch (params.getType()) {
        case Substructure: url.append("&type=sub"); break;
        case Superstructure: url.append("&type=sup"); break;
        case Exact: url.append("&type=exact"); break;
        case Similarity: 
            url.append("&type=sim"); 
            if (params.getThreshold() != null) {
                url.append("&cutoff="+String.format
                           ("%1$.3f", params.getThreshold()));
            }
            else {
                throw new IllegalArgumentException
                    ("No threshold specified for similarity search!");
            }
            break;
        }
        url.append("&expand=true");

        return url.toString();
    }
    
    protected String getSearchResource(SearchParams params) {
        try {
            StringBuilder f = new StringBuilder("");
            if (params.getFilters() != null 
                && params.getFilters().size() > 0) {
                f.append("&filter=");
                String sep = "";
                for (String[] entry : params.getFilters()) {
                    f.append(sep).
                        append("fq(")
                        .append(URLEncoder.encode(entry[0], "utf-8")).
                        append(":")
                        .append(URLEncoder.encode(entry[1], "utf-8"))
                        .append(")");
                    sep = ",";
                }
            }
            //f.append(",");
            return baseURL + "/search" + getResourceContext() + "/?q="
                    + URLEncoder.encode(params.getQuery(), "utf-8") 
                + f.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    protected ExecutorService getThreadPool() {
        return threadPool;
    }

    /*
     * EntityService interface
     */
    public EntityServiceManager getServiceManager() {
        return srvman;
    }

    public boolean isReadOnly() {
        return false;
    }

    public DataSource getDataSource() {
        return DataSource.getCurrent();
    }

    public DataSource getDataSource(String name, String version) {
        return new DataSource(name, version);
    }

    public DataSource getDataSource(String name) {
        return new DataSource(name);
    }

    protected Collection<E> getMultiple (String uri) {
	BardHttpClient httpclient = new BardHttpClient("get1b");
	
	try {
	    HttpGet get = new HttpGet(uri);
	    HttpResponse response = httpclient.execute(get);

	    HttpEntity entity = response.getEntity();
	    if (entity != null && response.getStatusLine()
		    .getStatusCode() == 200) {
		InputStream is = entity.getContent();
		try {
		    ArrayList<E> list = new ArrayList<E>();
		    ObjectMapper mapper = new ObjectMapper();
		    JsonNode node = mapper.readTree(is);
		    if (node.isArray()) {
			ArrayNode array = (ArrayNode) node;
			for (int i = 0; i < array.size(); ++i) {
			    node = array.get(i);
			    list.add(getEntity(null, node));
			}
		    }
                  
		    return list;
		} catch (Exception ex) {
		    ex.printStackTrace();
		} finally {
		    is.close();
		}
	    }
	} catch (IOException ex) {
	    ex.printStackTrace();
	} finally {
	    httpclient.close();
	}
	return null;
    }
    
    protected E get (E ent, Object id) {
	BardHttpClient httpclient = new BardHttpClient("get1");
//        DefaultHttpClient httpclient = new DefaultHttpClient();

        try {
            HttpGet get = new HttpGet(getResource(id)+"?expand=true");
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is);
                    if (node.isArray()) {
                        ArrayNode array = (ArrayNode) node;
                        if (array.size() > 1 || array.size() == 0)
                            logger.severe("Expected single entity in response, got "+array.size()+" entities.");
                        for (int i = 0; i < array.size(); ++i) {
                            node = array.get(i);
                        }
                    }
                    
                    return getEntity((E)ent, node);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    is.close();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
//            httpclient.getConnectionManager().shutdown();
            httpclient.close();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public E get (Object id) {
        Element el = cache.get(id);
        if (el != null) {
            return (E)el.getObjectValue();
        }
        E ent = get (null, id);
        cache.put(new Element(id, ent));
        return ent;
    }
    
    @SuppressWarnings("unchecked")
    public E update (Entity ent) {
	ent = get ((E)ent, ent.getId());
	cache.put(new Element(ent.getId(), ent));
	return (E)ent;
    }
    
    public E getEntityFromJsonNode(JsonNode entry, Serializable id) { 
        Element el = cache.get(id);
        if (el != null) {
            return (E)el.getObjectValue();
        }
        E ent = getEntity(null, entry); 
        cache.put(new Element(id, ent));
        return ent;
    }
    
    protected Entity addEntityValues(Entity entity, DataSource ds, JsonNode node, Class<? extends Entity> clazz, String IdFieldName) {
	if (node != null && node.isArray()) {
	    ArrayNode entries = (ArrayNode)node;
	    for (int i=0; i<entries.size(); i++) {
		JsonNode entry = entries.get(i);
		EntityValue entValue = new EntityValue(ds, clazz.toString());
		JsonNode value = entry;
	        if (!entry.isInt() && !entry.isTextual()) {
	            value = entry.get(IdFieldName);
	            if (value == null)
	        	System.err.println("Yoho!");
	            String id = value.asText();
	            // parse entity and add to cache
	            if (value != null && entry.isObject()) {
	        	if (clazz == Document.class) {
	        	    Document t = ((RESTDocumentService)getServiceManager().getService(clazz)).getEntityFromJsonNode(entry, id);
	        	} else if (clazz == Biology.class) {
	        	    Biology t = ((RESTBiologyService)getServiceManager().getService(clazz)).getEntityFromJsonNode(entry, id);
	        	} else if (clazz == Assay.class) {
	        	    Assay t = ((RESTAssayService)getServiceManager().getService(clazz)).getEntityFromJsonNode(entry, id);
	        	} else if (clazz == Project.class) {
	        	    Project t = ((RESTProjectService)getServiceManager().getService(clazz)).getEntityFromJsonNode(entry, id);
	        	} else if (clazz == Experiment.class) {
	        	    Experiment t = ((RESTExperimentService)getServiceManager().getService(clazz)).getEntityFromJsonNode(entry, id);
	        	} else if (clazz == Compound.class) {
	        	    Compound t = ((RESTCompoundService)getServiceManager().getService(clazz)).getEntityFromJsonNode(entry, id);
	        	}
	            }
	        }
	        if (value.isInt()) {
	            entValue.setValue(value.asInt());
	        } else if (value.isTextual()) {
	            entValue.setValue(value.asText());
	        }
	        if (value != null && !value.isNull())
	            entity.add(entValue);
	    }
	}
	return entity;
    }

    public Collection<Value> getAnnotations (Entity entity) {
	BardHttpClient httpclient = new BardHttpClient("getAnnot");
        try {
            HttpGet get = new HttpGet(getResource(entity.getId())+"/annotations?expand=true");
            HttpResponse response = httpclient.execute(get);

            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = httpEntity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is);
                    return getAnnotations(entity, node);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    is.close();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            httpclient.close();
        }
        return null;
    }

    protected Collection<Value> getAnnotations(Entity entity, JsonNode node) {
        // pull in entity annotations - process different source with different DataSource's
        List<Value> annotations = new ArrayList<Value>();
        if (node != null && !node.isNull()) {
            String[] sections = {"contexts", "measures", "docs", "misc"};
            for (String section: sections) {
                JsonNode n = node.get(section);
                if (n != null && !n.isNull()) {
                    for (JsonNode context: (ArrayNode)n) {
                        String contextId = context.get("id").asText();
                        String contextName = "No name given";
                        if (context.get("name") != null)
                            contextName = context.get("name").asText();
                        Value contextValue = null;
                        if (context.get("comps") == null || context.get("comps").isNull()) {
                            // misc section nows has slightly different formatting, *sigh*
                            contextValue = getContextValue(context, contextValue, section, contextName);
                        } else {
                            for (JsonNode ci: (ArrayNode)context.get("comps")) {
                                contextValue = getContextValue(ci, contextValue, section, contextName);
                            }
                        }
                        if (contextValue != null) {
                            entity.add(contextValue);
                            annotations.add(contextValue);
                        }
                    }
                }
            }
        }
	return annotations;
    }

    HashMap<String, DataSource> datasources = new HashMap<String, DataSource>();
    {
        datasources.put("cap-context", getDataSource(EntityNamedSources.CAPAnnotationSource));
        datasources.put("cap-measure", getDataSource(EntityNamedSources.CAPAnnotationSource));
        datasources.put("cap-doc", getDataSource(EntityNamedSources.CAPAnnotationSource));
        datasources.put("GO", getDataSource(EntityNamedSources.GOMFAnnotationSource));
        datasources.put("KEGG", getDataSource(EntityNamedSources.KEGGDiseaseNameAnnotationSource));
        datasources.put("pubchem", getDataSource(EntityNamedSources.PubChemSynonymSource));
    }

    protected Value getContextValue (JsonNode ci, Value contextValue, String section, String contextName) {
        /* "entityId": null,
        "entity": "assay",
        "source": "cap-context",
        "id": 55526,
        "display": "Pyruvate kinase isozymes M1/M2",
        "contextRef": "assay kit name",
        "key": "protein",
        "value": null,
        "extValueId": null,
        "url": null,
        "displayOrder": 2,
        "related": null     */
        String id = ci.get("key").asText();
        String value = ci.get("value").asText();
        String extValue = ci.get("extValueId").asText();
        if ((value == null || "null".equals(value)) && extValue != null && !"null".equals(extValue))
            value = extValue;
        String display = ci.get("display").asText();
        if ((value == null || "null".equals(value)) && display != null && !"null".equals(display))
            value = display;
        if (contextValue == null) {
            DataSource dataSource;
            String source = ci.get("source").asText();
            if (datasources.containsKey(source)) dataSource = datasources.get(source);
            else dataSource = new DataSource(source);
            contextValue = new StringValue(dataSource, section, contextName);
        }
        Value itemValue = new StringDisplayValue(contextValue, id, value, display);
        String url = ci.get("url").asText();
        if (url != null && !"null".equals(url))
            itemValue.setURL(url);
        return contextValue;
    }

    
    public Collection<E> get (Collection ids) {
	BardHttpClient httpclient = new BardHttpClient("get2");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<E> results = new ArrayList<E>();
        try {
            List<NameValuePair> nvp = new ArrayList<NameValuePair>();
            nvp.add(new BasicNameValuePair("ids", toString(ids)));

            HttpPost post = new HttpPost(getResource() + "?expand=true");
            post.setEntity(new UrlEncodedFormEntity(nvp, HTTP.UTF_8));

            HttpResponse response = httpclient.execute(post);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readTree(is);
                    if (node == null) {
                    } else if (node.isArray()) {
                        ArrayNode array = (ArrayNode) node;
                        for (int i = 0; i < array.size(); ++i) {
                            JsonNode n = array.get(i);
                            if (n != null) {
                                E e = getEntity(null, n);
                                if (e != null) {
                                    results.add(e);
                                }
                            }
                        }
                    } else {
                        // single element
                        E e = getEntity(null, node);
                        if (e != null) {
                            results.add(e);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    is.close();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
//            httpclient.getConnectionManager().shutdown();
            httpclient.close();
        }
        return results;
    }

    public Value getETag (Value etag) {
	return getETag(getETagId(etag));
    }
	
    public Value getETag (String etagId) {
	if (etagId == null)
	    return null;

	BardHttpClient httpclient = new BardHttpClient("getSingleETag");
	//    DefaultHttpClient httpclient = new DefaultHttpClient();

	try {
	    String url = baseURL + "/etags/" + etagId + "/meta";
	    //logger.info("## url="+url);

	    HttpGet get = new HttpGet (url);
	    HttpResponse response = httpclient.execute(get);

	    HttpEntity entity = response.getEntity();
	    if (entity != null && response.getStatusLine()
		    .getStatusCode() == 200) {
		InputStream is = entity.getContent();
		try {
		    ObjectMapper mapper = new ObjectMapper();
		    JsonNode root = mapper.readTree(is);
		    if (root != null) {
			Value v = parseETag (root);
			if (v != null) {
			    return v;
			}
		    }
		} 
		catch (Exception ex) {
		    ex.printStackTrace();
		}
		finally {
		    is.close();
		}
	    }
	} 
	catch (IOException ex) {
	    ex.printStackTrace();
	} 
	finally {
	    //        httpclient.getConnectionManager().shutdown();
	    httpclient.close();
	}
	return null;
    }

    protected List<Value> getETags 
        (long top, long skip, Principal principal) {

	BardHttpClient httpclient = new BardHttpClient("get3");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<Value> etags = new ArrayList<Value>();

        try {
            String url = getResource()+ "etag?skip=" + skip 
                + "&top=" + top + "&expand=true";
            //logger.info("## url="+url);

            HttpGet get = new HttpGet (url);
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode root = mapper.readTree(is);
                    ArrayNode node = (ArrayNode)root.get("collection");
                    if (node != null) {
                        for (int i = 0; i < node.size(); ++i) {
                            Value v = parseETag (node.get(i));
                            if (v != null) {
                                etags.add(v);
                            }
                        }
                    }
                } 
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    is.close();
                }
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
//            httpclient.getConnectionManager().shutdown();
            httpclient.close();
        }
        return etags;
    }

    protected Value parseETag (JsonNode node) {
        JsonNode n = node.get("etag");
        if (n == null) n = node.get("etag_id");

        Value etag = null;
        DataSource ds = getDataSource ();
        if (n != null && !n.isNull()) {
            etag = new Value (ds, n.asText());
        }
        else {
            return null;
        }

        n = node.get("name");
        if (n != null && !n.isNull()) {
            new StringValue (etag, "name", n.asText());
        }

        n = node.get("type");
        if (n != null && !n.isNull()) {
            new StringValue (etag, "type", n.asText());
        }

        n = node.get("count");
        if (n != null && !n.isNull()) {
            new IntValue (etag, "count", n.asInt());
        }

        n = node.get("url");
        if (n != null && !n.isNull()) {
            new StringValue (etag, "url", n.asText());
        }

        n = node.get("linkedTags");
        if (n != null && !n.isNull()) {
            for (JsonNode link: (ArrayNode)n) {
                Value child = parseETag(link);
                new EntityValue (etag, "child", child);
            }
            //new StringValue (etag, "description", n.asText());
        }

        return etag;
    }

    protected List<E> get(String resource, boolean expand,
                          long top, long skip) {
        return get(resource, expand, top, skip, null, null);
    }

    protected List<E> get (String resource, boolean expand,
                           long top, long skip,
                           List<String> etags, List<Value> facets) {

	BardHttpClient httpclient = new BardHttpClient("get4");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<E> values = new ArrayList<E>();
        try {
            String url = resource + (resource.indexOf("?") < 0 ? "?" :"&") 
                + "skip=" + skip + "&top=" + top
                + (expand ? "&expand=true" : "");

            long start = System.currentTimeMillis();
            logger.info(">> "+getClass()+" "+Thread.currentThread().getName()+" fetching "+url);

            HttpGet get = new HttpGet(url);
            HttpResponse response = httpclient.execute(get);
            if (response.containsHeader("ETag") || response.containsHeader("Etag")) {
                //System.out.print("** ETag:");
                for (Header h : response.getHeaders("ETag")) {
                    String e = h.getValue();
                    String etag = e.substring(1, e.length() - 1);
                    if (etags.indexOf(etag) < 0) {
                        etags.add(etag);
                    }
                    //System.out.print(" "+etag);
                }
                for (Header h : response.getHeaders("Etag")) {
                    String e = h.getValue();
                    String etag = e.substring(1, e.length() - 1);
                    if (etags.indexOf(etag) < 0) {
                        etags.add(etag);
                    }
                    //System.out.print(" "+etag);
                }
                //System.out.println();
            }

            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode root = mapper.readTree(is);
                    if (root.isArray() && ((ArrayNode)root).size() > 0 && ((ArrayNode)root).get(0).get("etag") != null) { // catch composite etags here
                        if (etags.size() == 0) {
                            String etagString = resource.substring(resource.indexOf("/etags/")+7, resource.length()-1);
                            etags.add(etagString);
                        }
                        for (int i=0; i< ((ArrayNode)root).size(); ++i) {
                            JsonNode etag = ((ArrayNode)root).get(i);
                            if ("compounds".equals(etag.get("type").asText())) {
                                RESTAbstractEntityService<Compound> cmpdService = getServiceManager().getService(Compound.class);
                                ArrayNode entities = (ArrayNode)etag.get("entities");
                                for (int j=0; j<entities.size(); j++) {
                                    if (this.getEntityClass() == Compound.class)
                                        values.add(getEntity(entities.get(j)));
                                    else cmpdService.getEntity(entities.get(j));
                                }
                            } else if ("assays".equals(etag.get("type").asText())) {
                                RESTAbstractEntityService<Assay> assayService = getServiceManager().getService(Assay.class);
                                ArrayNode entities = (ArrayNode)etag.get("entities");
                                for (int j=0; j<entities.size(); j++) {
                                    if (this.getEntityClass() == Assay.class)
                                        values.add(getEntity(entities.get(j)));
                                    else assayService.getEntity(entities.get(j));
                                }
                            } else {
                                logger.severe("Composite etag parsing error:"+resource);
                            }
                        }
                    } else if (root.isArray()) {
                        ArrayNode node = (ArrayNode)root;
                        for (int i = 0; i < node.size(); ++i) {
                            JsonNode n = node.get(i);
                            values.add(getEntity(n));
                        }
                    }
                    else {
                        ArrayNode node = (ArrayNode) root.get("collection");
                        if (node != null) {
                            for (int i = 0; i < node.size(); ++i) {
                                JsonNode n = node.get(i);
                                values.add(getEntity(n));
                            }
                        } 
                        else { // now try "docs" for search
                            node = (ArrayNode) root.get("docs");
                            for (int i = 0; i < node.size(); ++i) {
                                JsonNode n = node.get(i);
                                values.add(getEntity(n));
                            }
                            
//                            // process metadata
//                            JsonNode n;
//                            DataSource ds = getDataSource ();
//                            n = node.get("highlight");
//                            if (n != null) {
//                        	assay.add(new StringValue 
//                        		(ds, Entity.SearchHighlightValue, n.asText()));
//                            }

                            // parse facets if any
                            if (facets != null) {
                                int count = parseFacets(facets, root, values);
                                // internal value representing hit count
                                facets.add(new IntValue
                                           (getDataSource(), "_HitCount_",
                                            count));
                            }
                        }
                    }
                } 
                catch (Exception ex) {
                    ex.printStackTrace();
                } 
                finally {
                    is.close();
                }
            }
            logger.info("<< "+Thread.currentThread().getName()
                        +" finishes fetching "+url+"..."
                        +String.format("%1$.3fs", 1e-3*(System.currentTimeMillis()-start)));
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
//            httpclient.getConnectionManager().shutdown();
            httpclient.close();
        }
        return values;
    }

    public List<E> get(long top, long skip) {
        return get(getResource(), true, top, skip);
    }

    public List<E> get(long top, long skip, String ordering) {
        return get(top, skip); // ordering is not currently supported
    }

    public Value newETag(String name) {
        return newETag (name, null);
    }

    public Value newETag (String name, Collection ids) {
	BardHttpClient httpclient = new BardHttpClient("newETag");
//	DefaultHttpClient httpclient = new DefaultHttpClient();
        try {
            List<NameValuePair> nvp = new ArrayList<NameValuePair>();
            nvp.add(new BasicNameValuePair("name", name));
            if (ids != null) {
                nvp.add(new BasicNameValuePair("ids", toString(ids)));
            }

            HttpPost post = new HttpPost(getResource("etag"));
            post.setEntity(new UrlEncodedFormEntity(nvp, HTTP.UTF_8));

            HttpResponse response = httpclient.execute(post);
            if (response.containsHeader("ETag")) {
                // there should only be one ETag returned
                Value etag = null;
                for (Header h : response.getHeaders("ETag")) {
                    String e = h.getValue();
                    // remove quotes
                    e = e.substring(1, e.length() - 1);
                    etag = this.getETag(e);
                    return etag;
                }
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return null;
    }

    protected String toString(Collection ids) {
        StringBuilder sb = new StringBuilder();
        for (Iterator iter = ids.iterator(); iter.hasNext(); ) {
            Object id = iter.next();
            if (id instanceof Entity) {
                sb.append(((Entity)id).getId());
            }
            else {
                sb.append(id.toString());
            }
            if (iter.hasNext()) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    public Value newCompositeETag (String name, Collection ids) {
        BardHttpClient httpclient = new BardHttpClient("newCompositeETag");
//      DefaultHttpClient httpclient = new DefaultHttpClient();
        try {
            List<NameValuePair> nvp = new ArrayList<NameValuePair>();
            nvp.add(new BasicNameValuePair("name", name));
            if (ids != null) {
                nvp.add(new BasicNameValuePair("etagids", toString(ids)));
            }

            HttpPost post = new HttpPost(baseURL + "/etags/etag");
            post.setEntity(new UrlEncodedFormEntity(nvp, HTTP.UTF_8));

            HttpResponse response = httpclient.execute(post);
            if (response.containsHeader("ETag")) {
                // there should only be one ETag returned
                Value etag = null;
                for (Header h : response.getHeaders("ETag")) {
                    String e = h.getValue();
                    // remove quotes
                    e = e.substring(1, e.length() - 1);
                    etag = new Value(getDataSource(), e);
                    etag.add(new StringValue(etag, "name", name));
                    return etag;
                }
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return null;
    }

    // return all known etags that this client has 
    public ServiceIterator etags (Principal principal) {
        return new ETagIterator (principal);
    }

    public int putETag (Value etag, Collection ids) {
        if (etag == null) {
            throw new IllegalArgumentException("No ETag specified!");
        }

        if (ids == null || ids.isEmpty()) {
            throw new IllegalArgumentException("Invalid collection!");
        }

	BardHttpClient httpclient = new BardHttpClient("putETag");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        int count = -1;
        try {
            List<NameValuePair> nvp = new ArrayList<NameValuePair>();
            nvp.add(new BasicNameValuePair("ids", toString(ids)));

            HttpPut put = new HttpPut 
                (getResource ("etag/" + getETagId (etag)));
            put.setEntity(new UrlEncodedFormEntity(nvp, HTTP.UTF_8));

            HttpResponse response = httpclient.execute(put);
            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    BufferedReader br = new BufferedReader
                            (new InputStreamReader(is));
                    count = Integer.parseInt(br.readLine());
                } 
                catch (Exception ex) {
                    ex.printStackTrace();
                } 
                finally {
                    is.close();
                }
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return count;
    }

    public Collection<Value> getFacets (Object etag) {
	BardHttpClient httpclient = new BardHttpClient("getFacets");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<Value> facets = new ArrayList<Value>();
        try {
            String url = getResource("etag/" + getETagId (etag) + "/facets");

            HttpGet get = new HttpGet(url);
            HttpResponse response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();
            if (entity != null && response.getStatusLine()
                    .getStatusCode() == 200) {
                InputStream is = entity.getContent();
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode root = mapper.readTree(is);
                    if (root.isArray()) {
                        DataSource ds = getDataSource(Entity.FacetSource);
                        ArrayNode array = (ArrayNode) root;
                        for (int i = 0; i < array.size(); ++i) {
                            facets.add(parseFacet(ds, array.get(i)));
                        }
                    }
                } 
                catch (Exception ex) {
                    ex.printStackTrace();
                } 
                finally {
                    is.close();
                }
            }
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }

        return facets;
    }

    protected String getETagId (Object etag) {
        if (etag == null) return null;

        if (etag instanceof Value) {
            return ((Value)etag).getId();
        }
        return etag.toString();
    }

    public void put(E... entities) {
        throw new UnsupportedOperationException("Method not supported!");
    }

    public long size() {
	if (totalSize < 1) {
	    BardHttpClient httpclient = new BardHttpClient("size");
	    //        DefaultHttpClient httpclient = new DefaultHttpClient();
	    try {
		HttpGet get = new HttpGet(getResource() + "_count");
		HttpResponse response = httpclient.execute(get);

		HttpEntity entity = response.getEntity();
		if (entity != null) {
		    InputStream is = entity.getContent();
		    try {
			BufferedReader br = new BufferedReader
				(new InputStreamReader(is));
			totalSize = Long.parseLong(br.readLine());
		    } catch (NumberFormatException ex) {
			ex.printStackTrace();
		    } finally {
			is.close();
		    }
		}
	    } catch (IOException ex) {
		ex.printStackTrace();
	    } finally {
		httpclient.close();
		//            httpclient.getConnectionManager().shutdown();
	    }
	}
        return totalSize;
    }

    protected ServiceIterator<E>
    getIterator(String resource, ServiceParams params) {
        return new RESTServiceIterator(getEntityClass(), resource, params);
    }

    /*
     * subclass needs to override this to provide specific related 
     * entities
     */
    public <T extends Entity> ServiceIterator<T> iterator 
               (E entity, Class<T> clazz) {
        return null;
    }

    public ServiceIterator<E> iterator() {
        return new RESTServiceIterator
                (getEntityClass(), getResource(), null);
    }

    public ServiceIterator<E> iterator (Object etag) {
        if (etag instanceof Value) {
            Value composite = (Value)etag;
            if (composite.getChild("type") != null && composite.getChild("type").getValue().toString().endsWith("ETag")) {
                String resource = baseURL + "/etags/"+getETagId (etag)+"/";
                return new RESTServiceIterator
                        (getEntityClass (), resource, null);
            }
        }
        return new RESTServiceIterator
            (getEntityClass (), getResource ("etag/"+getETagId (etag)), null);
    }

    public ServiceIterator<E> iterator(ServiceParams params) {
        return getIterator(getResource(), params);
    }

    public ServiceIterator<E> filter(FilterParams params) {
        throw new UnsupportedOperationException
                ("Method currently not supported");
    }

    public ServiceIterator<E> search(SearchParams params) {
	String resource = getSearchResource(params);
	if (params instanceof StructureSearchParams)
	    resource = getStructureSearchResource((StructureSearchParams)params);
        return new RESTServiceIterator
                (getEntityClass(), resource, params);
    }

    public void shutdown() {
    }

    public Map<String, List<String>> suggest(SuggestParams params) {
        String SEARCH_RESOURCE = baseURL + "/search" + getResourceContext() + "/suggest";
	BardHttpClient httpclient = new BardHttpClient("suggest");
//        DefaultHttpClient httpclient = new DefaultHttpClient();
        Map<String, List<String>> suggestions = new HashMap<String, List<String>>();
        try {
            HttpGet get = new HttpGet(SEARCH_RESOURCE + "?q=" + URLEncoder.encode(params.getQuery(), "utf-8") + "&top=" + params.getNumSuggestion());
            HttpResponse response = httpclient.execute(get);
            HttpEntity entity = response.getEntity();

            if (entity != null && response.getStatusLine().getStatusCode() == 200) {
                InputStream is = entity.getContent();
                ObjectMapper mapper = new ObjectMapper();

                JsonNode root = mapper.readTree(is);
                Iterator<String> fiter = root.getFieldNames();
                while (fiter.hasNext()) {
                    String fieldName = fiter.next();
                    if (fieldName.equals("query")) continue;

                    JsonNode node = root.get(fieldName);
                    List<String> values = new ArrayList<String>();
                    for (int i = 0; i < node.size(); i++) values.add(node.get(i).asText());
                    suggestions.put(fieldName, values);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            httpclient.close();
//            httpclient.getConnectionManager().shutdown();
        }
        return suggestions;
    }
}
