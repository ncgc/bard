package bard.core.rest;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.JsonNode;

import bard.core.*;
import bard.core.impl.*;


public class RESTSubstanceService extends RESTAbstractEntityService<Substance> 
    implements SubstanceService, SubstanceValues {
    static final private Logger logger = 
        Logger.getLogger(RESTSubstanceService.class.getName());

    protected RESTSubstanceService 
        (RESTEntityServiceManager srvman, String baseURL) {
        super (srvman, baseURL);
    }

    public Class<Substance> getEntityClass () { return Substance.class; }
    public String getResourceContext () { return "/substances"; }

    protected Substance getEntity (Substance s, JsonNode node) {
        if (s == null) {
            s = new Substance ();
        }
        return s;
    }

    protected Substance getEntitySearch (Substance s, JsonNode node) {
        return getEntity (s, node);
    }
}
