package bard.core;

import javax.jdo.annotations.PersistenceCapable;
import java.util.Collection;
import java.util.Map;

@PersistenceCapable(detachable="true")
public class Compound extends Entity implements CompoundValues {
    private static final long serialVersionUID = 0xf59548a25c44645cl;

    protected Collection<Value> synonyms;
    protected Collection<Value> substances;
    protected Collection<Value> annotations;

    protected Map<String, Object> summary;
    
    public Compound () {}
    public Compound (CompoundService cmpdService) { this.entityService = cmpdService; }
    public Compound (CompoundService cmpdService, String name) {
        super (name);
        this.entityService = cmpdService;
    }

    public Collection<Value> getSynonyms () { 
	if (synonyms == null && entityService != null) {
	    synonyms = ((CompoundService)entityService).getSynonyms(this);
	}
	return synonyms; 
    }
    
    public Collection<Value> getSubstances () { 
	if (substances == null && entityService != null) {
	    substances = ((CompoundService)entityService).getSubstances(this);
	}
	return substances; 
    }

    public Map<String, Object> getSummary() {
        if (summary == null && entityService != null) {
            summary = ((CompoundService) entityService).getSummary(this);
        }
        return this.summary;
    }
    
    public static boolean lazyLoad(String id) {
	return false;
    }
}
