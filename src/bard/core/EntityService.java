package bard.core;

import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.io.Serializable;
import java.security.Principal;


public interface EntityService<E extends Entity> extends Serializable {
    /*
     * Service metadata
     */
    Class<E> getEntityClass ();
    EntityServiceManager getServiceManager ();

    // is this service read-only? if so all write operations (e.g., put, ETag)
    //  will throw UnsupportedOperationException
    boolean isReadOnly (); 

    long size ();

    /*
     * Return a unique data source based on name and version
     */
    DataSource getDataSource (); // return default data source
    DataSource getDataSource (String name);
    DataSource getDataSource (String name, String version);

    /*
     * The distinction between get() and iterator is that get() is meant
     * for small data retrieval. As such, each entity returned by get() is
     * fully constructed whereas that's not necessary the case for 
     * iterator().
     */
    E get (Object id);
    E update (Entity entity);
    Collection<Value> getAnnotations(Entity entity);
    Collection<E> get (Collection<?> ids);
    List<E> get (long top, long skip);
    // format for ordering is FIELD [DESC|ASC]
    List<E> get (long top, long skip, String ordering);

    /*
     * 
     */
    void put (E... entities);
    //void remove (E... entities);


    /*
     * ETag's
     */
    Value newETag (String name);
    Value newETag (String name, Collection<?> ids);
    Value newCompositeETag (String name, Collection ids);
    int putETag (Value etag, Collection<?> ids);

    // return facets based on given etag
    Collection<Value> getFacets (Object etag);
    
    // return meta data about an etag
    Value getETag(Value etag);
    Value getETag(String etagId);
    
    // return all known etags that are accessible by the 
    //  given principal
    ServiceIterator<Value> etags (Principal principal);

    /*
     * retrieval of related entities
     */
    <T extends Entity> ServiceIterator<T> iterator (E entity, Class<T> clazz);

    /*
     * Various entity retrieval methods
     */
    ServiceIterator<E> iterator ();
    ServiceIterator<E> iterator (Object etag);
    ServiceIterator<E> iterator (ServiceParams params);

    /*
     * Search & filtering
     */
    ServiceIterator<E> filter (FilterParams params);
    ServiceIterator<E> search (SearchParams params);

    // suggestion
    Map<String, List<String>> suggest (SuggestParams params);

    /*
     * clean up
     */
    void shutdown ();
}
