package bard.core;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Collection;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.NotPersistent;

@PersistenceCapable(detachable="true")
public class Entity implements EntityValues, 
                               EntityNamedSources, 
                               java.io.Serializable {
    private static final long serialVersionUID = 0xed49f700e8675bd0l;

    @NotPersistent
    protected Object id; // transient entity id

    protected String name; // entity name
    // protected String description; // short description of the entity
    // creation timestamp
    protected long created = new java.util.Date().getTime();
    protected long modified; // last modified

    protected AccessControl acl; // access control list

    protected EntityService<? extends Entity> entityService; // for lazy loading of entity aspects like project summary
    protected Collection<Value> annotations;

    /**
     * Values for entity
     */
    protected List<Value> values = new ArrayList<Value>();

    /**
     * Links
     */
    protected List<Link> links = new ArrayList<Link>();

    protected Entity () {
    }

    public Entity (String name) {
        this.name = name;
    }

    public Object getId () { return id; }
    public void setId (Object id) { this.id = id; }

    public String getName () { return name; }
    public void setName (String name) { this.name = name; }

    public Collection<Value> getAnnotations () { 
	if (annotations == null && entityService != null) {
	    annotations = entityService.getAnnotations(this);
	}
	return annotations; 
    }

    @Deprecated
    public String getDescription () { 
	Value val = getValue(EntityDescription);
	return val == null ? "" : ""+val.getValue();
    }
    
    @Deprecated
    public void setDescription (String description) { 
        setDeprecatedString(description, EntityDescription);
    }

    public long getCreated () { return created; }
    public void setCreated (long created) { this.created = created; }
    public void setCreatedNow () {
        this.created = new java.util.Date().getTime();
    }

    public long getModified () { return modified; }
    public void setModified (long modified) { 
        this.modified = modified; 
    }
    public void setModifiedNow () {
        this.modified = new java.util.Date().getTime();
    }

    public AccessControl getACL () { return acl; }
    public void setACL (AccessControl acl) { this.acl = acl; }

    public void add (Value a) {
        values.add(a);
    }
    public boolean remove (Value a) {
        return values.remove(a);
    }

    protected void setDeprecatedString (String value, String valueName) {
	for (Value val: findValues(valueName))
	    remove(val);
	StringValue val = new StringValue();
	val.setId(valueName);
	val.setValue(value);
	add(val);	
    }
    
    public Collection<Value> getValues () { 
        return Collections.unmodifiableCollection(values); 
    }

    public void setEntityService (EntityService<? extends Entity> entityService) {
        this.entityService = entityService;
    }
    
    static public boolean lazyLoad(String id) {
	return false;
    }
    
    // lazy load values if missing
    // could be problematic if requesting value type that is just non existent
    public synchronized Collection<Value> getValues (String id) {
	Collection<Value> vals = findValues(id);
	if (vals.isEmpty() && entityService != null && lazyLoad(id)) {
	    entityService.update(this);
	    vals = findValues(id);
	    //!!! This appends a stringvalue to pubchemsids, which is very unhelpful
//	    if (vals.isEmpty()) {
//		setDeprecatedString ("", id);
//	    }
	}
	return vals;	
    }
    
    public Entity getEntityFromValue(EntityValue ev) {
	if (ev != null && ev.getValue() != null) {
	    if (ev.getValue() instanceof Entity) 
		return (Entity)ev.getValue();
	    else if (entityService != null) {
		try {
		    String clazzName = ev.getId();
		    if (clazzName.startsWith("class ")) clazzName = clazzName.substring(6);
		    Class<? extends Entity> clazz = (Class<? extends Entity>)Class.forName(clazzName);
		    EntityService es = entityService.getServiceManager().getService(clazz);
		    if (es == null)
			return null;
		    Entity entity = es.get(ev.getValue());
		    return entity;
		} catch (ClassNotFoundException e) {
		    e.printStackTrace();
		}
	    }
	}
	return null;
    }
    
    // get singleton 
    public Value getValue (String id) {
        Collection<Value> vals = getValues (id);
        return vals.isEmpty() ? null : vals.iterator().next();
    }

    public synchronized Collection<Value> getValues (DataSource ds) {
        List<Value> lv = new ArrayList<Value>();
        for (Iterator<Value> iter = values.iterator(); iter.hasNext(); ) {
            findValues (lv, iter.next(), ds);
        }
        return lv;
    }

    protected synchronized Collection<Value> findValues (String id) {
        List<Value> lv = new ArrayList<Value>();
        for (Iterator<Value> iter = this.values.iterator(); iter.hasNext();) {
            Value v = iter.next();
            if (v != null)
        	findValues (lv, v, id);
        }
        return lv;
    }

    protected synchronized void findValues (List<Value> lv, Value value, String id) {
        if (value.getId().equals(id)) {
            lv.add(value);
        }
        for (Value v : value.children) {
            findValues (lv, v, id);
        }
    }

    protected synchronized void findValues (List<Value> lv, Value value, DataSource ds) {
	if (value == null)
	    return;
        if (ds.equals(value.getSource())) {
            lv.add(value);
        }
        else {
            // only traverse the children if the data source doesn't match
            for (Value v : value.children) {
                findValues (lv, v, ds);
            }
        }
    }

    public synchronized int getValueCount () { return values.size(); }
    public Iterator<Value> values () { 
        return getValues().iterator(); 
    }

    public void add (Link l) { links.add(l); }
    public boolean remove (Link l) { return links.remove(l); }
    public Collection<Link> getLinks () { 
        return Collections.unmodifiableCollection(links);
    }
    public int getLinkCount () { return links.size(); }
    public Iterator<Link> links () { return getLinks().iterator(); }

    public String toString () {
        StringBuilder sb = new StringBuilder 
            (getClass().getName()+"{id="+id+",name="+name
             //+",description="+description
             +",created="+created
             +",modified="+modified+",acl="+acl+",values="+values.size()+"[");
        for (Iterator<Value> it = values(); it.hasNext(); ) {
            Value v = it.next();
            sb.append(v.toString());
            if (it.hasNext()) {
                sb.append(" ");
            }
        }
        sb.append("],links="+links.size());
        sb.append("}");
        return sb.toString();
    }
}
