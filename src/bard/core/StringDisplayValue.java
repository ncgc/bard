package bard.core;

public class StringDisplayValue extends StringValue {

    private static final long serialVersionUID = 7031508074337288581L;
    protected String display;
    
    protected StringDisplayValue () {}
    public StringDisplayValue (Value parent) {
        super (parent);
    }
    public StringDisplayValue (Value parent, String id) {
        super (parent, id);
    }
    public StringDisplayValue (Value parent, String id, String value, String display) {
        super (parent, id, value);
        this.display = display;
    }
    public StringDisplayValue (DataSource source, String id) {
        super (source, id, null);
    }
    public StringDisplayValue (DataSource source, String id, String value, String display) {
        super (source, id, value);
        this.display = display;
    }

    public void setDisplay (String display) {
        this.display = display;
    }
    public String getDisplay () {
        return display;
    }

}
