package bard.core;

import java.util.HashMap;
import java.util.List;

/**
 * Basic text search parameters
 */
public class SearchParams extends ServiceParams {
    private static final long serialVersionUID = 0xedd95256480bd363l;

    protected String query;
    protected List<String[]> filters = null;
    protected String origText;

    public SearchParams () {
    }

    public SearchParams (String query) {
        this.query = query;
    }
    
    public SearchParams(String query, List<String[]> filters) {
        this.query = query;
        this.filters = filters;
    }

    public List<String[]> getFilters() {
        return filters;
    }

    public void setFilters(List<String[]> filters) {
        this.filters = filters;
    }

    public String getQuery () { return query; }
    public SearchParams setQuery (String query) {
        this.query = query;
        return this;
    }
    
    public void setOrigText(String origText) {this.origText = origText;}
    public String getOrigText() {return this.origText;}
    
    private static HashMap<String, BARD_SOLR_KEYS> solrDict = new HashMap<String, BARD_SOLR_KEYS>();
    public static enum BARD_SOLR_KEYS {
	goBP ("gobp_term", "GO biological process"),
	goMF ("gomf_term", "GO molecular function"),
	goCC ("gomf_term", "GO cellular compartment"),
	av_dict_label ("av_dict_label", "CAP annotation"),
	ak_dict_label ("ak_dict_label", "CAP annotation2"),
	kegg_disease_names ("kegg_disease_names", "KEGG disease name"),
	kegg_disease_cat ("kegg_disease_cat", "KEGG disease category"),
	target_name ("target_name", "Target Name"),
	anno_val ("anno_val", "Compoung Tag"),
	cmpd_collection ("COLLECTION", "Compoung Collection"),
	cmpd_class ("compoundClass", "Compoung Type"),
	iupacName ("iupacName", "Compoung IUPAC Name"),
	entity_name ("name", "Name"),
	entity_description ("description", "Description");
	public String label;
	public String name;
	BARD_SOLR_KEYS (String label, String name) {
	    this.label = label;
	    this.name = name;
	    solrDict.put(label, this);
	    solrDict.put(name, this);
	}
	
	static public String convert(String term) {
	    if (!solrDict.containsKey(term )) return term;
	    for (BARD_SOLR_KEYS entry: BARD_SOLR_KEYS.values()) {
		if (entry.label.equals(term))
		    return entry.name;
	    	if (entry.name.equals(term))
	    	    return entry.label;
	    }
	    return term;
	}
    }
}
