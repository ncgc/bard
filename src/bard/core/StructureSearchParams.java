package bard.core;

import java.io.Serializable;

public class StructureSearchParams extends SearchParams {
    private static final long serialVersionUID = 0xaeddb6dac8a37ae8l;

    /*
     * Different search types supported at the moment
     */
    public enum Type {
        Substructure,
            Superstructure,
            Exact,
            Similarity;
    }

    /*
     * Actual search or we're only care about count
     */
    public enum Method {
        Search, 
            Count
            };

    /*
     * Future we support different metrics
     */
    public enum Metric {
        Tanimoto, // similarity
            Dice, // similarity
            Cosine, // similarity
            Hamming; // distance
    }

    protected Type type = Type.Substructure;
    protected Method method = Method.Search;
    protected Metric metric = Metric.Tanimoto;
    protected Double threshold = 0.7; // for similarity

    public StructureSearchParams () {
    }

    public StructureSearchParams (String query) {
        this (query, Type.Substructure);
    }

    public StructureSearchParams (String query, Type type) {
        super (query);
        this.type = type;
    }

    public Type getType () { return type; }
    public StructureSearchParams setType (Type type) {
        this.type = type;
        return this;
    }

    public Method getMethod () { return method; }
    public StructureSearchParams setMethod (Method method) {
        this.method = method;
        return this;
    }

    public Metric getMetric () { return metric; }
    public StructureSearchParams setMetric (Metric metric) { 
        this.metric = metric;
        return this;
    }

    public Double getThreshold () { return threshold; }
    public StructureSearchParams setThreshold (Double threshold) {
        this.threshold = threshold;
        return this;
    }

    public String toString () {
        return "{type="+type+",method="+method+",metric="
            +metric+",threshold="+threshold+"}";
    }
}
