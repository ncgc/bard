package bard.core.jdo;

import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jdo.JDOHelper;
import javax.jdo.ObjectState;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;
import javax.jdo.Query;

import bard.core.*;
import bard.core.impl.*;

public class JDOLinkService 
    extends JDOAbstractEntityService<Link> implements LinkService {
    static final private Logger logger = 
        Logger.getLogger(JDOLinkService.class.getName());

    class JDONeighborIterator<E extends Entity> extends JDOServiceIterator<E> {
        Object id;

        JDONeighborIterator (Class<? extends Entity> clazz, 
                             Object id, ServiceParams params) {
            super (clazz, params);
            this.id = id;
        }

        @Override
        void executeQuery (PersistenceManager pm) {
            Query q = newQuery (Link.class, pm, params);
            q.setFilter("JDOHelper.getObjectId(source) == :id "
                        +" || JDOHelper.getObjectId(target) == :id");
            Collection<Link> results = (Collection<Link>)q.execute(id);

            try {
                for (Iterator<Link> iter = results.iterator(); 
                     iter.hasNext() 
                         && !Thread.currentThread().isInterrupted(); ) {
                    Link lnk = iter.next();
                    if (id.equals(JDOHelper.getObjectId(lnk.getSource()))) {
                        getBuffer().put(detach (pm, lnk.getTarget()));
                    }
                    else { // id.equals(lnk.getTarget())
                        getBuffer().put(detach (pm, lnk.getSource()));
                    }
                }
                getBuffer().put(done);
                //done ();
            }
            catch (InterruptedException ex) {
                getBuffer().offer(done);
                logger.info("** " + Thread.currentThread() + " interrupted!");
            }
            q.closeAll();
        }
    }

    protected JDOLinkService (EntityServiceManager srvman, 
                              PersistenceManagerFactory pmf) {
        super (srvman, pmf);
    }

    public Class<Link> getEntityClass () { return Link.class; }

    <E extends Entity> Object getId (E e) {
        Object id = JDOHelper.getObjectId(e);
        if (id == null) {
            id = e.getId();
        }

        if (id == null) {
            throw new IllegalArgumentException
                ("Not a persistent object: "+JDOHelper.getObjectState(e));
        }
        return id;
    }

    public <E extends Entity> ServiceIterator<E> neighbors (E entity) {
        return new JDONeighborIterator<E>
            (entity.getClass(), getId (entity), new ServiceParams (5));
    }

    @Override
    public Collection<Value> getAnnotations(Entity enityt) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Value newCompositeETag(String name, Collection ids) {
        // TODO Auto-generated method stub
        return null;
    }

}

