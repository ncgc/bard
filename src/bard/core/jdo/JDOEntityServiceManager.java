package bard.core.jdo;

import java.util.List;
import java.util.ArrayList;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

import bard.core.*;

public class JDOEntityServiceManager implements EntityServiceManager {
    EntityService[] services;

    public JDOEntityServiceManager (PersistenceManagerFactory pmf) {
        services = new EntityService[]{
            new JDOLinkService (this, pmf),
            new JDOCompoundService (this, pmf)
        };
    }

    public JDOEntityServiceManager (String config) {
        this (JDOHelper.getPersistenceManagerFactory(config));
    }

    public <T extends EntityService<? extends Entity>> T getService
                      (Class<? extends Entity> clazz) {
        if (services == null) {
            throw new IllegalStateException
                ("Service manager already shutdown!");
        }

        Class c = clazz;
        do {
            for (EntityService<? extends Entity> es : services) {
                if (clazz.equals(es.getEntityClass())) {
                    return (T)es;
                }
            }
            c = c.getSuperclass();
        }
        while (c != null);

        return null;
    }

    public <T extends EntityService<? extends Entity>> List<T> getServices () {
        List<T> srv = new ArrayList<T>();
        for (EntityService<? extends Entity> es : services) {
            srv.add((T)es);
        }
        return srv;
    }

    public void shutdown () {
        if (services != null) {
            for (EntityService es : services) {
                es.shutdown();
            }
            services = null;
        }
    }
}
