package bard.core.jdo;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

import bard.core.*;
import bard.core.impl.*;

public class JDOTestServices {
    /****************************************************************
     * TESTING
     ***************************************************************/
    static final String[] URL = new String[]{
        "http://tripod.nih.gov",
        "http://ncats.nih.gov",
        "http://pubchem.ncbi.nlm.nih.gov",
        "http://ncgcweb.nhgri.nih.gov"
    };

    static final String[] MOA = new String[]{
        "antineoplastic",
        "antipsychotic",
        "antimetabolic",
        "antihistamine",
        "anticancer",
    };

    static final String[] STATUS = new String[]{
        "approved",
        "phase 1",
        "phase 2",
        "phase 3",
        "unknown",
        "withdrawn"
    };

    static final String[] ACL_GROUPS = new String[]{
        "group 1",
        "group 2",
        "group a",
        "group b",
        "group c"
    };

    static final String[] LINKS = new String[] {
        "tanimoto-neighbor",
        "shape-neighbor",
        "target",
        "MeSH",
        "GO"
    };

    static final String[] MOLECULES = new String[]{
        "O=S(=O)(NC1=CC=CC=C1)C2=CC=CC=C2",
        "C(CC1CCCCC1)C2CCCC2",
        "O=C1CC2=C(N1)C=CC=C2",
        "C(N1C=CC2=C1C=CC=C2)C3=CC=CC=C3",
        "C1CC2CCC(CC2C1)C3CCCCC3",
        "N(C1=CC=CC=C1)C2=C3C=CC=CC3=NC=C2",
        "N1C=CN=N1",
        "C(CC1CCCCC1)C2CCCCC2",
        "O=C(NC1=CC=CC=C1)NC2=CC=CC=C2",
        "C(=CC1=CC=CC=C1)C2=CC=CC=C2"
    };

    
    static void testCreate (String config) throws Exception {
        EntityServiceManager esm = new JDOEntityServiceManager (config);

        CompoundService cs = esm.getService(Compound.class);
        DataSource ds = cs.getDataSource("pubchem", "08052012");

        long size = cs.size();
        System.out.println("** size: "+size);

        Random rand = new Random(1);
        Compound[] cmpds = new Compound[500];
        System.out.println("## generting compounds...");
        for (int i = 0; i < cmpds.length; ++i) {
            Compound c = new Compound (); //"compound "+(i+1));
            
            AccessControl acl = null;
            if (rand.nextDouble() < 0.1) {
                acl = AccessControl.getInstance
                    (AccessControl.ACC_GROUP, AccessControl.PERM_ALL);
                int n = rand.nextInt(10);
                for (int j = 0; j < n; ++j) {
                    acl.add(ACL_GROUPS[rand.nextInt(ACL_GROUPS.length)]);
                }
                
                c.setACL(acl);
            }

            Value v;
            c.add(v = new StringValue 
                  (ds, "url", URL[rand.nextInt(URL.length)]));
            for (int j = 0; j < 5; ++j) {
                if (rand.nextDouble() < rand.nextDouble()) {
                    v.setACL(acl);
                }
                v.add(v = new StringValue
                      (ds, "url."+j, URL[rand.nextInt(URL.length)]));
            }
            c.add(v = new StringValue
                  (ds, "moa", MOA[rand.nextInt(MOA.length)]));
            for (int j = 0; j < 5; ++j) {
                if (rand.nextDouble() < rand.nextDouble()) {
                    v.setACL(acl);
                }
                v.add(v = new StringValue
                      (ds, "moa."+j, MOA[rand.nextInt(MOA.length)]));
            }
            c.add(v = new StringValue 
                  (ds, "status", STATUS[rand.nextInt(STATUS.length)]));
            for (int j = 0; j < 5; ++j) {
                if (rand.nextDouble() < rand.nextDouble()) {
                    v.setACL(acl);
                }
                v.add(v = new StringValue
                      (ds, "status."+j, STATUS[rand.nextInt(STATUS.length)]));
            }
            
            c.add(v = new IntValue (ds, "druglikeness", rand.nextInt(10)));
            for (int j = 0; j < 5; ++j) {
                if (rand.nextDouble() < rand.nextDouble()) {
                    v.setACL(acl);
                }
                // simulate different druglikeness implementations
                v.add(v = new IntValue 
                      (ds, "druglikeness."+j, rand.nextInt(10)));
            }
            
            c.add(v = new NumericValue (ds, "logP", rand.nextDouble()));
            for (int j = 0; j < 5; ++j) {
                if (rand.nextDouble() < rand.nextDouble()) {
                    v.setACL(acl);
                }
                // simulate different logP implementations
                v.add(v = new NumericValue 
                      (ds, "logP."+j, rand.nextDouble()));
            }

            for (int j = 0; j < 5; ++j) {
                MolecularData md = new MolecularDataJChemImpl ();
                md.setMolecule(MOLECULES[rand.nextInt(MOLECULES.length)]);
                v.add(v = new MolecularValue (ds, "substance."+j, md));
            }

            cmpds[i] = c;
        }
        long start = System.currentTimeMillis();
        cs.put(cmpds);
        double time = 1e-3*(System.currentTimeMillis()-start);
        System.out.println("## persisting "+cmpds.length
                           +" compounds in "+String.format("%1$.3fs!", time));

        LinkService ls = esm.getService(Link.class);
        // create random links between Compounds
        List<Link> links = new ArrayList<Link>();
        System.out.println("## generating links...");
        for (int i = 0; i < cmpds.length; ++i) {
            for (int j = i+1; j < cmpds.length; ++j) {
                if (rand.nextDouble() < 0.05) {
                    Link l = new Link (cmpds[i], cmpds[j]);
                    links.add(l);
                }
            }
        }
        start = System.currentTimeMillis();
        ls.put(links.toArray(new Link[0]));
        time = 1e-3*(System.currentTimeMillis()-start);
        System.out.println("## persisting "+links.size()
                           +" links in "+String.format("%1$.3fs!", time));

        cs.shutdown();
        ls.shutdown();
    }

    static void testQuery (String config) throws Exception {
        EntityServiceManager esm = new JDOEntityServiceManager (config);

        CompoundService cs = esm.getService(Compound.class);
        /*
         * find all compounds for which the mechanism of action is "anticancer"
         */
        FilterParams filter = new FilterParams 
            ("values.contains(a) && a.id==:id && a.value==:value", 
             "moa", "anticancer");
        // declare the variable used in the filter experession
        filter.setVariables("bard.core.StringValue a");
        filter.setPartial(false);

        long start = System.currentTimeMillis();
        ServiceIterator<Compound> iter = cs.filter(filter);
        while (iter.hasNext()) {
            Compound c = iter.next();
            System.out.println(c);
        }
        iter.done();

        double time = 1e-3*(System.currentTimeMillis()-start);
        System.out.println("** "+iter.getConsumedCount()
                           +" compound(s) found in "
                           +String.format("%1$.3fs!", time));
        cs.shutdown();
    }

    static void testLinks0 (String config) throws Exception {
        EntityServiceManager esm = new JDOEntityServiceManager (config);

        LinkService ls = esm.getService(Link.class);
        long size = ls.size();
        System.out.println("** size: "+size);        

        long start = System.currentTimeMillis();
        // specify the fetch depth to be deeper for links
        ServiceIterator<Link> iter = ls.iterator(new ServiceParams (5));
        Random rand = new Random ();
        while (iter.hasNext()) {
            Link lnk = iter.next();
            //System.out.println(lnk);
            if (rand.nextDouble() < 0.01) {
                System.out.println(lnk);
            }
        }
        iter.done();

        double time = 1e-3*(System.currentTimeMillis()-start);
        System.out.println("** "+iter.getConsumedCount()+" links in "
                           +String.format("%1$.3fs!", time));
        ls.shutdown();
    }

    static void testLinks1 (String config) throws Exception {
        EntityServiceManager esm = new JDOEntityServiceManager (config);

        LinkService ls = esm.getService(Link.class);
        CompoundService cs = esm.getService(Compound.class);
        /*
         * find all compounds for which the parent mechanism of action 
         * is "antineoplastic" and so is its direct descendent
         */
        FilterParams filter = new FilterParams 
            ("values.contains(a) && a.id==:parent && a.value==:pv"
             +"&& a.children.contains(b) && b.value==:value", 
             "moa", "antineoplastic", "antineoplastic");
        // declare the variable used in the filter experession
        filter.setVariables
            ("bard.core.StringValue a; bard.core.StringValue b;");

        long start = System.currentTimeMillis();
        ServiceIterator<Compound> i = cs.filter(filter);
        List<Compound> results = new ArrayList<Compound>();
        while (i.hasNext()) {
            Compound c = i.next();
            results.add(c);
        }
        i.done();

        double time = 1e-3*(System.currentTimeMillis()-start);
        System.out.println("** "+i.getConsumedCount()
                           +" results from filter in "
                           +String.format("%1$.3fs!", time));

        for (Compound c : results) {
            System.out.println("-- Neighbors of "+c);
            ServiceIterator<Compound> ii = ls.neighbors(c);
            while (ii.hasNext()) {
                Compound nb = ii.next();
                System.out.println("  "+nb.getId());
            }
            ii.done();
            System.out.println("** "+ii.getConsumedCount()+" neighbor(s)!");
        }

        ls.shutdown();
        cs.shutdown();
    }

    public static void main (String[] argv) throws Exception {
        testCreate ("etc/datanucleus.properties");
        testQuery ("etc/datanucleus.properties");
        testLinks0 ("etc/datanucleus.properties");
        testLinks1 ("etc/datanucleus.properties");
    }
}
