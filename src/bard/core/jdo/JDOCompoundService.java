package bard.core.jdo;

import bard.core.Assay;
import bard.core.Compound;
import bard.core.CompoundService;
import bard.core.Entity;
import bard.core.EntityServiceManager;
import bard.core.ServiceIterator;
import bard.core.StructureSearchParams;
import bard.core.Value;

import javax.jdo.PersistenceManagerFactory;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

public class JDOCompoundService 
    extends JDOAbstractEntityService<Compound> implements CompoundService {
    static final private Logger logger = 
        Logger.getLogger(JDOCompoundService.class.getName());

    protected JDOCompoundService (EntityServiceManager srvman, 
                                  PersistenceManagerFactory pmf) {
        super (srvman, pmf);
    }

    public Class<Compound> getEntityClass () { return Compound.class; }
    public ServiceIterator<Compound> structureSearch
        (StructureSearchParams params) {
        throw new UnsupportedOperationException ("Method not yet implemented");
    }

    public Collection<Value> getSynonyms (Compound compound) {
        throw new UnsupportedOperationException ("Method not yet implemented");
    }

    public Collection<Assay> getTestedAssays(Compound compound, boolean activeOnly) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }

    public Collection<Value> getPubChemSIDs(Compound compound) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }

    @Override
    public Map<String, Object> getSummary(Compound compound) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }

    public Collection<Value> getSubstances(Compound compound) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }

    public Collection<Value> getAnnotations(Compound compound) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }

    @Override
    public Collection<Value> getAnnotations(Entity enityt) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Value newCompositeETag(String name, Collection ids) {
        // TODO Auto-generated method stub
        return null;
    }

}
