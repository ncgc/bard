package bard.core.jdo;

import bard.core.DataSource;
import bard.core.Entity;
import bard.core.EntityService;
import bard.core.EntityServiceManager;
import bard.core.FilterParams;
import bard.core.SearchParams;
import bard.core.ServiceIterator;
import bard.core.ServiceParams;
import bard.core.Value;
import bard.core.SuggestParams;

import javax.jdo.Extent;
import javax.jdo.FetchPlan;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.listener.AttachLifecycleListener;
import javax.jdo.listener.CreateLifecycleListener;
import javax.jdo.listener.DetachLifecycleListener;
import javax.jdo.listener.DirtyLifecycleListener;
import javax.jdo.listener.InstanceLifecycleEvent;
import javax.jdo.listener.LoadLifecycleListener;
import javax.jdo.listener.StoreLifecycleListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;
import java.security.Principal;

public abstract class JDOAbstractEntityService<E extends Entity>
        implements EntityService<E>,
        StoreLifecycleListener,
        AttachLifecycleListener,
        DetachLifecycleListener,
        CreateLifecycleListener,
        DirtyLifecycleListener,
        LoadLifecycleListener {

    private static final long serialVersionUID = 0x7b03799fafa71a12l;

    static final Logger logger = Logger.getLogger
            (JDOAbstractEntityService.class.getName());

    static final int DEFAULT_BUFSIZ = 1000;
    static final int DEFAULT_TIMEOUT = 2000; // 2s

    /*
     * not thread safe
     */
    protected class JDOServiceIterator<T extends Entity>
            implements ServiceIterator<T>, Runnable {

        BlockingQueue buffer;
        AtomicLong consumed = new AtomicLong();
        AtomicLong count = new AtomicLong();

        ServiceParams params;
        Class<? extends Entity> clazz;
        List<Value> facets = new ArrayList<Value>();

        volatile T next;
        volatile Object done;
        Future future;

        JDOServiceIterator(int bufferSize, Class<? extends Entity> clazz) {
            this (bufferSize, clazz, null);
        }

        JDOServiceIterator(Class<? extends Entity> clazz) {
            this (DEFAULT_BUFSIZ, clazz, null);
        }

        JDOServiceIterator(Class<? extends Entity> clazz,
                           ServiceParams params) {
            this (DEFAULT_BUFSIZ, clazz, params);
        }

        JDOServiceIterator(int bufferSize, Class<? extends Entity> clazz,
                           ServiceParams params) {
            this.buffer = new ArrayBlockingQueue(bufferSize);
            this.params = params;
            this.clazz = clazz;

            try {
                this.done = clazz.newInstance();
            } 
            catch (Exception ex) {
                ex.printStackTrace();
            }
            future = threadPool.submit(this);
        }


        void executeQuery(PersistenceManager pm) {
            Query q = newQuery(clazz, pm, params);
            Collection<T> results = (Collection<T>) q.execute();
            fetch (pm, results.iterator());
            q.closeAll();
        }

        PersistenceManager persistenceManager() {
            PersistenceManager pm = getPersistenceManager();
            pm.setDetachAllOnCommit(false);
            pm.setMultithreaded(true);
            pm.setIgnoreCache(false); // use cache if we can even for queries
            pm.getFetchPlan()
                .setDetachmentOptions(FetchPlan.DETACH_UNLOAD_FIELDS)
                .setGroup(FetchPlan.ALL)
                .setMaxFetchDepth(3);

            return pm;
        }

        public void run () {
            PersistenceManager pm = persistenceManager ();
            Transaction tx = pm.currentTransaction();
            tx.setRetainValues(true);
            try {
                tx.begin();

                if (params != null) {
                    executeQuery(pm);
                } 
                else {
                    Extent ex = pm.getExtent(clazz, false);
                    fetch(pm, ex.iterator());
                    ex.closeAll();
                }

                tx.commit();
                //logger.info("## "+Thread.currentThread().getName()+" complete!");
            } 
            catch (Exception ex) {
                ex.printStackTrace();
            } 
            finally {
                if (tx.isActive()) {
                    tx.commit();
                }
                pm.close();
                logger.info("## " + Thread.currentThread() + " done!");
            }
        }

        protected void fetch (PersistenceManager pm,
                              Iterator<? extends Entity> iter) {
            try {
                while (iter.hasNext()
                        && !Thread.currentThread().isInterrupted()) {
                    buffer.put(detach(pm, iter.next()));
                }

                // signal end of buffer
                buffer.put(done);
            } 
            catch (InterruptedException ex) {
                buffer.offer(done);
                logger.info("** " + Thread.currentThread() + " interrupted!");
            }
        }

        protected Object detach (PersistenceManager pm, Entity e) {
            //pm.makeTransient(e);
            Entity copy = (Entity) pm.detachCopy(e);
            copy.setId(e.getId());
            return copy;
        }

        public void done () {
            future.cancel(true);
        }

        public Collection<Value> getFacets() {
            return facets;
        }

        protected ServiceParams getParams () {
            return params;
        }

        protected BlockingQueue getBuffer () {
            return buffer;
        }

        public boolean hasNext () {
            try {
                Object obj = buffer.take();
                next = obj != done ? (T) obj : null;
            } 
            catch (InterruptedException ex) {
                logger.info("** " + Thread.currentThread().getName()
                        + " interrupted!");
                next = null;
            }

            return next != null;
        }

        public T next() {
            consumed.incrementAndGet();
            T t = next;
            next = null;
            return t;
        }

        public List<T> next(int size) {
            int s = Math.min(size, buffer.size());

            List<T> page = new ArrayList<T>();
            buffer.drainTo(page, s);
            for (; hasNext() && s < size; ++s) {
                page.add(next());
            }

            return page;
        }

        public long getConsumedCount() {
            return consumed.get();
        }

        public long getCount() {
            return count.get();
        }
        
        public Value getETag () { return null; }

        public void remove() {
            throw new UnsupportedOperationException
                    ("remove() not supported!");
        }
    }

    protected class JDOFilterIterator<T extends Entity>
            extends JDOServiceIterator<T> {
        JDOFilterIterator(Class<T> clazz, FilterParams params) {
            this(DEFAULT_BUFSIZ, clazz, params);
        }

        JDOFilterIterator(int bufferSize,
                          Class<T> clazz, FilterParams params) {
            super(bufferSize, clazz, params);
        }


        @Override
        void executeQuery(PersistenceManager pm) {
            FilterParams params = (FilterParams) getParams();
            Query q = newQuery(clazz, pm, params);
            q.setFilter(params.getFilter());
            q.declareVariables(params.getVariables());
            Collection<T> results = (Collection<T>)
                    q.executeWithArray(params.getArguments());
            fetch(pm, results.iterator());
            q.closeAll();
        }
    }

    protected static <T extends Entity> Query newQuery
            (Class<T> clazz, PersistenceManager pm, ServiceParams params) {
        Boolean partial = params.getPartial();
        if (partial != null) {
            if (!partial) {
                pm.getFetchPlan().setGroup(FetchPlan.ALL);
            }
        }

        Integer depth = params.getMaxDepth();
        if (depth != null) {
            pm.getFetchPlan().setMaxFetchDepth(depth);
        }

        Query q = pm.newQuery(clazz);
        Long top = params.getTop();
        Long skip = params.getSkip();

        if (skip != null && top != null) {
            q.setRange(skip, skip + top);
        } else if (skip != null) {
            q.setRange(skip, Long.MAX_VALUE);
        } else if (top != null) {
            q.setRange(0, top);
        }

        q.setOrdering(params.getOrdering());
        return q;
    }

    protected final PersistenceManagerFactory pmf;
    protected ExecutorService threadPool = Executors.newCachedThreadPool();
    protected DataSource datasource; // default data source
    protected EntityServiceManager srvman;

    protected JDOAbstractEntityService(EntityServiceManager srvman,
                                       PersistenceManagerFactory pmf) {
        this.srvman = srvman;
        this.pmf = pmf;
    }

    public void preStore(InstanceLifecycleEvent ev) {
        Entity e = (Entity) ev.getPersistentInstance();
        e.setModifiedNow();
        /*
        logger.info("$$ "+JDOHelper.getObjectState(e) +" "
                    +e+" "+JDOHelper.getObjectId(e));
        */
    }

    public void postStore(InstanceLifecycleEvent ev) {
        Entity e = (Entity) ev.getPersistentInstance();
        setId(e);
        /*
        logger.info("## "+JDOHelper.getObjectState(e) +" "
                    + e + " "+JDOHelper.getObjectId(e));
        */
    }

    public void postAttach(InstanceLifecycleEvent ev) {
        Entity e = (Entity) ev.getPersistentInstance();
        setId(e);
        //e.setId(id.toString());
    }

    public void preAttach(InstanceLifecycleEvent ev) {
        Entity e = (Entity) ev.getPersistentInstance();
        /*
        logger.info("## "+JDOHelper.getObjectState(e) + " "+e
                    +" " +JDOHelper.getObjectId(e));
        */
    }

    public void postDetach(InstanceLifecycleEvent ev) {
        setId((Entity) ev.getPersistentInstance());
    }

    public void preDetach(InstanceLifecycleEvent ev) {
    }

    public void postCreate(InstanceLifecycleEvent ev) {
        Entity e = (Entity) ev.getPersistentInstance();
        //logger.info("## "+JDOHelper.getObjectState(e) +" "+e);
    }

    public void postDirty(InstanceLifecycleEvent ev) {
        Entity e = (Entity) ev.getPersistentInstance();
        PersistenceManager pm = JDOHelper.getPersistenceManager(e);
        //logger.info("-- "+JDOHelper.getObjectState(e) + " "+e);
    }

    public void preDirty(InstanceLifecycleEvent ev) {

    }

    public void postLoad(InstanceLifecycleEvent ev) {
        Entity e = (Entity) ev.getPersistentInstance();
        setId(e);
    }

    protected void setId(Entity e) {
        /*
        OID oid = (OID)JDOHelper.getObjectId(e);
        e.setId(oid.getKeyValue());
        */
        e.setId(JDOHelper.getObjectId(e));
    }

    protected PersistenceManager getPersistenceManager () {
        PersistenceManager pm = pmf.getPersistenceManager();
        pm.setDetachAllOnCommit(true);
        pm.addInstanceLifecycleListener(this, Entity.class);
        return pm;
    }

    protected PersistenceManagerFactory getPMF () { return pmf; }

    /*
     * EntityService interface
     */
    public EntityServiceManager getServiceManager () { return srvman; }
    public boolean isReadOnly () { return false; }

    public DataSource getDataSource () { 
        // get default data source
        if (datasource == null) {
            DataSource def = DataSource.getCurrent();
            datasource = getDataSource (def.getName(), def.getVersion());
        }
        return datasource;
    }

    public DataSource getDataSource (String name) {
        return getDataSource (name, "*");
    }

    public DataSource getDataSource (String name, String version) {
        PersistenceManager pm = getPersistenceManager ();
        Transaction tx = pm.currentTransaction();
        tx.setRetainValues(true);
        try {
            tx.begin();
            Query q = pm.newQuery
                (DataSource.class, "name == :name && version == :version");
            q.setUnique(true);
            DataSource ds = (DataSource)q.execute(name, version);
            if (ds == null) {
                ds = new DataSource (name, version);
                pm.makePersistent(ds);
            }
            tx.commit();

            return ds;
        }
        finally {
            if (tx.isActive()) 
                tx.rollback();
            pm.close();
        }
    }

    public E get (Object id) {
        PersistenceManager pm = getPersistenceManager ();
        pm.getFetchPlan()
            .setGroup(FetchPlan.ALL)
            .setMaxFetchDepth(-1);
        pm.setDetachAllOnCommit(true);

        Transaction tx = pm.currentTransaction();
        tx.setIsolationLevel(JDOHelper.TX_READ_UNCOMMITTED);

        try {
            tx.begin();
            E e = (E) pm.getObjectById(id);
            //pm.retrieve(e);
            //pm.makeTransient(e);
            tx.commit();

            return e;
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
            pm.close();
        }
    }

    public E update (Entity entity) {
	return get(entity.getId());
    }
    
    public Collection<E> get(Collection ids) {
        PersistenceManager pm = getPersistenceManager();
        pm.getFetchPlan()
                .setGroup(FetchPlan.ALL)
                .setMaxFetchDepth(-1);
        pm.setDetachAllOnCommit(true);

        List<E> results = new ArrayList<E>();
        Transaction tx = pm.currentTransaction();
        tx.setIsolationLevel(JDOHelper.TX_READ_UNCOMMITTED);
        try {
            tx.begin();
            Collection res = pm.getObjectsById(ids);
            for (Iterator iter = res.iterator(); iter.hasNext(); ) {
                E e = (E) iter.next();
                results.add(e);
            }
            tx.commit();

            return results;
        } finally {
            if (tx.isActive())
                tx.rollback();
            pm.close();
        }
    }

    public Value newETag(String name) {
        return newETag(name, null);
    }

    public Value newETag(String name, Collection ids) {
        throw new UnsupportedOperationException();
    }

    public int putETag(Value etag, Collection ids) {
        throw new UnsupportedOperationException();
    }

    public Value getETag(Value etag) {
        return getETag(etag.getId());
    }
    public Value getETag(String etag) {
        throw new UnsupportedOperationException();
    }
    public ServiceIterator etags (Principal principal) {
        throw new UnsupportedOperationException();
    }

    public Collection<Value> getFacets(Object etag) {
        return null;
    }

    public List<E> get(long top, long skip) {
        return get(top, skip, null);
    }

    public List<E> get(long top, long skip, String ordering) {
        if (top <= 0l) {
            throw new IllegalArgumentException("top must be > 0");
        }
        if (skip < 0l) {
            throw new IllegalArgumentException("skip must be >= 0");
        }

        List<E> results = new ArrayList<E>();
        PersistenceManager pm = getPersistenceManager();
        pm.getFetchPlan()
                .setGroup(FetchPlan.ALL)
                .setMaxFetchDepth(3);

        Transaction tx = pm.currentTransaction();
        tx.setIsolationLevel(JDOHelper.TX_READ_UNCOMMITTED);

        try {
            tx.begin();
            Query q = pm.newQuery(getEntityClass());
            q.setOrdering(ordering);
            q.setRange(skip, skip + top);

            Collection<E> list = (Collection<E>) q.execute();
            results.addAll(list);

            pm.makeTransientAll(results, true);
            q.closeAll();
            tx.commit();

            return results;
        } finally {
            if (tx.isActive())
                tx.rollback();
            pm.close();
        }
    }

    public void put(E... entities) {
        /*
        logger.info("updating entitites...");
        for (E e: entities) {
            System.out.println(JDOHelper.getObjectState(e)+" "+e);
        }
        */

        PersistenceManager pm = getPersistenceManager();
        /*
        pm.getFetchPlan()
            .setGroup(FetchPlan.ALL)
            .setMaxFetchDepth(-1);
        */

        Transaction tx = pm.currentTransaction();
        tx.setOptimistic(true);
        tx.setRetainValues(true);
        try {
            tx.begin();
            pm.makePersistentAll(entities);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
            pm.close();
        }
    }

    public long size() {
        PersistenceManager pm = pmf.getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            Query q = pm.newQuery(getEntityClass());
            q.setResult("count(this)");
            Number cnt = (Number) q.execute();
            tx.commit();

            return cnt.longValue();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
            pm.close();
        }
    }

    public ServiceIterator<E> iterator() {
        return new JDOServiceIterator<E>(getEntityClass());
    }

    public <T extends Entity> ServiceIterator<T> iterator
                      (E entity, Class<T> clazz) {
        return null;
    }

    public ServiceIterator<E> iterator (Object etag) {
        throw new UnsupportedOperationException
                ("Method search currently not supported yet!");
    }

    public ServiceIterator<E> iterator(ServiceParams params) {
        return new JDOServiceIterator<E>(getEntityClass(), params);
    }

    public ServiceIterator<E> filter(FilterParams params) {
        return new JDOFilterIterator<E>(getEntityClass(), params);
    }

    public ServiceIterator<E> search(SearchParams params) {
        throw new UnsupportedOperationException
                ("Method search currently not supported yet!");
    }

    public void shutdown() {
        try {
            threadPool.shutdown();
            if (!threadPool.awaitTermination
                    (DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)) {
                logger.warning("Timeout while waiting for " + getEntityClass()
                        + " service to shutdown!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            pmf.close();
        }
    }

    public Map<String, List<String>> suggest(SuggestParams params) {
        throw new UnsupportedOperationException("Method not yet implemented");
    }
}
