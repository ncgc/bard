package bard.core;

/**
 * Well-known entity values
 */
public interface EntityValues {
    static final String SynonymValue = "Synonym";
    static final String SearchHighlightValue = "SearchHighlightValue";
    static final String SearchHighlightField = "SearchHighlightField";
    static final String URLValue = "URL";
    static final String WikipediaValue = "Wikipedia";
    
    static final String EntityDescription = "EntityDescription";
}
