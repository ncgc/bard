package bard.core.adapter;

import java.util.Collection;
import java.util.Iterator;

import bard.core.DataSource;
import bard.core.Entity;
import bard.core.SearchParams;
import bard.core.StringValue;
import bard.core.StructureSearchParams;
import bard.core.Value;


public class EntityAdapter<E extends Entity> {
    protected E entity;

    protected EntityAdapter () {}
    
    public EntityAdapter (E entity) {
        setEntity (entity);
    }
    
    public void setEntity (E entity) {
        this.entity = entity;
    }
    
    public E getEntity () { return entity; }

    public String getName () {
        return entity.getName();
    }

    public String[] getSynonyms () {
        Collection<Value> values = entity.getValues(Entity.SynonymValue);
        String[] syns = new String[values.size()];
        Iterator<Value> iter = values.iterator();
        for (int i = 0; iter.hasNext(); ++i) {
            Value v = iter.next();
            syns[i] = (String)v.getValue();
        }
        return syns;
    }

    public String getSearchHighlight (SearchParams query) {
        Value hl = entity.getValue(Entity.SearchHighlightValue);
        if (hl != null)
            return (String)hl.getValue();
        hl = entity.getValue(Entity.SearchHighlightField);
        if (query instanceof StructureSearchParams) {
            if (hl == null)
                return null;
            return "Based on a match to the structure-based search.";
        }
        if (query == null) {
            return null;
        }
        String qText = query.getQuery().toLowerCase();
        if (qText.charAt(0) == '"' && qText.charAt(qText.length()-1) == '"') qText = qText.substring(1, qText.length()-1);
        if (hl == null) {
            entity.getAnnotations();
            for (Value val: entity.getValues()) {
        	String mText = val.getValue().toString().toLowerCase();
        	int index = mText.indexOf(qText);
        	if (index > -1) {
                    hl = new StringValue(val, Entity.SearchHighlightField, val.getId());
        	}
            }
            if (hl == null)
        	return null;
            entity.add(hl);
        }
        Collection<Value> values = values = entity.getValues((String)hl.getValue());
        if (((String)hl.getValue()).equals("anno_val") || ((String)hl.getValue()).equals("gobp_term"))
            values = entity.getValues();
        for (Value val: values) {
            String mText = val.getValue().toString().toLowerCase();
            String sText = val.getValue().toString();
            int index = mText.indexOf(qText);
            if (index > -1) {
        	String match = (String)hl.getValue()+": ";
        	match = match.substring(0, 1).toUpperCase() + match.substring(1);
        	if (index < 45)
        	    match = match + sText.substring(0, index);
        	else {
        	    match = match + "..." + sText.substring(index-35, index);
        	}
        	match = match + "<strong>" + sText.substring(index, index+qText.length())+ "</strong>";
        	if (index+qText.length()+35 > sText.length())
        	    match = match + sText.substring(index+qText.length(), sText.length());
        	else
        	    match = match + sText.substring(index+qText.length(), index+qText.length()+25) + "...";
                hl = new StringValue(hl, Entity.SearchHighlightValue, match);
                entity.add(hl);
        	return match;
            }
        }
        if (query.getFilters() != null && query.getFilters().size() > 0) {
            String filterString = "";
            for (String[] filter: query.getFilters())
        	for (String item: filter)
        	    filterString += item + " ";
            return "Based on a match to: "+filterString;
        }
        return "Based on a match to "+query.getQuery();
    }

    public Collection<Value> getAnnotations () {
        return entity.getValues
            (new DataSource (Entity.AnnotationSource));
    }
}
