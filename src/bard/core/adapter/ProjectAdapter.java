package bard.core.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import bard.core.DataSource;
import bard.core.EntityNamedSources;
import bard.core.Project;
import bard.core.ProjectService;
import bard.core.Value;
import bard.util.BardHttpClient;

public class ProjectAdapter extends EntityAdapter<Project> {
    
    public ProjectAdapter(Project proj) {
	setProject(proj);
    }

    public Project getProject() {
        return getEntity();
    }

    public void setProject(Project proj) {
        setEntity(proj);
    }

    public Integer getNumberOfExperiments() {
        Value expt = getEntity().getValue(Project.NumberOfExperimentsValue);
        return expt != null ? (Integer) expt.getValue() : null;
    }

    public Collection<Value> getAnnotations() {
        Collection<Value> annos = new ArrayList<Value>();
        String[] srcNames = {
                EntityNamedSources.CAPAnnotationSource,
                EntityNamedSources.GOBPAnnotationSource,
                EntityNamedSources.GOMFAnnotationSource,
                EntityNamedSources.GOCCAnnotationSource,
                EntityNamedSources.KEGGDiseaseCategoryAnnotationSource,
                EntityNamedSources.KEGGDiseaseNameAnnotationSource
        };
        for (String srcName : srcNames) {
            Collection<Value> values = entity.getValues(new DataSource(srcName));
            if (values != null) annos.addAll(values);
        }
        return annos;
    }
}
