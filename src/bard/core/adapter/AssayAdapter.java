package bard.core.adapter;

import bard.core.Assay;
import bard.core.DataSource;
import bard.core.EntityNamedSources;
import bard.core.Value;

import java.util.ArrayList;
import java.util.Collection;

public class AssayAdapter extends EntityAdapter<Assay> {

    public AssayAdapter () {}
    public AssayAdapter (Assay assay) { 
        super (assay);
    }

    public Assay getAssay () { return getEntity (); }
    public void setAssay (Assay assay) {
        setEntity (assay);
    }

    public Collection<Value> getAnnotations() {
        Collection<Value> annos = new ArrayList<Value>();
        String[] srcNames = {
                EntityNamedSources.CAPAnnotationSource,
                EntityNamedSources.GOBPAnnotationSource,
                EntityNamedSources.GOMFAnnotationSource,
                EntityNamedSources.GOCCAnnotationSource,
                EntityNamedSources.KEGGDiseaseCategoryAnnotationSource,
                EntityNamedSources.KEGGDiseaseNameAnnotationSource
        };
        for (String srcName : srcNames)   {
            Collection<Value> values =  entity.getValues(new DataSource(srcName));
            if (values != null) annos.addAll(values);
        }
        return annos;
    }
}
