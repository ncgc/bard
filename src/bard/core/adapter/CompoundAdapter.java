package bard.core.adapter;

import java.util.*;
import bard.core.Compound;
import bard.core.CompoundService;
import bard.core.EntityServiceManager;
import bard.core.Value;
import bard.core.MolecularValue;
import bard.core.MolecularData;
import bard.core.DataSource;

public class CompoundAdapter 
    extends EntityAdapter<Compound> implements MolecularData {

    protected EntityServiceManager esm;
    protected MolecularValue mv;
    public CompoundAdapter () {
    }

    public CompoundAdapter (Compound compound) {
        setCompound (compound);
    }

    public CompoundAdapter (Compound compound, EntityServiceManager esm) {
        super (compound);
        this.esm = esm;
    }

    public Compound getCompound () { return getEntity (); }
    public void setCompound (Compound compound) {
        setEntity (compound);
        mv = (MolecularValue)compound.getValue(Compound.MolecularValue);
    }

    public synchronized Long[] getPubChemSIDs () {
        Collection<Value> values = 
            getEntity().getValues(Compound.PubChemSIDValue);
        if (values.size() == 0 && esm != null) {
            values = ((CompoundService)esm.getService(Compound.class)).getPubChemSIDs(getEntity());
            for (Value value: values) 
        	getEntity().add(value);
        }
        Long[] sids = new Long[values.size()];
        Iterator<Value> iter = values.iterator();
        for (int i = 0; iter.hasNext(); ++i) {
            Value v = iter.next();
            if (v.getValue() instanceof String)
        	System.err.println(v);
            sids[i] = (Long)v.getValue();
        }
        return sids;
    }

    public Long getPubChemCID () {
        Value cid = getEntity().getValue(Compound.PubChemCIDValue);
        return cid != null ? (Long)cid.getValue() : null;
    }

    public String getStructureSMILES () {
        return (String) toFormat (Format.SMILES);
    }

    public String getStructureMOL () {
        return (String) toFormat (Format.MOL);
    }

    public Object getStructureNative () {
        return toFormat (Format.NATIVE);
    }


    /*
     * MolecularData interface
     */
    public String formula () { return mv != null ? mv.formula() : null; }
    public Double mwt () { return mv != null ? mv.mwt() : null; }
    public Double exactMass () { return mv != null ? mv.exactMass() : null; }
    public Integer hbondDonor () { 
        return mv != null ? mv.hbondDonor() : null; 
    }
    public Integer hbondAcceptor () { 
        return mv != null ? mv.hbondAcceptor() : null; 
    }
    public Integer rotatable () { 
        return mv != null ? mv.rotatable() : null; 
    }
    public Integer definedStereo () { 
        return mv != null ? mv.definedStereo() : null; 
    }
    public Integer stereocenters () { 
        return mv != null ? mv.stereocenters() : null; 
    }
    public Double TPSA () { 
        return mv != null ? mv.TPSA() : null; 
    }
    public Double logP () { 
        return mv != null ? mv.logP() : null; 
    }
    public Boolean ruleOf5 () { 
        return mv != null ? mv.ruleOf5() : null; 
    }
    public int[] fingerprint () { 
        return mv != null ? mv.fingerprint() : null; 
    }
    public Object toFormat (Format format) { 
        return mv != null ? mv.toFormat(format) : null; 
    }
    public byte[] toImage (int size, int background) { 
        return mv != null ? mv.toImage(size, background) : null; 
    }
    public void setMolecule (Object input) { 
        if (mv != null) {
            mv.setMolecule(input);
        }
    }
}
