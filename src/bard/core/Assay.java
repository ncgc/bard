package bard.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.jdo.annotations.PersistenceCapable;

@PersistenceCapable(detachable="true")
public class Assay extends Entity implements AssayValues {
    private static final long serialVersionUID = 0x740b685435192d40l;

//    protected String protocol; // assay protocol
//    protected String comments; // assay comments
    protected AssayType type;
    protected AssayRole role;
    protected AssayCategory category;

//    protected int[] projects = {};
//    protected int[] experiments = {};
//    protected int[] documents = {};
//    protected List<Document> publications = new ArrayList<Document>();
//    protected List<Biology> targets = new ArrayList<Biology>();

    public Assay () {}
    public Assay (AssayService service) {
	this.entityService = service;
    }
    
    public Assay (String name) {
        super (name);
    }

    static public boolean lazyLoad(String id) {
	if (id.equals(AssayProtocol) || id.equals(AssayComments) || id.equals(EntityDescription))
	    return true;
	return false;
    }
    
    @Deprecated
    public void setProtocol (String protocol) { setDeprecatedString(protocol, AssayProtocol); }
    @Deprecated
    public String getProtocol () { 
	Value val = getValue(AssayProtocol);
	return val == null ? "" : ""+val.getValue();
    }

    @Deprecated
    public void setComments (String comments) { setDeprecatedString(comments, AssayComments); }
    @Deprecated
    public String getComments () { 
	Value val = getValue(AssayComments);
	return val == null ? "" : ""+val.getValue();	
    }

    public void setType (AssayType type) { this.type = type; }
    public AssayType getType () { return type; }

    public void setCategory (AssayCategory category) { 
        this.category = category; 
    }
    public AssayCategory getCategory () { return category; }

    public void setRole (AssayRole role) { this.role = role; }
    public AssayRole getRole () { return role; }

//    public void setDocumentIds (int[] documents) {
//	this.documents = documents;
//    }
//    public Collection<Integer> getDocumentIds () {
//	ArrayList<Integer> documentIds = new ArrayList<Integer>();
//	for (int i: this.documents)
//	    documentIds.add(i);
//	return documentIds;
//    }
    
//    @Deprecated
//    public void setProjectIds (int[] projects) {
//	this.projects = projects;
//    }
    
//    @Deprecated
//    public Collection<Integer> getProjectIds () {
//	ArrayList<Integer> projectIds = new ArrayList<Integer>();
//	for (int i: this.projects)
//	    projectIds.add(i);
//	return projectIds;
//    }
//    
//    @Deprecated
//    public Iterator<Integer> projectIds () { 
//	return getProjectIds().iterator(); 
//    }

//    public void setExperimentIds (int[] experiments) {
//	this.experiments = experiments;
//    }
//    public Collection<Integer> getExperimentIds () {
//	ArrayList<Integer> exptIds = new ArrayList<Integer>();
//	for (int i: this.experiments)
//	    exptIds.add(i);
//	return exptIds;
//    }
//    public Iterator<Integer> experimentIds () { 
//	return getExperimentIds().iterator();
//    }

//    public void add (Document expr) {
//           publications.add(expr);
//       }
//    public boolean remove (Document expr) {
//        return publications.remove(expr);
//    }
//    public Collection<Document> getPublications () {
//        return Collections.unmodifiableCollection(publications);
//    }
//    public Iterator<Document> publications () {
//        return getPublications().iterator();
//    }
//
//    public void add (Biology target) {
//        targets.add(target);
//    }
//    public boolean remove (Biology target) {
//        return targets.remove(target);
//    }
//    public Collection<Biology> getTargets () {
//        return Collections.unmodifiableCollection(targets);
//    }
//    public Iterator<Biology> targets () {
//        return getTargets().iterator();
//    }

    public int getProjectCount() { return this.getValues(Project.class.toString()).size(); }
    public int getDocumentCount() { return this.getValues(Document.class.toString()).size(); }
    public int getExperimentCount () { return this.getValues(Experiment.class.toString()).size(); }
    public int getTargetCount () { return this.getValues(Biology.class.toString()).size(); }


    public String toString () { // !!! should remove lazy loaded items to avoid triggering that
        return getClass().getName()+"{protocol="+getProtocol()+",comments="
            +getComments()+",type="+type+",role="+role+",category="+category
            +",projects="+getProjectCount()+",experiments="+getExperimentCount()
            +",publications="+getDocumentCount()+",targets="+getTargetCount()
            +","+super.toString()+"}";
    }
}
