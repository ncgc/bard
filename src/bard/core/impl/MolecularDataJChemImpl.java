package bard.core.impl;

import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.NotPersistent;

import bard.core.MolecularData;

import chemaxon.struc.Molecule;
import chemaxon.formats.MolImporter;
import chemaxon.formats.MolFormatException;
import chemaxon.util.MolHandler;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;


/**
 * Default implementation of MolecularData based on JChem
 */
@PersistenceCapable(detachable="true")
public class MolecularDataJChemImpl implements MolecularData {
    private static final Logger logger = Logger.getLogger
        (MolecularDataJChemImpl.class.getName());

    private static final long serialVersionUID = 0x2ce64ddd6b96665al;

    protected String formula;
    protected Double mwt;
    protected Double exactMass;
    protected Integer hbondDonor;
    protected Integer hbondAcceptor;
    protected Integer rotatable;
    protected Integer definedStereo;
    protected Integer stereocenters;
    protected Double tpsa;
    protected Double logP;
    protected Boolean ruleOf5;
    protected int[] fingerprint;
    protected byte[] image;

    /*
     * a persistent string representation of a molecule stored as
     * a mol/sd format at the moment
     */
    protected String structure;

    /*
     * a transient internal representation of the molecule
     */
    //@NotPersistent
    //transient protected Molecule mol;

    public MolecularDataJChemImpl () {
    }

    /*
     * MolecularData interface
     */
    public String formula () {  return formula; }

    public Double mwt () { // molecular weight
        return mwt;
    }

    public Double exactMass () {  return exactMass; }

    public Integer hbondDonor () { // count of H-bond donors
        return hbondDonor;
    }

    public Integer hbondAcceptor () { // count of H-bond acceptors
        return hbondAcceptor;
    }

    public Integer rotatable () { // count of rotatable bonds
        return rotatable;
    }

    public Integer definedStereo () { // count of defined stereo centers
        return definedStereo;
    }

    // count of stereo centers in the molecule
    public Integer stereocenters () { 
        return stereocenters;
    }

    public Double TPSA () { // topological polar surface area
        return tpsa;
    }

    public Double logP () { // logP
        return logP;
    }

    public Boolean ruleOf5 () {
        return ruleOf5;
    }

    public int[] fingerprint () { // bit fingerprint for indexing/searching
        if (fingerprint == null && structure != null) { // lazy evaluation
            try {
                MolHandler mh = new MolHandler (structure);
                mh.aromatize();
                fingerprint = mh.generateFingerprintInInts(16, 2, 6);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return fingerprint;
    }

    /*
     * output formats
     */
    public Object toFormat (Format format) {
        if (format == Format.MOL || format == Format.SDF) {
            return structure;
        }

        try {
            Molecule mol = new MolHandler (structure).getMolecule();
            
            switch (format) {
            case SMI: 
            case SMILES:
                return mol.toFormat("smiles:q");
                
            case SMARTS:
                return mol.toFormat("smarts:q");
                
            case NATIVE:
                return mol;
            }
        }
        catch (Exception ex) {
            throw new IllegalStateException (ex);
        }

        throw new IllegalArgumentException ("Unsupported format "+format);
    }

    public byte[] toImage (int size, int background) {
        return image;
    }

    public void setMolecule (Object input) {
        try {
            //logger.info("Input: "+input+" "+input.getClass());

            if (input instanceof Molecule) {
                parsePropertiesMol ((Molecule)input);
            }
            else if (input instanceof byte[]) {
                parsePropertiesMol ((byte[])input);
            }
            else if (input instanceof JsonNode) {
                parsePropertiesJson ((JsonNode)input);
            }
            else { // treat as string
                // last resort... try parsing it as the molecule
                parsePropertiesMol (input.toString());
            }
        }
        catch (MolFormatException ex) {
            throw new IllegalArgumentException
                ("Not a molecular format "+ex.getMessage());
        }
    }

    protected void parsePropertiesMol (Molecule mol) {
        mol.hydrogenize(false);
        mol.dearomatize();
        if (mol.getDim() < 2) {
            //mol.clean(2, null);
        }
        structure = mol.toFormat("sdf");

        formula = mol.getFormula();
        mwt = mol.getMass();
        exactMass = mol.getExactMass();
        calcStereos (mol);
        fingerprint = null;
        //this.mol = mol;
    }

    protected void parsePropertiesMol (String molstr) 
        throws MolFormatException {
        parsePropertiesMol (MolImporter.importMol(molstr));
    }

    protected void parsePropertiesMol (byte[] mol) 
        throws MolFormatException {
        parsePropertiesMol (MolImporter.importMol(mol));
    }

    protected void calcStereos (Molecule mol) {
        int total = 0, defined = 0;
        for (int i = 0; i < mol.getAtomCount(); ++i) {
            int flag = mol.getChirality(i);
            if (flag == Molecule.CHIRALITY_R 
                || flag == Molecule.CHIRALITY_S) {
                ++defined;
            }
            if (flag != 0) {
                ++total;
            }
        }
        stereocenters = total;
        definedStereo = defined;
    }

    protected void parsePropertiesJson (String content) 
        throws JsonProcessingException, IOException, MolFormatException {
        ObjectMapper mapper = new ObjectMapper ();
        parsePropertiesJson (mapper.readTree(content));
    }

    protected void parsePropertiesJson (JsonNode node) 
        throws MolFormatException {
        JsonNode n;

        n = node.get("highlight");
        boolean struc = false;
        if (n != null && !n.isNull()) {
            // try to parse as smiles
            try {
                parsePropertiesMol (n.asText());
                struc = true;
            }
            catch (MolFormatException ex) {
            }
        }

        if (!struc) {
            n = node.get("smiles");
            if (n != null) {
                String smiles = n.asText();
                parsePropertiesMol (smiles);
            }
        }

        // now override these values...
        n = node.get("mwt");
        if (n != null) {
            mwt = n.asDouble();
        }

        n = node.get("exactMass");
        if (n != null) {
            exactMass = n.asDouble();
        }

        n = node.get("hbondDonor");
        if (n != null) {
            hbondDonor = n.asInt();
        }

        n = node.get("hbondAcceptor");
        if (n != null) {
            hbondAcceptor = n.asInt();
        }

        n = node.get("tpsa");
        if (n != null) {
            tpsa = n.asDouble();
        }

        n = node.get("xlogp");
        if (n != null) {
            logP = n.asDouble();
        }

        n = node.get("rotatable");
        if (n != null) {
            rotatable = n.asInt();
        }
    }
}
