package bard.core;

public interface ProjectValues extends AssayValues {
    static final String NumberOfExperimentsValue = "NumberOfExperiments";
    static final String SourceValue = "Source";
    static final String NumberOfProbesValue = "NumberOfProbes";
}
