package bard.core;

public interface EntityNamedSources {
    public static final String CAPAnnotationSource = "CAPAnnotationDataSource";
    public static final String AnnotationSource = "AnnotationDataSource";
    public static final String GOBPAnnotationSource = "GOBPAnnotationDataSource";
    public static final String GOMFAnnotationSource = "GOMFAnnotationDataSource";
    public static final String GOCCAnnotationSource = "GOCCAnnotationDataSource";
    public static final String KEGGDiseaseCategoryAnnotationSource = "KEGGDiseaseCategoryAnnotationDataSource";
    public static final String KEGGDiseaseNameAnnotationSource = "KEGGDiseaseNameAnnotationDataSource";
    public static final String FacetSource = "FacetDataSource";
    public static final String PubChemSynonymSource = "PubChemSynonymDataSource";
    public static final String PubchemSidSource = "PubchemSidDataSource";
}
