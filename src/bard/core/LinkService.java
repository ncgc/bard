package bard.core;

public interface LinkService extends EntityService<Link> {
    <E extends Entity> ServiceIterator<E> neighbors (E entity);
}
