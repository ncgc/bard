package bard.core;

import javax.jdo.annotations.PersistenceCapable;

@PersistenceCapable(detachable="true")
public class EntityValue extends Value {  
    private static final long serialVersionUID = 0x47ed3eed965a7f86l;

    protected Object idValue;

    protected EntityValue () {}
    public EntityValue (Value parent) {
        super (parent);
    }
    public EntityValue (Value parent, String id) {
        super (parent, id);
    }
    public EntityValue (Value parent, String id, Object idValue) {
        super (parent, id);
        this.idValue = idValue;
    }
    public EntityValue (DataSource source, String id) {
        this (source, id, null);
    }
    public EntityValue (DataSource source, String id, Object idValue) {
        super (source, id);
        this.idValue = idValue;
    }

    public void setValue (Object value) {
        this.idValue = value;
    }

    @Override
    public Object getValue () { return idValue; }
}
