package bard.core;

import javax.jdo.annotations.PersistenceCapable;
import java.util.ArrayList;
import java.util.List;

@PersistenceCapable(detachable="true")
public class HillCurveValue extends Value {  
    private static final long serialVersionUID = 0x8952d92d94f21b00l;
    private static final double ln10 = 2.30258509299404568401;
    
    // fitted parameters
    protected Double s0, sInf;
    protected Double slope; // ac50
    protected Double coef; // hill coef

    // units
    String testUnit = null, responseUnit = null;
    protected Double testUnitOffset = 0D;

    // data
    protected List<Double> conc = new ArrayList<Double>();
    protected List<Double> response = new ArrayList<Double>();

    protected HillCurveValue () {}
    public HillCurveValue (Value parent) {
        super (parent);
    }
    public HillCurveValue (Value parent, String id) {
        super (parent, id);
    }
    public HillCurveValue (DataSource source) {
        this (source, null);
    }
    public HillCurveValue (DataSource source, String id) {
        super (source, id);
    }

    @Override
    public Object getValue () { return slope; }

    public Double getSlope () { return slope; }
    public void setSlope (Double slope) { this.slope = slope; }

    public Double getS0 () { return s0; }
    public void setS0 (Double s0) { this.s0 = s0; }
    
    public Double getSinf () { return sInf; }
    public void setSinf (Double sInf) { this.sInf = sInf; }

    public Double getCoef () { return coef; }
    public void setCoef (Double coef) { this.coef = coef; }

    public void add (Double conc, Double response) {
        this.conc.add(conc);
        this.response.add(response);
    }
    public int size () { return conc.size(); }
    public Double[] getConc () {
        return conc.toArray(new Double[0]);
    }
    public Double[] getResponse () {
        return response.toArray(new Double[0]);
    }

    public double[][] getLogConcResponse () {
	double[][] yx = new double[0][];
        Double[] conc = getConc ();
	if (conc != null && conc.length > 0) {
            yx = new double[2][conc.length];
            Double[] res = getResponse ();
	    for (int i = 0; i < conc.length; ++i) {
		yx[0][i] = res[i];
		yx[1][i] = Math.log10(conc[i]);
	    }
	}
	return yx;
    }

    public double[][] getFittedConcResponseCurve () { 
	double[][] curve = new double[0][];
	Double[] conc = getConc ();
	if (conc == null || conc.length == 0) {
	    if (slope != null) {
	        conc = new Double[10];
	        double minconc = slope, maxconc = slope;
	        if (slope < 10) maxconc = 100;
	        else maxconc = slope*30;
	        if (slope > 10) minconc = 0.1;
	        else minconc = slope/30;
	        
	        for (int i=0; i<conc.length; i++)
	            conc[i] = Math.pow(10, i*(Math.log10(maxconc) - Math.log10(minconc))/conc.length + Math.log10(minconc));
	    }
	}

	if (slope != null && s0 != null && sInf != null && this.s0.doubleValue() != this.sInf.doubleValue()) {
	    double[] x = new double[conc.length];
	    //System.out.println();
	    for (int i = 0; i < x.length; ++i) {
		x[i] = Math.log10(conc[i]);
	    }
	    
	    // upsample enough time to get a smooth curve...
	    x = upsample (upsample (upsample (upsample (x))));
	    	    
	    curve = new double[2][x.length];
	    for (int i = 0; i < x.length; ++i) {
		curve[0][i] = evaluate (x[i]);
		curve[1][i] = x[i];
	    }
	} else if (slope != null) {
	    double min = response.get(0), max = response.get(0);
	    for (double resp: this.response) {
		if (min < resp) min = resp;
		else if (max > resp) max = resp;
	    }
	    curve = new double[2][2];
	    curve[0][0] = min;
	    curve[0][1] = max;
	    curve[1][0] = Math.log10(slope)+this.testUnitOffset;
	    curve[1][1] = Math.log10(slope)+this.testUnitOffset;
	}
	return curve;
    }

    private static double[] upsample (double[] x) {
        if (x.length == 0) {
            return new double[]{};
        }
	
        double[] x2 = new double[2*x.length-1];
        for (int i=0; i<x2.length; i++) {
            if (i%2 == 0)
                x2[i] = x[i/2];
            else
                x2[i] = 0.5*(x[(i-1)/2]+x[(i+1)/2]);
        }
        return x2;
    }


    public Double evaluate (double logconc) {
        if (slope == null || coef == null) {
            return null;
        }

        double z0 = s0 != null ? s0 : 0.;
        double zinf = sInf != null ? sInf : 100.;
        double logslope = Math.log10(slope);
        //double logconc = Math.log10(conc);

        return z0 + ((zinf - z0)  
                          / (1. + Math.exp(ln10 * coef * 
                                           (testUnitOffset + logslope - logconc))));
    }

    public void setResponseUnit(String responseUnit) {
        this.responseUnit = responseUnit;
    }

    public void setTestUnit(String testUnit) {
	if (testUnit.charAt(testUnit.length()-1) == 'm') testUnit = testUnit.substring(0, testUnit.length()-1) + 'M'; //!!! hack to fix
        this.testUnit = testUnit;
        this.testUnitOffset = lookupOffset(testUnit);
        return;
    }

    public String getTestUnit() {return testUnit;}
    
    public static double lookupOffset(String testUnit) {
        if ("nm".equals(testUnit.toLowerCase())) return 3.0;
        if ("um".equals(testUnit.toLowerCase())) return 0.0;
        if ("mm".equals(testUnit.toLowerCase())) return -3.0;
	return -6.0;
    }
    
    public double getOffset() {
	return this.testUnitOffset;
    }
    
    public void setOffset(double testUnitOffset) {
	this.testUnitOffset = testUnitOffset;
    }
    
    public String toString () {
        return getClass()+"{id="+getId()+",s0="+getS0()+",sInf="+getSinf()
            +",slope="+getSlope()+",coef="+getCoef()+",concs="+size()+"}";
    }
}
