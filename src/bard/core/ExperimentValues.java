package bard.core;

public interface ExperimentValues extends EntityValues {
    static final String ExperimentCategoryValue = "ExperimentCategory";
    enum ExperimentCategory {
        Unknown,
            MLSCN,
            MLPCN,
            MLSCN_AP,
            MLPCN_AP;

        public static ExperimentCategory valueOf (int i) {
            for (ExperimentCategory c : values ()) {
                if (i == c.ordinal()) 
                    return c;
            }
            return Unknown;
        }
    }

    static final String ExperimentRoleValue = "ExperimentRole";
    enum ExperimentRole {
        Primary,
            Counterscreen,
            SecondaryConfirmation,
            SecondaryAlternative,
            SecondaryOrthogonal,
            SecondarySelectivity,
            Unknown;

        public static ExperimentRole valueOf (int i) {
            for (ExperimentRole c : values ()) {
                if (i == c.ordinal()) 
                    return c;
            }
            return Unknown;
            //throw new IllegalArgumentException ("Bogus ExperimentRole "+i);
        }
    }

    static final String ExperimentTypeValue = "ExperimentType";
    enum ExperimentType {
	Other, //0
	Screening, //1
	Confirmatory, //2
	Summary, //3
	Unknown; //4

        public static ExperimentType valueOf (int i) {
            for (ExperimentType t : values ()) {
                if (t.ordinal() == i) {
                    return t;
                }
            }
            return Unknown;
            //throw new IllegalArgumentException ("Bogus ExperimentType "+i);
        }
    }

    static final String ExperimentCompoundCountValue = 
        "ExperimentCompoundCount";
    static final String ExperimentSubstanceCountValue = 
        "ExperimentSubstanceCount";
    static final String ExperimentActiveCompoundsValue = 
            "ExperimentActiveCompounds";
    static final String ExperimentCAPIDValue = 
            "ExperimentCAPID";
    static final String ExperimentConfidenceValue = 
            "ExperimentConfidence";
}
