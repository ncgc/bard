// $Id$

package bard.util;

import java.util.logging.Logger;
import java.util.logging.Level;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Future;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class MultiThreadedTask<T> {
    private static final Logger logger = Logger.getLogger
	(MultiThreadedTask.class.getName());

    private static final int DEFAULT_QUEUE_CAPACITY = 1000;
    private static final int DEFAULT_THREADS = 2;
    private static final int DEFAULT_SLEEP = 100; // 100ms

    private static final int DEFAULT_MIN_ITEMS_PER_QUEUE = 50;

    private ExecutorService threadPool;
    private BlockingQueue<T> queue;
    private int capacity, nthreads;
    private boolean isThreadPoolLocal = false;

    final AtomicBoolean done = new AtomicBoolean ();

    class TaskRunner implements Runnable {
	String name;

	TaskRunner (String name) {
	    this.name = name;
	}

	public void run () {
	    Thread.currentThread().setName(name);
	    
	    ArrayDeque<T> work = new ArrayDeque<T>
		(getQueueCapacity () / (getNumThreads()+1));
		
	    int size;
	    long start;
	    double rate;
	    while (!Thread.interrupted()) {
		size = queue.drainTo
		    (work, Math.max(DEFAULT_MIN_ITEMS_PER_QUEUE,
				    queue.size()/getNumThreads()));

		if (size > 0) {
		    start = System.currentTimeMillis();
		    consume (work);

		    /*
		    rate = size*1000./(System.currentTimeMillis()-start);
		    logger.info(Thread.currentThread().getName()
				+": processing task at rate = " 
				+ String.format("%1$.2f", rate)
				+ " entities per second; "
				+ size + "/" + queue.size());
		    */
		    work.clear(); // ensure we don't accumulate
		}
		else if (done.get()) {
		    logger.info("Thread " 
				+ Thread.currentThread().getName() 
				+ " is done!");
		    return;
		}
		else try {
		    Thread.sleep(DEFAULT_SLEEP);
		}
		catch (InterruptedException ex) {
		    logger.info("Interrupting " 
				+ Thread.currentThread().getName());
		    Thread.currentThread().interrupt();
		}
	    }
	}
    }

    public MultiThreadedTask () {
	this (DEFAULT_THREADS);
    }

    public MultiThreadedTask (int nthreads) {
	this (DEFAULT_QUEUE_CAPACITY, nthreads);
    }

    public MultiThreadedTask (int capacity, int nthreads) {
	this (Executors.newCachedThreadPool(), capacity, nthreads);
	this.isThreadPoolLocal = true;
    }

    public MultiThreadedTask (ExecutorService threadPool, 
			      int capacity, int nthreads) {
	this.threadPool = threadPool;
	this.capacity = Math.max(capacity, 1000);
	this.nthreads = Math.max(nthreads, 1);
	queue = new LinkedBlockingQueue<T>(capacity);
    }

    public void execute () throws Exception {
	Future[] futures = new Future[nthreads];
	for (int i = 0; i < nthreads; ++i) {
	    futures[i] = threadPool.submit
		(new TaskRunner (getTaskName () + "-"+(i+1)));
	}

	preprocessing ();

	/*
	 * Task specific queuing machinism
	 */
	produce (getQueue ());

	while (queue.size() > 0) {
	    Thread.sleep(DEFAULT_SLEEP);
	}
	done.set(true); // signal all other threads that we're done

	// now wait for all threads to finish
	for (Future f : futures) {
	    try {
		logger.info("waiting for thread "+f+" to finish!");
		f.get();
	    }
	    catch (Exception ex) { }
	}

	if (isThreadPoolLocal) {
	    threadPool.shutdown();
	}

	/*
	do {
	    int cnt = 0;
	    for (Future f : futures) {
		if (f.isDone() || f.isCancelled()) {
		    ++cnt;
		}
	    }
	    if (cnt == futures.length) {
		break;
	    }
	    Thread.sleep(DEFAULT_SLEEP);
	}
	while (true);
	*/

	postprocessing ();
    }

    protected BlockingQueue<T> getQueue () { return queue; }
    public int getQueueCapacity () { return capacity; }
    public void setQueueCapacity (int capacity) { this.capacity = capacity; }
    public int getNumThreads () { return nthreads; }
    public void setNumThreads (int nthreads) { this.nthreads = nthreads; }

    /*
     * Override by subclass to do preprocessing prior to queuing begins
     */
    protected void preprocessing () { }

    /*
     * Override by subclass to do postprocessing after execution finishes
     */
    protected void postprocessing () { }

    /*
     * Override by subclass to give meaningful name to the task; e.g.,
     * SubstructureSearch.
     */
    public String getTaskName () { return getClass().getSimpleName(); }
	
    /*
     * May also override by subclass to do batch processing
     */
    protected void consume (Deque<T> payload) {
	for (T item; !payload.isEmpty(); ) {
	    item = payload.pop();
	    consume (item);
	}
    }

    /*
     * Override by subclass to do actual work; this function is called
     * by multiple threads, so it must be thread safe!
     */
    protected void consume (T item) {}

    /*
     * Override by subclass to fill up the queue
     */
    protected abstract void produce (BlockingQueue<T> queue) 
	throws Exception;    
}
