// $Id$
package bard.util;


/**
 * Yet another base26 codec.  This one doesn't use lookup table, so it
 * can be much slower than Base26T!
 */
public class Base26Codec {
    static char[] ALPHA = new char[]{'A','B','C','D','E','F',
				     'G','H','I','J','K','L',
				     'M','N','O','P','Q','R',
				     'S','T','U','V','W','X',
				     'Y','Z'};
    private Base26Codec () {}

    /*
     * encode an input integer based on the following ALPHAbetical order
     * mapping:
     *   0  => A
     *   1  => B
     * ...  
     *  25  => Z
     *  26  => AA
     * ...
     *  52  => BA
     * ... 
     * 702  => AAA
     * ... etc
     */
    static public String encode (long N) {
	StringBuilder sb = new StringBuilder ();

	boolean negInf = false;
	if (N < 0l) {
	    sb.append('@');
	    if (N == Long.MIN_VALUE) {
		N = Long.MAX_VALUE;
		negInf = true;
	    }
	    else {
		N = -N;
	    }
	}

	long n = N/ALPHA.length;
	int m = (int)(N%ALPHA.length);

	if (n > 0) {
	    if (n > ALPHA.length) {
		sb.append(encode (n - 1));
	    }
	    else {
		sb.append(ALPHA[(int)(n-1)]);
	    }
	}
	sb.append(negInf ? ALPHA[m+1] : ALPHA[m]);

	return sb.toString();
    }

    static long pow (int base, int exp) {
	long n = 1l;
	for (int k = 0; k < exp; ++k) {
	    n *= base;
	}
	return n;
    }

    static public long decode (String s) {
	long n = 0;
	boolean neg = false;

	int k = 0;
	if (s.charAt(0) == '@') {
	    neg = true;
	    ++k;
	}

	int l = s.length() - 1;
	for (; k < l; ++k) {
	    int c  = s.charAt(k) - 'A' + 1;
	    if (c <= 0 || c > ALPHA.length) {
		throw new IllegalArgumentException 
		    ("Input string \""+s+"\" is not a proper "
		     +"base26-encoded string!");
	    }
	    n += pow (ALPHA.length, l-k) * c;
	}
	n += s.charAt(l) - 'A';
	return neg ? -n : n;
    }

    static void validation1 (int N) {
	for (int i = -N; i <= N; ++i) {
	    String enc = encode (i);
	    int dec = (int) decode (enc);
	    //System.out.println(i + " => " + enc + " => " + dec);
	    if (dec != i) {
		System.out.println(i + " => " + enc + " => "+ dec);
		System.err.println("** fatal error: bogus codec value!!!");
		System.exit(1);
	    }
	}
	java.text.NumberFormat nf = java.text.NumberFormat.getInstance();
	nf.setGroupingUsed(true);
	System.out.println("** successfully encode/decode "
			   +nf.format(2*N)+" values!!!");
    }

    static void validation3 (int N) {
	int[] primes = Primes.generatePrimes(N);
	for (int i = 0; i < primes.length; ++i) {
	    String enc = encode (primes[i]);
	    int p = (int) decode (enc);
	    System.out.println(primes[i] + " => " + enc + " => "+p);
	}
	System.out.println("** successfully encode/decode "
			   +primes.length+" primes!!!");
    }

    static void validate (long x) {
	validate (x, false);
    }

    static void validate (long x, boolean print) {
	String enc = encode (x);
	long m = decode (enc);
	
	if (print) {
	    System.out.println(x +" => "+enc + " => "+ m);
	}

	if (m != x) {
	    System.out.println(x +" => "+enc + " => "+ m);
	    System.err.println("** fatal error: bogus codec value!");
	    System.exit(1);
	}
    }

    static void validation2 (int N) throws Exception {
	for (int i = 0; i < N; ++i) {
	    java.util.UUID uuid = java.util.UUID.randomUUID();
	    long x = uuid.getMostSignificantBits();
	    long y = uuid.getLeastSignificantBits();
	    validate (x);
	    validate (y);
	    validate (x ^ y);
	}
    }

    static void dump (int i, int N) {
	for (; i < N; ++i) {
	    System.out.println(String.format("%1$10d",i) + " " + encode (i));
	}
    }

    public static void main (String[] argv) throws Exception {
	/*
	if (argv.length > 0) {
	    dump (0, Integer.parseInt(argv[0]));
	}
	else {
	    dump (0, 1024);
	}
	*/
	validate (Long.MAX_VALUE, true);
	validate (Long.MIN_VALUE, true);
	for (String s : argv) {
	    long N = Long.parseLong(s);
	    System.out.println
		(N + " => " + encode (N) + " => "+ decode (encode (N)));
	}
	//validation2 (1000000);
	//validation1 (1000000);
	//validation3 (1000000);
    }

    static public class Decode {
	public static void main (String[] argv) throws Exception {
	    for (String s : argv) {
		s = s.toUpperCase();
		long x = decode (s);
		System.out.println(s + " => " + x + " => "+ encode (x));
	    }
	}
    }
}
