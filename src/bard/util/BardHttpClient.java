package bard.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Logger;

public class BardHttpClient {

    private static boolean checkProxy = false;
    private static HttpHost httpProxy = null;
    private static Proxy urlProxy = null;

    HttpClient httpclient;

    static final Logger logger = Logger.getLogger
            (BardHttpClient.class.getName());
    String origin = "unknown";

    public BardHttpClient(String origin) {
        this.httpclient = new DefaultHttpClient();
        if (!checkProxy) setupProxy();
        if (httpProxy != null) {
            httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, httpProxy);
        }
        this.origin = origin;
    }

    static public void setupProxy() {
        //-Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=8888
        checkProxy = true;
        try {
            String host = System.getProperty("http.proxyHost");
            if (host != null) {
                String port = System.getProperty("http.proxyPort");
                if (port == null) {
                    httpProxy = new HttpHost(host);
                    urlProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, 0));
                } else {
                    httpProxy = new HttpHost(host, Integer.valueOf(port));
                    urlProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, Integer.valueOf(port)));
                }
            }
        } catch (Exception e) {
            logger.severe("System proxy settings on http.proxyHost and http.proxyPort improperly set.");
        }
    }

    static public InputStream getURLInputStream(URL url) throws IOException {
        if (!checkProxy) setupProxy();
        if (urlProxy != null) {
            return url.openConnection(urlProxy).getInputStream();
        }
        return url.openConnection().getInputStream();
    }

    public HttpResponse execute(HttpUriRequest request) {
        long time = System.currentTimeMillis();

        HttpResponse response = null;

        try {
            response = httpclient.execute(request);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        time = System.currentTimeMillis() - time;
        if (time > 0) {
            String uri = request.getURI().toString();
            if (request.getMethod().equals("POST"))
                uri = uri + "POST";
            logger.severe("<< " + Thread.currentThread().getName()
                    + " " + origin + " finishes fetching " +
                    uri + "..." +
                    String.format("%1$.3fs", 1e-3 * (time)));

            if (System.getProperty("dump.url") != null) {
                String method = request.getMethod();
                if (method.equals("POST")) {
                    uri = request.getURI().toString();
                    HttpEntity e = ((HttpPost) request).getEntity();
                    try {
                        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>(URLEncodedUtils.parse(e));
                        StringBuilder sb = new StringBuilder("\"");
                        String delim = "";
                        for (NameValuePair nvp : params) {
                            String name = nvp.getName();
                            String value = nvp.getValue();
                            sb.append(delim).append(name).append("=").append(value);
                            delim = "&";
                        }
                        sb.append("\"");
                        System.out.println("UTEST@curl -o /dev/null -sL -w \"%{http_code} %{url_effective} %{time_total} %{size_download}\\\\n\"  -X POST --data " + sb.toString() + " '" + uri + "'");
                    } catch (IOException e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else
                    System.out.println("UTEST@curl -o /dev/null -sL -w \"%{http_code} %{url_effective} %{time_total} %{size_download}\\\\n\"  -X GET " + "'" + uri + "'");
            }

        }

        return response;
    }

    public void close() {
        this.httpclient.getConnectionManager().shutdown();
    }
}
