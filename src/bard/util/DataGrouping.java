// $Id$

package bard.util;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentHashMap;

public class DataGrouping<K, E> {
    private ConcurrentMap<K, List<E>> grouping = 
	new ConcurrentHashMap<K, List<E>>();

    public DataGrouping () {
    }

    public void add (K group, E element) {
	List<E> list = grouping.get(group);
	if (list == null) {
	    list = new ArrayList<E>();
	    List<E> prev = grouping.putIfAbsent(group, list);
	    if (prev != null) {
		list.addAll(prev);
	    }
	}

	if (list.indexOf(element) < 0) {
	    list.add(element);
	}
    }

    public void addAll (DataGrouping<K, E> dg) {
	grouping.putAll(dg.grouping);
    }

    public void clear () { 
	grouping.clear();
    }

    public List<E> remove (K group) { 
	return grouping.remove(group); 
    }

    public void remove (E element) {
	Set<K> remove = new HashSet<K>();
	for (Map.Entry<K, List<E>> me : grouping.entrySet()) {
	    List<E> list = me.getValue();
	    list.remove(element);
	    if (list.isEmpty()) {
		remove.add(me.getKey());
	    }
	}

	for (K group : remove) {
	    grouping.remove(group);
	}
    }

    public boolean hasGroup (K group) { return grouping.containsKey(group); }
    public Set<K> getGroups () { return grouping.keySet(); }
    public int getGroupCount () { return grouping.size(); }
    public int getGroupSize (K group) { 
	List<E> list = grouping.get(group);
	return list != null ? list.size() : 0;
    }

    public List<K> getGroups (E element) {
	List<K> list = new ArrayList<K>();
	for (Map.Entry<K, List<E>> me : grouping.entrySet()) {
	    List<E> li = me.getValue();
	    if (li.contains(element)) {
		list.add(me.getKey());
	    }
	}
	return list;
    }
    public List<E> getGroupMembers (K group) { return grouping.get(group); }

    public static <K,E> DataGrouping<E,K> flip (DataGrouping<K,E> dg) {
	DataGrouping<E,K> gd = new DataGrouping<E,K>();
	for (K group : dg.getGroups()) {
	    List<E> data = dg.getGroupMembers(group);
	    for (E e : data) {
		gd.add(e, group);
	    }
	}
	return gd;
    }
}
