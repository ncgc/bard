package bard.util;

import java.net.*;
import java.util.*;
import java.io.*;

import chemaxon.struc.Molecule;
import chemaxon.formats.MolImporter;
import gov.nih.ncgc.util.MolStandardizer;

/**
 * Example of hash retrieval
 */
public class CompoundHash {
    static final String APIURL = "http://bard.nih.gov/api/latest/compounds";

    public static void process (InputStream is) throws Exception {
        MolImporter mi = new MolImporter (is);

        MolStandardizer mstd = new MolStandardizer ();
        for (Molecule mol; (mol = mi.read()) != null; ) {
            mstd.standardize(mol);

            /**
             * here we progressively match from coarse to fine 
             * resolution
             */
            String[] hkeys = MolStandardizer.hashKeyArrayExt(mol);
            for (int i = 0; i < hkeys.length; ++i) {
                URL url = new URL 
                    (APIURL+"/"+hkeys[i]+"/h"+(i+1)+"?expand=true");
                BufferedReader br = new BufferedReader
                    (new InputStreamReader (BardHttpClient.getURLInputStream(url)));
                for (String line; (line = br.readLine()) != null; ) {
                    System.out.println(line);
                }
                br.close();
            }
        }
    }

    public static void main (String[] argv) throws Exception {
        System.err.println("## reading from STDIN...");
        process (System.in);
    }
}