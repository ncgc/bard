package bard.util;

import java.util.prefs.Preferences;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.security.SecureRandom;

/** 
 * Generate unique client id for each client
 */
public class ClientPrefs {
    private static final Logger logger = 
	Logger.getLogger(ClientPrefs.class.getName());

    private static final String CLIENT_KEY = "bard-client";

    /*
     * private master key
     */
    private static byte[] MAGIC = {
	(byte)0x6d,
	(byte)0x20,
	(byte)0xf3,
	(byte)0xa9,
	(byte)0x50,
	(byte)0x82,
	(byte)0xac,
	(byte)0x1e
    };

    public static Preferences getClientPrefs () {
	return Preferences.userNodeForPackage(ClientPrefs.class);
    }

    /**
     * Each installation of Tripod has a unique (random) context Id.  
     * It's used mainly to ensure uniqueness among clients.
     */
    public static String getClientId () {
	Preferences prefs = getClientPrefs ();
	String id = null;
	try {
	    prefs.sync();
	    id = prefs.get(CLIENT_KEY, null);
	    if (id == null) {
		id = clientIdString (createClientId ());
		prefs.put(CLIENT_KEY, id);
		prefs.flush();
	    }
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Exception in preferences", ex);
	}
	return id;
    }

    public static boolean isValidClientId (String id) {
	return isValidClientId (clientIdBytes (id));
    }

    public static boolean isValidClientId (byte[] id) {
	if (id.length != 16) {
	    return false;
	}

	for (int i = 0; i < 8; ++i) {
	    if ((byte)(id[i] ^ MAGIC[i]) != id[i+8]) {
		return false;
	    }
	}
	return true;
    }

    public static byte[] createClientId () {
	byte[] uu = new byte[8];
	new SecureRandom().nextBytes(uu);

	byte[] id = new byte[16];
	int i = 0;
	for (; i < 8; ++i) {
	    id[i] = (byte)(uu[i] ^ MAGIC[i]);
	}
	for (; i < 16; ++i) {
	    id[i] = uu[i-8];
	}
	return id;
    }

    // format the context id
    protected static String clientIdString (byte[] id) {
	if (id.length != 16) {
	    throw new IllegalArgumentException
		("Invalid client byte length: "+id.length);
	}

	StringBuilder sb = new StringBuilder ();
	int i = 0;
	for (; i < 4; ++i) {
	    sb.append(String.format("%1$02x", id[i]));
	}
	sb.append("-");
	for (int j = 0; j < 4; ++i, ++j) {
	    sb.append(String.format("%1$02x", id[i]));
	}
	sb.append("-");
	for (int j = 0; j < 4; ++i, ++j) {
	    sb.append(String.format("%1$02x", id[i]));
	}
	sb.append("-");
	for (int j = 0; j < 4; ++i, ++j) {
	    sb.append(String.format("%1$02x", id[i]));
	}

	return sb.toString();
    }

    protected static byte[] clientIdBytes (String cid) {
	String[] s = cid.split("-");
	if (s.length != 4) {
	    throw new IllegalArgumentException 
		("Invalid ClientId format: "+cid);
	}
	byte[] id = new byte[16];
	int i = 0;
	for (int j = 0; j < 8; ++i) {
	    id[i] = (byte)Integer.parseInt(s[0].substring(j,j+2), 16);
	    j += 2;
	}
	for (int j = 0; j < 8; ++i) {
	    id[i] = (byte)Integer.parseInt(s[1].substring(j,j+2), 16);
	    j += 2;
	}
	for (int j = 0; j < 8; ++i) {
	    id[i] = (byte)Integer.parseInt(s[2].substring(j,j+2), 16);
	    j += 2;
	}
	for (int j = 0; j < 8; ++i) {
	    id[i] = (byte)Integer.parseInt(s[3].substring(j,j+2), 16);
	    j += 2;
	}

	return id;
    }
}
