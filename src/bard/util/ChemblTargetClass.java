package bard.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * A one line summary.
 *
 * @author Rajarshi Guha
 */
public class ChemblTargetClass {
    static Map<String, String[]> target2class = null;

    public ChemblTargetClass() {
    }

    void init() {
        target2class = new HashMap<String, String[]>();
        InputStream is = this.getClass().getResourceAsStream
            ("/bard/ui/main/resources/chembl-targets.csv");
        if (is == null) {
            try {
                is = new FileInputStream("src/bard/ui/main/resources/chembl-targets.csv");
            } catch (FileNotFoundException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return;
            }
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        try {
            String line = reader.readLine(); // header
            while ((line = reader.readLine()) != null) {
                String[] toks = line.trim().split(",");
                String acc = toks[0];
                String[] klasses = new String[8];
                for (int i = 1; i <= 8; i++) klasses[i - 1] = toks[i].equals("NULL") ? null : toks[i].replace("\"", "");
                target2class.put(acc, klasses);
            }
        } catch (IOException e) {

        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public String getTargetClass(String acc, int level) {
        if (level < 1 || level > 8) throw new IllegalArgumentException("level must be between 1 & 8, inclusive");
        if (target2class == null) init();
        String[] klasses = target2class.get(acc);
        if (klasses == null) return null;
        else return klasses[level - 1];
    }
}
