// $Id: Primes.java 2386 2008-09-30 17:42:07Z nguyenda $

package bard.util;


public class Primes {

    // compute all primes <= N based on the sieve of Atkin algorithm
    //   see: http://en.wikipedia.org/wiki/Prime_number
    public static int[] generatePrimes (int N) {
	if (N < 2) {
	    throw new IllegalArgumentException ("N must be >= 2");
	}

	if (N == 2) return new int[]{2};
	if (N < 5) return new int[] {3, 2};
	if (N < 7) return new int[] {5, 3, 2};

	int[] isprime = new int[(N+32)>>5];
	int M = (int)(Math.sqrt(N) + 0.5);

	int a, b, x2, y2, n, k;
	for (int x = 1; x <= M; ++x) {
	    x2 = x*x;
	    a = 4*x2;
	    b = 3*x2;
	    for (int y = 1; y <= M; ++y) {
		y2 = y*y;
		n = a + y2;
		k = n % 12;
		if (n <= N && (k == 1 || k == 5)) 
		    isprime[n>>5] ^= 1<<(n%32);
		n = b + y2;
		if (n <= N && (n%12 == 7))
		    isprime[n>>5] ^= 1<<(n%32);
		n = b - y2;
		if (x > y && n < N && (n%12 == 11))
		    isprime[n>>5] ^= 1<<(n%32);
	    }
	}

	for (n = 5; n <= M; ++n) {
	    if ((isprime[n>>5] & (1<<(n%32))) != 0) {
		for (k = n*n; k <= N; k += n) {
		    isprime[k>>5] &= ~(1<<(k%32));
		}
	    }
	}

	// number of primes <= N is approximately N/(ln(N) - 1)
	int cnt = 0;
	for (n = N; n > 5; --n) {
	    if ((isprime[n>>5] & (1<<(n%32))) != 0) {
		++cnt;
	    }
	}

	int[] primes = new int[cnt+3];
	{ int i = 0;
	    for (n = N; n > 5; --n) {
		if ((isprime[n>>5] & (1<<(n%32))) != 0) {
		    primes[primes.length - ++i] = n;
		}
	    }

	    primes[2] = 5;
	    primes[1] = 3;
	    primes[0] = 2;
	}
	isprime = null;

	return primes;
    }

    // same as in generatePrimes, but without allocating an array for
    //  storing primes
    public static int largestPrime (int N) {
	if (N < 2) {
	    throw new IllegalArgumentException ("N must be >= 2");
	}

	if (N == 2) return 2;
	if (N < 5) return 3;
	if (N < 7) return 5;

	int[] isprime = new int[(N+32)>>5];
	int M = (int)(Math.sqrt(N) + 0.5);

	int a, b, x2, y2, n, k;
	for (int x = 1; x <= M; ++x) {
	    x2 = x*x;
	    a = 4*x2;
	    b = 3*x2;
	    for (int y = 1; y <= M; ++y) {
		y2 = y*y;
		n = a + y2;
		k = n % 12;
		if (n <= N && (k == 1 || k == 5)) 
		    isprime[n>>5] ^= 1<<(n%32);
		n = b + y2;
		if (n <= N && (n%12 == 7))
		    isprime[n>>5] ^= 1<<(n%32);
		n = b - y2;
		if (x > y && n < N && (n%12 == 11))
		    isprime[n>>5] ^= 1<<(n%32);
	    }
	}

	for (n = 5; n <= M; ++n) {
	    if ((isprime[n>>5] & (1<<(n%32))) != 0) {
		for (k = n*n; k <= N; k += n) {
		    isprime[k>>5] &= ~(1<<(k%32));
		}
	    }
	}

	for (n = N; n > 5; --n) {
	    if ((isprime[n>>5] & (1<<(n%32))) != 0) {
		return n;
	    }
	}
	return -1;
    }

    // return an estimated number M such that there exist approximately 
    //   N smaller primes
    public static int estimatePrimeOfSize (int N) {
	int M = N;
	for (int est; (est = estimatePrimeCount (M)) < N;) {
	    M += 3*(N - est);
	}
	return M;
    }

    // generate primes.length primes 
    public static int generatePrimes (int[] primes) {
	if (primes.length < 2) {
	    throw new IllegalArgumentException 
		("Input array must be of size >= 2");
	}

	int N = primes.length < 8 ? 8 : estimatePrimeOfSize (primes.length);

	// now prime is approximately the largest prime that will give us
	//   close to primes.length primes
	int[] pa = generatePrimes (N);
	if (pa.length < primes.length) {
	    // argh... our approximation under estimate...
	    N *= Math.max(2, primes.length - pa.length); 
	    pa = generatePrimes (N);
	}

	if (primes.length <= pa.length) {
	    // now copy the primes in reverse order
	    for (int i = 0, j = pa.length-1; i < primes.length; ++i, --j) {
		primes[i] = pa[j];
	    }
	}
	else {
	    throw new IllegalStateException 
		("Failed to estimate an upper bound for " 
		 + primes.length + " prime numbers");
	}
	pa = null;

	return N; // return the estimated upper bound
    }

    public static int estimatePrimeCount (int N) {
	return (int)(N/(Math.log(N) - 1.) + 0.5);
    }

    public static void main (String[] argv) throws Exception {
	if (argv.length == 0) {
	    System.out.println("Usage: Primes N");
	    System.exit(1);
	}

	int N = Integer.parseInt(argv[0]);
	final int[] primes = generatePrimes (N);
	System.out.print(primes.length + " primes <= " + N + ":");
	for (int i = 0; i < primes.length; ++i) {
	    System.out.print(" " + primes[i]);
	}
	System.out.println();

	/*
	final java.util.Map<Integer, java.util.Set<java.util.BitSet>> 
	    bucket = 
	    new java.util.HashMap<Integer, java.util.Set<java.util.BitSet>>();

	gov.nih.ncgc.util.GrayCode g = 
	    gov.nih.ncgc.util.GrayCode.createBinaryGrayCode(primes.length);
	g.addObserver(new java.util.Observer () {
		public void update (java.util.Observable o, Object arg) {
		    int[] perm = (int[])arg;
		    
		    java.util.BitSet ev = new java.util.BitSet(primes.length);
		    int sum = 0, cnt = 0;
		    for (int i = 0; i < perm.length; ++i) {
			if (perm[i] != 0) {
			    ev.set(i);
			    sum += primes[i];
			    ++cnt;
			}
		    }
		    if (cnt > 1) {
			java.util.Set<java.util.BitSet> cell 
			    = bucket.get(sum);
			if (cell == null) {
			    bucket.put
				(sum, cell = new java.util.HashSet
				 <java.util.BitSet>());
			}
			
			cell.add(ev);
			if (cell.size() > 1) {
			    System.out.println
				("** collision found for sum " + sum);
			    for (java.util.BitSet bs : cell) {
				for (int i = bs.nextSetBit(0); i >= 0; 
				     i = bs.nextSetBit(i+1)) {
				    System.out.print(" " + primes[i]);
				}
				System.out.println();
			    }
			}
		    }
		}
	    });
	g.generate();
	*/
    }
}
