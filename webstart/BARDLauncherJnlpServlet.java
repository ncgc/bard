import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import javax.sql.*;


public class BARDLauncherJnlpServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(BADLauncherJnlpServlet.class.getName());

    StringBuffer jnlpCache = new StringBuffer ();
    URL jnlpURL;
    
    @Override
    public void init (ServletConfig config) throws ServletException {
        logger.info("** initializing "+config.getServletName());
        ServletContext context = config.getServletContext();

        String url = context.getInitParameter("jnlp-template");
        if (url == null) {
            throw new ServletException
                ("No jnlp template parameter defined!");
        }

        try {
            jnlpURL = new URL (url);
        }
        catch (Exception ex) {
            logger.log(Level.SEVERE, "Bogus URL for jnlp-template: "+url);
        }
        logger.info("## jnlp template url: "+url);
    }

    @Override
    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
        res.setContentType("application/x-java-jnlp-file");
        PrintWriter pw = res.getWriter();

        String path = req.getPathInfo();
        BufferedReader br = new BufferedReader
            (new InputStreamReader (jnlpURL.openStream()));
        for (String line; (line = br.readLine()) != null; ) {
            int pos = line.indexOf("$$ARGV");
            if (pos > 0) {
                pw.print(line.substring(0, pos-6));
                if (path != null && path.length() > 1) {
                    pw.print(path);
                }
                pw.println(line.substring(pos));
            }
            else {
                pw.println(line);
            }
        }
        br.close();
    }

    @Override
    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
        throw new ServletException ("POST not supported!");
    }
}
